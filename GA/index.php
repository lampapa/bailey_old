<?php
    
 ini_set('display_errors', 1); 
 ini_set('display_startup_errors', 1); 
 error_reporting(E_ALL);
 
 require_once 'google-api-php-client-2.2.3/vendor/autoload.php';
 require_once('DBConnect.php');

 $conn = MainDBConnect();


$client = new Google_Client();
$client->addScope(Google_Service_Analytics::ANALYTICS_READONLY);


$accessToken = initializeAnalytics();

function initializeAnalytics()
{
  $KEY_FILE_LOCATION = __DIR__ . '/test-application-248407-49c612a04238.json';

  $client = new Google_Client();
  $client->setApplicationName("Hello Analytics Reporting");
  $client->setAuthConfig($KEY_FILE_LOCATION);
  $client->setScopes(['https://www.googleapis.com/auth/analytics.readonly']);

  $client->refreshTokenWithAssertion();
  $token = $client->getAccessToken();
  $accessToken = $token['access_token'];

  return $accessToken;
} 
    $client->setAccessToken($accessToken);
    // Create an authorized analytics service object.
    $analytics = new Google_Service_Analytics($client);



$optParams = array(
        'dimensions' => 'ga:deviceCategory,ga:region,ga:latitude,ga:longitude,ga:dateHourMinute,ga:pageTitle,ga:pagePath'
);
$today = date('Y-m-d');
$yestrdy = date('Y-m-d',strtotime("-1 days"));
try {
    $results = $analytics->data_ga->get(
      'ga:199753499', // //199363998
        $yestrdy,
        $yestrdy,
      'ga:sessions,ga:pageviews,ga:users,ga:hits',
      $optParams);
    // Success. Do something cool!

  } catch (apiServiceException $e) {
    // Handle API service exceptions.
    $error = $e->getMessage();
   
  }

printRealtimeReport($results,$conn);
/**
 * 2. Print out the Real-Time Data
 * The components of the report can be printed out as follows:
 */

function printRealtimeReport($results,$conn) {
   /* printDataTable($results);
    printTotalsForAllResults($results);*/
    insert_data($results,$conn);
}

function insert_data(&$results,$conn) {
  if (count($results->getRows()) > 0) {
    // Print table rows.
    foreach ($results->getRows() as $row) {
        $deviceCategory = $row[0];
        $region = $row[1];
        $latitude = $row[2];
        $longitude = $row[3];
        $datetimehour = date('Y-m-d H:i:s',strtotime($row[4]));
        $pagetitle = $row[5];
        $pagepath = $row[6];
        $sessions = $row[7];
        $pageviews = $row[8];
        $users = $row[9];
        $hits = $row[10];
        
        
        $CronIns = "INSERT into catapult_ga_master(deviceCategory, region, datetimehour, pagetitle,pagepath,sessions,pageviews,users,hits,latitude,longitude,created_on,created_by)
                    VALUES ('".$deviceCategory."', '".$region."', '".$datetimehour."', '" .$pagetitle."', '" .$pagepath."',
                     '" .$sessions."', '" .$pageviews."', '" .$users."', '" .$hits."', '" .$latitude."', '" .$longitude."',now(),'mei')";
                    // echo $CronIns;die();
        $conn->query($CronIns);
    }
    
    echo "success";
  } else {
       echo 'No Results Found.';
  }

}

function printDataTable(&$results) {
    $table='';
  if (count($results->getRows()) > 0) {
    $table .= '<table>';

    // Print headers.
    $table .= '<tr>';

    foreach ($results->getColumnHeaders() as $header) {
      $table .= '<th>' . $header->name . '</th>';
    }
    $table .= '</tr>';

    // Print table rows.
    foreach ($results->getRows() as $row) {
      $table .= '<tr>';
        foreach ($row as $cell) {
          $table .= '<td>'
                 . htmlspecialchars($cell, ENT_NOQUOTES)
                 . '</td>';
        }
      $table .= '</tr>';
    }
    $table .= '</table>';

  } else {
    $table .= '<p>No Results Found.</p>';
  }
  print $table;
}


function printTotalsForAllResults(&$results) {
    $html='';
  $totals = $results->getTotalsForAllResults();
  foreach ($totals as $metricName => $metricTotal) {
    $html .= "Metric Name  = $metricName\n";
    $html .= "Active User = $metricTotal\n";
  }

  print $html;
}

