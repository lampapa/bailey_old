<?php
namespace Drupal\site_admin\Controller;
use Drupal\node\Entity\NodeType;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\commerce_product\Entity\Product;
use Drupal\Core\Session\AccountProxyInterface;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\commerce\commerce_product;
use Drupal\commerce;
use Drupal\commerce_cart;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\commerce_cart\CartProviderInterface;
use Drupal\commerce_cart\CartManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_order\Entity\Order;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\CssCommand;
use Drupal\Core\Database;

//for catlisting in header
//use Drupal\drupalup_controller\Controller;

class TaxSetting{

  public function page(){
  	$connection = \Drupal::database();
    global $base_url;  
      $success_status = "";
      $error = "";
      if($_SESSION['postid'] == ""){
        $_SESSION['postid'] = rand(10,100);      
    }
  if(!empty($_POST)){ 
  	if($_POST['tax_sub'] !=''){
  		
  		$tax_per =$tax_status ='';
      
      if(isset($_POST['tax_per']) && !empty($_POST['tax_per'])){
        $tax_per     = $_POST['tax_per'];


      }else{
       
        $error .=" Tax Percentage";
      }
     
      if(isset($_POST['tax_status']) && !empty($_POST['tax_status'])){
        $tax_status     = 1;
      }else{
       // $error .=",Tax Status";
            $tax_status     = 2;
      }

    
     
      if(isset($_POST['hidden_id'])){
        $configid     = $_POST['hidden_id'];
      }
      if($configid ==''){
        if($tax_per !='' && $tax_status !='' ){
         // print_r("insert into catapult_tax_setting(tax_percentage,tax_status)values('".$tax_per."',".$tax_status.")");die;
          $connection->query("insert into catapult_tax_setting(tax_percentage,tax_status)values('".$tax_per."',".$tax_status.")");
          $success_status = "Tax Added Successfully";    
        }  
      }else{
        if($tax_per !='' && $tax_status !='' ){
           $connection->query("update catapult_tax_setting set tax_percentage='".$tax_per."',tax_status='".$tax_status."',modified_on=Now() where id='".$configid."'"); 
           $success_status = "Tax Updated Successfully"; 
           }  
      }
      unset($_POST);

  	}
    $_SESSION['postid'] = "";
  }
  if($_SESSION['postid'] == ""){
        $_SESSION['postid'] = rand(10,100);      
  }

    if(isset($error) && $error != '')
    {
      $error ="Please Fill The".$error;
    }

   /* echo  $error;
    exit;*/
    return array(
        '#theme' => 'tax_setting',
        '#title' => $success_status,
        '#postid'=>$_SESSION['postid'],
        '#error'=>$error
    );
  }
  public function gettaxinfo() {
    $connection = \Drupal::database();
    $query = $connection->query("SELECT * FROM catapult_tax_setting");
    $tax_results=[];
    while ($row = $query->fetchAssoc()) {  
       $tax_results[] = $row;
    }
   // $data['paypal_results'] =$paypal_results;
    $data['tax_results'] =$tax_results;
    echo json_encode($data);
    exit; 
  }
}
