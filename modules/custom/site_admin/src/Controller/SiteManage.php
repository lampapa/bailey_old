<?php
namespace Drupal\site_admin\Controller;
use Drupal\node\Entity\NodeType;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\commerce_product\Entity\Product;
use Drupal\Core\Session\AccountProxyInterface;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\commerce\commerce_product;
use Drupal\commerce;
use Drupal\commerce_cart;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\commerce_cart\CartProviderInterface;
use Drupal\commerce_cart\CartManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_order\Entity\Order;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\CssCommand;
use Drupal\user\Entity;
require_once 'sites/libraries/spreadsheet-reader-master/SpreadsheetReader.php';
/*require_once 'sites/libraries/spreadsheet-reader-master/php-excel-reader/excel_reader2.php';
*/require_once "sites/libraries/PHPExcel/Classes/PHPExcel.php";

class SiteManage{

  public function page() {
      global $base_url;
      global $user;
      $success_status = "";
      if($_SESSION['postid'] == ""){
        $_SESSION['postid'] = rand(10,100);      
      }
     
      //$tab="make-1";
      return array(

        '#theme' => 'site_admin',
        '#postid' => $_SESSION['postid'],
        '#title' => $success_status,       
        '#tab'=> ''
     );

  }
  
   public function getmakedata() {
      $connection = \Drupal::database();
      $query = $connection->query("SELECT * FROM catapult_make order by make");
      while ($row = $query->fetchAssoc()) {
      $make_array[] = array(
          'make_id'=>$row['make_id'],
          'make'=>$row['make']
        );
      }
      echo json_encode($make_array);
      exit();
   
  } 
  public function getmodeldata() {
      $connection = \Drupal::database();
      $query = $connection->query("SELECT cmo.make,cmo.make_id,cm.model,cm.model_id FROM catapult_model cm INNER JOIN catapult_make cmo ON cm.make_id = cmo.make_id ORDER BY cmo.make,cm.model");
 
    while ($row = $query->fetchAssoc()) {
      $model_array[] = array(
          'model_id'=>$row['model_id'],
          'model'=>$row['model'],
          'make'=>$row['make'],
          'make_id'=>$row['make_id']
        );
    }
    echo json_encode($model_array);
    exit;
   
  } 
  public function getyeardata() {
      $connection = \Drupal::database();
      $query = $connection->query("SELECT cm.make,cm.make_id,cmo.model,cmo.model_id,cy.year,cy.year_id FROM catapult_year cy 
INNER JOIN catapult_make cm ON cm.make_id = cy.make_id INNER JOIN catapult_model cmo ON cmo.model_id = cy.model_id order by cm.make,cmo.model,cy.year ");
 
    while ($row = $query->fetchAssoc()) {
        $year_array[] = array(
          'year_id'=>$row['year_id'],
          'year'=>$row['year'],
          'make'=>$row['make'],
          'make_id'=>$row['make_id'],
          'model'=>$row['model'],
          'model_id'=>$row['model_id']
        );
    }
    /*$data['year_results'] =$year_results;*/
    echo json_encode($year_array);
    exit;
   
  } 
 
 
 
  public function makedeletenode(){ 
    $connection = \Drupal::database();
      $id = $_POST['id'];
      //$id =900;
      $typess = $_POST['typess'];
      $exist=[];
      $query = $connection->query("SELECT model_id FROM catapult_model where make_id='".$id."'");
      while ($row = $query->fetchAssoc()) {
      $model[]=$row['model_id'];
      }
      $query = $connection->query("SELECT year_id FROM catapult_year where make_id='".$id."'");
      while ($row = $query->fetchAssoc()) {
      $year[]=$row['year_id'];
      }
      $query = $connection->query("SELECT engine_id FROM catapult_engine where make_id='".$id."'");
      while ($row = $query->fetchAssoc()) {
      $engine[]=$row['engine_id'];
      }
      $query = $connection->query("SELECT map_id  FROM catapult_application_map am INNER JOIN  catapult_engine en ON en.engine_id = am.engine_id WHERE en.make_id='".$id."'");
      while ($row = $query->fetchAssoc()) {
      $aplmap[]=$row['map_id'];
      }
      $exist['model']=(count($model) > 0 ) ? 1 : 0;
      $exist['year']=(count($year) > 0 ) ? 1 : 0;
      $exist['engine']=(count($engine) > 0 ) ? 1 : 0;
      $exist['aplmap']=(count($aplmap) > 0 ) ? 1 : 0;

      if($exist['model'] ==0 && $exist['year']  ==0 && $exist['engine'] ==0 && $exist['aplmap'] ==0){
        $connection->query("delete FROM catapult_make where make_id='".$id."'");
        echo "deleted";
        exit;
      }else{
         echo json_encode($exist);
         exit;
      }
       
  }

    public function makesinglenode(){ 
       $connection = \Drupal::database();
      $id   = $_POST['id'];
      $query = $connection->query("SELECT * FROM catapult_make where make_id='".$id."'");
      $make_array =[];
    while ($row = $query->fetchAssoc()) {
    $make_array[] = array(
        'make_id'=>$row['make_id'],
        'make'=>$row['make']
      );
    }
    echo json_encode($make_array);
    exit();
  }
  public function modeldeletenode(){ 
   /* $connection = \Drupal::database();
      $id = $_POST['id'];
      $typess = $_POST['typess'];
    if($typess == 'model'){
      $connection->query("delete FROM catapult_model where model_id='".$id."'");
    }
    exit;*/
     $connection = \Drupal::database();
      $id = $_POST['id'];
      //$id =900;
      $typess = $_POST['typess'];
      $exist=[];
      
      $query = $connection->query("SELECT year_id FROM catapult_year where model_id='".$id."'");
      while ($row = $query->fetchAssoc()) {
      $year[]=$row['year_id'];
      }
      $query = $connection->query("SELECT engine_id FROM catapult_engine where model_id='".$id."'");
      while ($row = $query->fetchAssoc()) {
      $engine[]=$row['engine_id'];
      }
      $query = $connection->query("SELECT map_id  FROM catapult_application_map am INNER JOIN  catapult_engine en ON en.engine_id = am.engine_id WHERE en.model_id='".$id."'");
      while ($row = $query->fetchAssoc()) {
      $aplmap[]=$row['map_id'];
      }
     
      $exist['year']=(count($year) > 0 ) ? 1 : 0;
      $exist['engine']=(count($engine) > 0 ) ? 1 : 0;
      $exist['aplmap']=(count($aplmap) > 0 ) ? 1 : 0;

      if($exist['year']  ==0 && $exist['engine'] ==0 && $exist['aplmap'] ==0){
        $connection->query("delete FROM catapult_model where model_id='".$id."'");
        echo "deleted";
        exit;
      }else{
         echo json_encode($exist);
         exit;
      }
  }

    public function modelsinglenode(){ 
       $connection = \Drupal::database();
      $id   = $_POST['id'];
      $query = $connection->query("SELECT cmo.make,cmo.make_id,cm.model,cm.model_id FROM catapult_model cm INNER JOIN catapult_make cmo ON cm.make_id = cmo.make_id WHERE model_id='".$id."'");
      $model_array =[];
    while ($row = $query->fetchAssoc()) {
    $model_array[] = array(
        'model_id'=>$row['model_id'],
        'model'=>$row['model'],
        'make' =>$row['make'],
        'make_id' =>$row['make_id']
      );
    }
    echo json_encode($model_array);
    exit();
  }
  public function yeardeletenode(){ 
      $connection = \Drupal::database();
      $id = $_POST['id'];
      $typess = $_POST['typess'];
      $exist=[];
      $query = $connection->query("SELECT engine_id FROM catapult_engine where year_id='".$id."'");
      while ($row = $query->fetchAssoc()) {
      $engine[]=$row['engine_id'];
      }
      $query = $connection->query("SELECT map_id  FROM catapult_application_map am INNER JOIN  catapult_engine en ON en.engine_id = am.engine_id WHERE en.year_id='".$id."'");
      while ($row = $query->fetchAssoc()) {
      $aplmap[]=$row['map_id'];
      }
      $exist['engine']=(count($engine) > 0 ) ? 1 : 0;
      $exist['aplmap']=(count($aplmap) > 0 ) ? 1 : 0;

      if($exist['engine'] ==0 && $exist['aplmap'] ==0){
        $connection->query("delete FROM catapult_year where year_id='".$id."'");
        echo "deleted";
        exit;
      }else{
         echo json_encode($exist);
         exit;
      }
  }

  public function yearsinglenode(){ 
       $connection = \Drupal::database();
      $id   = $_POST['id'];
      $query = $connection->query("SELECT year_id,year,model_id,make_id FROM catapult_year where year_id='".$id."'");
      $year_array =[];
    while ($row = $query->fetchAssoc()) {
    $year_array[] = array(
        'year_id'=>$row['year_id'],
        'year'=>$row['year'],
        'model_id'=>$row['model_id'],
        'make_id'=>$row['make_id']
      );
    }
    echo json_encode($year_array);
    exit();
  }

   public function getenginedata() {
      $connection = \Drupal::database();
      $query = $connection->query("SELECT cm.make,cm.make_id,cmo.model,cmo.model_id,ce.engine,ce.engine_id,cy.year,cy.year_id FROM catapult_engine ce INNER JOIN catapult_make cm ON cm.make_id = ce.make_id INNER JOIN catapult_model cmo ON cmo.model_id = ce.model_id   INNER JOIN catapult_year cy ON cy.year_id = ce.year_id order by cm.make,cmo.model,cy.year,ce.engine");
      while ($row = $query->fetchAssoc()) {
      $make_array[] = array(
          'engine_id'=>$row['engine_id'],
          'engine'=>$row['engine'],
          'make'=>$row['make'],
          'make_id'=>$row['make_id'],
          'model_id'=>$row['model_id'],
          'model'=>$row['model'],
          'year'=>$row['year'],
          'year_id'=>$row['year_id']
        );
      }
      echo json_encode($make_array);
      exit();
   
  }

  public function enginesinglenode(){ 
       $connection = \Drupal::database();
      $id   = $_POST['id'];
      $query = $connection->query("SELECT cm.make,cm.make_id,cmo.model,cmo.model_id,ce.engine,ce.engine_id,cy.year,cy.year_id FROM catapult_engine ce INNER JOIN catapult_make cm ON cm.make_id = ce.make_id INNER JOIN catapult_model cmo ON cmo.model_id = ce.model_id INNER JOIN catapult_year cy ON cy.year_id = ce.year_id WHERE engine_id='".$id."'");
      $engine_array =[];
    while ($row = $query->fetchAssoc()) {
    $engine_array[] = array(
        'engine_id'=>$row['engine_id'],
        'engine'=>$row['engine'],
        'model_id'=>$row['model_id'],
        'make_id'=>$row['make_id'],
        'make'=>$row['make'],
        'year'=>$row['year'],
        'year_id'=>$row['year_id']

      );
    }
    echo json_encode($engine_array);
    exit();
  }

   public function enginedeletenode(){ 
    /*$connection = \Drupal::database();
      $id = $_POST['id'];
      $typess = $_POST['typess'];
    if($typess == 'engine'){
      $connection->query("delete FROM catapult_engine where engine_id='".$id."'");
    }
    exit;*/
      $connection = \Drupal::database();
      $id = $_POST['id'];
      $typess = $_POST['typess'];
      $exist=[];
      $query = $connection->query("SELECT map_id  FROM catapult_application_map am INNER JOIN  catapult_engine en ON en.engine_id = am.engine_id WHERE en.engine_id='".$id."'");
      while ($row = $query->fetchAssoc()) {
      $aplmap[]=$row['map_id'];
      }
      $exist['aplmap']=(count($aplmap) > 0 ) ? 1 : 0;

      if($exist['aplmap'] ==0){
        $connection->query("delete FROM catapult_engine where engine_id='".$id."'");
        echo "deleted";
        exit;
      }else{
         echo json_encode($exist);
         exit;
      }
  }


  public function applicationform(){
    $user        = \Drupal::currentUser();
    $user_display_name = $user->getDisplayName();    
    $connection = \Drupal::database();
    $query = $connection->query("SELECT make_id,model_id,year_id,make,model,year FROM `catapult_application` WHERE make_id='".$_POST['makeid_data']."' AND model_id='".$_POST['modelid_data']."' AND year_id BETWEEN '".$_POST['min']."' and '".$_POST['max']."'");
    while ($row = $query->fetchAssoc()) {
      $aplinsert_exit_array[] =array('make_id' =>$row['make_id'],
        'model_id' =>$row['model_id'],
        'year_id' =>$row['year_id'],
        'make' =>$row['make'],
        'model' =>$row['model'],
        'year' =>$row['year']

      );
    }

    
    if(count($aplinsert_exit_array) > 0){

       echo "Combination Exist";  


    }else{

          $query= $connection->query("SELECT * FROM catapult_year where year_id BETWEEN '".$_POST['min']."' and '".$_POST['max']."'");
          $input_array =[];
          while ($row = $query->fetchAssoc()) {
          $input_array[] = array('make_id' => $_POST['makeid_data'],
          'model_id' => $_POST['modelid_data'],
          'year_id'=>$row['year_id'],
          'make' => $_POST['make_data'],
          'model' => $_POST['model_data'],
          'year'=>$row['year']
          );
          $result=$connection->query("insert into catapult_application(make,model,year,make_id,model_id,year_id,created_by,created_on) Values('".$_POST['make_data']."','".$_POST['model_data']."','".$row['year']."','".$_POST['makeid_data']."','".$_POST['modelid_data']."','".$row['year_id']."','".$user_display_name."',Now())"); 
          }

       echo "success";    

    }
    


   
    exit();


}

public function getapldata() {
      $connection = \Drupal::database();
      $query = $connection->query("SELECT application_id,make,model,year,make_id,model_id,year_id FROM catapult_application");
      while ($row = $query->fetchAssoc()) {
      $apl_array[] = array(
          'application_id'=>$row['application_id'],
          'make'=>$row['make'],
          'model'=>$row['model'],
          'year'=>$row['year']
        );
      }
      echo json_encode($apl_array);
      exit();
   
  }

   public function apldeletenode(){ 
    $connection = \Drupal::database();
      $id = $_POST['id'];
      $typess = $_POST['typess'];
      $app_array = [];
    if($typess == 'apl'){
      $connection->query("delete FROM catapult_application where application_id='".$id."'");
    }
    exit;
  }

  public function getapplication(){ 
    $connection = \Drupal::database();
    $app_array =[];
    if($_POST['maxyear'] != ''){
           for($i=$_POST['minyear'];$i<=$_POST['maxyear'];$i++){
            $year_array[]=$i;
           }
    }
    $query = $connection->query("SELECT cy.year_id FROM catapult_engine ce INNER JOIN catapult_year cy ON cy.year_id = ce.year_id 
    WHERE ce.make_id='".$_POST['make_id']."' AND ce.model_id='".$_POST['model_id']."' and  year between '".$_POST['minyear']."' and '".$_POST['maxyear']."'");

    while ($row = $query->fetchAssoc()) {
        $year_id[] =$row['year_id'];
    }   
     $id = implode(',',$year_id);
     if($_POST['engine'] == 'NULL'){
        $query = $connection->query("SELECT ce.engine_id FROM catapult_engine ce INNER JOIN catapult_make cm ON cm.make_id = ce.make_id 
        INNER JOIN catapult_model cmo ON cmo.model_id = ce.model_id INNER JOIN catapult_year cy ON cy.year_id = ce.year_id 
        WHERE ce.engine IS NULL AND  ce.model_id='".$_POST['model_id']."' and ce.make_id ='".$_POST['make_id']."' and ce.year_id in(".$id.")");
     }else{
        $query = $connection->query("SELECT ce.engine_id FROM catapult_engine ce INNER JOIN catapult_make cm ON cm.make_id = ce.make_id 
         INNER JOIN catapult_model cmo ON cmo.model_id = ce.model_id INNER JOIN catapult_year cy ON cy.year_id = ce.year_id 
         WHERE  ce.engine='".$_POST['engine']."' and ce.model_id='".$_POST['model_id']."' and ce.make_id ='".$_POST['make_id']."' and ce.year_id in(".$id.")");
     }
     
   /*  print_r($query);
     print_r($_POST['engine']);
     exit;*/
    while ($row = $query->fetchAssoc()) {
      $app_array[] =array('engine_id' =>$row['engine_id']);
    }  

  
    if(count($app_array) > 0){
      echo json_encode($app_array);
    }else{
      echo "NoCombination";
    }
    
    exit();
     
  }

  public function getproductdata(){ 
    $connection = \Drupal::database();
    $product_array =[];

    $query = $connection->query("SELECT distinct fp.`field_partno_value` FROM commerce_product cp 
    INNER JOIN `commerce_product__field_partno` fp ON fp.entity_id = cp.product_id where 
    cp.delete_flag = 0"); 
    while($row = $query->fetchAssoc()){

      $product_array[]=$row['field_partno_value'];

    }
     echo json_encode($product_array);
     exit();


   /* $single_array = [];
    $query = $connection->query("SELECT product_id FROM commerce_product");  
      while($row = $query->fetchAssoc()){
        $id = $row['product_id'];
        $product      = \Drupal\commerce_product\Entity\Product::load($id);     
      
        $pno       =  $product->get('field_partno')->getValue();
      $product_array[] = array(
       
        'part_no' =>  $pno[0]['value']
      
      );

    }


    foreach ($product_array as $key => $value) {
     
      $single_array[]=$value['part_no'];
     

    }

   $uniq_arr=(array_unique($single_array));
   sort($uniq_arr);
   
   echo json_encode($uniq_arr);
   exit();*/
     
  }

  public function applicationmap(){

    $user        = \Drupal::currentUser();
    $user_display_name = $user->getDisplayName();    
    $connection = \Drupal::database();


   /* print_r($_POST);
    exit;*/
    //$existing_map_array=[];
    $query = $connection->query("SELECT map_id,engine_id,product_id FROM catapult_application_map WHERE engine_id in(".$_POST['app_comma'].") AND product_id='".$_POST['product_id']."'");
    while ($row = $query->fetchAssoc()) {

      $existing_map_array[] = array(
        /*'product_id'=>$row['product_id'],*/
        'engine_id' =>  $row['engine_id']
      
      );

    }

    $input_app_array=$_POST['applicationid']; 




    if(count($existing_map_array) > 0){
    
    foreach ($existing_map_array as $key => $value) {

      $e_array[]=$value['engine_id'];
    
    }

    foreach ($input_app_array as $key => $value) {

      $i_array[]=$value['engine_id'];
    
    }
    $rr=array_diff($i_array,$e_array);
    $ff=array_values($rr);
   /* print_r($ff);
    exit;*/

      if(count($ff) > 0){
        foreach ($ff as $key => $value) {
         // echo $value;
         $result=$connection->query("insert into catapult_application_map(product_id,engine_id,created_by,created_on) Values('".$_POST['product_id']."','".$value."','".$user_display_name."',Now())"); 
         $msg="success";

        }
      }else{
        $msg="Exist";

      }

       
    }else{
       foreach ($input_app_array as $key => $value) {
       // echo $value;
       $result=$connection->query("insert into catapult_application_map(product_id,engine_id,created_by,created_on) Values('".$_POST['product_id']."','".$value['engine_id']."','".$user_display_name."',Now())"); 
       $msg="success";

      }
    }
    echo $msg;
    exit;

  }


 

  public function getaplmapdata() {
    ini_set('memory_limit', '-1');
    $connection = \Drupal::database();
    $aplmap_array=[];
    $query = $connection->query("SELECT cpm.map_id,cpm.product_id,cy.year as yearname,cy.year_id,ce.engine,ce.engine_id,cmo.model,cmo.model_id,cm.make,cm.make_id FROM catapult_application_map cpm 
      INNER JOIN catapult_engine ce ON cpm.engine_id = ce.engine_id 
      INNER JOIN catapult_year cy ON cy.year_id = ce.year_id 
      INNER JOIN catapult_model cmo ON cmo.model_id = ce.model_id 
      INNER JOIN catapult_make cm ON cm.make_id = cmo.make_id ORDER BY cm.make,cmo.model,cy.year,ce.engine,cpm.product_id");


        while ($row = $query->fetchAssoc()) {
          
          $aplmap_array[] = array(
              'year_id'=>isset($row['year_id']) ? $row['year_id'] : '',
              'make'=>isset($row['make']) ? $row['make'] : '',
              'model'=>isset($row['model']) ? $row['model'] : '',
              'year'=>isset($row['yearname']) ? $row['yearname'] : '',
             'part_no'=>isset($row['product_id']) ? $row['product_id'] : '',
              'map_id' => isset($row['map_id']) ? $row['map_id'] : '',
              'engine' => isset($row['engine']) ? $row['engine'] : ''
            );
        }

        echo json_encode($aplmap_array);
        exit();
     
  } 

  public function aplmapdeletenode(){ 
    $connection = \Drupal::database();
      $id = $_POST['id'];
      $typess = $_POST['typess'];
    if($typess == 'aplmap'){
      $connection->query("delete FROM catapult_application_map where map_id='".$id."'");
    }
    exit;
  }


  public function aplsinglenode(){

      $connection = \Drupal::database();
      $id   = $_POST['id'];
      $query = $connection->query("SELECT * FROM catapult_application where application_id='".$id."'");
      $apledit_array =[];
      while ($row = $query->fetchAssoc()) {
      $apledit_array[] = array(
          'make'=>$row['make'],
          'model'=>$row['model'],
          'year'=>$row['year'],
          'make_id'=>$row['make_id'],
          'model_id'=>$row['model_id'],
          'year_id'=>$row['year_id'],
          'application_id' =>$row['application_id']

        );
      }
      echo json_encode($apledit_array);
      exit();

  }



  public function applicationformedit(){
    $user        = \Drupal::currentUser();
    $user_display_name = $user->getDisplayName();    
    $connection = \Drupal::database();
    $apledit_exit_array =[];
    $query = $connection->query("SELECT application_id FROM `catapult_application` WHERE make_id='".$_POST['makeid_data']."' AND model_id='".$_POST['modelid_data']."' AND year_id='".$_POST['year_id']."' AND application_id !='".$_POST['application_id']."'");
     while ($row = $query->fetchAssoc()) {
      $apledit_exit_array =array('application_id' =>$row['application_id']);
     }
     /*echo count($apledit_exit_array);
     exit;*/

     if(count($apledit_exit_array) > 0){
        echo "Already Exist Same Combination";
     }else{
      $connection->query("update catapult_application set make='".$_POST['make_data']."',make_id='".$_POST['makeid_data']."',model='".$_POST['model_data']."',model_id='". $_POST['modelid_data']."',year_id='".$_POST['year_id']."',year='".$_POST['year_data']."',modified_by='".$user_display_name."',modified_on=Now() where application_id='".$_POST['application_id']."'");
      echo "success";
     }
     exit; 

  }

   public function aplmapsinglenode(){
    $connection = \Drupal::database();
    $id   = $_POST['id'];

    $query = $connection->query("SELECT cpm.map_id,cpm.product_id,cy.year,cy.year_id,ce.engine,ce.engine_id,cmo.model,cmo.model_id,cm.make,cm.make_id FROM catapult_application_map cpm INNER JOIN catapult_engine ce ON ce.engine_id = cpm.engine_id 
       INNER JOIN catapult_year cy ON cy.year_id = ce.year_id INNER JOIN catapult_model cmo ON cmo.model_id = ce.model_id 
       INNER JOIN catapult_make cm ON cm.make_id = ce.make_id WHERE cpm.map_id='".$id."'");
    $aplmapedit_array =[];
    while ($row = $query->fetchAssoc()) {
      $id_pno = $row['product_id'];
     /* $product      = \Drupal\commerce_product\Entity\Product::load($id_pno);     
      $pno       =  $product->get('field_partno')->getValue();*/
      $aplmapedit_array[] = array(
          'make'=>$row['make'],
          'model'=>$row['model'],
          'year'=>$row['year'],
          'make_id'=>$row['make_id'],
          'model_id'=>$row['model_id'],
          'year_id'=>$row['year_id'],
          'map_id' =>$row['map_id'],
         /* 'product_id' =>$row['product_id'],*/
          'product_id' => $row['product_id'],
          'engine'=>$row['engine'],
          'engine_id'=>$row['engine_id']



        );
      }
      echo json_encode($aplmapedit_array);
      exit();

  }

  public function application_map_edit(){

    $user        = \Drupal::currentUser();
    $user_display_name = $user->getDisplayName();    
    $connection = \Drupal::database();
    $apl_map_edit_exit_array =[];
/*    print_r($_POST);
    exit;*/

    $query = $connection->query("SELECT map_id FROM `catapult_application_map` WHERE product_id='".$_POST['product_id']."' AND engine_id in (".$_POST['app_comma'].") AND map_id !='".$_POST['map_id']."'");
     while ($row = $query->fetchAssoc()) {
      $apl_map_edit_exit_array =array('map_id' =>$row['map_id']);
     }
     if(count($apl_map_edit_exit_array) > 0){
        echo "Already Exist Same Combination";
     }else{
      $connection->query("update catapult_application_map set product_id='".$_POST['product_id']."',engine_id='".$_POST['app_comma']."',modified_by='".$user_display_name."',modified_on=Now() where map_id='".$_POST['map_id']."'");
      echo "success";
     }
     exit; 



  }


//////////////////////////////////////////////Rewrite Make Model year////////////////////



  public function make_create(){
    $user        = \Drupal::currentUser();
    $user_display_name = $user->getDisplayName();    
    $connection = \Drupal::database();
    $make_exist=[];
    



    if($_POST['make_id'] == ''){

       $query = $connection->query("SELECT make_id FROM catapult_make WHERE make='".trim($_POST['make'])."'");
       while ($row = $query->fetchAssoc()) {
        $make_exist =array('make_id' =>$row['make_id']);
       }  
       if(count($make_exist) > 0){
          echo "MakeExist";
       }else{
           $connection->query("insert into catapult_make(make,created_by,created_on) Values('".trim($_POST['make'])."','".$user_display_name."',Now())"); 
          echo "Inserted";
       }
     
    }else{

      $query = $connection->query("SELECT make_id FROM catapult_make WHERE make='".trim($_POST['make'])."' and make_id !='".$_POST['make_id']."'");
       while ($row = $query->fetchAssoc()) {
        $make_exist =array('make_id' =>$row['make_id']);
       }
       if(count($make_exist) > 0){
          echo "MakeExist";
       }else{
           $connection->query("update catapult_make set make='".trim($_POST['make'])."',modified_by='".$user_display_name."',modified_on=Now() where make_id='".$_POST['make_id']."'"); 
            echo "Updated";
       }
     
    }

    exit;
  
  }


  public function model_create(){
    $user        = \Drupal::currentUser();
    $user_display_name = $user->getDisplayName();    
    $connection = \Drupal::database();
    $model_exist=[];
    /*print_r($_POST);
    exit;*/
    if($_POST['model_id'] == ''){

       $query = $connection->query("SELECT model_id FROM catapult_model WHERE model='".trim($_POST['model'])."' and make_id ='".$_POST['make_id']."'");
       while ($row = $query->fetchAssoc()) {
        $model_exist =array('model_id' =>$row['model_id']);
       }  
       if(count($model_exist) > 0){
          echo "ModelExist";
       }else{
           $connection->query("insert into catapult_model(make_id,model,created_by,created_on) Values('".$_POST['make_id']."','".trim($_POST['model'])."','".$user_display_name."',Now())"); 
          echo "Inserted";
       }
     
    }else{

      $query = $connection->query("SELECT model_id FROM catapult_model WHERE model='".trim($_POST['model'])."' and make_id ='".$_POST['make_id']."' and model_id !='".$_POST['model_id']."'");
       while ($row = $query->fetchAssoc()) {
        $model_exist =array('model_id' =>$row['model_id']);
       }
       if(count($model_exist) > 0){
          echo "ModelExist";
       }else{
           $connection->query("update catapult_model set model='".trim($_POST['model'])."',make_id='".$_POST['make_id']."',modified_by='".$user_display_name."',modified_on=Now() where model_id='".$_POST['model_id']."'"); 
            echo "Updated";
       }
     
    }
    exit;


}

  public function getmodel_of_make(){
    $related_model=[];
    $connection = \Drupal::database();
    if($_POST['tab_name'] == 'apl_map_tab'){
      $query = $connection->query("SELECT DISTINCT(cmo.model),cmo.model_id FROM catapult_engine ce INNER JOIN catapult_model cmo ON cmo.model_id = ce.model_id WHERE ce.make_id='".$_POST['make_id']."' order by cmo.model");
    }else{
       $query = $connection->query("SELECT model_id,model FROM catapult_model WHERE make_id='".$_POST['make_id']."' order by model"); 
    }
    
    while ($row = $query->fetchAssoc()) {
      $related_model[] =array('model_id' =>$row['model_id'],'model' =>$row['model']);
    }
      echo json_encode($related_model);
      exit;


  }



  public function engine_create(){
    $user        = \Drupal::currentUser();
    $user_display_name = $user->getDisplayName();    
    $connection = \Drupal::database();
    $engine_exist=[];
    if($_POST['engine_id'] == ''){
      if($_POST['max_y'] != ''){
           for($i=$_POST['min_y'];$i<=$_POST['max_y'];$i++){
            $year_array[]=$i;
           }
      }else{
            for($i=$_POST['min_y'];$i<=$_POST['min_y'];$i++){
            $year_array[]=$i;
           }
           $_POST['max_y']=$_POST['min_y'];
      }
      $query = $connection->query("SELECT year_id FROM catapult_year WHERE   year between '".$_POST['min_y']."' and '".$_POST['max_y']."' and model_id='".$_POST['model_id']."' and make_id ='".$_POST['make_id']."'");
      while ($row = $query->fetchAssoc()) {
        $year_id[] =$row['year_id'];
             

      }   
     $id = implode(',',$year_id);

    
      if($_POST['engine'] == ''){
        
          $query = $connection->query("SELECT engine_id FROM catapult_engine WHERE engine='' and model_id='".$_POST['model_id']."' and make_id ='".$_POST['make_id']."' and year_id in(".$id.")");
      }else{
        
      $query = $connection->query("SELECT engine_id FROM catapult_engine WHERE engine='".trim($_POST['engine'])
      ."' and model_id='".$_POST['model_id']."' and make_id ='".$_POST['make_id']."' and year_id in(".$id.")");
      }
      
      while ($row = $query->fetchAssoc()) {
        $engine_exist =array('engine_id' =>$row['engine_id']);
      }  

      /*print_r($query);
      exit;*/

       if(count($engine_exist) > 0){
          echo "EngineExist";
       }else{
        if($_POST['engine'] == '' ){
          foreach ($year_id as $key => $value) {
             $connection->query("insert into catapult_engine(make_id,model_id,year_id,created_by,created_on) Values('".$_POST['make_id']."','".$_POST['model_id']."','".$value."','".$user_display_name."',Now())");
           
          }
        }else{
          foreach ($year_id as $key => $value) {
          $connection->query("insert into catapult_engine(engine,make_id,model_id,year_id,created_by,created_on) Values('".trim($_POST['engine'])
          ."','".$_POST['make_id']."','".$_POST['model_id']."','".$value."','".$user_display_name."',Now())");

          }
        }
          
          echo "Inserted";
       }   


      
     
    }else{
      $id='';
      $query = $connection->query("SELECT year_id FROM catapult_year WHERE   year='".$_POST['min_y']."'  and model_id='".$_POST['model_id']."' and make_id ='".$_POST['make_id']."'");
      while ($row = $query->fetchAssoc()) {
        $id =$row['year_id'];
      }  
       if($_POST['engine'] == ''){
           $query = $connection->query("SELECT engine_id FROM catapult_engine WHERE  model_id='".$_POST['model_id']."' and make_id ='".$_POST['make_id']."' and year_id ='".$id."' and engine_id !='".$_POST['engine_id']."'");
       }else{
        $query = $connection->query("SELECT engine_id FROM catapult_engine WHERE engine='".trim($_POST['engine'])
        ."' and model_id='".$_POST['model_id']."' and make_id ='".$_POST['make_id']."' and year_id ='".$id."' and engine_id !='".$_POST['engine_id']."'");

       }

       while ($row = $query->fetchAssoc()) {
        $engine_exist =array('engine_id' =>$row['engine_id']);
       }
       /*print_r($engine_exist);
       exit;*/

       if(count($engine_exist) > 0){
          echo "EngineExist";
       }else{

          if($_POST['engine'] != '' ){
       
            $connection->query("update catapult_engine set engine='".trim($_POST['engine'])
            ."',make_id='".$_POST['make_id']."',model_id='".$_POST['model_id']."',year_id='".$id."',modified_by='".$user_display_name."',modified_on=Now() where engine_id='".$_POST['engine_id']."'"); 
         }else{
       
           $qry= $connection->query("update catapult_engine set engine=NULL and make_id='".$_POST['make_id']."',model_id='".$_POST['model_id']."',year_id='".$id."',modified_by='".$user_display_name."',modified_on=Now() where engine_id='".$_POST['engine_id']."'"); 
         }
        

            echo "Updated";
       }
     
    }
    exit;


  }

  public function year_create(){
    $user        = \Drupal::currentUser();
    $user_display_name = $user->getDisplayName();    
    $connection = \Drupal::database();
    $year_exist=[];
    $year_array=[];

    // print_r($_POST);
    // exit;



    if($_POST['year_id'] == ''){


        if($_POST['Toyear'] != ''){
           for($i=trim($_POST['year']);$i<=$_POST['Toyear'];$i++){
            $year_array[]=$i;
           }
        }else{
            for($i=trim($_POST['year']);$i<=$_POST['year'];$i++){
            $year_array[]=$i;
           }
           $_POST['Toyear']=trim($_POST['year']);
        }


        /*if($_POST['engine_id'] != ''){*/

           $query = $connection->query("SELECT year_id FROM catapult_year WHERE make_id='".$_POST['make_id']."' and model_id='".$_POST['model_id']."' and  year between '".trim($_POST['year'])."' and '".trim($_POST['Toyear'])."'");
        /* }else{
            $query = $connection->query("SELECT year_id FROM catapult_year WHERE make_id='".$_POST['make_id']."' and model_id='".$_POST['model_id']."' and engine_id ='".NULL."' and  year between '".$_POST['year']."' and '".$_POST['Toyear']."'");
         }*/
     
       while ($row = $query->fetchAssoc()) {
        $year_exist =array('year_id' =>$row['year_id']);
       }  

      /* print_r($query);
       exit;*/

       if(count($year_exist) > 0){
          echo "YearExist";
       }else{

          foreach ($year_array as $key => $value) {
             $connection->query("insert into catapult_year(year,make_id,model_id,created_by,created_on) Values('".$value."','".$_POST['make_id']."','".$_POST['model_id']."','".$user_display_name."',Now())");
           
          }
          
          echo "Inserted";
       }
     
    }else{
       $query = $connection->query("SELECT year_id FROM catapult_year WHERE year='".trim($_POST['year'])."' and  model_id='".$_POST['model_id']."' and make_id ='".$_POST['make_id']."' and year_id !='".$_POST['year_id']."'"); 
       while ($row = $query->fetchAssoc()) {
        $year_exist =array('year_id' =>$row['year_id']);
       }
      if(count($year_exist) > 0){
          echo "YearExist";
      }else{
           $connection->query("update catapult_year set year='".trim($_POST['year'])."',make_id='".$_POST['make_id']."',model_id='".$_POST['model_id']."',modified_by='".$user_display_name."',modified_on=Now() where year_id='".$_POST['year_id']."'"); 
            echo "Updated";
       }
     
    }
    exit;


  }



public function getyear_of_model(){
    $related_year=[];
    $connection = \Drupal::database();
    if($_POST['tab_name'] == 'apl_map_tab'){
      $query = $connection->query("SELECT DISTINCT cy.year,cy.year_id FROM catapult_engine ce INNER JOIN catapult_year cy ON cy.year_id = ce.year_id WHERE ce.make_id='".$_POST['make_id']."' AND ce.model_id='".$_POST['model_id']."' order by cy.year");
    }else{
      $query = $connection->query("SELECT DISTINCT year,year_id FROM `catapult_year` WHERE make_id='".$_POST['make_id']."' AND model_id='".$_POST['model_id']."' order by year");
    }
    while ($row = $query->fetchAssoc()) {
      $related_year[] =array('year' =>$row['year'],'year_id' =>$row['year_id']);
    }
    echo json_encode($related_year);
    exit;
}




public function getengine_of_year(){
    $related_year=[];
    $y_id = [];
    $connection = \Drupal::database();

    if($_POST['max_y'] != ''){
       for($i=$_POST['min_y'];$i<=$_POST['max_y'];$i++){
        $year_array[]=$i;
       }
    }else{
        for($i=$_POST['min_y'];$i<=$_POST['min_y'];$i++){
        $year_array[]=$i;
       }
       $_POST['max_y']=$_POST['min_y'];
    }
    $query = $connection->query("SELECT cy.year_id FROM catapult_engine ce 
INNER JOIN catapult_year cy ON cy.year_id = ce.year_id 
WHERE ce.make_id='".$_POST['make_id']."' AND ce.model_id='".$_POST['model_id']."' and  year between '".$_POST['min_y']."' and '".$_POST['max_y']."'
");
    while ($row = $query->fetchAssoc()) {
        $y_id[] =$row['year_id'];
    }  

   $id = implode(',',$y_id);
/*
   print_r($id);
print_r($query);
exit;*/



    $query = $connection->query("SELECT DISTINCT(IFNULL(engine, 'NULL')) AS engine FROM `catapult_engine` WHERE make_id='".$_POST['make_id']."' AND model_id='".$_POST['model_id']."'  and year_id in(".$id.")");
     while ($row = $query->fetchAssoc()) {
      $related_year[] =array('engine' =>$row['engine']);
     }
      echo json_encode($related_year);
      exit;
}






  

  public function getyear_of_engine(){
    $related_year=[];
    $connection = \Drupal::database();
    if($_POST['engine_id'] != ''){
    $query = $connection->query("SELECT year,year_id FROM `catapult_year` WHERE make_id='".$_POST['make_id']."' AND model_id='".$_POST['model_id']."' AND engine_id='".$_POST['engine_id']."'");
    }else{
       $query = $connection->query("SELECT year,year_id FROM `catapult_year` WHERE make_id='".$_POST['make_id']."' AND model_id='".$_POST['model_id']."'");
    }
     while ($row = $query->fetchAssoc()) {
      $related_year[] =array('year' =>$row['year'],'year_id' =>$row['year_id']);
     }
      echo json_encode($related_year);
      exit;
  }



public function mmey_xls_upload(){ 
  ini_set('max_execution_time', 0);
  $connection = \Drupal::database();
  $mmey=[];
  $user        = \Drupal::currentUser();
  $user_display_name = $user->getDisplayName(); 
  $filename = $_FILES['file']['name'];
  $path="public://mmyeupload/";
  $mmey=[];
  $make=[];
  $model=[];
  $engine=[];
  $year=[];
  //mkdir($path, 0777, true) || chmod($path, 0777);
  if(!$_FILES['file']['error']){
    if(file_exists("public://mmyeupload/".$filename)) {
      unlink("public://mmyeupload/".$filename);
    }
    move_uploaded_file($_FILES['file']['tmp_name'],"public://mmyeupload/".$filename);
  }else{
    $msg="checkfilesize";
    exit;
  }
  $targetPath = 'site/default/files/mmyeupload/'.$filename;        
  $inputFile="public://mmyeupload/".$filename;
  $extension = strtoupper(pathinfo($inputFile, PATHINFO_EXTENSION));
  $inputFileType = \PHPExcel_IOFactory::identify($inputFile);
  $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
  $objPHPExcel = $objReader->load($inputFile);
  $sheet = $objPHPExcel->getSheet(0);
  $highestRow = $sheet->getHighestDatarow();
  $highestColumn = $sheet->getHighestDataColumn();
  $title_array = $sheet->rangeToArray('A' . $row=1 . ':' . $highestColumn . $row=1, NULL, TRUE, FALSE);
  $header=array('Make','Model','Year','Engine');
  if($header != $title_array[0]) {
    echo "Headernotmatch";
    exit;
  }
  if($highestRow<=1){
    echo "Nodata";
    exit;
  }

              
    for ($row = 1; $row <= $highestRow; $row++){
        $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);  
        if($rowData[0][0]!=''){
          if($row!=1){
           array_push($make,$rowData[0][0]);
           array_push($model,$rowData[0][1]);
           array_push($engine,$rowData[0][3]);
           array_push($year,$rowData[0][2]);
           array_push($mmey,array('make'=>$rowData[0][0],'model'=>$rowData[0][1],'engine'=>$rowData[0][2],'year'=>$rowData[0][3]));
          }  
        }
    } 

    /*print_r($make);
    exit;*/

    /****************************************make insert**************************************/
    $make_ins='';
    $model_ins='';
    $year_ins='';
    $engine_ins='';
    $inserted_make=[];
    $make_exist_msg=[];
    $makeid_exist=[];
    $make_in = "'" . implode ( "', '", $make ) . "'";
    $query = $connection->query("SELECT make_id,make FROM catapult_make WHERE make in(".$make_in.")");
     while ($row = $query->fetchAssoc()) {
      $make_exist_msg[] =$row['make'];
      $makeid_exist[] =array($row['make'],$row['make_id']);
     }  
     $diff_arr_make=array_diff($make,$make_exist_msg);
     $arr_make=array_unique($diff_arr_make);
     if(count($arr_make) > 0){

        foreach ($arr_make as $key => $value) {
          $inserted_make[]=$value;
          $insert_make =$connection->query("insert into catapult_make(make,created_by,created_on) Values('".$value."','".$user_display_name."',Now())"); 
        }
        $make_ins=1;
      }

      $query = $connection->query("SELECT make_id,make FROM catapult_make WHERE make in(".$make_in.")");
         while ($row = $query->fetchAssoc()) {
          $makeid_exist[] =array($row['make'],$row['make_id']);
      }

     /****************************************make insert End**************************************/
     /****************************************model insert**************************************/
 
     $make_count=count($make);
     $empty=[];
    for ($i=0; $i <$make_count ; $i++) { 

      $comb=$make[$i]."$".$model[$i];
        if(in_array($comb, $empty)){

        }else{
          array_push($empty,$comb);
        }
       
     }

     foreach ($empty as $key => $value) {
        $explod_make_model[]=explode("$",$value);
     }
     
   
    $make_exist_count=count($explod_make_model);
    $makeid_exist_count=count($makeid_exist);
     for ($j=0; $j < $makeid_exist_count;$j++) { 
        for ($i=0; $i < $make_exist_count ; $i++) { 
               if($explod_make_model[$i][0] == $this->in_array_r($makeid_exist[$j])){
                $explod_make_model[$i][0]=$this->in_array_r_id($makeid_exist[$j]);
               }
        } 
    }

    

      $inserted_model=[];
      $model_make_exist=[];
      $model_make_exist_msg=[];
      $model_make_id_exist=[];
      
    foreach ($explod_make_model as $key=>$value) {
      $query = $connection->query("SELECT cmo.make,cmo.make_id,cm.model,cm.model_id FROM catapult_model cm INNER JOIN catapult_make cmo ON cm.make_id = cmo.make_id WHERE cm.model ='".$value[1]."' AND cm.make_id='".$value[0]."'");
        while ($row = $query->fetchAssoc()) {
            $model_make_exist_msg[] =array($row['make'],$row['model']);
            $model_make_exist[]=array($row['make_id'],$row['model']);
            $model_make_id_exist[] =array($row['model'],$row['model_id']);
        } 

    }
   
     $diff_arr_make_model = $this->make_model_array_diff($explod_make_model,$model_make_exist);
     if(count($diff_arr_make_model) > 0){
      foreach ($diff_arr_make_model as $key => $value) {

          $inserted_model[]=$value;
          $insert_make =$connection->query("insert into catapult_model(model,make_id,created_by,created_on) Values('".$value[1]."','".$value[0]."','".$user_display_name."',Now())"); 
      }
      
     
  }
    foreach ($explod_make_model as $key=>$value) {
      $query = $connection->query("SELECT cmo.make,cmo.make_id,cm.model,cm.model_id FROM catapult_model cm INNER JOIN catapult_make cmo ON cm.make_id = cmo.make_id WHERE cm.model ='".$value[1]."' AND cm.make_id='".$value[0]."'");
        while ($row = $query->fetchAssoc()) {
            
            $model_make_exist[]=array($row['make_id'],$row['model']);
            $model_make_id_exist[] =array($row['model'],$row['model_id']);
        } 

    }

     /****************************************model insert  End **************************************/
     

      /***************************************** Year Start *********************************************/
    
    $empty_year=[];
    for ($i=0; $i <$make_count ; $i++) { 

      $comb_year=$make[$i]."$".$model[$i]."$".$year[$i];
        if(in_array($comb_year, $empty_year)){

        }else{
          array_push($empty_year,$comb_year);
        }
       
     }

     foreach ($empty_year as $key => $value) {
        $explod_make_model_engine_year[]=explode("$",$value);
     }


    $make_exist_count=count($explod_make_model_engine_year);
    $makeid_exist_count=count($makeid_exist);
     for ($j=0; $j < $makeid_exist_count;$j++) { 
        for ($i=0; $i < $make_exist_count ; $i++) { 
               if($explod_make_model_engine_year[$i][0] == $this->in_array_r($makeid_exist[$j])){
                $explod_make_model_engine_year[$i][0]=$this->in_array_r_id($makeid_exist[$j]);
               }
        } 
    }

    $model_make_id_exist_count=count($model_make_id_exist);
     for ($j=0; $j < $model_make_id_exist_count;$j++) { 
        for ($i=0; $i < $make_exist_count ; $i++) { 
           if($explod_make_model_engine_year[$i][1] == $this->in_array_r($model_make_id_exist[$j])){
            $explod_make_model_engine_year[$i][1]=$this->in_array_r_id($model_make_id_exist[$j]);
           }
        } 
    }

   

      $inserted_year=[];
      $model_make_engine_year_exist=[];
      $model_make_engine_year_exist_msg=[];
      $year_engine_id_model_id_make_id_exist=[];
      
      foreach ($explod_make_model_engine_year as $key=>$value) {
      $query = $connection->query("SELECT cm.make,cm.make_id,cmo.model,cmo.model_id,cy.year,cy.year_id FROM catapult_year cy INNER JOIN catapult_make cm ON cm.make_id = cy.make_id INNER JOIN catapult_model cmo ON cmo.model_id = cy.model_id 
        WHERE cy.year='".$value[2]."' AND  cy.make_id='".$value[0]."' AND cy.model_id='".$value[1]."'");
              while ($row = $query->fetchAssoc()) {
                  $model_make_engine_year_exist_msg[] =array($row['make'],$row['model'],$row['year']);
                  $model_make_engine_year_exist[]=array($row['make_id'],$row['model_id'],$row['year']);
                  $year_engine_id_model_id_make_id_exist[] =array($row['make_id'],$row['model_id'],$row['year'],$row['year_id']);
              } 

      }

     
      if(count($model_make_engine_year_exist) > 0){
          $diff_arr_make_model_engine_year = $this->make_model_array_diff($explod_make_model_engine_year,$model_make_engine_year_exist);
      }else{
          $diff_arr_make_model_engine_year = $explod_make_model_engine_year;
      }

    
      
      if(count($diff_arr_make_model_engine_year) > 0){
        foreach ($diff_arr_make_model_engine_year as $key => $value) {
         
            $inserted_year[]=$value;
            $insert_make =$connection->query("insert into catapult_year(year,make_id,model_id,created_by,created_on) Values('".$value[2]."','".$value[0]."','".$value[1]."','".$user_display_name."',Now())"); 
           
        }
       
       
    }

    foreach ($explod_make_model_engine_year as $key=>$value) {
      $query = $connection->query("SELECT cm.make,cm.make_id,cmo.model,cmo.model_id,cy.year,cy.year_id FROM catapult_year cy INNER JOIN catapult_make cm ON cm.make_id = cy.make_id INNER JOIN catapult_model cmo ON cmo.model_id = cy.model_id 
        WHERE cy.year='".$value[2]."' AND  cy.make_id='".$value[0]."' AND cy.model_id='".$value[1]."'");
              while ($row = $query->fetchAssoc()) {
                if(count($model_make_engine_year_exist_msg) < 0){
                  $model_make_engine_year_exist_msg[] =array($row['make'],$row['model'],$row['year']);
                }
                  
                  $model_make_engine_year_exist[]=array($row['make_id'],$row['model_id'],$row['year']);
                  $year_engine_id_model_id_make_id_exist[] =array($row['make_id'],$row['model_id'],$row['year'],$row['year_id']);
              } 

      }

      /***************************************** Year end *********************************************/


      /****************************************Engine insert**************************************/

   
   
    $empty_engine=[];
    for ($i=0; $i <$make_count ; $i++) { 

      $comb_engine=$make[$i]."$".$model[$i]."$".$year[$i]."$".$engine[$i];
        if(in_array($comb_engine, $empty_engine)){

        }else{
          array_push($empty_engine,$comb_engine);
        }
       
     }

     foreach ($empty_engine as $key => $value) {
        $explod_make_model_engine[]=explode("$",$value);
     }

    

    $make_exist_count=count($explod_make_model_engine);
    $makeid_exist_count=count($makeid_exist);
     for ($j=0; $j < $makeid_exist_count;$j++) { 
        for ($i=0; $i < $make_exist_count ; $i++) { 
               if($explod_make_model_engine[$i][0] == $this->in_array_r($makeid_exist[$j])){
                $explod_make_model_engine[$i][0]=$this->in_array_r_id($makeid_exist[$j]);
               }
        } 
    }
    

    $make_exist_count=count($explod_make_model_engine);
    $model_make_id_exist_count=count($model_make_id_exist);
     for ($j=0; $j < $model_make_id_exist_count;$j++) { 
        for ($i=0; $i < $make_exist_count ; $i++) { 
           if($explod_make_model_engine[$i][1] == $this->in_array_r($model_make_id_exist[$j])){
            $explod_make_model_engine[$i][1]=$this->in_array_r_id($model_make_id_exist[$j]);
           }
        } 
    }
    
    $make_exist_count=count($explod_make_model_engine);
    $model_make_id_year_exist_count=count($year_engine_id_model_id_make_id_exist);
     for ($j=0; $j < $model_make_id_year_exist_count;$j++) { 
        for ($i=0; $i < $make_exist_count ; $i++) { 
         
          if($explod_make_model_engine[$i][0] == $this->in_array_r_zero($year_engine_id_model_id_make_id_exist[$j])){
            if($explod_make_model_engine[$i][1] == $this->in_array_r_one($year_engine_id_model_id_make_id_exist[$j])){
               if($explod_make_model_engine[$i][2] == $this->in_array_r_two($year_engine_id_model_id_make_id_exist[$j])){
                $explod_make_model_engine[$i][2]=$this->in_array_r_three($year_engine_id_model_id_make_id_exist[$j]);
               }
            }
          }
        } 
    }
   
      $inserted_engine=[];
      $model_make_engine_exist=[];
      $model_make_engine_exist_msg=[];
      $engine_model_id_make_id_exist=[];

      
      foreach ($explod_make_model_engine as $key=>$value) {
        if($value[3] != ''){
          $rep_eng=str_replace(";"," ",$value[3]);
          $query = $connection->query("SELECT cm.make,cm.make_id,cmo.model,cmo.model_id,ce.engine,ce.engine_id,cy.year,cy.year_id FROM catapult_engine ce 
      INNER JOIN catapult_make cm ON cm.make_id = ce.make_id INNER JOIN catapult_model cmo ON cmo.model_id = ce.model_id  
      INNER JOIN catapult_year cy ON cy.year_id = ce.year_id WHERE ce.engine='".$rep_eng."' AND ce.make_id='".$value[0]."' AND ce.model_id='".$value[1]."' AND ce.year_id='".$value[2]."'");
            /*exit;*/
        }else{
          $query = $connection->query("SELECT cm.make,cm.make_id,cmo.model,cmo.model_id,ce.engine,ce.engine_id,cy.year,cy.year_id FROM catapult_engine ce 
      INNER JOIN catapult_make cm ON cm.make_id = ce.make_id INNER JOIN catapult_model cmo ON cmo.model_id = ce.model_id  
      INNER JOIN catapult_year cy ON cy.year_id = ce.year_id WHERE  ce.make_id='".$value[0]."' AND ce.model_id='".$value[1]."' AND ce.year_id='".$value[2]."'");
        }
      
              while ($row = $query->fetchAssoc()) {
                  $model_make_engine_exist_msg[] =array($row['make'],$row['model'],$row['year'],$row['engine']);
                  $model_make_engine_exist[]=array($row['make_id'],$row['model_id'],$row['year_id'],$row['engine']);
                  $engine_model_id_make_id_exist[] =array($row['engine'],$row['engine_id']);
              } 

      }

      $diff_arr_make_model_engine = $this->make_model_array_diff($explod_make_model_engine,$model_make_engine_exist);
      if(count($diff_arr_make_model_engine) > 0){
        foreach ($diff_arr_make_model_engine as $key => $value) {
          if($value[3] != ''){
            $rep_eng=str_replace(";"," ",$value[3]);
            
            $insert_make =$connection->query("insert into catapult_engine(engine,make_id,model_id,year_id,created_by,created_on) Values('".$rep_eng."','".$value[0]."','".$value[1]."','".$value[2]."','".$user_display_name."',Now())"); 
          }
          else{
            $insert_make =$connection->query("insert into catapult_engine(make_id,model_id,year_id,created_by,created_on) Values('".$value[0]."','".$value[1]."','".$value[2]."','".$user_display_name."',Now())"); 
          }

           
        }
       
       
    }
      /****************************************Engine insert  End **************************************/
      if(count($model_make_engine_exist_msg) >0){
         echo "somealeadyexist";
       
       }else{
         echo "allinserted"; 
       }
      exit;
    

}  

public function in_array_r($needle) {
/*  print_r($needle);
  exit; */
  return $needle[0];
    
}


public function in_array_r_id($needle) {
 /* print_r($needle);
  exit; */
  return $needle[1];
    
}

public function in_array_r_zero($needle) {
 
  return $needle[0];
    
}

public function in_array_r_one($needle) {

  return $needle[1];
    
}

public function in_array_r_two($needle) {

  return $needle[2];
    
}
public function in_array_r_three($needle) {

  return $needle[3];
    
}



public function make_model_array_diff($arraya, $arrayb) {

    foreach ($arraya as $keya => $valuea) {
        if (in_array($valuea, $arrayb)) {
            unset($arraya[$keya]);
        }
    }
    return $arraya;
}



public function aplmap_xls_upload(){
  ini_set('max_execution_time', 0);
  $connection = \Drupal::database();
  $mmey=[];
  $make=[];
  $model=[];
  $engine=[];
  $year=[];
  $user        = \Drupal::currentUser();
  $user_display_name = $user->getDisplayName(); 
  $filename = $_FILES['file']['name'];
  $path="public://mmyeupload/";
  mkdir($path, 0777, true) || chmod($path, 0777);
  if(!$_FILES['file']['error']){
    if(file_exists("public://mmyeupload/".$filename)) {
    
    unlink("public://mmyeupload/".$filename);
    }
    //chmod("public://mmyeupload/".$filename, 0755);
    move_uploaded_file($_FILES['file']['tmp_name'],"public://mmyeupload/".$filename);
  }else{
    echo "checkfilesize";
    exit;
  }
  
  $path="public://mmyeupload/";
  move_uploaded_file($_FILES['file']['tmp_name'],"public://mmyeupload/".$filename);
  $targetPath = 'site/default/files/mmyeupload/'.$filename;        
  $inputFile="public://mmyeupload/".$filename;
  $extension = strtoupper(pathinfo($inputFile, PATHINFO_EXTENSION));
  $inputFileType = \PHPExcel_IOFactory::identify($inputFile);
  $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
  $objPHPExcel = $objReader->load($inputFile);

  $sheet = $objPHPExcel->getSheet(0);
  $highestRow = $sheet->getHighestDatarow();
  $highestColumn = $sheet->getHighestDataColumn();
  $title_array = $sheet->rangeToArray('A' . $row=1 . ':' . $highestColumn . $row=1, NULL, TRUE, FALSE);
 /* echo "<pre>";
  print_r($title_array);
  die();*/


  if($highestRow<=1){
    $msg="There is no data in excel";
  }
  
  for ($row = 1; $row <= $highestRow; $row++){
      $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);  
      if($rowData[0][0]!=''){
        if($row!=1){

         array_push($make,$rowData[0][1]);
         array_push($model,$rowData[0][2]);
         array_push($engine,$rowData[0][4]);
         array_push($year,$rowData[0][3]);
         array_push($mmey,array('product_id'=>$rowData[0][0],'make'=>$rowData[0][1],'model'=>$rowData[0][2],'engine'=>$rowData[0][4],'year'=>$rowData[0][3]));
        }  
      }

  } 

$engine_id=[];
$mmye_comb_not=[];
$exist_apl_map=[];
$exist_apl_map_data=[];



foreach ($mmey as $key => $value) {

  if($value['engine'] != ''){
     $query = $connection->query("SELECT cm.make,cm.make_id,cmo.model,cmo.model_id,ce.engine,ce.engine_id,cy.year,cy.year_id FROM catapult_engine ce 
      INNER JOIN catapult_make cm ON cm.make_id = ce.make_id INNER JOIN catapult_model cmo ON cmo.model_id = ce.model_id  
      INNER JOIN catapult_year cy ON cy.year_id = ce.year_id WHERE ce.engine='".$value['engine']."' AND cm.make='".$value['make']."' AND cmo.model='".$value['model']."' AND cy.year='".$value['year']."'");
  }else{
       $query = $connection->query("SELECT cm.make,cm.make_id,cmo.model,cmo.model_id,ce.engine,ce.engine_id,cy.year,cy.year_id FROM catapult_engine ce 
      INNER JOIN catapult_make cm ON cm.make_id = ce.make_id INNER JOIN catapult_model cmo ON cmo.model_id = ce.model_id  
      INNER JOIN catapult_year cy ON cy.year_id = ce.year_id WHERE ce.engine is null AND cm.make='".$value['make']."' AND cmo.model='".$value['model']."' AND cy.year='".$value['year']."'");
  }
 

  $query->allowRowCount = TRUE;
  $count = $query->rowCount();
  /*print_r($query);
  exit;*/
  if($count == 0){
    //$mmye_comb_not[]=$key;
    $mmye_comb_not[]=array(
        'partno' => $value['product_id'],
        'make' => $value['make'],
        'model' => $value['model'],
        'year' => $value['year'],
        'engine' => $value['engine'],
        'status' => 'mmye_comb'
      );
  }

   while ($row = $query->fetchAssoc()) {
    //echo "welcome";
      $engine_id[$key]=$row['engine_id'].'#'.$value['product_id'];

   }
}

if(count($engine_id) > 0){
    foreach ($engine_id as $key => $value) {
        $expolde=explode("#",$value);
        $query = $connection->query("SELECT product_id,engine_id FROM catapult_application_map WHERE engine_id ='".$expolde[0]."' AND product_id='".$expolde[1]."'");
        $query->allowRowCount = TRUE;
        $count = $query->rowCount();
        if($count == 0){
          //echo $expolde[0]."#".$expolde[1];
           /*$insert_make =$connection->query("insert into catapult_application_map(product_id,engine_id,created_by,created_on) Values('".$expolde[0]."','".$expolde[1]."','".$user_display_name."',Now())"); */

            $insert_make =$connection->query("insert into catapult_application_map(product_id,engine_id,created_by,created_on) Values('".$expolde[1]."','".$expolde[0]."','".$user_display_name."',Now())"); 

        }
        while ($row = $query->fetchAssoc()) {
          $exist_apl_map[]=$row['product_id']."#".$row['engine_id'];
        }
      
    }
}

if(count($exist_apl_map)){
    foreach ($exist_apl_map as $val) {
      
       $expolde=explode("#",$val);
        $query = $connection->query("SELECT cm.make,cm.make_id,cmo.model,cmo.model_id,ce.engine,ce.engine_id,cy.year,cy.year_id FROM catapult_engine ce 
      INNER JOIN catapult_make cm ON cm.make_id = ce.make_id INNER JOIN catapult_model cmo ON cmo.model_id = ce.model_id  
      INNER JOIN catapult_year cy ON cy.year_id = ce.year_id WHERE ce.engine_id= '".$expolde[1]."'");
        while ($row = $query->fetchAssoc()) {
          $exist_apl_map_data[]=array(
            'partno' => $expolde[0],
            'make' => $row['make'],
            'model' => $row['model'],
            'year' => $row['year'],
            'engine' => $row['engine'],
            'status' => 'already_exist'

            );
        }
      
    }
}
$merge_arr=array_merge($mmye_comb_not,$exist_apl_map_data);

if(count($merge_arr) > 0){
  $res_file = "sites/default/files/mmyeupload/Errors.xls";
  $data = $this->excel_export($merge_arr,$res_file);
  echo "Some Mapping Not Inserted.Please Check Your Error File";
}else{
  echo "Allinserted";
}

exit;
/*print_r($mmye_comb_not);
print_r($exist_apl_map_data);

*/


/*print_r($mmye_comb_not);
print_r($engine_id);

exit;*/



}


  




 public function excel_export($data,$fileName){
/* print_r($fileName);die;
*/   
    global $base_url;
     $reason='';
    $objPHPExcel = new \PHPExcel();

    $sht = $objPHPExcel->getActiveSheet();

    $i=1;
    $sht->setCellValue('A'.$i, 'Partnumber');
    $sht->setCellValue('B'.$i, 'Make');
    $sht->setCellValue('C'.$i, 'Model');
    $sht->setCellValue('D'.$i, 'Year');
    $sht->setCellValue('E'.$i, 'Engine');
    $sht->setCellValue('F'.$i, 'Reason');

  /*  print_r($sht);
    exit;*/
 

    foreach ($data as $data_key => $data_value) {
     /* echo $data_value['partno'];*/
      $i++;
      $sht->setCellValue('A'.$i, $data_value['partno']);
      $sht->setCellValue('B'.$i, $data_value['make']);
      $sht->setCellValue('C'.$i, $data_value['model']);
      $sht->setCellValue('D'.$i, $data_value['year']);
      $sht->setCellValue('E'.$i, $data_value['engine']);
      if($data_value['engine'] == 'mmye_comb'){
        $reason='There Is No Combination Make,Model,Year,Engine';
      }else if($data_value['engine'] == 'already_exist'){
        $reason='Applocation Mapping Already Exist';
      }
      $sht->setCellValue('F'.$i, $reason);
   
    }
    


    $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    ob_end_clean();
    $objWriter->save($fileName);
  }


public function excel_field_validation($make,$model,$year,$engine){


}





  
}