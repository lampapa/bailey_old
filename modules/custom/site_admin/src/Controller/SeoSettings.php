<?php
namespace Drupal\site_admin\Controller;


//for catlisting in header
//use Drupal\drupalup_controller\Controller;

class SeoSettings{

  public function page() {
    $connection = \Drupal::database();
    $success_status='';
    $error='';
    if(isset($_SESSION['postid']) ){
      if($_SESSION['postid'] == ""){
        $_SESSION['postid'] = rand(10,100);
      }
    }else{
      $_SESSION['postid'] = rand(10,100);
    }    
      
   if(!empty($_POST)){
       if( (isset($_POST['postid'])) && ($_POST['postid'] != "")  ){
            if($_SESSION['postid'] == $_POST['postid']){

                if($_POST['select_page'] != ''){
              /*     echo $_POST['select_page']."fg";
              exit;*/
                  if($_POST['meta_title'] != '' ){
                    $connection = \Drupal::database();
                     $user        = \Drupal::currentUser();
                     $user_display_name = $user->getDisplayName();  
                     $seo_cnt=[];
                     $query = $connection->query("SELECT seo_id  FROM catapult_seo where page_name='".$_POST['select_page']."'");
 
                      while ($row = $query->fetchAssoc()) {
                        
                         $seo_cnt['seo_id'] = $row['seo_id'];
                      }


                      if(count($seo_cnt) > 0){
                         $connection->query("update catapult_seo set page_name='".$_POST['select_page']."',meta_title='".$_POST['meta_title']."',meta_keyword='".$_POST['meta_keyword']."',meta_description='". $_POST['meta_description']."',g_snippet_heading='".$_POST['g_snippet_heading']."',g_snippet_description='".$_POST['g_snippet_description']."',h1_tag='".$_POST['h1_tag']."',h2_tag='".$_POST['h2_tag']."',heading='".$_POST['heading']."',sub_heading='".$_POST['sub_heading']."',description='".$_POST['description']."',modified_by='".$user_display_name."',modified_on=Now() where seo_id='".$seo_cnt['seo_id']."'");
                         $success_status="Seo Details Updated Successfully";
                      }else{
                         $connection->query("insert into catapult_seo(page_name,meta_title,meta_keyword,meta_description,g_snippet_heading,g_snippet_description,h1_tag,h2_tag,heading,sub_heading,description,created_by,created_on) Values('".$_POST['select_page']."','".$_POST['meta_title']."','".$_POST['meta_keyword']."','".$_POST['meta_description']."','".$_POST['g_snippet_heading']."','".$_POST['g_snippet_description']."','".$_POST['h1_tag']."','".$_POST['h2_tag']."','".$_POST['heading']."','".$_POST['sub_heading']."','".$_POST['description']."','".$user_display_name."',Now())");
                         unset($_POST); 
                        $success_status="Seo Details Inserted Successfully";
                      }
                  }else{
                    $error="Please Fill The Meta Title";
                  }
                     

                     
                }
                else{
                  $error="Please choose Page";
                }
             }
        }
    } 
    $form = array(); 
    return array(
        '#theme' => 'seo_settings',
        '#postid' => $_SESSION['postid'],
        '#items'=>$form,
        '#variables'=>'',
        '#title' =>$success_status,
        '#error'=>$error
    );
  }

   public function getcontactinfo() {
    
      $connection = \Drupal::database();
      $query = $connection->query("SELECT * FROM catapult_contact_info");
 
    while ($row = $query->fetchAssoc()) {
      
       $contact_results[] = $row;
    }
    $data['contact_results'] =$contact_results;
    echo json_encode($data);
    exit;
   
  }
  public function getbranchinfo() {
    
      $connection = \Drupal::database();
      $query = $connection->query("SELECT * FROM catapult_branch_info");
 
    while ($row = $query->fetchAssoc()) {
      
       $branch_results[] = $row;
    }
    $data['branch_results'] =$branch_results;
    echo json_encode($data);
    exit;
   
  }

   public function get_seo_details(){
     
      $connection = \Drupal::database();
      $seo_results=[];
      $query = $connection->query("SELECT page_name,meta_title,meta_keyword,meta_description,g_snippet_heading,g_snippet_description,h1_tag,h2_tag,heading,sub_heading,description FROM catapult_seo where page_name='".$_POST['seo_page']."'");
   
      while ($row = $query->fetchAssoc()) {
        
         $seo_results[] =array('page_name'=>$row['page_name'],
          'meta_title'=>$row['meta_title'],
          'meta_keyword'=>$row['meta_keyword'],
          'meta_description'=>$row['meta_description'],
          'g_snippet_heading'=>$row['g_snippet_heading'],
          'g_snippet_description'=>$row['g_snippet_description'],
          'h1_tag'=>$row['h1_tag'],
          'h2_tag'=>$row['h2_tag'],
          'heading'=>$row['heading'],
          'sub_heading'=>$row['sub_heading'],
          'description'=>$row['description']
          );
      }
      if(count($seo_results) > 0){
         echo json_encode($seo_results);
      }else{
         echo "norecord"; 
      }
      exit;
     
  }

  public function seodashboard(){

      $connection = \Drupal::database();
      $seo_results=[];
      $query = $connection->query("SELECT page_name,meta_title,meta_keyword,meta_description,g_snippet_heading,g_snippet_description,h1_tag,h2_tag,heading,sub_heading,description FROM catapult_seo");
   
      while ($row = $query->fetchAssoc()) {
        
         $seo_results[] =array('page_name'=>$row['page_name'],
          'meta_title'=>$row['meta_title'],
          'meta_keyword'=>$row['meta_keyword'],
          'meta_description'=>$row['meta_description'],
          'g_snippet_heading'=>$row['g_snippet_heading'],
          'g_snippet_description'=>$row['g_snippet_description'],
          'h1_tag'=>$row['h1_tag'],
          'h2_tag'=>$row['h2_tag'],
          'heading'=>$row['heading'],
          'sub_heading'=>$row['sub_heading'],
          'description'=>$row['description']
          );
      }
     /* if(count($seo_results) > 0){
         echo json_encode($seo_results);
      }else{
         echo "norecord"; 
      }

      print_r($seo_results);
      exit;
*/
      /*print_r($seo_results);
      exit;*/

    return array(
        '#theme' => 'seo_dashboard',
        '#postid' => '',
        '#items'=>$seo_results,
        '#variables'=>'',
        '#title' =>'',
        '#error'=>''
    );

  }

     
}