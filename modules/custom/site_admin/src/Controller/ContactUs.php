<?php
namespace Drupal\site_admin\Controller;
class ContactUs{
  public function page() {
    $connection = \Drupal::database();
    $success_status = "";
   if(isset($_SESSION['postid']) ){
      if($_SESSION['postid'] == ""){
        $_SESSION['postid'] = rand(10,100);
      }
    }else{
      $_SESSION['postid'] = rand(10,100);
    }   
    $success_status  = "";    
    $error           = "";    
    if(!empty($_POST['contact'])){
      if( (isset($_POST['postid'])) && ($_POST['postid'] != "") ){
        if($_SESSION['postid'] == $_POST['postid']){                      
            $company_name     = $_POST['company_name'];            
            $address1         = $_POST['address1'];            
            $address2         = $_POST['address2'];                      
            $zip_code         = $_POST['zip_code'];
            $country          = $_POST['country'];            
            $state            = $_POST['state'];                        
            $city             = $_POST['city'];           
            $email            = $_POST['email'];                        
            $phone            = $_POST['phone'];            
            if(isset($_POST['facebook'])){
              $facebook     = $_POST['facebook'];
            }else{
              $facebook     = '';
            }
            if(isset($_POST['twitter'])){
              $twitter     = $_POST['twitter'];
            }else{
              $twitter     = '';
            }
            if(isset($_POST['youtube'])){
              $youtube     = $_POST['youtube'];
            }else{
              $youtube     = '';
            }
            if(isset($_POST['linkedin'])){
              $linkedin     = $_POST['linkedin'];
            }else{
              $linkedin     = '';
            }
            if(isset($_POST['googleplus'])){
              $googleplus     = $_POST['googleplus'];
            }else{
              $googleplus     = '';
            }
            if(isset($_POST['emaildropline'])){
              $emaildropline     = $_POST['emaildropline'];
            }else{
              $emaildropline     = "";
            }
            if(isset($_POST['showmap'])){
              $showmap     = $_POST['showmap'];
            }else{
              $showmap     = 'off';
            }
            if(isset($_POST['googlemap'])){
              $googlemap     = $_POST['googlemap'];
            }else{
              $googlemap     = '';
            }            
            $contactusid     = $_POST['contactusid'];                     
            $connection->query("update catapult_contact_info set company_name='".$company_name."',address1='".$address1."',address2='".$address2."',zip_code='".$zip_code."',country='".$country."',state='".$state."',city='".$city."',email='".$email."',phone='".$phone."',facebook='".$facebook."',twitter='".$twitter."',youtube='".$youtube."',linkedin='".$linkedin."',googleplus='".$googleplus."',emaildropline='".$emaildropline."',showmap='".$showmap."',googlemap='".$googlemap."',modified_by='JP',modified_on=Now() where contact_id='".$contactusid."'");   
            $success_status = "Contact Address Updated Successfully";                    
        }
      }
      $_SESSION['postid'] = "";      
    }      
    if(!empty($_POST['branch'])){
      if( (isset($_POST['postid'])) && ($_POST['postid'] != "")  ){
        if($_SESSION['postid'] == $_POST['postid']){ 
          $branch_title     = $_POST['branch_title'];      
          $branch_name      = $_POST['branch_name'];     
          $branch_address1  = $_POST['branch_address1'];      
          $branch_address2  = $_POST['branch_address2'];          
          $branch_zipcode   = $_POST['branch_zipcode'];      
          $branch_country   = $_POST['branch_country'];      
          $branch_state     = $_POST['branch_state'];      
          $branch_city      = $_POST['branch_city'];      
          $branch_email     = $_POST['branch_email'];      
          $branch_phone     = $_POST['branch_phone'];      
          $branchid         = $_POST['hidden_id'];      
          if($branchid  ==''){            
            $connection->query("insert into catapult_branch_info(branch_heading,branch_name,branch_address1,branch_address2,branch_zip_code,branch_country,branch_state,branch_city,branch_email,branch_phone,created_by,created_on)values('".$branch_title."','".$branch_name."','".$branch_address1."','".$branch_address2."','".$branch_zipcode."','".$branch_country."','".$branch_state."','".$branch_city."','".$branch_email."','".$branch_phone."','JP',Now())");
            $success_status = "Branch Added Successfully";
          }else{
             $connection->query("update catapult_branch_info set branch_heading='".$branch_title."',branch_name='".$branch_name."',branch_address1='".$branch_address1."',branch_address2='".$branch_address2."',branch_zip_code='".$branch_zipcode."',branch_country='".$branch_country."',branch_state='".$branch_state."',branch_city='".$branch_city."',branch_email='".$branch_email."',branch_phone='".$branch_phone."',modified_by='JP',modified_on=Now() where branch_id='".$branchid."'");
             $success_status = "Branch Updated Successfully";
          }          
        }
      }  
    }

    if(isset($_POST) && (!empty($_POST)) ){      
      $_SESSION['postid'] = "";
    }
    if($_SESSION['postid'] == ""){
      $_SESSION['postid'] = rand(10,100);      
    }        
    return array(
      '#theme' => 'siteadmincontact_us',
      '#title' => $success_status,
      '#postid'=>$_SESSION['postid'],
      '#error'=>$error
    );
  }

  public function getcontactinfo() {
    $connection = \Drupal::database();
    $query = $connection->query("SELECT * FROM catapult_contact_info");
    while ($row = $query->fetchAssoc()) {      
      $contact_results[] = $row;
    }
    $data['contact_results'] =$contact_results;
    echo json_encode($data);
    exit;   
  }

  public function getbranchinfo() {    
    $connection = \Drupal::database();
    $branch_results=[];
    $query = $connection->query("SELECT * FROM catapult_branch_info");
    while ($row = $query->fetchAssoc()){      
       $branch_results[] = $row;
    }
    $data['branch_results'] =$branch_results;
    echo json_encode($data);
    exit;   
  }

  public function branchdelete(){
    $connection = \Drupal::database();
    $id         = $_POST['id'];
    $typess     = $_POST['typess'];
    if($typess == 'branch'){
      $connection->query("delete FROM catapult_branch_info where branch_id='".$id."'");
    }
    exit;
  }

  public function branchedit(){ 
    $connection   = \Drupal::database();
    $id           = $_POST['id'];
    $query        = $connection->query("SELECT * FROM catapult_branch_info where branch_id='".$id."'");
    $branch_array =[];
    while ($row = $query->fetchAssoc()){
      $branch_array[] = array(
          'branch_id'=>$row['branch_id'],
          'branch_title'=>$row['branch_heading'],
          'branch_name'=>$row['branch_name'],
          'branch_address1'=>$row['branch_address1'],
          'branch_address2'=>$row['branch_address2'],
          'branch_zipcode'=>$row['branch_zip_code'],
          'branch_country'=>$row['branch_country'],
          'branch_state'=>$row['branch_state'],
          'branch_city'=>$row['branch_city'],
          'branch_email'=>$row['branch_email'],
          'branch_phone'=>$row['branch_phone']
        );
    }
    echo json_encode($branch_array);
    exit();
  }
}