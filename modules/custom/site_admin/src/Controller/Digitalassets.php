<?php
namespace Drupal\site_admin\Controller;
use Drupal\node\Entity\NodeType;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\commerce_product\Entity\Product;
use Drupal\Core\Session\AccountProxyInterface;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\commerce\commerce_product;
use Drupal\commerce;
use Drupal\commerce_cart;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\commerce_cart\CartProviderInterface;
use Drupal\commerce_cart\CartManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_order\Entity\Order;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\CssCommand;

//for catlisting in header
//use Drupal\drupalup_controller\Controller;

class Digitalassets{

  public function page(){

    $form = \Drupal::formBuilder()->getForm('Drupal\amazing_forms\Form\WebSetForm');
    return array(
        '#theme' => 'digital_assets',
        '#items'=>$form
    );
  }
  public function digitalimage_upload(){
  	$connection = \Drupal::database();
    /*echo "<pre>";
    print_r($_FILES);
    die();*/
  	foreach ($_FILES as $key => $value) {
			$filecount= count($_FILES['file']['name']);				
			$currenttimestamp =time();				
			for($i=0;$i<$filecount;$i++){
				$filename = $_FILES['file']['name'][$i];
				$imageformat = explode('/',$_FILES['file']['type'][$i]);
				$resultimageformat = $imageformat[1];
				$currentfilename = $currenttimestamp.$i.'.'.$resultimageformat;
				move_uploaded_file($_FILES['file']['tmp_name'][$i],"public://digitalassetsimages/".$filename);
				$data = file_get_contents("public://digitalassetsimages/".$filename);
				$file = file_save_data($data, "public://digitalassetsimages/".$filename, FILE_EXISTS_REPLACE);
				chmod("public://digitalassetsimages/".$filename,0777);
				//array_push($currentfilenamearray,$currentfilename);
				$connection->query("insert into catapult_digitalassestimages(digital_file_id,digital_image_name,digital_image_type,created_on,created_by)values('".$file->id()."','".$filename."','".$resultimageformat."',NOW(),'Siteadmin')");
			}
		}		
		exit;
  }
  public function getalldigitalimages(){
  	$connection = \Drupal::database();
  	$query = $connection->query("SELECT * FROM catapult_digitalassestimages");
    $digitalassests_results=[];
    while ($row = $query->fetchAssoc()) {  
       $digitalassests_results[] = $row;
    }
    $data['digitalassests_results'] =$digitalassests_results;
    echo json_encode($data);
    exit; 
  }
  public function deletedigitalassetsimage(){
  	$connection = \Drupal::database();
  	if(isset($_POST['id'])){
  		$connection->query("delete from catapult_digitalassestimages where digital_id='".$_POST['id']."'");
  	}
  	exit;
  }
}
