<?php
namespace Drupal\site_admin\Controller;
use Drupal\node\Entity\NodeType;
use Drupal\node\Entity\Node;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Cache\CacheableMetadata;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\file\Entity\File;

class Banner{
  	public function page(){
	  	global $base_url;  
	  	$success_status = "";
	  	if(isset($_SESSION['postid']) ){
	      if($_SESSION['postid'] == ""){
	        $_SESSION['postid'] = rand(10,100);
	      }
	    }else{
	      $_SESSION['postid'] = rand(10,100);
	    }   
		$success_status = "";
	  	$upload_error = "";
	  	$error = "";
	  	$editor_validate = "";
	  	if(!empty($_POST)){
	      	if( (isset($_POST['postid'])) && ($_POST['postid'] != "")  ){
		        if($_SESSION['postid'] == $_POST['postid']){			  								
					$alt      = $_POST['alt'];
					$sequence = $_POST['sequence']; 
					if($_FILES["newf"]["tmp_name"] != ""){
						$name        = $_FILES["newf"]["name"];					
						$exts        = explode(".", $name);
						$extension   = $exts[1];
						$allowedExts = array("jpeg", "jpg", "png","PNG","JPEG",'jpg');
						if(in_array($extension, $allowedExts)){
							$target_file =  basename($_FILES["newf"]["name"]);
							move_uploaded_file($_FILES["newf"]["tmp_name"], $target_file);
							//chmod($_FILES["newf"]["name"],0777);
							$data = file_get_contents($base_url."/".$_FILES["newf"]["name"]);
							$file = file_save_data($data, "public://".$_FILES["newf"]["name"], FILE_EXISTS_REPLACE);
						}else{
							$upload_error = "File Type Should Be jpg,png";
						}
					}
			  		if($_POST['hidden_id'] != ""){
			  			$node                         = Node::load($_POST['hidden_id']);			
						$node->body->value            = 'test';
						$node->body->format           = 'full_html';
						$node->title                  = 'test';
						$node->field_banner_sequence->value = $sequence;
						if($upload_error == ""){
							if($_FILES["newf"]["tmp_name"] != ""){
								$field_banner_upload = array(
								    'target_id' => $file->id(),
								    'alt' => $alt,
								    'title' => "My title"
								);
								$node->field_banner_upload = $field_banner_upload;
							}
							$node->save();
							$success_status = "Banner Updated Successfully";	
						}	
						
			  		}else{	  								    
					    if( ($_FILES["newf"]["name"] != "") && ($upload_error == "") ){
					    	$body = [
							    'value' => 'test',
							    'format' => 'basic_html',
						    ];
							$node = Node::create([
								'type'  => 'banner',
								'title'	=> 'test',
								'body'	=> ['value'=> 'test','format'=> 'basic_html'],
								'field_banner_sequence' => $sequence,
							  	'field_banner_upload' => [
							    'target_id' => $file->id(),
							    'alt' => $alt,
							    'title' => 'Sample File'
							  ],
							]);
							$node->save();
							chmod($_FILES["newf"]["name"],0777);
							unlink($_FILES["newf"]["name"]);
							$success_status = "Banner Uploaded Successfully";
						}else{
							$error = "please upload file";
						}	
					}					
				}			
				
			}
			$_SESSION['postid'] = "";	
	  	} 
	  	if($_SESSION['postid'] == ""){
	  		$_SESSION['postid'] = rand(10,100);      
		}
		if($upload_error != ""){
			$error = $upload_error;
		}
		if($editor_validate != ""){
			$error = $editor_validate;
		}
    	return array('#theme' => 'banner',
    				 '#title' => $success_status,
    				 '#postid'=>$_SESSION['postid'],
    				 '#error'=>$error
    				);
  	}

  	public function bannerurl(){  	
		$service_array =[];
	  	$nids          = \Drupal::entityQuery('node')->condition('type','banner')->execute();
	  	foreach($nids as $key => $ids){	  			  		
			$node = \Drupal\node\Entity\Node::load($ids);				
			$res = $node->field_banner_sequence->getValue();

			$service_array[] = array(							       
						        'file'=>file_create_url($node->field_banner_upload->entity->getFileUri()),
						        'sequence'=>$res[0]['value'],
						        'id'=>$ids,
						        'altvalue'=>$node->field_banner_upload->alt
        						);			
		}
		echo json_encode($service_array);
		exit();	
  	}
  	public function deletebanner(){ 
	  	$nodeid = $_POST['id'];	  		  	
		$result = \Drupal::entityQuery('node')
	      ->condition('type', 'banner')
	      ->execute();			     
	  	$res = array($nodeid=>$nodeid);
		entity_delete_multiple('node', $res);
		die();
  	}   
	public function singlebanner(){ 
	  	$id   = $_POST['id'];
	  	$node = \Drupal\node\Entity\Node::load($id);
		$res  = $node->field_banner_sequence->getValue();		
		$service_array[] = array(						        
						        'file'=>file_create_url($node->field_banner_upload->entity->getFileUri()),
						        'sequence'=>$res[0]['value'],
						        'id'=>$id,
						        'altvalue'=>$node->field_banner_upload->alt
	    						);
		echo json_encode($service_array);
		exit();
	}
}