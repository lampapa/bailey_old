<?php
namespace Drupal\site_admin\Controller;
use Drupal\node\Entity\NodeType;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\commerce_product\Entity\Product;
use Drupal\Core\Session\AccountProxyInterface;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\commerce\commerce_product;
use Drupal\commerce;
use Drupal\commerce_cart;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\commerce_cart\CartProviderInterface;
use Drupal\commerce_cart\CartManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_order\Entity\Order;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\CssCommand;

//for catlisting in header
//use Drupal\drupalup_controller\Controller;

class WebSetting{

  public function page(){
    
    $form = \Drupal::formBuilder()->getForm('Drupal\amazing_forms\Form\WebSetForm');     
    return array(
        '#theme' => 'web_setting',
        '#items'=>$form        
    );
  }

  public function menulist(){
    $connection  = \Drupal::database();
    $user        = \Drupal::currentUser();
    $user_id     = $user->id();
    $query = $connection->query("SELECT * FROM menu_link_content_data");
    while ($row = $query->fetchAssoc()) {
      $menu_name = $row['title'];
      $enabled   = $row['enabled'];
      if($enabled == 1){
        $classess = "No";
      }else{
        $classess = "Yes";
      }
      if($enabled == 1){
        $enabled = 0;
      }else{
        $enabled = 1;
      }
      $id         = $row['id'];
      $product1   = array($menu_name,$enabled,$id,$classess);
      $products[] = $product1;        
    }
    echo json_encode($products);
    exit();   
  } 
  public function updatemenu(){
    if($_POST['id']){
      $connection  = \Drupal::database();
      $id      = $_POST['id'];
      $enabled = $_POST['value'];
      if($enabled == 1){
        $enabled = 0;
      }else{
        $enabled = 1;
      }
     // echo "update menu_link_content_data set enabled='".$enabled."' where id='".$id."' ";
      $query   = $connection->query("update menu_link_content_data set enabled='".$enabled."' where id='".$id."' "); 
      $connection->query("update menu_link_content_field_revision set enabled='".$enabled."' where id='".$id."' "); 

      drupal_flush_all_caches();
      exit();   
    }  
  } 
  public function topbar(){
    $connection  = \Drupal::database();
    $query = $connection->query("SELECT *from catapult_topbar");
    while ($row = $query->fetchAssoc()){ 
      $faq_enable = $row['faq_enable'];
      $terms_enable = $row['terms_enable'];
      $phone_enable = $row['phone_enable'];
      $shipping_enable = $row['shipping_enable'];
      $id = $row['id'];
      $classess = "Yes";
      /*$product1 = array($row['phone'],$row['shipping'],$faq_enable,$terms_enable,$phone_enable,$shipping_enable,$id);*/
      /*  $catapult_topbar[] = $row['phone'];
        $catapult_topbar[] = $row['shipping'];
        $catapult_topbar[] = $faq_enable;
        $catapult_topbar[] = $terms_enable;
        $catapult_topbar[] = $phone_enable;
        $catapult_topbar[] = $shipping_enable;
        $catapult_topbar[] = $id;*/
        $catapult_topbar[] = array('phone'=>$row['phone'],'shipping'=>$row['shipping'],'faq_enable'=>$faq_enable,'phone_enable'=>$phone_enable,'shipping_enable'=>$shipping_enable,'faq_enable'=>$faq_enable,'id'=>$id);
        /*$catapult_topbar[] = $row['shipping'];
        $catapult_topbar[] = $faq_enable;
        $catapult_topbar[] = $terms_enable;
        $catapult_topbar[] = $phone_enable;
        $catapult_topbar[] = $shipping_enable;
        $catapult_topbar[] = $id;*/

    }
    echo json_encode($catapult_topbar);
    exit();   
  }
  public function footerservice(){
    $connection  = \Drupal::database();
    $query = $connection->query("SELECT *from catapult_footer");
    while ($row = $query->fetchAssoc()){ 
      $footerbar[] = $row['newsletter'];
      $footerbar[] = $row['contact'];
      $footerbar[] = $row['sitemap'];
      $footerbar[] = $row['keep'];
      $footerbar[] = $row['copy'];
      $footerbar[] = $row['id'];      
    }
    echo json_encode($footerbar);    
    exit();   
  }
  public function themesetting(){  
    $connection  = \Drupal::database();
    $query       = $connection->query("SELECT * from catapult_theme_setting");    
    while ($row = $query->fetchAssoc()){ 
      $footerbar[] = $row['products'];
      $footerbar[] = $row['category'];
      $footerbar[] = $row['cart'];      
      $footerbar[] = $row['brand'];
      $footerbar[] = $row['ymm'];
      $footerbar[] = $row['compare'];
      $footerbar[] = $row['wish'];
      $footerbar[] = $row['part_category'];
	  $footerbar[] = $row['reviews'];
    }
    echo json_encode($footerbar);    
    exit();   
  }
  public function homeservice(){
    $connection  = \Drupal::database();
    $query       = $connection->query("SELECT *from catapult_homepage");
    while ($row = $query->fetchAssoc()){ 
      $banner           = $row['banner'];
      $category         = $row['category'];
      $featured         = $row['featured'];
      $about            = $row['about'];
      $brand            = $row['brand'];
      $testimonial      = $row['testimonial'];
      $video            = $row['video'];      
      $home[]           = $banner;
      $home[]           = $category;
      $home[]           = $featured;
      $home[]           = $about;
      $home[]           = $brand;
      $home[]           = $testimonial;
      $home[]           = $video;      
    }
    echo json_encode($home);
    exit();   
  }
  public function make(){
    echo "test";
    die();
  }
  public function updatetop(){
    $connection   = \Drupal::database();
    $type         = $_POST['type'];
    $value        = $_POST['value'];    
    if($type == "phone"){
      $query   = $connection->query("update catapult_topbar set phone_enable='".$value."' where id=1");
    }elseif($type == "shipid"){
      $query   = $connection->query("update catapult_topbar set shipping_enable='".$value."' where id=1");
    }elseif($type == "faqid"){
      $query   = $connection->query("update catapult_topbar set faq_enable='".$value."' where id=1");
    }elseif($type == "tid"){
      $query   = $connection->query("update catapult_topbar set terms_enable='".$value."' where id=1");
    }else if($type == "phone_save"){
      $query   = $connection->query("update catapult_topbar set phone='".$value."' where id=1 ");
    }else if($type == "ship_save"){     
      $query   = $connection->query("update catapult_topbar set shipping='".$value."' where id=1");
    }else if($type == "sitemap"){      
      $query   = $connection->query("update catapult_footer set sitemap='".$value."' where id=1");
    }else if($type == "newset"){      
      $query   = $connection->query("update catapult_footer set newsletter='".$value."' where id=1");
    }else if($type == "keepset"){      
      $query   = $connection->query("update catapult_footer set keep='".$value."' where id=1");
    }else if($type == "contactset"){      
      $query   = $connection->query("update catapult_footer set contact='".$value."' where id=1");
    }else if($type == "bannerset"){      
      $query   = $connection->query("update catapult_homepage set banner='".$value."' where id=1");
    }else if($type == "categoryset"){      
      $query   = $connection->query("update catapult_homepage set category='".$value."' where id=1");
    }else if($type == "featuredset"){      
      $query   = $connection->query("update catapult_homepage set featured='".$value."' where id=1");
    }else if($type == "aboutset"){      
      $query   = $connection->query("update catapult_homepage set about='".$value."' where id=1");
    }else if($type == "brandset"){      
      $query   = $connection->query("update catapult_homepage set brand='".$value."' where id=1");
    }else if($type == "testimonialset"){      
      $query   = $connection->query("update catapult_homepage set testimonial='".$value."' where id=1");
    }else if($type == "videoset"){      
      $query   = $connection->query("update catapult_homepage set video='".$value."' where id=1");
    }else if($type == "productsoverall"){        
      $query   = $connection->query("update catapult_theme_setting set products='".$value."' where id=1");
      $query   = $connection->query("update menu_link_content_data set enabled='".$value."' where id=3 ");
      $query   = $connection->query("update catapult_homepage set featured='".$value."' ");

     // if($value == 0){
        $query   = $connection->query("update catapult_theme_setting set wish='".$value."' where id=1");
        $query   = $connection->query("update catapult_theme_setting set cart='".$value."' where id=1");  
      //}      
      drupal_flush_all_caches();
    }else if($type == "cartoverall"){        
      $query   = $connection->query("update catapult_theme_setting set cart='".$value."' where id=1");
    }else if($type == "ymm"){        
      $query   = $connection->query("update catapult_theme_setting set ymm='".$value."' where id=1");
    }else if($type == "compare"){        
      $query   = $connection->query("update catapult_theme_setting set compare='".$value."' where id=1");
    }else if($type == "wish"){        
      $query   = $connection->query("update catapult_theme_setting set wish='".$value."' where id=1");
    }else if($type == "categoryset_part"){        
      $query   = $connection->query("update catapult_theme_setting set part_category='".$value."' where id=1");
    }else if($type == "brandset_part"){       
      $query   = $connection->query("update catapult_theme_setting set brand='".$value."' where id=1");
    } else if($type == "reviews") {
		$query   = $connection->query("update catapult_theme_setting set reviews='".$value."' where id=1");
	}
    //drupal_flush_all_caches();
    exit();
  }
  public function updatecopy(){
    $connection   = \Drupal::database();
    $copy         = $_POST['copy'];
    $query        = $connection->query("update catapult_footer set copy='".$copy."' where id=1");
    drupal_flush_all_caches();
    exit();
  }
}