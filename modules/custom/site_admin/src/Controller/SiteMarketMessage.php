<?php
namespace Drupal\site_admin\Controller;
use Drupal\node\Entity\NodeType;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\commerce_product\Entity\Product;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Cache\CacheableMetadata;
/*use Drupal\commerce\commerce_product;
use Drupal\commerce;
use Drupal\commerce_cart;*/
use Symfony\Component\HttpFoundation\RedirectResponse;
/*use Drupal\commerce_cart\CartProviderInterface;
use Drupal\commerce_cart\CartManagerInterface;*/
use Symfony\Component\DependencyInjection\ContainerInterface;
/*
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_order\Entity\Order;*/
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\CssCommand;
use Drupal\taxonomy\Entity\Term;

//for catlisting in header
//use Drupal\drupalup_controller\Controller;

class SiteMarketMessage{


  public function page(){
    $connection    = \Drupal::database();
    global $base_url;
    $success_status     = "";
    $upload_error       = "";    
    $error              = "";
    $empty_error        = "";       
    if(isset($_SESSION['postid']) ){
      if($_SESSION['postid'] == ""){
        $_SESSION['postid'] = rand(10,100);
      }
    }else{
      $_SESSION['postid'] = rand(10,100);
    }    
    if(!empty($_POST)){
      if( (isset($_POST['postid'])) && ($_POST['postid'] != "")  ){
        if($_SESSION['postid'] == $_POST['postid']){
          $marketmessage = $_POST['marketmessage'];
          $percentage = $_POST['percentage'];
          $product_id=[];
          $variation_value=[];
          $order_id = [];
          if( (isset($_POST['hidden_id'])) && ($_POST['hidden_id'] != "") ){                       
              $term = Term::load($_POST['hidden_id']);
              $term->name->setValue($marketmessage);
              $term->Save(); 
              $success_status = "Message Updated Successfully";

              $query1    = $connection->query("update save_market set percentage='".$percentage."' where market_id='".$_POST['hidden_id']."' ");

              if($percentage == "" || $percentage == null){
                $percentage = 0;
              } 

              /*getting list of product id if having this market  message */
              $query = $connection->query("SELECT cf.product_id,cf.price__number,
              cf.variation_id,cfc.field_color_target_id FROM commerce_product__field_market_message as cm left join commerce_product_variation_field_data as cf on cf.product_id = cm.entity_id 
                left join commerce_product_variation__field_color as cfc on cfc.entity_id=cf.variation_id
                where cm.field_market_message_target_id='".$_POST['hidden_id']."'");
              while($row = $query->fetchAssoc()) {                
                  $variation_value[$row['variation_id']] = array('entity_id'=>$row['product_id'],
                                              'price'=>$row['price__number'],
                                              'variation_id'=>$row['variation_id'],
                                              'color'=>$row['field_color_target_id']
                                            );          
              } 
             

              $order_purchase =[];


              foreach ($variation_value as $value) {
                $query = $connection->query("SELECT co.order_id,ci.purchased_entity FROM commerce_order as co left join  commerce_order_item as ci on ci.order_id = co.order_id where co.cart=1 and ci.purchased_entity='".$value['variation_id']."'");
                while($row_order = $query->fetchAssoc()) {
                      $order_id[] = $row_order['order_id'];
                      $order_purchase[] =array('order_id'=>$row_order['order_id'],
                        'purchased_entity'=>$row_order['purchased_entity']
                      );
                }                
              }

             /* echo "<pre>";
              print_r($order_purchase);
              echo "<br>";*/
              
              $basic_price = $basic_price1 = $basic_price2 =  $original_price =  0;

              foreach($order_purchase as $value_order){                               
                $purchased_entity = $value_order['purchased_entity'];                 
                foreach ($variation_value as $key => $value) {                  
                  if($key == $purchased_entity){                   
                      $query = $connection->query("SELECT * FROM taxonomy_term__field_color_price_values where entity_id='".$variation_value[$key]['color']."'");
                      while($row_order = $query->fetchAssoc()) {
                        $basic_price = $row_order['field_color_price_values_value'];
                      }                      
                      $basic_price1 = $variation_value[$key]['price']- $basic_price;
                      
                      $basic_price2 = $basic_price1-(($basic_price1/100)*$percentage);                     
                      $res = $basic_price2+$basic_price;  
                      $variation_id = $variation_value[$key]['variation_id'];
                      $query = $connection->query("update commerce_order_item set unit_price__number='".$res."' where purchased_entity='".$variation_id."'
                         and order_id = '".$value_order['order_id']."'
                        ");                                          
                  }
                }                              
              }
              if(!empty($order_id)){
                foreach(array_unique($order_id) as $value){
                  $this->order_retotal($value);
                }  
              }                           

          }else{
             $term = Term::create([
                'name' => $marketmessage, 
                'vid' => 'marketmessage'
             ])->save(); 
             
              $query = $connection->query("SELECT max(tid) as maxid from taxonomy_term_field_data");
              while($row = $query->fetchAssoc()) {
                    $maxid = $row['maxid'];          
              }
             $query1    = $connection->query("insert into save_market(market_id,percentage) values('".$maxid."','".$percentage."')"); 
            $success_status = "Message Added Successfully";   
          }        
        }
      }  
      $_SESSION['postid'] = "";
    }  
   if($_SESSION['postid'] == ""){
      $_SESSION['postid'] = rand(10,100);      
    }  
    if($upload_error != ""){
      $error = $upload_error;     
    }     
    return array(
        '#theme' => 'site_market',
        '#postid'=> $_SESSION['postid'],
        '#title' => $success_status,
        '#error' => $error
    );
  }  

  public function order_retotal($order_id){
    $connection    = \Drupal::database();
    $query1 = $connection->query("SELECT total_price__number FROM commerce_order_item WHERE order_id ='".$order_id."' ");
    $tot = 0;
    while($res_order = $query1->fetchAssoc()){
       $res_order['total_price__number'];
      //echo "<br>";
      $tot += $res_order['total_price__number'];
    }
    /*echo $tot;
    die();*/
    $query2 = $connection->query("update commerce_order set total_price__number='".$tot."' WHERE order_id ='".$order_id."' ");

    
  }
  public function fullmarketmsg(){
    $connection    = \Drupal::database();
    $vid = 'marketmessage';
    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);
    $term_data = [];
    foreach ($terms as $term) {
      $percentage = "";
      $term_obj = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($term->tid);
      /*echo "SELECT *from save_market where market_id='".$term->tid."'";
      die();*/
      $query1    = $connection->query("SELECT *from save_market where market_id='".$term->tid."'");
      while ($row2 = $query1->fetchAssoc()){
        $percentage = $row2['percentage'];
      } 
      $term_data[] = [
        'tid' => $term->tid,
        'tname' => $term->name,
        'percentage'=>$percentage      
      ];
    } 
    echo json_encode($term_data);
    die();
  }
  
   public function singlemarketmsg(){
    $percentage     = "";
    $connection     = \Drupal::database();
    $id             = $_POST['id'];   
    $term_obj       = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($id);

    $query1         = $connection->query("SELECT *from save_market where market_id='".$term_obj->get('tid')->value."'");
    while ($row2  = $query1->fetchAssoc()){
      $percentage = $row2['percentage'];
    } 
    $term_data[] = [
      'tid' => $term_obj->get('tid')->value,
      'tname' => $term_obj->get('name')->value,
      'percentage'=>$percentage      
    ];     
    echo json_encode($term_data);  
    exit();
  }  
  public function deletemarketmsg(){
    $id = $_POST['tid'];
    $order_id = [];
    $connection = \Drupal::database();
    $query  = $connection->query("SELECT count(*) as counts FROM commerce_product__field_category_name where field_category_name_target_id='".$id."'");
    $counts = 0;
    while($row = $query->fetchAssoc()){   
      $counts = $row['counts'];
    }    
    if($counts > 0){
      echo "record_exist";
    }else{
      $tid = $_POST['tid'];
      if($term = \Drupal\taxonomy\Entity\Term::load($tid)){      
        $term->delete();
        $query1 = $connection->query("delete from save_market where market_id='".$tid."'");
        $query1 = $connection->query("delete from commerce_product__field_market_message where entity_id='".$tid."'");


        /*getting list of product id if having this market  message */
              $query = $connection->query("SELECT cf.product_id,cf.price__number,
              cf.variation_id,cfc.field_color_target_id FROM commerce_product__field_market_message as cm left join commerce_product_variation_field_data as cf on cf.product_id = cm.entity_id 
                left join commerce_product_variation__field_color as cfc on cfc.entity_id=cf.variation_id
                where cm.field_market_message_target_id='".$_POST['tid']."'");
              while($row = $query->fetchAssoc()) {                
                  $variation_value[$row['variation_id']] = array('entity_id'=>$row['product_id'],
                                              'price'=>$row['price__number'],
                                              'variation_id'=>$row['variation_id'],
                                              'color'=>$row['field_color_target_id']
                                            );          
              } 
             

              $order_purchase =[];


              foreach ($variation_value as $value) {
                $query = $connection->query("SELECT co.order_id,ci.purchased_entity FROM commerce_order as co left join  commerce_order_item as ci on ci.order_id = co.order_id where co.cart=1 and ci.purchased_entity='".$value['variation_id']."'");
                while($row_order = $query->fetchAssoc()) {
                      $order_id[] = $row_order['order_id'];
                      $order_purchase[] =array('order_id'=>$row_order['order_id'],
                        'purchased_entity'=>$row_order['purchased_entity']
                      );
                }                
              }

            
              
              $basic_price = $basic_price1 = $basic_price2 =  $original_price =  0;

              foreach($order_purchase as $value_order){                               
                $purchased_entity = $value_order['purchased_entity'];                 
                foreach ($variation_value as $key => $value) {                  
                  if($key == $purchased_entity){
                      $basic_price = $variation_value[$key]['price'];                      
                      $variation_id = $variation_value[$key]['variation_id'];
                      $query = $connection->query("update commerce_order_item set unit_price__number='".$basic_price."' where purchased_entity='".$variation_id."'
                         and order_id = '".$value_order['order_id']."'
                        ");                                          
                  }
                }                              
              }
              if(!empty($order_id)){
                foreach(array_unique($order_id) as $value){
                  $this->order_retotal($value);
                }
              }                    
      }      
    }    
    die();
  }
}