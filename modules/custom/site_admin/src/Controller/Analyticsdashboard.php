<?php
namespace Drupal\site_admin\Controller;
use Drupal\node\Entity\NodeType;
use Drupal\node\Entity\Node;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Cache\CacheableMetadata;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\file\Entity\File;

class Analyticsdashboard{
  	public function page(){
	  	
    	return array('#theme' => 'analytics_dashboard');
  	}

  	public function get_index(){
    $intervl = $_POST['intervl'];
    $connection  = \Drupal::database();

  		$query = $connection->query("SELECT
							IFNULL(SUM(pageviews) ,0)AS totalpageviews,
							IFNULL(SUM(sessions),0) AS totalsession,
							IFNULL(SUM(hits),0) AS totalhits
							FROM catapult_ga_master 
							WHERE (DATE_FORMAT(datetimehour, '%Y-%m-%d')) >= (DATE(NOW() - INTERVAL ".$intervl." DAY))");
        $dashboard = $query->fetchAll();

        $query1 = $connection->query("SELECT deviceCategory, COUNT(deviceCategory) AS devicecount ,
					ROUND(((COUNT(deviceCategory)/(SELECT COUNT(*) FROM catapult_ga_master WHERE (DATE_FORMAT(datetimehour, '%Y-%m-%d')) >= (DATE(NOW() - INTERVAL ".$intervl." DAY))) )*100),1) AS percent
					  FROM catapult_ga_master
					  WHERE (DATE_FORMAT(datetimehour, '%Y-%m-%d')) >= (DATE(NOW() - INTERVAL ".$intervl." DAY))
					  GROUP BY deviceCategory");
        $devicelist = $query1->fetchAll();
        
        $query2 = $connection->query("SELECT 
						region as title, SUM(sessions) AS sessioncount, 
						-- latitude, longitude,
						ROUND(((COUNT(region)/(SELECT COUNT(*) FROM catapult_ga_master WHERE (DATE_FORMAT(datetimehour, '%Y-%m-%d')) >= (DATE(NOW() - INTERVAL ".$intervl." DAY))) )*100),2) AS percent  
						FROM catapult_ga_master 
						WHERE (DATE_FORMAT(datetimehour, '%Y-%m-%d')) >= (DATE(NOW() - INTERVAL ".$intervl." DAY))
						GROUP BY region ");
        $regionlist = $query2->fetchAll();

        $query2_map = $connection->query("SELECT 
						region as title, latitude, longitude
						FROM catapult_ga_master 
						WHERE (DATE_FORMAT(datetimehour, '%Y-%m-%d')) >= (DATE(NOW() - INTERVAL ".$intervl." DAY))
						GROUP BY region ,latitude, longitude");
        $regionlist_map = $query2_map->fetchAll();

       	$query3 = $connection->query("SELECT
						pagepath,SUM(pageviews) AS pageviews
						FROM catapult_ga_master 
						WHERE (DATE_FORMAT(datetimehour, '%Y-%m-%d')) >= (DATE(NOW() - INTERVAL ".$intervl." DAY))
						GROUP BY pagepath
						ORDER BY pageviews DESC");
        $pagelist = $query3->fetchAll();


        $data = array('dashboard'=>$dashboard,'devicelist'=>$devicelist,'regionlist'=>$regionlist,'regionlist_map'=>$regionlist_map,'pagelist'=>$pagelist);
       
        echo json_encode($data);
        die();

    
  }

  public function dashboard_datepicker_analytics(){
    $from = $_POST['from'];
    $to = $_POST['to'];
    $connection  = \Drupal::database();
    
  		$query = $connection->query("SELECT
							IFNULL(SUM(pageviews) ,0)AS totalpageviews,
							IFNULL(SUM(sessions),0) AS totalsession,
							IFNULL(SUM(hits),0) AS totalhits
							FROM catapult_ga_master 
							WHERE (DATE_FORMAT(datetimehour, '%Y-%m-%d')) BETWEEN '".$from."'  AND '".$to."'");
        $dashboard = $query->fetchAll();

        $query1 = $connection->query("SELECT deviceCategory, COUNT(deviceCategory) AS devicecount ,
					ROUND(((COUNT(deviceCategory)/(SELECT COUNT(*) FROM catapult_ga_master  WHERE (DATE_FORMAT(datetimehour, '%Y-%m-%d')) BETWEEN '".$from."'  AND '".$to."') )*100),1) AS percent
					  FROM catapult_ga_master
					  WHERE (DATE_FORMAT(datetimehour, '%Y-%m-%d')) BETWEEN '".$from."'  AND '".$to."'
					  GROUP BY deviceCategory");
        $devicelist = $query1->fetchAll();
        
        $query2 = $connection->query("SELECT 
						region as title, SUM(sessions) AS sessioncount ,
						ROUND(((COUNT(region)/(SELECT COUNT(*) FROM catapult_ga_master WHERE (DATE_FORMAT(datetimehour, '%Y-%m-%d')) BETWEEN '".$from."'  AND '".$to."') )*100),2) AS percent  
						FROM catapult_ga_master 
						WHERE (DATE_FORMAT(datetimehour, '%Y-%m-%d')) BETWEEN '".$from."'  AND '".$to."'
						GROUP BY region");
        $regionlist = $query2->fetchAll();

        $query2_map = $connection->query("SELECT 
						region as title, latitude, longitude
						FROM catapult_ga_master 
						WHERE (DATE_FORMAT(datetimehour, '%Y-%m-%d')) BETWEEN '".$from."'  AND '".$to."'
						GROUP BY region ,latitude, longitude");
        $regionlist_map = $query2_map->fetchAll();

       	$query3 = $connection->query("SELECT
						pagepath,SUM(pageviews) AS pageviews
						FROM catapult_ga_master 
						WHERE (DATE_FORMAT(datetimehour, '%Y-%m-%d'))  BETWEEN '".$from."'  AND '".$to."'
						GROUP BY pagepath
						ORDER BY pageviews DESC");
        $pagelist = $query3->fetchAll();


        $data = array('dashboard'=>$dashboard,'devicelist'=>$devicelist,'regionlist'=>$regionlist,'regionlist_map'=>$regionlist_map,'pagelist'=>$pagelist);
       
        echo json_encode($data);
        die();

    
  }

  	
}