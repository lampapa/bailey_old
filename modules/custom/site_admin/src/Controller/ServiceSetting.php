<?php
namespace Drupal\site_admin\Controller;
use Drupal\node\Entity\NodeType;
use Drupal\node\Entity\Node;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Cache\CacheableMetadata;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\file\Entity\File;

class ServiceSetting{
  public function page(){
  	global $base_url;  	
  	$success_status = "";
  	$upload_error = "";
  	$error = "";
  	$editor_validate = "";
  	if($_SESSION['postid'] == ""){
  		$_SESSION['postid'] = rand(10,100);      
	}
  	if(!empty($_POST)){
      	if( (isset($_POST['postid'])) && ($_POST['postid'] != "")  ){
	        if($_SESSION['postid'] == $_POST['postid']){
		  		$value    = $_POST['editor1'];
		  		if($value == ""){
		  			$editor_validate = "Please Enter Content";
		  		}else{
					$title    = $_POST['head'];
					$alt      = $_POST['alt'];
					$sequence = $_POST['sequence']; 
					if($_FILES["newf"]["tmp_name"] != ""){
						$name = $_FILES["newf"]["name"];					
						$exts = explode(".", $name);
						$extension = $exts[1];
						$allowedExts = array("jpeg", "jpg", "png","PNG","JPEG",'jpg');
						if(in_array($extension, $allowedExts)){
							$target_file =  basename($_FILES["newf"]["name"]);
							move_uploaded_file($_FILES["newf"]["tmp_name"], $target_file);
							//chmod($_FILES["newf"]["name"],0777);
							$data = file_get_contents($base_url."/".$_FILES["newf"]["name"]);
							$file = file_save_data($data, "public://".$_FILES["newf"]["name"], FILE_EXISTS_REPLACE);
						}else{
							$upload_error = "File Type Should Be jpg,png";
						}
					}	
			  		if($_POST['hidden_id'] != ""){
			  			$node                         = Node::load($_POST['hidden_id']);			
						$node->body->value            = $value;
						$node->body->format           = 'full_html';
						$node->title                  = $title;
						$node->field_service_sequence->value = $sequence;
						if($_FILES["newf"]["tmp_name"] != ""){
							$name = $_FILES["newf"]["name"];
							$exts = explode(".", $name);
							$extension = $exts[1];
							$allowedExts = array("jpeg", "jpg", "png","PNG","JPEG",'jpg');
							if ( in_array($extension, $allowedExts)){
								$field_service_image = array(
								    'target_id' => $file->id(),
								    'alt' => $alt,
								    'title' => "My title"
								);
								$node->field_service_image = $field_service_image;	
							}else{
								$upload_error = "File Type Should Be jpg,png";
							}
						}
						if($upload_error == ""){
							$node->save();
							$success_status = "Service Updated Successfully";	
						}
						
			  		}else{
			  			if($_FILES["newf"]["tmp_name"] == "" ){
			  				$upload_error = "Please Upload a image";
			  			}else{
			  				if($upload_error == ""){  		
							    $body = [
							    'value' => $value,
							    'format' => 'basic_html',
							    ];		    
								$node = Node::create([
									'type'  => 'services',
									'title'	=> $title,
									'body'	=> ['value'=> $value,'format'=> 'basic_html'],
									'field_service_sequence' => $sequence,
								  	'field_service_image' => [
								    'target_id' => $file->id(),
								    'alt' => $alt,
								    'title' => 'Sample File'
								  ],
								]);
								$node->save();
								//chmod($_FILES["newf"]["name"],0777);
								unlink($_FILES["newf"]["name"]);
								$success_status = "Service Added Successfully";
							}
			  			}
			  			
					}
				}	
			}
		}	
		$_SESSION['postid'] = "";	
  	} 
  	if($_SESSION['postid'] == ""){
		$_SESSION['postid'] = rand(10,100);      
	} 
	if($upload_error != ""){
		$error = $upload_error;
	}
	if($editor_validate != ""){
		$error = $editor_validate;
	}
    return array('#theme' => 'service_setting',
    			 '#title' => $success_status,
    			 '#postid'=>$_SESSION['postid'],
    			 '#error'=> $error
				);
  }

  	public function serviceurl(){  		
		$service_array =[];
	  	$nids          = \Drupal::entityQuery('node')->condition('type','services')->execute();
	  	foreach($nids as $key => $ids){
			$node = \Drupal\node\Entity\Node::load($ids);
			$res = $node->field_service_sequence->getValue();
			if(empty($res)){
				$res = "";
			}else{
				$res = $res[0]['value'];	
			}	
			$service_array[] = array(
							        'title'=>$node->getTitle(),
							        'body'=>strip_tags($node->body->value),
							        'file'=>file_create_url($node->field_service_image->entity->getFileUri()),
							        'sequence'=>$res,
							        'id'=>$ids
	        						);
		}
		echo json_encode($service_array);	
		exit();	
  	}

  	public function deletenode(){ 
	  	$nodeid = $_POST['id'];
	  	$typess = $_POST['typess'];
	  	if($typess == 'services'){
		  	$result = \Drupal::entityQuery('node')
		    ->condition('type', 'services')
		    ->execute();	
		}else if($typess == 'aboutus'){
			$result = \Drupal::entityQuery('node')
		    ->condition('type', 'about_us')
		    ->execute();	
		}else if($typess == 'products'){
			$delete_product = $_POST['id'];
		    $res = array($nodeid=>$delete_product);
		    $storage_handler = \Drupal::entityTypeManager()->getStorage("commerce_product");
		    $entities = $storage_handler->loadMultiple($res);
		    $storage_handler->delete($entities);
		}      
	  	$res = array($nodeid=>$nodeid);
		entity_delete_multiple('node', $res);
		die();
  	}
    public function singlenode(){ 
	  	$id   = $_POST['id'];
	  	$node = \Drupal\node\Entity\Node::load($id);
		$res  = $node->field_service_sequence->getValue();		
		$service_array[] = array(
						        'title'=>$node->getTitle(),
						        'body'=>strip_tags($node->body->value),
						        'file'=>file_create_url($node->field_service_image->entity->getFileUri()),
						        'sequence'=>$res[0]['value'],
						        'id'=>$id,
						        'altvalue'=>$node->field_service_image->alt
	    						);
		echo json_encode($service_array);
		exit();
	}
}