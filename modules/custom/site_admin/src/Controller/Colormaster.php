<?php
namespace Drupal\site_admin\Controller;
use Drupal\taxonomy\Entity\Term;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\commerce\commerce_product;
use Drupal\commerce_product\Entity\Product;
use Drupal\commerce_price\Price;


//for catlisting in header
//use Drupal\drupalup_controller\Controller;

class Colormaster{

  public function page(){
    ini_set('memory_limit', '-1');
    global $base_url;
    $success_status = "";
    $error = "";
    $connection = \Drupal::database();
    if(isset($_SESSION['postid']) ){
      if($_SESSION['postid'] == ""){
        $_SESSION['postid'] = rand(10,100);
      }
    }else{
      $_SESSION['postid'] = rand(10,100);
    }   
    if(!empty($_POST)){
      if( (isset($_POST['postid'])) && ($_POST['postid'] != "")  ){
        if($_SESSION['postid'] == $_POST['postid']){
          $field_colorname          = $_POST['field_colorname'];
          $colorname                = $_POST['colorname'];
          $field_color_price_values = $_POST['field_color_price_values'];
          if( (isset($_POST['hidden_id'])) && ($_POST['hidden_id'] != "") ){
            $if_exist=$this->ifcolorexist(trim($colorname),trim($field_colorname),'update');
         /* echo $if_exist;
          exit;  */
          if($if_exist == 'y'){
            $error = "Color Name and Color Code Already Exist";
            /*return $error;*/
          }else{

            
            $field_color_target_id=$_POST['hidden_id'];
            $var_pro_id=[];
            $entityid_price=[];
            $color_price=[];
            
            $query  = $connection->query("SELECT entity_id  FROM commerce_product_variation__field_color where field_color_target_id='".$field_color_target_id."'");

            $dum_arr=[];
            while($row = $query->fetchAssoc()){
              $variation_id = $row['entity_id'];
              $query1  = $connection->query("SELECT fd.product_id,fd.variation_id,ftp.field_total_price_number  FROM commerce_product_variation_field_data fd left join commerce_product__field_total_price ftp on  ftp.entity_id = fd.product_id  where fd.variation_id='".$variation_id."'");

               while($row1 = $query1->fetchAssoc()){
                $total_price=$row1['field_total_price_number'] + $field_color_price_values;

                $upd=$connection->query("update commerce_product_variation_field_data set price__number='".$total_price."' where variation_id='".$row1['variation_id']."'");
               }           
            }
            $order_array = [];
            
            $cart_items  = $connection->query("SELECT `order_id`,`unit_price__number`,`quantity`,`purchased_entity` FROM `commerce_order_item` WHERE order_id IN(SELECT order_id FROM  `commerce_order` WHERE state='draft' AND cart=1)");

             while($row = $cart_items->fetchAssoc()){
  
                $qry=$connection->query("SELECT pv.price__number  FROM `commerce_product_variation_field_data` as pv join commerce_product_variation__field_color as cf on cf.entity_id = pv.variation_id
                  WHERE cf.field_color_target_id='".$field_color_target_id."' and pv.variation_id ='".$row['purchased_entity']."'")->fetchAssoc();
               

                  if( ($qry['price__number'] != "") && ($qry['price__number'] != null) && ($qry['price__number'] != $row['unit_price__number']) ){
                      
                       $upd1=$connection->query("update commerce_order_item  set  unit_price__number = '".$qry['price__number']."', total_price__number = '".($row['quantity']*$qry['price__number'])."' where order_id = '".$row['order_id']."' and purchased_entity = '".$row['purchased_entity']."'");
                      $order_array[] = $row['order_id'];

                  }

             }
             /*exit;*/
             foreach(array_unique($order_array) as $value){
                 $upd2=$connection->query("update commerce_order  set   total_price__number = ( select sum(total_price__number) from commerce_order_item where order_id = '".$value."') where order_id = '".$value."'");
             }

            
           
            
            drupal_flush_all_caches();
            $term = Term::load($_POST['hidden_id']);
            $term->field_colorname->setValue($field_colorname);
            $term->field_color_price_values->setValue($field_color_price_values);
            $term->name->setValue($colorname);
            $term->Save();
            $success_status = "Color Updated Successfully";
          }

          }else{

            $if_exist=$this->ifcolorexist(trim($colorname),trim($field_colorname),'insert');
            if($if_exist == 'y'){
              $error = "Color Name and Color Code Already Exist";
            }else{

               $term = Term::create([
                  'name' => $colorname,
                  'field_colorname' => $field_colorname,
                  'field_color_price_values' => $field_color_price_values,
                  'vid' => 'color_parent'
                ]);
               $term->save();
               $tid = $term->id();
               $query  = $connection->query("SELECT product_id  FROM commerce_product");
                while($row = $query->fetchAssoc()){
                    $product2 =Product::load($row['product_id']);
                      $var_id=isset($product2->get('variations')->getValue()[0])?$product2->get('variations')->getValue()[0]['target_id']: "";
                      $field_total_price=isset($product2->get('field_total_price')->getValue()[0])?$product2->get('field_total_price')->getValue()[0]['number']:"";
                      $get_sku  = $connection->query("SELECT sku  FROM commerce_product_variation_field_data where variation_id='".$var_id."'")->fetchAssoc();
                      $sku =$get_sku['sku'];
                      $prices =  $field_total_price+$field_color_price_values;
                      $variation   = ProductVariation::create([
                                                'type' => 'default',
                                                'sku' => $sku,
                                                'price' => new Price((string)$prices, 'USD'),
                                                'field_color'=>$tid
                                              ]);
                      $variation->save();
                      $product2->addVariation($variation);

                     // var_dump($product2);
                      $product2->save();

                } 
            
              $success_status = "Color Added Successfully";
              //echo $success_status;
            }
          }
        }
      }
      $_SESSION['postid'] = "";
    }
    if($_SESSION['postid'] == ""){
      $_SESSION['postid'] = rand(10,100);
    }


    /*echo $error;
    exit;*/
    return array(
        '#theme' => 'color_master',
        '#postid'=>$_SESSION['postid'],
       '#title' => $success_status,
       '#error' => $error
    );
  }
  public function ifcolorexist($clrname,$clrcode,$upd_ins){
    $vid = 'color_parent';
    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);
    foreach ($terms as $term) {
      $term_obj = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($term->tid);
      $url = "";
      if(isset($term_obj->get('field_colors')->entity)){
        $url = file_create_url($term_obj->get('field_colors')->entity->getFileUri());
      }
      $term_data[] = [
        'tid' => $term->tid,
        'tname' => $term->name,
        'field_colorname' => $term_obj->get('field_colorname')->value,
        'image_url' => $url,
        'altvalue'=>"sd"
      ];
    }
     //print_r($term_data);
    // exit;
    if($upd_ins == 'insert'){
        if(array_search($clrname, array_column($term_data, 'tname')) !== false && array_search($clrcode, array_column($term_data, 'field_colorname')) !== false) {
         return "y";
        } else {
         return "n";
        }
    }else{
      $term_data = $this->removeElementWithValue($term_data, "tid",$_POST['hidden_id']);
      if(array_search($clrname, array_column($term_data, 'tname')) !== false && array_search($clrcode, array_column($term_data, 'field_colorname')) !== false) {
        return "y";
      }else{
         return "n";
      }

    }

  //exit;


}

public function removeElementWithValue($array, $key, $value){
     foreach($array as $subKey => $subArray){
          if($subArray[$key] == $value){
               unset($array[$subKey]);
          }
     }
     return $array;
}





  public function fullcolormaster(){

    $vid = 'color_parent';
    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);
    foreach ($terms as $term){
      $term_obj = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($term->tid);
      $url = "";
      $field_color_price_values = "";
      if(isset($term_obj->get('field_colors')->entity)){
        $url = file_create_url($term_obj->get('field_colors')->entity->getFileUri());
      }
      if(isset($term_obj->get('field_color_price_values')->value)){
        $field_color_price_values = $term_obj->get('field_color_price_values')->value;
      }
      $term_data[] = [
        'tid' => $term->tid,
        'tname' => $term->name,
        'field_colorname' => $term_obj->get('field_colorname')->value,
        'field_color_price_values' => $field_color_price_values,
        'image_url' => $url,
        'altvalue'=>"sd"
      ];
    }
    echo json_encode($term_data);
    die();
  }
  public function deletecolor(){
    $connection = \Drupal::database();
    $query  = $connection->query("SELECT count(product_id) as counts FROM commerce_product where delete_flag=0");
    $counts = 0;
    while($row = $query->fetchAssoc()){
      $counts = $row['counts'];
    }
    if($counts > 0){
      echo "record_exist";
    }else{
      $tid = $_POST['id'];
      if($term = \Drupal\taxonomy\Entity\Term::load($tid)){
        $term->delete();
      }
    }
    die();
  }

  public function singlecolor(){
    $id       = $_POST['id'];
    $term_obj = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($id);
    if(isset($term_obj->get('field_colors')->entity)){
      $url = file_create_url($term_obj->get('field_colors')->entity->getFileUri());
    }
    if($url == null){
      $url = "";
    }

    if( ($term_obj->get('field_colorname')->value) == null){
      $colorname = "";
    }else{
       $colorname = $term_obj->get('field_colorname')->value;
    }
    $field_color_price_values = "";
    if(isset($term_obj->get('field_color_price_values')->value)){
      $field_color_price_values = $term_obj->get('field_color_price_values')->value;
    }
     /*echo $colorname;
    }
    die();*/
    $term_data[] = [
      'tid' => $term_obj->get('tid')->value,
      'tname' => $term_obj->get('name')->value,
      'field_colorname' => $colorname,
      'image_url' => $url,
      'field_color_price_values'=> $field_color_price_values,
      'altvalue'=>"sd"
    ];
    echo json_encode($term_data);
    exit();
  }



  /*public function remove_unwanted_var(){


    $connection = \Drupal::database();
    $query  = $connection->query("SELECT DISTINCT variation_id, product_id,COUNT(product_id) FROM commerce_product_variation_field_data  GROUP BY product_id HAVING COUNT(product_id) > 33");
    $entity_id_values = [];
    while($row = $query->fetchAssoc()){
      


        $query1  = $connection->query("SELECT DISTINCT `field_color_target_id`,entity_id  FROM commerce_product_variation__field_color WHERE entity_id IN ( SELECT variation_id FROM `commerce_product_variation_field_data` WHERE product_id ='".$row['product_id']."') GROUP BY field_color_target_id");
      while($row1 = $query1->fetchAssoc()){

        $entity_id_values[] = $row1['product_id'];

      } 

       //$connection->query("DELETE FROM commerce_product_variation_field_data WHERE product_id ='".$row['product_id']."' AND variation_id NOT IN(entity_id_values )");


       //$connection->query("DELETE FROM commerce_product_variation__field_color WHERE entity_id NOT IN ( entity_id_values )");


    }








    






  }*/

}
