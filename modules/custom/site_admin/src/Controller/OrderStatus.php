<?php
namespace Drupal\site_admin\Controller;
include_once "modules/phpmailer/PHPMailer.php";
include_once "modules/phpmailer/SMTP.php";

class OrderStatus{


  public function page() {
      return array(
        '#theme' => 'order_status',
        '#variables' => array()   
      );
  }


  public function getorderstatus() {

    $connection = \Drupal::database();
    $entity_manager           = \Drupal::entityManager();
      $query = $connection->query("SELECT c.order_id,IFNULL(c.product_status,'placed') AS  product_status,
              (SELECT NAME FROM users_field_data WHERE uid=c.uid) AS customername,
              i.title,ROUND(i.quantity) AS quantity ,
              DATE_FORMAT(FROM_UNIXTIME(i.created), '%Y-%m-%d %H:%i') AS purchased_on,i.purchased_entity
              FROM commerce_order c JOIN commerce_order_item i
              ON c.order_id = i.order_id
              WHERE c.cart=0 AND c.state='completed' order by purchased_on desc
              ");
      //$query = $connection->query("SELECT order_id,order_number,uid FROM `commerce_order` WHERE completed IS NOT NULL");
      while ($row = $query->fetchAssoc()) {
        $product_variation = $entity_manager->getStorage('commerce_product_variation')->load((int)$row['purchased_entity']);

        $data_color = $product_variation->get('field_color')->getValue()[0]['target_id'];   
        $vid   = 'color_parent';
        $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid); 

        foreach ($terms as $term){
          $term_obj = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($term->tid);
            if($data_color == $term->tid){              
              $field_colorname[$row['order_id']] = $term_obj->get('field_colorname')->value;
              $color_name[$row['order_id']] = $term_obj->get('name')->value;               
            }
        }
      
      $order_array_temp[$row['order_id']][] = array(
          'order_id'=>$row['order_id'],
          'customername'=>$row['customername'],
          'purchased_on'=>$row['purchased_on'],
          'title'=>$row['title'],
          'quantity'=>$row['quantity'],
          'product_status'=>$row['product_status'],
          'color_name' => $color_name[$row['order_id']],
          'color_code' => $field_colorname[$row['order_id']]
        );
      }     

    if(!empty($order_array_temp)){
      foreach ($order_array_temp as $key => $value) {
        foreach ($value as $kkey => $vvalue) {
          $order_array[$key]['order_id']=$vvalue['order_id'];
          $order_array[$key]['customername']=$vvalue['customername'];
          $order_array[$key]['purchased_on']=$vvalue['purchased_on'];
          $order_array[$key]['product_status']=$vvalue['product_status'];

          $title[$key][]=$vvalue['title'];
          $quantity[$key][]=$vvalue['quantity'];
          $clr_name[$key][]=$vvalue['color_name'];
          $clr_code[$key][]=$vvalue['color_code'];
          
          

        }
          $order_array[$key]['color_name']=(object)$clr_name[$key];
          $order_array[$key]['color_code']=(object)$clr_code[$key];
          $order_array[$key]['title']=(object)$title[$key];
          $order_array[$key]['quantity']=(object)$quantity[$key];
      }
    }
    
    echo json_encode($order_array,true);
    exit();
  }

  public function changestatus(){
    $connection = \Drupal::database();
    $qry= $connection->query("update commerce_order set  product_status='".$_POST['status']."' where order_id='".$_POST['id']."'"); 

    $qry1= $connection->query("SELECT A.order_id,DATE_FORMAT(from_unixtime(A.completed),'%d-%m-%Y') as order_date, C.bill_firstname, C.bill_lastname,C.bill_email FROM `commerce_order` A,commerce_order_item B,users_shipping_profile C,commerce_product_variation_field_data D WHERE A.order_id=B.order_id and A.order_id=C.order_id and B.purchased_entity=D.variation_id and A.order_id='".$_POST['id']."'");
    $order_details=$qry1->fetchAssoc();
    if($qry){
      $body=$this->mail_msg($order_details,$_POST['status']);
      $to=$order_details['bill_email'];
      $to_name=$order_details['bill_firstname']." ".$order_details['bill_lastname'];
      $subject="XCLUTCH SHIPPING STATUS";
      $mail_sent=$this->SendMail($to,$to_name,$subject,$body,$attachment='',$cc='',$bcc='');
      if($mail_sent=='Sent'){
        echo "Y";
      }else{
         echo "O";
      }
    }else{
      echo "N";
    }
    
    //echo $qry;
    exit;
  }


  function mail_msg($order_details,$status){
    global $base_url;
    $html='';
    $html.='<div style="margin-left:auto;width:1000px;margin-right:auto">
           <table style=" font-family: Verdana; font-size: 14px; background-color: white; margin-bottom: 25px; width: 100%;">
           <tr>
           <td>
           <table width="100%" style="font-size:14px;border:solid 1px #ccc">
           <tbody>
           <tr>
           <td style="height:10px"></td>
           <td></td>
           </tr>
           <tr width="100%" style="width:100%">
           <td align="right" style="font-weight:bold!important;text-decoration:underline;font-size:18px;width:58%">
           <span style="vertical-align:sub">Order Status </span>
           </td>
           <td style="width:42%;text-align:right">
           <span style="text-align:right;margin-right:10px">
            <img style="padding:5px;max-width:100%;width:200px;" src="http://61.16.143.100/xclutch_cp/sites/default/files/webshop_mail.png" width="100">
           </span>
           </td>
           </tr>
           <tr>
           <td style="height:10px"></td>
           <td></td>
           </tr>
           </tbody>
           </table>
           </td>
           </tr>
           <tr>
           <td>
             <table width="100%" cellspacing="0" style="font-size:14px;border:solid 1px #ccc">
              <tbody>
                <tr>
                  <td width="50%" style="border-right:1px solid #ccc;padding-left:5px">
                    <table>
                      <tbody>
                        <tr>
                          <td style="white-space:nowrap"><b>Purchase Order :</b>  '.$order_details['order_id'].'</td>
                        </tr>
                     </tbody>
                    </table>
                  </td>
                  <td width="50%" style="padding-left:5px">
                    <table>
                      <tbody>
                        <tr>
                          <td><b>Order Date :</b> '.$order_details['order_date'].'</td>
                        </tr>
                      </tbody
                    </table>
                  </td>
                </tr>
              </tbody>
             </table>
            <td>
          </tr>
          <tr>
            <td>
              <table width="100%" cellspacing="0" style="font-size:14px;border:solid 1px #ccc">
                <tbody>
                  <tr>
                    <td width="50%" style="border-right:1px solid #ccc;padding-left:5px">
                    <table>
                      <tbody>
                        <tr>
                          <td><b>Order Status :</b> '.$status.'</td>
                        </tr>
                      </tbody>
                     </table>
                     </td>
                  </tr>
                </tbody>
              </table>
            </td>           
          </tr>
           </tbody>
          </table>
          </div>';
              return $html;

  }

  public function SendMail($to,$to_name='',$subject='',$body,$attachment='',$cc='',$bcc=''){

    $db = \Drupal::database();
    $result=$db->query("SELECT * from tbl_smtp_setting where status='Y'");
    while ($row = $result->fetchAssoc()) {
      $smtp_settings[]=$row;
     }
     if(!isset($smtp_settings)){
        return 'Error';
     }else{

      $mail = new \PHPMailer();
      $mail->isSMTP();
      $mail->SMTPDebug = 0;
      $mail->Host = $smtp_settings[0]['hostid'];
      $mail->Port = $smtp_settings[0]['portno'];
      $mail->SMTPSecure = $smtp_settings[0]['protocol'];
      $mail->SMTPAuth = true;
      $mail->Username = $smtp_settings[0]['emailid'];
      $mail->Password = $smtp_settings[0]['password'];
      $mail->setFrom($smtp_settings[0]['emailid'], $smtp_settings[0]['username']);
      //$mail->addReplyTo('reply-box@hostinger-tutorials.com', 'Your Name');
      $mail->addAddress($to, $to_name);
      $mail->addBCC($smtp_settings[0]['emailid']);
     // $mail->addBCC("autotech@apaengineering.com");
      $mail->Subject = $subject;
      $mail->msgHTML($body);
      // $mail->AltBody = 'This is a plain text message body';
      /*if($attachment){
        $mail->addAttachment($attachment);
      }

      if(!empty($cc)){
        if(is_array($cc)){
          foreach ($cc as $key => $value) {
            $mail->addCC($value);
          }
        }else{
          $mail->addCC($cc);
        }
      }

      if(!empty($bcc)){
        if(is_array($bcc)){
          foreach ($bcc as $key => $value) {
            $mail->addBCC($value);
          }
        }else{
          $mail->addBCC($bcc);
        }
      }*/
      // $mail->addAttachment('test.txt');
      if (!$mail->send()) {
        //die;        
        return 'Error';
      } else {
        return 'Sent';
      }
    }
    die();
  }














} 