<?php
namespace Drupal\site_admin\Controller;
use Drupal\node\Entity\NodeType;
use Drupal\node\Entity\Node;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Cache\CacheableMetadata;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\file\Entity\File;

class VideoSetting{
  	public function page(){
	  	global $base_url;
	  	$success_status = "";
	  	if(isset($_SESSION['postid']) ){
	      if($_SESSION['postid'] == ""){
	        $_SESSION['postid'] = rand(10,100);
	      }
	    }else{
	      $_SESSION['postid'] = rand(10,100);
	    }      	
	  	$upload_error = "";
	  	$upload_video_error = "";
	  	$error           = "";
	  	$editor_validate = "";
	  	if(!empty($_POST)){
	      	if( (isset($_POST['postid'])) && ($_POST['postid'] != "")  ){
		        if($_SESSION['postid'] == $_POST['postid']){		              	
					$title    = $_POST['heading'];			
					 $iframe   = $_POST['editor3'];
					
					$sequence = $_POST['video_sequence'];
			  		if($_POST['hidden_id'] != ""){
			  			$node                         		= Node::load($_POST['hidden_id']);
						$node->title                  		= $title;
						$node->field_video_sequence->value  = $sequence;
						$node->body->value            		= $iframe;
						$node->body->format           		= 'full_html';
						$node->save();
						$success_status = "Video Updated Successfully";
						/*echo "correct";
						die();	*/				
			  		}else{			  			
		  				$node = Node::create([
							'type'  => 'videos',
							'title'	=> $title,							
							'field_video_sequence' => $sequence,
							'body'	=> ['value'=> $iframe,'format'=> 'basic_html']
						]);
						$node->save();						
						$success_status = "Video Added Successfully";	
					}			  	 		
		      	}	      			     
			} 
			$_SESSION['postid'] = "";
		}	
		if($_SESSION['postid'] == ""){
	  		$_SESSION['postid'] = rand(10,100);      
		}
		if($upload_error != ""){
			$error = $upload_error;			
		} 	  	
		if($upload_video_error != ""){
			$error = $upload_video_error;			
		} 
	   	return array('#theme' => 'video_setting',
    				 '#title' => $success_status,
    				 '#postid'=>$_SESSION['postid'],
    				 '#error'=>$error
    				);
  	}
  	public function getvideos(){
		$connection  = \Drupal::database();
      	$user        = \Drupal::currentUser();
      	$user_id     = $user->id();
     	$query       = $connection->query("SELECT * FROM catapult_video_category");
     	$products = [];
        while($row = $query->fetchAssoc()){        		
        	$product1 = array("id"=>$row['id'],"videocategory"=>$row['video_category']);
        	$products[] = $product1;
        }
        echo json_encode($products);
        exit();       
  	}
  	public function getfullvideos(){ 	
		$service_array =[];
	  	$nids          = \Drupal::entityQuery('node')->condition('type','videos')->execute();
	  	/*echo "<pre>";
	  	print_r($nids);*/
	  	foreach($nids as $key => $ids){	  		
  			$node = \Drupal\node\Entity\Node::load($ids);				
			$res = $node->field_video_sequence->getValue();	
			if(empty($res)){
				$res = "";
			}else{
				$res = $res[0]['value'];
			}		
			/*echo $node->body->value;
			die();*/
			$service_array[] = array(
						        'title'=>$node->getTitle(),
						        'body'=>$node->body->value,					        
						        'sequence'=>$res,
						        'id'=>$ids
        						);
					
		}
		echo json_encode($service_array);	
		exit();	
  	}
  	public function deletevidoes(){ 
	  	$nodeid = $_POST['id'];	  		  	
		$result = \Drupal::entityQuery('node')
	      ->condition('type', 'videos')
	      ->execute();			     
	  	$res = array($nodeid=>$nodeid);
		entity_delete_multiple('node', $res);
		die();
  	}
    public function singlevideo(){ 
	  	$id   = $_POST['id'];
	  	$node = \Drupal\node\Entity\Node::load($id);
		$res  = $node->field_video_sequence->getValue();		
		$service_array[] = array(
						        'title'=>$node->getTitle(),
						        'body'=>strip_tags($node->body->value),
						        'sequence'=>$res[0]['value'],
						        'id'=>$id						        
	    						);
		echo json_encode($service_array);
		exit();
	}
	public function addtopics(){
		$topics      = $_POST['topics'];
		$connection  = \Drupal::database(); 		    
     	$query       = $connection->query("insert into catapult_video_category(video_category) values('".$topics."')");        
        exit();       
  	}
  	public function deletetopics(){
		$id          = $_POST['ids'];
		$connection  = \Drupal::database();
     	$query       = $connection->query("delete from catapult_video_category where id='".$id."'");
        exit();
  	}
}