<?php
namespace Drupal\site_admin\Controller;
use Drupal\node\Entity\NodeType;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\commerce_product\Entity\Product;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Cache\CacheableMetadata;
/*use Drupal\commerce\commerce_product;
use Drupal\commerce;
use Drupal\commerce_cart;*/
use Symfony\Component\HttpFoundation\RedirectResponse;
/*use Drupal\commerce_cart\CartProviderInterface;
use Drupal\commerce_cart\CartManagerInterface;*/
use Symfony\Component\DependencyInjection\ContainerInterface;
/*
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_order\Entity\Order;*/
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\CssCommand;
use Drupal\taxonomy\Entity\Term;

//for catlisting in header
//use Drupal\drupalup_controller\Controller;

class SiteCategory{


  public function page(){
    global $base_url;
    $success_status     = "";
    $upload_error       = "";    
    $error              = "";
    $empty_error        = "";       
    if(isset($_SESSION['postid']) ){
      if($_SESSION['postid'] == ""){
        $_SESSION['postid'] = rand(10,100);
      }
    }else{
      $_SESSION['postid'] = rand(10,100);
    }    
    if(!empty($_POST)){
      if( (isset($_POST['postid'])) && ($_POST['postid'] != "")  ){
        if($_SESSION['postid'] == $_POST['postid']){
          $alt           = $_POST['altext'];
          $categorynames = $_POST['categorynames'];          
          if($_FILES["field_category_image"]["name"] != ""){
            $name = $_FILES["field_category_image"]["name"];          
            $exts = explode(".", $name);
            $extension   = $exts[1];
            $allowedExts = array("jpeg", "jpg", "png","PNG","JPEG",'jpg');
            if(in_array($extension, $allowedExts)){
              $target_file =  basename($_FILES["field_category_image"]["name"]);
              move_uploaded_file($_FILES["field_category_image"]["tmp_name"], $target_file);
              chmod($_FILES["field_category_image"]["name"],0777);
              $data = file_get_contents($base_url."/".$_FILES["field_category_image"]["name"]);
              $file = file_save_data($data, "public://".$_FILES["field_category_image"]["name"], FILE_EXISTS_REPLACE);
            }else{
              $upload_error = "File Type Should Be jpg,png";
            }
          }
          if( (isset($_POST['hidden_id'])) && ($_POST['hidden_id'] != "") ){ 
           $term = Term::load($_POST['hidden_id']);                      
            if( ($_FILES["field_category_image"]["name"] != "") && ($upload_error == "") ){ 
                $field_category_image = array(
                    'target_id' => $file->id(),
                    'alt' => $alt,
                    'title' => "My title"
                );
               $term->field_category_image = $field_category_image;              
            }
            if($upload_error == ""){
              $term = Term::load($_POST['hidden_id']);
              $term->name->setValue($categorynames);
              $term->Save(); 
              $success_status = "Category Updated Successfully";
            }  
          }else{
             if( ($_FILES["field_category_image"]["name"] != "") && ($upload_error == "") ){      
              $term = Term::create([
                'name' => $categorynames, 
                'vid' => 'category',
                'field_category_image' => array(
                                  'target_id' => $file->id(),
                                  'alt' => $alt,
                                  'title' => "My title",
                                )
              ])->save();
              chmod($_FILES["field_category_image"]["name"],0777);
              unlink($_FILES["field_category_image"]["name"]);
              $success_status = "Category Added Successfully"; 
            }else{
              $error = "Please Upload Valid File";
            }       
          }        
        }
      }  
      $_SESSION['postid'] = "";
    }  
   if($_SESSION['postid'] == ""){
      $_SESSION['postid'] = rand(10,100);      
    }  
    if($upload_error != ""){
      $error = $upload_error;     
    }     
    return array(
        '#theme' => 'site_categories',
        '#postid'=> $_SESSION['postid'],
        '#title' => $success_status,
        '#error' => $error
    );
  }  
  public function fullcatgoryservice(){
    $vid = 'category';
    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);
    foreach ($terms as $term) {
      $term_obj = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($term->tid);
      $url = "";
      if(isset($term_obj->get('field_category_image')->entity)){
        $url = file_create_url($term_obj->get('field_category_image')->entity->getFileUri());
      }
      $term_data[] = [
        'tid' => $term->tid,
        'tname' => $term->name,
        /*'allias' => $term_obj->get('field_urlaliaspaths')->value,*/
        'image_url' => $url,
        'altvalue'=>$term_obj->field_category_image->alt
      ];
    } 
    echo json_encode($term_data);
    die();
  }
  
   public function singlecategory(){ 
    $id       = $_POST['id'];   
    $term_obj = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($id);
    if(isset($term_obj->get('field_category_image')->entity)){
      $url = file_create_url($term_obj->get('field_category_image')->entity->getFileUri());
    }
    $term_data[] = [
      'tid' => $term_obj->get('tid')->value,
      'tname' => $term_obj->get('name')->value,
      /*'allias' => $term_obj->get('field_urlaliaspaths')->value,*/
      'image_url' => $url,
      'altvalue'=>$term_obj->field_category_image->alt
    ];     
    echo json_encode($term_data);  
    exit();
  }  
  public function deletecategory(){
    $connection = \Drupal::database();
    $tid = $_POST['tid'];
    $query  = $connection->query("SELECT count(entity_id) as counts FROM commerce_product__field_category_name where field_category_name_target_id='".$tid."'");
    $counts = 0;
    while($row = $query->fetchAssoc()){   
      $counts = $row['counts'];
    }    
    if($counts > 0){
      echo "record_exist";
    }else{
      $tid = $_POST['tid'];
      if($term = \Drupal\taxonomy\Entity\Term::load($tid)){      
        $term->delete();
      }
      echo "";
    }    
    die();
  }
}