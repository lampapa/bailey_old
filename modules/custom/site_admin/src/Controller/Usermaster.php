<?php
namespace Drupal\site_admin\Controller;
use Drupal\node\Entity\NodeType;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\commerce_product\Entity\Product;
use Drupal\Core\Session\AccountProxyInterface;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\commerce\commerce_product;
use Drupal\commerce;
use Drupal\commerce_cart;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\commerce_cart\CartProviderInterface;
use Drupal\commerce_cart\CartManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_order\Entity\Order;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\CssCommand;
use  \Drupal\user\Entity\User;

//for catlisting in header
//use Drupal\drupalup_controller\Controller;

class Usermaster{

  public function page(){
		$ids = \Drupal::entityQuery('user')->execute();
		$users = User::loadMultiple($ids);

		foreach ($users as $key => $value) {
			if(!empty($value->get('name')->value)){
				$user_details[$key]['name']=$value->get('name')->value;
				$user_details[$key]['pass']=$value->get('pass')->value;
				$user_details[$key]['mail']=$value->get('mail')->value;
				$user_details[$key]['created']=!empty($value->get('created')->value)?date('d-m-Y',$value->get('created')->value):"";
				$user_details[$key]['login']=!empty($value->get('login')->value)?date('d-m-Y',$value->get('login')->value):"";
			}
			// $user_detaisl[$key]['roles']=$value->get('roles')->value;
			// $user_detaisl[$key]['user_picture']=$value->get('user_picture')->value;
		}	

    $form = \Drupal::formBuilder()->getForm('Drupal\amazing_forms\Form\WebSetForm');
    return array(
    	'#user_details'=>$user_details,
        '#theme' => 'user_master',
        '#items'=>$form
    );
  }
}
