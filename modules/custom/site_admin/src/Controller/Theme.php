<?php
namespace Drupal\site_admin\Controller;
use Drupal\node\Entity\NodeType;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\commerce_product\Entity\Product;
use Drupal\Core\Session\AccountProxyInterface;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\commerce\commerce_product;
use Drupal\commerce;
use Drupal\commerce_cart;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\commerce_cart\CartProviderInterface;
use Drupal\commerce_cart\CartManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_order\Entity\Order;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\CssCommand;

//for catlisting in header
//use Drupal\drupalup_controller\Controller;

class Theme{

  public function page(){

    $form = \Drupal::formBuilder()->getForm('Drupal\amazing_forms\Form\WebSetForm');
    return array(
        '#theme' => 'theme',
        '#items'=>$form
    );
  }
}
