<?php
namespace Drupal\site_admin\Controller;
use Drupal\node\Entity\NodeType;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\commerce_product\Entity\Product;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\commerce\commerce_product;
use Drupal\commerce;
use Drupal\commerce_cart;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\commerce_cart\CartProviderInterface;
use Drupal\commerce_cart\CartManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_order\Entity\Order;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\CssCommand;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\commerce_price\Price;
use Drupal\taxonomy\Entity\Term;
use Drupal\Core\Database\Database;
 //require_once 'sites/libraries/spreadsheet-reader-master/SpreadsheetReader.php';
 require_once 'sites/libraries/spreadsheet-reader-master/php-excel-reader/excel_reader2.php';
require_once "sites/libraries/PHPExcel/Classes/PHPExcel.php";
require_once "sites/libraries/vendor/autoload.php";
$GLOBALS['partdetail'] = \Elasticsearch\ClientBuilder::create()->build();

class PartMaster{

  public function array_has_dupes($array) {
    $tags = implode(',',$array);
    $tag1 = implode(',',array_unique($array));
    if(sizeof($array) == 1){
      return ;
    }else{
      if($tags == $tag1 ) {
        return 0;
      }else{
        return 1;
      }
    }
  }

////original function //
  public function page1(){
    // echo "<pre>";
    //     print_r($_POST);
    //     exit;
    global $base_url;
    $upload_error       = "";
    $upload_video_error = "";
    $error              = "";
    $editor_validate    = "";
    $slider_error       = "";
    $slider_empty_error = "";
    $variation_error    = "";
    $color_array        = [];
    $sku_array          = [];
    $price_array        = [];
    $editor1            = '';
    $field_call_price   = '';
    $vid = 'brand';
    $total_price_inital = "";
    $var_price_inital = "";
    $final_price      = "";
    $last_price       = "";
    $var_id = "";
    $elastic = $GLOBALS['partdetail'];
    $connection = \Drupal::database();
    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);
    foreach ($terms as $term) {
      $term_obj = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($term->tid);
      $url = "";
      if(isset($term_obj->get('field_brand_image')->entity)){
        $url = file_create_url($term_obj->get('field_brand_image')->entity->getFileUri());
      }
      $brand[] = [
        'tid' => $term->tid,
        'tname' => $term->name
      ];
    }
    $vid   = 'color_parent';
    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);
    foreach ($terms as $term) {
      $term_obj = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($term->tid);
      $url = "";
      $colors[] = [
        'tid' => $term->tid,
        'tname' => $term->name
      ];
    }
    $success_status = "";
    if(isset($_SESSION['postid']) ){
      if($_SESSION['postid'] == ""){
        $_SESSION['postid'] = rand(10,100);
      }
    }else{
      $_SESSION['postid'] = rand(10,100);
    }    
    if(!empty($_POST)){

      if( (isset($_POST['postid'])) && ($_POST['postid'] != "")  ){

        if($_SESSION['postid'] == $_POST['postid']){
          $field_partno         = $_POST['field_partno'];
          $field_category_name  = $_POST['field_category_name'];
          $field_brand          = $_POST['field_brand'];
          $field_product_name   = $_POST['title'];
          $stock                = $_POST['stock'];
          $marketing_message    = $_POST['marketing_message'];
          $field_featured       = $_POST['field_featured'];
          $field_new            = $_POST['field_new'];
          $editor1              = $_POST['editor1'];
          $price                = (isset($_POST['price']))? $_POST['price'] : '';
          $sku                  = $_POST['sku'];
          $field_total_price    = $_POST['field_total_price'];
          $field_lester         = $_POST['field_lester'];
          $field_interchange    = $_POST['field_interchange'];
          $color                = (isset($_POST['color']))? $_POST['color'] : '';
          if(isset($_POST['field_call_price'])){
            $field_call_price     = $_POST['field_call_price'];
          }
          $emptysku             = [];
          $emptyprice           = [];
          $r                    = 0;
          /*product image validation*/
          if($_FILES["product_image"]["name"] != ""){
            $name = $_FILES["product_image"]["name"];
            $exts = explode(".", $name);
            $extension = $exts[1];
            $allowedExts = array("jpeg", "jpg", "png","PNG","JPEG",'jpg');
            if(in_array($extension, $allowedExts)){
               move_uploaded_file($_FILES['product_image']['tmp_name'],"public://digitalassetsimages/".$_FILES['product_image']['name']);
              $data = file_get_contents("public://digitalassetsimages/".$_FILES["product_image"]["name"]);
              $file_product = file_save_data($data, "public://upload_parts/".$_FILES["product_image"]["name"], FILE_EXISTS_REPLACE);

            }else{
              $upload_error = "Product Image Type Should Be jpg,png";
            }
          }
          /*end of product image validation*/


          /*variation validation*/
            foreach($color as $key=>$valuecolor){
              $color_array[] = $valuecolor;
            }
            foreach($price  as $key=>$valueprice){
              $price_array[] = $valueprice;

            }
            $color_errors = $this->array_has_dupes($color_array);
            //$price_array  = $this->array_has_dupes($price_array);

            if( ($color_errors  == 1)  ) {
              $variation_error = "variation Color should not be duplicate";
            }

          /*end of varation validation*/

          /*echo count($_POST['color']);
          die();*/
          foreach($color as $key=>$value){
            $variation_array[$r] = ['color'=>$value,'sku'=>$sku,'price'=>$price[$key]];
            $r++;
          }
          $variation = '';
          $variation1 = '';
          if($_POST['hidden_id'] != ""){

        
             $hid = $_POST['hidden_id'];
           
             $query4 = $connection->query("SELECT field_total_price_number from commerce_product__field_total_price WHERE  entity_id = '".$hid."' ");
                while($row_data1 = $query4->fetchAssoc()){
                  $total_price_inital = $row_data1['field_total_price_number'];
                }


                $query6 = $connection->query("SELECT variation_id,price__number FROM commerce_product_variation_field_data WHERE  product_id = '".$hid."' ");
               
                while($row_data2 = $query6->fetchAssoc()){
                 
                    $query7 = $connection->query("SELECT field_color_target_id FROM commerce_product_variation__field_color WHERE  entity_id = '".$row_data2['variation_id']."'")->fetchAssoc();


                    $query_clr = $connection->query("SELECT field_color_price_values_value FROM taxonomy_term__field_color_price_values WHERE  entity_id = '".$query7['field_color_target_id']."'")->fetchAssoc();

                    $last_price =$query_clr['field_color_price_values_value']+$field_total_price;

                 

                  $var_id = $row_data2['variation_id'];

                  $query8 = $connection->query("update commerce_product_variation_field_data set price__number ='".$last_price."',title='".$field_product_name."'  WHERE  variation_id = '".$var_id."' ");

                  

                }


                /*price update */
                  $query6 = $connection->query("SELECT variation_id,price__number FROM commerce_product_variation_field_data WHERE  product_id = '".$hid."' ");
                  while($row_data2 = $query6->fetchAssoc()){

                    $order_array = [];
                    $cart_items  = $connection->query("SELECT order_id,quantity FROM commerce_order_item WHERE order_id IN(SELECT order_id FROM  commerce_order WHERE state='draft' AND cart=1)");

                     while($row = $cart_items->fetchAssoc()){
            
                         $upd1=$connection->query("update commerce_order_item  set  unit_price__number = '".$row_data2['price__number']."', total_price__number = '".($row['quantity']*$row_data2['price__number'])."' where order_id = '".$row['order_id']."' and purchased_entity = '".$row_data2['variation_id']."'");
                        $order_array[] = $row['order_id'];                          
                     }
                     /*exit;*/
                     foreach(array_unique($order_array) as $value){
                         $upd2=$connection->query("update commerce_order  set   total_price__number = ( select sum(total_price__number) from commerce_order_item where order_id = '".$value."') where order_id = '".$value."'");
                     }
                  }   

                /*end of price update */

                drupal_flush_all_caches();

               
          
              if( ($_FILES["product_image"]["name"] != "") && ($upload_error != "") ){
                 $error = "Product Image Should Be jpg or png";
              }else if($slider_error != ""){
                 $error = $slider_error;
              }else if($variation_error != ""){
                 $error = $variation_error;
              }else{
               
                $products = array();
                $exist_data = [];
               
                  $query = $connection->query("SELECT bundle,deleted,entity_id,revision_id,langcode,delta,variations_target_id
                          FROM commerce_product__variations 
                          WHERE  entity_id = '".$hid."' ");
                while($row_datas = $query->fetchAssoc()){
                  $exist_data[] = array('bundle'=>$row_datas['bundle'],
                                        'deleted'=>$row_datas['deleted'],
                                        'entity_id'=>$row_datas['entity_id'],
                                        'revision_id'=>$row_datas['revision_id'],
                                        'langcode'=>$row_datas['langcode'],
                                        'delta'=>$row_datas['delta'],
                                        'variations_target_id'=>$row_datas['variations_target_id']
                                       );
                }
               /* echo "<pre>";
                print_r($exist_data);
                die();*/
                $node                           = product::load($_POST['hidden_id']);
                $node->body->value              = $editor1;
                $node->body->format             = 'full_html';
                $node->title                    = $field_product_name;
                $node->field_product_name       = $field_product_name;
                $node->field_partno             = $field_partno;
                $node->field_category_name      = $field_category_name;
                $node->field_featured           = $field_featured;
                $node->field_brand              = $field_brand;
                $node->field_stock_info         = $stock;
                $node->field_market_message     = $marketing_message;
                $node->field_new                = $field_new;
                $node->field_lester             = $field_lester;
                $node->field_interchange        = $field_interchange;
                $node->field_sku                = 15;
                $node->field_price              = 15;
                $node->field_call_price         = $field_call_price;
                $node->field_total_price        = new Price($field_total_price, 'USD');

                $node->variations = [];
                if($_FILES["product_image"]["name"] != ""){
                  $field_product_image = array(
                      'target_id' => $file_product->id(),
                      'alt' => 'test',
                      'title' => "My title"
                  );
                  $node->field_product_image = $field_product_image;
                }
                $node->save();
                $product2 =Product::load($_POST['hidden_id']);



                foreach( $_POST['fileid'] as $key=>$value){
                  $target_id[] = $value;
                }

                if(!empty($_FILES['sliderimage']) ){
                  $targets = 0;
                  foreach($_FILES['sliderimage']['tmp_name'] as $key => $tmp_name){
                    $file_name  = $_FILES['sliderimage']['name'][$key];
                    $file_name1 = $_FILES['sliderimage']['name'][$key];
                    if($file_name1 != ""){
                      $file_tmp =$_FILES['sliderimage']['tmp_name'][$key];
                      $ext = pathinfo($file_name1, PATHINFO_EXTENSION);
                      $file = basename($file_name1,".".$ext);
                      $file_name1=$file.time().".".$ext;

                     

                          move_uploaded_file($file_tmp,"public://digitalassetsimages/".$file_name1);
                    $data = file_get_contents("public://digitalassetsimages/".$file_name1);
                    $file_product = file_save_data($data, "public://upload_parts/".$file_name1, FILE_EXISTS_REPLACE);


                      $filew[] = array(
                          'target_id' => $file_product->id(),
                          'alt' => 'teee',
                          'title' => "My title"
                      );
                    }else{
                      $filew[] = array(
                          'target_id' => $target_id[$targets],
                          'alt' => 'teee',
                          'title' => "My title"
                      );
                    }
                    $targets++;
                  }
                  if(!empty($filew)){
                    $product2->field_image_one = $filew;
                  }
                }
                $product2->save();

                 
                foreach ($exist_data as $key => $value){
                  $bundle      = $value['bundle'];
                  $deleted     = $value['deleted'];
                  $entity_id   = $value['entity_id'];
                  $revision_id = $value['revision_id'];
                  $langcode    = $value['langcode'];
                  $delta       = $value['delta'];
                  $variations_target_id = $value['variations_target_id'];                
                  $query = $connection->query("insert into commerce_product__variations(bundle,deleted,entity_id,revision_id,langcode,delta,variations_target_id)   values('".$bundle."','".$deleted."','".$entity_id."','".$revision_id."',
                    '".$langcode."','".$delta."','".$variations_target_id."')");                  
                }
                $hid   = $_POST['hidden_id'];
                $query = $connection->query("update commerce_product_variation_field_data set 
                  sku = '".$sku."' where product_id= '".$hid."' ");
                $elastic_body =[];
                $elastic_body["product_name"]=$field_product_name;
                $elastic_body["featured"]=$field_featured;
                $elastic_body["partno"]=$field_partno;
                $elastic_body["partnobcc"]=str_replace("-","",$field_partno);
                $elastic_body["stock"]=$stock;
                $elastic_body["price"]=$field_total_price;
                if($_FILES["product_image"]["name"] != ""){
                $elastic_body["product_image"]=$_FILES["product_image"]["name"]; 
              }
                $elastic_body["category"]=$field_category_name;
                $elastic_body["brand"]=$field_brand;
                $elastic_body["new"]=$field_new;
                $elastic_body["sku"]=$sku;
                $elastic_body["marketing_message"]=$marketing_message;
                 
                   $paramss = [
                         'index' => 'xclutchcpindex',
                         'type' => 'products',
                         'id' => $_POST['hidden_id'],
                          'body' => [                                  
                                    'doc' => $elastic_body                  
                                  ]
                            ];
                   $elastic->update($paramss);

                drupal_flush_all_caches();
                
                $success_status = "Product updated Successfully";
              }
           
          }else{
          
            if($slider_empty_error != ""){
               $error = $slider_empty_error;
            }else if($slider_error != ""){
               $error = $slider_error;
            }else if($variation_error != ""){
               $error = $variation_error;
            }else if( ($_FILES["product_image"]["name"] != "") && ($upload_error == "") ){


              $product = Product::create([
                'type' => 'default',
                'body'  => ['value'=> $editor1,'format'=> 'basic_html'],
                'title' => t($field_product_name),
                'field_product_name' => $field_product_name,
                 'field_partno'=>$field_partno,
                 'field_category_name' => $field_category_name,
                 'field_featured'=>$field_featured,
                 'field_brand' => $field_brand,
                 'field_stock_info' => $stock,
                 'field_market_message'=> $marketing_message,
                 'field_new'=>$field_new,
                 'field_sku' => 15,
                 'field_price' => 15,
                 'field_lester' => $field_lester,
                 'field_interchange' => $field_interchange,
                 'field_total_price'=> new Price($field_total_price, 'USD'),
                 'field_call_price' => $field_call_price,
                  'field_product_image' => array(
                              'target_id' => $file_product->id(),
                              'alt' => 'test',
                              'title' => "My title",
                            )
              ]);
              $product->save();
              $id =  $product->id();
              $product =Product::load($id);
              $productt  = \Drupal\commerce_product\Entity\Product::load($id);
              $product_idd    =  $product->get('product_id')->getValue();

              //if(isset($_POST['setallcolors'])){
                $variation1   = ProductVariation::create([
                                            'type' => 'default',
                                            'sku' => $sku,
                                             'price' => new Price((string)$field_total_price, 'USD'),
                                            'field_color'=>152
                                          ]);
                $variation1->save();
                $product->addVariation($variation1);
                $vid    = 'color_parent';
                $terms  = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);

                foreach ($terms as $term) {  
                  $term_obj = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($term->tid);
                  $field_color_price_values = $term_obj->get('field_color_price_values')->value;                  
                  $prices =  $field_total_price+$field_color_price_values;                
                  $color  =  $term->tid; 

                  if($term->tid != 152){
                    $variation   = ProductVariation::create([
                                            'type' => 'default',
                                            'sku' => $sku,
                                            'price' => new Price((string)$prices, 'USD'),
                                            'field_color'=>$color
                                          ]);
                    $variation->save();
                    $product->addVariation($variation);
                  }
                }
             
              $filew = [];

              if(!empty($_FILES['sliderimage'])){

                foreach($_FILES['sliderimage']['tmp_name'] as $key => $tmp_name){
                  $file_name = $_FILES['sliderimage']['name'][$key];
                  if($file_name != ""){
                    $file_tmp =$_FILES['sliderimage']['tmp_name'][$key];
                   
                      $ext = pathinfo($file_name, PATHINFO_EXTENSION);
                      $file = basename($file_name,".".$ext);
                      $file_name=$file.time().".".$ext;
                      

                    move_uploaded_file($file_tmp,"public://digitalassetsimages/".$file_name);
                    $data = file_get_contents("public://digitalassetsimages/".$file_name);
                    $file_product = file_save_data($data, "public://upload_parts/".$file_name, FILE_EXISTS_REPLACE);

                    $filew[] = array(
                          'target_id' => $file_product->id(),
                          'alt' => 'teee',
                          'title' => "My title"
                      );
                  }
                }
                if(!empty($filew)){
                  $product->field_image_one = $filew;
                }
              }
              $product->save();

              $field_product_image_in = array(
                      'target_id' => $file_product->id(),
                      'alt' => 'test',
                      'title' => "My title"
                  );

              $colors =array();
              $colors = $this->color_set($product_idd[0]['value']);
              /*print_r($colors);
              exit;*/

               $params = [
                         'index' => 'xclutchcpindex',
                         'type' => 'products',
                         'id' => $product_idd[0]['value'],
                          'body' => [      
                                    'productid'  =>  $product_idd[0]['value'],                            
                                    'product_name'  => $field_product_name,
                                     'featured'  => $field_featured,
                                     'partno'  => $field_partno,
                                     'partnobcc'  => $field_partno,
                                     'stock'  => $stock,
                                     'price'  => $field_total_price,
                                     'marketing_message' => $marketing_message,
                                     'product_image'  => $_FILES["product_image"]["name"],                  
                                     'category'  => $field_category_name,
                                     'brand'  => $field_brand,
                                     'new'  => $field_new,                  
                                     'sku'  => $sku,
                                     'call'  => 'off',
                                     'colors'=>$colors                    
                                  ]
                            ];
                   $elastic->index($params);
                   drupal_flush_all_caches();

              $success_status = "Product Added Successfully";
            }else{
              $error = "Product Image Should Be jpg or png";
            }
          }
        }
      }
      $_SESSION['postid'] = "";
    }
    if($_SESSION['postid'] == ""){
      $_SESSION['postid'] = rand(10,100);
    }
    return array(
        '#theme' => 'part_master',
        '#error'=>$error,
        '#category'=>$colors,
        '#title' => $success_status,
        '#postid'=>$_SESSION['postid']
    );
  }

////modified function balaji //
  public function page(){
    global $base_url;
    $upload_error       = "";
    $upload_video_error = "";
    $error              = "";
    $editor_validate    = "";
    $slider_error       = "";
    $slider_empty_error = "";
    $variation_error    = "";
    $color_array        = [];
    $sku_array          = [];
    $price_array        = [];
    $editor1            = '';
    $field_call_price   = '';
    $vid = 'brand';
    $total_price_inital = "";
    $var_price_inital = "";
    $final_price      = "";
    $last_price       = "";
    $var_id = "";
    $elastic = $GLOBALS['partdetail'];
    $connection = \Drupal::database();
    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);
    foreach ($terms as $term) {
      $term_obj = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($term->tid);
      $url = "";
      if(isset($term_obj->get('field_brand_image')->entity)){
        $url = file_create_url($term_obj->get('field_brand_image')->entity->getFileUri());
      }
      $brand[] = [
        'tid' => $term->tid,
        'tname' => $term->name
      ];
    }
    $vid   = 'color_parent';
    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);
    foreach ($terms as $term) {
      $term_obj = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($term->tid);
      $url = "";
      $colors[] = [
        'tid' => $term->tid,
        'tname' => $term->name
      ];
    }
    $success_status = "";
    if(isset($_SESSION['postid']) ){
      if($_SESSION['postid'] == ""){
        $_SESSION['postid'] = rand(10,100);
      }
    }else{
      $_SESSION['postid'] = rand(10,100);
    }    
    if(!empty($_POST)){
     // $get_imgpath = $connection->query("SELECT * FROM `catapult_parts_path`" );
     //  $roww = $get_imgpath->fetchAssoc(); 
      if( (isset($_POST['postid'])) && ($_POST['postid'] != "")  ){
        if($_SESSION['postid'] == $_POST['postid']){
          $field_partno         = $_POST['field_partno'];
          $field_category_name  = $_POST['field_category_name'];
          $field_brand          = $_POST['field_brand'];
          $field_product_name   = $_POST['title'];
          $stock                = $_POST['stock'];
          $marketing_message    = $_POST['marketing_message'];
          $field_featured       = $_POST['field_featured'];
          $field_new            = $_POST['field_new'];
          $editor1              = $_POST['editor1'];
          $price                = (isset($_POST['price']))? $_POST['price'] : '';
          $sku                  = $_POST['sku'];
          $field_total_price    = $_POST['field_total_price'];
          $field_lester         = $_POST['field_lester'];
          $field_interchange    = $_POST['field_interchange'];
          $field_product_image  = $_POST['product_image'];
           $field_slider_image  = $_POST['slider_image'];

          
          $color                = (isset($_POST['color']))? $_POST['color'] : '';
          if(isset($_POST['field_call_price'])){
            $field_call_price     = $_POST['field_call_price'];
          }
          $emptysku             = [];
          $emptyprice           = [];
          $r                    = 0;
          /*product image validation*/
        
          /*end of product image validation*/


          /*variation validation*/
          if(isset($_POST['color'])){
            foreach($color as $key=>$valuecolor){
              $color_array[] = $valuecolor;
            } 
          }
            
            foreach($price  as $key=>$valueprice){
              $price_array[] = $valueprice;

            }
            $color_errors = $this->array_has_dupes($color_array);
            //$price_array  = $this->array_has_dupes($price_array);

            if( ($color_errors  == 1)  ) {
              $variation_error = "variation Color should not be duplicate";
            }

          /*end of varation validation*/

          /*echo count($_POST['color']);
          die();*/
          if(isset($_POST['color'])){
            foreach($color as $key=>$value){
              $variation_array[$r] = ['color'=>$value,'sku'=>$sku,'price'=>$price[$key]];
              $r++;
            }
          }
          $variation = '';
          $variation1 = '';
          if($_POST['hidden_id'] != ""){

        
             $hid = $_POST['hidden_id'];
           
             $query4 = $connection->query("SELECT field_total_price_number from commerce_product__field_total_price WHERE  entity_id = '".$hid."' ");
                while($row_data1 = $query4->fetchAssoc()){
                  $total_price_inital = $row_data1['field_total_price_number'];
                }


                $query6 = $connection->query("SELECT variation_id,price__number FROM commerce_product_variation_field_data WHERE  product_id = '".$hid."' ");
               
                while($row_data2 = $query6->fetchAssoc()){
                 
                    $query7 = $connection->query("SELECT field_color_target_id FROM commerce_product_variation__field_color WHERE  entity_id = '".$row_data2['variation_id']."'")->fetchAssoc();


                    $query_clr = $connection->query("SELECT field_color_price_values_value FROM taxonomy_term__field_color_price_values WHERE  entity_id = '".$query7['field_color_target_id']."'")->fetchAssoc();

                    $last_price =$query_clr['field_color_price_values_value']+$field_total_price;

                 

                  $var_id = $row_data2['variation_id'];

                  $query8 = $connection->query("update commerce_product_variation_field_data set price__number ='".$last_price."',title='".$field_product_name."'  WHERE  variation_id = '".$var_id."' ");

                  

                }


                /*price update */
                  $query6 = $connection->query("SELECT variation_id,price__number FROM commerce_product_variation_field_data WHERE  product_id = '".$hid."' ");
                  while($row_data2 = $query6->fetchAssoc()){

                    $order_array = [];
                    $cart_items  = $connection->query("SELECT order_id,quantity FROM commerce_order_item WHERE order_id IN(SELECT order_id FROM  commerce_order WHERE state='draft' AND cart=1)");

                     while($row = $cart_items->fetchAssoc()){
            
                         $upd1=$connection->query("update commerce_order_item  set  unit_price__number = '".$row_data2['price__number']."', total_price__number = '".($row['quantity']*$row_data2['price__number'])."' where order_id = '".$row['order_id']."' and purchased_entity = '".$row_data2['variation_id']."'");
                        $order_array[] = $row['order_id'];                          
                     }
                     /*exit;*/
                     foreach(array_unique($order_array) as $value){
                         $upd2=$connection->query("update commerce_order  set   total_price__number = ( select sum(total_price__number) from commerce_order_item where order_id = '".$value."') where order_id = '".$value."'");
                     }
                  }   

                /*end of price update */

                drupal_flush_all_caches();

               
          
               if($variation_error != ""){
                 $error = $variation_error;
              }else{
               
                $products = array();
                $exist_data = [];
               
                  $query = $connection->query("SELECT bundle,deleted,entity_id,revision_id,langcode,delta,variations_target_id
                          FROM commerce_product__variations 
                          WHERE  entity_id = '".$hid."' ");
                while($row_datas = $query->fetchAssoc()){
                  $exist_data[] = array('bundle'=>$row_datas['bundle'],
                                        'deleted'=>$row_datas['deleted'],
                                        'entity_id'=>$row_datas['entity_id'],
                                        'revision_id'=>$row_datas['revision_id'],
                                        'langcode'=>$row_datas['langcode'],
                                        'delta'=>$row_datas['delta'],
                                        'variations_target_id'=>$row_datas['variations_target_id']
                                       );
                }
               /* echo "<pre>";
                print_r($exist_data);
                die();*/
                $node                           = product::load($_POST['hidden_id']);
                $node->body->value              = $editor1;
                $node->body->format             = 'full_html';
                $node->title                    = $field_product_name;
                $node->field_product_name       = $field_product_name;
                $node->field_partno             = $field_partno;
                $node->field_category_name      = $field_category_name;
                $node->field_featured           = $field_featured;
                $node->field_brand              = $field_brand;
                $node->field_stock_info         = $stock;
                $node->field_market_message     = $marketing_message;
                $node->field_new                = $field_new;
                $node->field_lester             = $field_lester;
                $node->field_interchange        = $field_interchange;
                $node->field_sku                = 15;
                $node->field_price              = 15;
                $node->field_call_price         = $field_call_price;
                $node->field_total_price        = new Price($field_total_price, 'USD');
                $node->field_product_image      = $field_product_image;
                $node->field_image_one          = $field_slider_image;
                $node->variations = [];
               
                $node->save();
               
            /*   $result_main_img = $connection->insert('catapult_parts_img_link')->fields([
                  'part_no'  => $field_partno,
                  'part_img' => $roww['image_path'].$field_product_image,
                  'img_flag' => 1                
                ])->execute();*/

          //   $field_product_imagee=$roww['image_path'].$field_product_image;
                 //$field_product_imagee=$field_product_image;

               //$connection->query("update catapult_parts_img_link set part_img ='".$field_product_imagee."' WHERE  part_no = '".$field_partno."' and  img_flag =1");
    // if(!empty($_POST['sliderimage']) ){

    //   $query = $connection->query("SELECT count(*) coun
    //                       FROM catapult_parts_img_link 
    //                       WHERE  part_no = '".$field_partno."'  and  img_flag =2");
    //            $nrows = $query->fetchAssoc();
    //            if($nrows['coun']>0)
    //            {
    //               $connection->query("delete from 
    //                        catapult_parts_img_link 
    //                        WHERE  part_no = '".$field_partno."'  and  img_flag =2");
    //            }

             
    //               foreach ($_POST['sliderimage'] as $key => $value) {
    //                      $result_main_img = $connection->insert('catapult_parts_img_link')->fields([
    //               'part_no'  => $field_partno,
    //             //  'part_img' => $roww['image_path'].$value,
    //               'part_img' => $value,
    //               'img_flag' => 2                
    //             ])->execute();
    //             }         
              
                 
                 
    //             }

                 
                foreach ($exist_data as $key => $value){
                  $bundle      = $value['bundle'];
                  $deleted     = $value['deleted'];
                  $entity_id   = $value['entity_id'];
                  $revision_id = $value['revision_id'];
                  $langcode    = $value['langcode'];
                  $delta       = $value['delta'];
                  $variations_target_id = $value['variations_target_id'];                
                  $query = $connection->query("insert into commerce_product__variations(bundle,deleted,entity_id,revision_id,langcode,delta,variations_target_id)   values('".$bundle."','".$deleted."','".$entity_id."','".$revision_id."',
                    '".$langcode."','".$delta."','".$variations_target_id."')");                  
                }
                $hid   = $_POST['hidden_id'];
                $query = $connection->query("update commerce_product_variation_field_data set 
                  sku = '".$sku."' where product_id= '".$hid."' ");
                $elastic_body =[];
                $elastic_body["product_name"]=$field_product_name;
                $elastic_body["featured"]=$field_featured;
                $elastic_body["partno"]=$field_partno;
                $elastic_body["partnobcc"]=str_replace("-","",$field_partno);
                $elastic_body["stock"]=$stock;
                $elastic_body["price"]=$field_total_price;               
             //   $elastic_body["product_image"]=$roww['image_path'].$field_product_image;
              $elastic_body["product_image"]=$field_product_image;               
                $elastic_body["category"]=$field_category_name;
                $elastic_body["brand"]=$field_brand;
                $elastic_body["new"]=$field_new;
                $elastic_body["sku"]=$sku;
                $elastic_body["marketing_message"]=$marketing_message;
                 
                   $paramss = [
                         'index' => 'xclutchcpindex',
                         'type' => 'products',
                         'id' => $_POST['hidden_id'],
                          'body' => [                                  
                                    'doc' => $elastic_body                  
                                  ]
                            ];
                            /*echo "<pre>";
                            print_r($paramss);
                            exit;*/
                   $elastic->update($paramss);

                drupal_flush_all_caches();
                
                $success_status = "Product updated Successfully";
              }
           
          }else{

          
                $product = Product::create([
                'type' => 'default',
                'body'  => ['value'=> $editor1,'format'=> 'basic_html'],
                'title' => t($field_product_name),
                'field_product_name' => $field_product_name,
                 'field_partno'=>$field_partno,
                 'field_category_name' => $field_category_name,
                 'field_featured'=>$field_featured,
                 'field_brand' => $field_brand,
                 'field_stock_info' => $stock,
                 'field_market_message'=> $marketing_message,
                 'field_new'=>$field_new,
                 'field_sku' => 15,
                 'field_price' => 15,
                 'field_lester' => $field_lester,
                 'field_interchange' => $field_interchange,
                 'field_total_price'=> new Price($field_total_price, 'USD'),
                 'field_call_price' => $field_call_price,
                 'field_product_image'      => $field_product_image,
                  'field_image_one'          => $field_slider_image
                  
                            
              ]);
              $product->save();
              $id =  $product->id();
              $product =Product::load($id);
              $productt  = \Drupal\commerce_product\Entity\Product::load($id);
              $product_idd    =  $product->get('product_id')->getValue();

              //if(isset($_POST['setallcolors'])){
                $variation1   = ProductVariation::create([
                                            'type' => 'default',
                                            'sku' => $sku,
                                             'price' => new Price((string)$field_total_price, 'USD'),
                                            'field_color'=>152
                                          ]);
                $variation1->save();
                $product->addVariation($variation1);
                $vid    = 'color_parent';
                $terms  = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);

                foreach ($terms as $term) {  
                  $term_obj = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($term->tid);
                  $field_color_price_values = $term_obj->get('field_color_price_values')->value;                  
                  $prices =  $field_total_price+$field_color_price_values;                
                  $color  =  $term->tid; 

                  if($term->tid != 152){
                    $variation   = ProductVariation::create([
                                            'type' => 'default',
                                            'sku' => $sku,
                                            'price' => new Price((string)$prices, 'USD'),
                                            'field_color'=>$color
                                          ]);
                    $variation->save();
                    $product->addVariation($variation);
                  }
                }
             
             

       
              $product->save();

          
              $colors =array();
              $colors = $this->color_set($product_idd[0]['value']);
              // print_r($colors);
              // exit;

              // $result_main_img = $connection->insert('catapult_parts_img_link')->fields([
              //     'part_no'  => $field_partno,
              //    // 'part_img' => $roww['image_path'].$field_product_image,
              //     'part_img' => $field_product_image,
              //     'img_flag' => 1                
              //   ])->execute();




              // if(!empty($_POST['sliderimage']) ){
              
              //      foreach ($_POST['sliderimage'] as $key => $value) {
              //            $result_main_img = $connection->insert('catapult_parts_img_link')->fields([
              //     'part_no'  => $field_partno,
              //   //  'part_img' => $roww['image_path'].$value,
              //     'part_img' =>$value,
              //     'img_flag' => 2                
              //   ])->execute();
              //   }
                 
              //   }

               $params = [
                         'index' => 'xclutchcpindex',
                         'type' => 'products',
                         'id' => $product_idd[0]['value'],
                          'body' => [      
                                    'productid'  =>  $product_idd[0]['value'],                            
                                    'product_name'  => $field_product_name,
                                     'featured'  => $field_featured,
                                     'partno'  => $field_partno,
                                     'partnobcc'  => $field_partno,
                                     'stock'  => $stock,
                                     'price'  => $field_total_price,
                                     'marketing_message' => $marketing_message,
                                  //   'product_image'  => $_FILES["product_image"]["name"],   
                                  'product_image'  => $field_product_image,               
                                     'category'  => $field_category_name,
                                     'brand'  => $field_brand,
                                     'new'  => $field_new,                  
                                     'sku'  => $sku,
                                     'call'  => 'off',
                                     'colors'=>$colors                    
                                  ]
                            ];
                   $elastic->index($params);
                   drupal_flush_all_caches();

              $success_status = "Product Added Successfully";
            
          }
        }
      }
      $_SESSION['postid'] = "";
    }
    if($_SESSION['postid'] == ""){
      $_SESSION['postid'] = rand(10,100);
    }
    return array(
        '#theme' => 'part_master',
        '#error'=>$error,
        '#category'=>$colors,
        '#title' => $success_status,
        '#postid'=>$_SESSION['postid']
    );
  }
  
 public function color_set($product_id){
      $result1 = [];
      $results = "";
       $connection               = \Drupal::database();
        $query1=$connection->query("SELECT group_concat(clr.field_color_target_id separator '|') as color_id
                                    FROM commerce_product_variation__field_color clr
                                    INNER JOIN commerce_product__variations clr_v
                                    ON clr.entity_id = clr_v.variations_target_id
                                    where clr_v.entity_id='".$product_id."'
                                    group by clr_v.entity_id;" );
      foreach($query1 as $row1)  {
        $results = $row1->color_id;
      }
      //$result1[] =  explode(",",$results);
      return $results;
  }

public function upload_page(){
    ini_set('memory_limit', '-1');
    ini_set('display_errors',1);
    ini_set('max_execution_time', '0');
    global $base_url;
    $msg = "";

    if(!empty($_FILES)){

    
      $vid = 'category';
      $cat_data=array();
      $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);
      foreach ($terms as $term) {
              $term_obj = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($term->tid);           
              $cat_data[$term->name] = $term->tid ;
      } 
      
      $bid = 'brand';
      $brand_data=array();
      $termss = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($bid);
    foreach ($termss as $termm) {
        $term_obj = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($termm->tid);
        $brand_data[$termm->name] = $termm->tid ;    
      }

       $mid = 'marketmessage';
      $marketing_data=array();
      $termss = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($mid);
    foreach ($termss as $termm) {
        $term_obj = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($termm->tid);
        $marketing_data[$termm->name] = $termm->tid ;    
      }

      $filename = $_FILES['file']['name'];
      $excel_array=array('xls','xlsx');
      if(in_array(strtolower(pathinfo($filename, PATHINFO_EXTENSION)),$excel_array)){


        move_uploaded_file($_FILES['file']['tmp_name'],"public://bulk_template/".$filename);
        $targetPath = 'site/default/files/bulk_template/'.$filename;        

        $inputFile="public://bulk_template/".$filename;
        $extension = strtoupper(pathinfo($inputFile, PATHINFO_EXTENSION));
        $inputFileType = \PHPExcel_IOFactory::identify($inputFile);
        $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($inputFile);

        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestDatarow();
        $highestColumn = $sheet->getHighestDataColumn();
        $title_array = $sheet->rangeToArray('A' . $row=1 . ':' . $highestColumn . $row=1, NULL, TRUE, FALSE);
          // echo "<pre>";
          // print_r($title_array);
          // die();
        $final_error_data=array();$error_count=0;$success_count=0;

        if($highestRow<=1){
          $msg="There is no data in excel";
        }

        for ($row = 1; $row <= $highestRow; $row++){
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);

            if($rowData[0][0]!=''){
              if($row!=1){
              /*$rowData[0][1]="Every AutoTech Engineering unit is assembled 100% in Riverside, California. With over 100
                years of combined, “on hands” experience and the fact that we have been in business for over
                28 years has allowed us to source and supply the highest quality component parts to ensure
                your unit exceeds expectations. All are plug and play unless over wise stated. Some
                modifications such as adding spacers or plug harness are included at no extra cost. Voltage is
                set to OEM minimum specifications on each unit ordered unless otherwise requested. If you
                need higher voltage, contact us before ordering online.";*/
                $rowData[0][1] = $rowData[0][1];

              if($rowData[0][6] != ""){
                $exp_slider = explode(',', $rowData[0][6]);
                $tem_exp_slider = (count($exp_slider)>5)?array_slice($exp_slider,5):$exp_slider;

                $slider_images=explode(',', $rowData[0][6]);
                $filew=array();
                foreach ($slider_images as $key => $value) {
                     $slider_data = file_get_contents("public://digitalassetsimages/".$value);
                     $slider_product = file_save_data($slider_data, "public://upload_parts/".$value, FILE_EXISTS_REPLACE);
                     $filew[] = array(
                                'target_id' => $slider_product->id(),
                                'alt' => 'teee',
                                'title' => "My title"
                            );
                }
              }else{
                $data = file_get_contents("public://digitalassetsimages/no_image_icon.PNG");
                $slider_product = file_save_data($data, "public://upload_parts/no_image_icon.PNG", FILE_EXISTS_REPLACE);
                $filew[] = array(
                                'target_id' => $slider_product->id(),
                                'alt' => 'teee',
                                'title' => "My title"
                            );
                $rowData[0][6]="no_image_icon.PNG";

              }              


             // $rowData[0][4]="Autotech";
            //  $rowData[0][5]="Alternators";
             
              
              }
              $error_message=$this->excel_field_validation($rowData[0],$row,$title_array[0]);


              $store_error[$row]=$error_message;
              $store_error[$row]['data'][11]=$store_error[$row]['error'];

              if($row!= 1){
                if(empty($error_message['error'])){
                     $field_partno               = $rowData[0][0];
                     $editor1                    = $rowData[0][1];
                     if($rowData[0][2] == ""){
                        $field_total_price       = 0;
                        $field_call_price        = "on";
                     }else{
                       $field_total_price       = $rowData[0][2];
                       $field_call_price        = "off";
                     }                     
                     $field_product_name         = $rowData[0][5]." | ".$rowData[0][0];


                     
                     if (array_key_exists($rowData[0][5],$cat_data)) {
                           $field_category=$cat_data[$rowData[0][5]];
                        }else{
                           $field_category="";
                        } 

                        if (array_key_exists($rowData[0][4],$brand_data)) {
                           $field_brand=$brand_data[$rowData[0][4]];
                        }else{
                           $field_brand="";
                        }
                        if (array_key_exists($rowData[0][11],$marketing_data)) {
                           $field_market_message=$marketing_data[$rowData[0][11]];
                        }else{
                           $field_market_message="";
                        }

                        // $field_category=!empty($category[$rowData[0][5]])?$category[$rowData[0][5]]:'';

                      if($rowData[0][3] != ""){
                        $data = file_get_contents("public://digitalassetsimages/".$rowData[0][3]);
                        $file_product = file_save_data($data, "public://upload_parts/".$rowData[0][3], FILE_EXISTS_REPLACE);
                      }else{
                        $data = file_get_contents("public://digitalassetsimages/no_image_icon.PNG");
                        $file_product = file_save_data($data, "public://upload_parts/no_image_icon.PNG", FILE_EXISTS_REPLACE);
                      }
                      if($rowData[0][3] == ""){
                        $alt = "No Image";
                      }else{
                        $alt = $rowData[0][3]; 
                      }
                      /*echo $field_total_price;
                      die();*/

                     $product = Product::create([
                          'type' => 'default',
                          'body'  => ['value'=> $editor1,'format'=> 'basic_html'],
                          'title' => t($field_product_name),
                          'field_product_name' => $field_product_name,
                           'field_partno'=>$field_partno,
                           'field_category_name' => $field_category,
                           'field_featured'=>1,
                           'field_brand' => $field_brand,
                           'field_stock_info' => 1,
                           'field_marketing_messages'=> 2,
                           'field_market_message'   => $field_market_message,
                           'field_new'=>1,
                           'field_sku' => $rowData[0][8],
                           'field_price' => 15,
                           'field_total_price'=> new Price((string)$field_total_price, 'USD'),
                           'field_product_image' => array(
                                        'target_id' => $file_product->id(),
                                        'alt' => $alt,
                                        'title' => "My title",
                                      ),
                           'field_image_one' => $filew,
                           'field_lester'=>$rowData[0][9],
                           'field_interchange'=>$rowData[0][10],
                           'field_call_price'=> $field_call_price
                      ]);
                      $product->save();
                      
                      $id =  $product->id();
                      $product_new_data =Product::load($id);
                      $vid    = 'color_parent';
                      $terms  = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);
                        foreach ($terms as $term) {
                           $term_obj = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($term->tid);
                          $field_color_price_values = $term_obj->get('field_color_price_values')->value;
                          //$color_price = ($term->tid==152)?0:15;
                          $prices      = $field_total_price+$field_color_price_values;
                          $color       = $term->tid;
                          $variation   = ProductVariation::create([
                                                    'type'  => 'default',
                                                    'sku'   => $rowData[0][8],
                                                    'price' => new Price((string)$prices , 'USD'),
                                                    'field_color'=>$color
                                                  ]);
                          $variation->save();
                          $product_new_data->addVariation($variation);
                        }
                        $product_new_data->save();                     
                        $success_count++;                        
                  }else{
                    $error_count++;
                    $final_error_data[]=$store_error[$row]['data'];
                  }
                }
            }

          }
          $res_file = "";
          if($error_count!=0){
            if($success_count<=0){
              $msg="Parts uploading failed";
            }else{
              $msg="Parts upload success with errors ";
            }
            $res_file = "sites/default/files/upload_excel_error/Errors.xls";
            $data = $this->excel_export($final_error_data,$res_file);
           
          }
        }else{
          $msg="Invalid format uploaded";
        }
      }else{
        $msg="There is no file selected";
      }

      $return_data['msg']=!empty($msg)?$msg:"Part uploaded successfully";
      $return_data['file']=!empty($res_file)?$res_file:"";
      echo json_encode($return_data);die;
    }


  

public function excel_field_validation($data,$row,$title){

      $image_array=array('jpg','jpeg','png','gif');
      $msg=array();
    

      if($row==1){
        $result_data['data']=$data;
        $result_data['error']='';
      }else{
        for ($i=0; $i <11 ; $i++) {
          if($i==0 || $i==4 || $i==5){
            if(empty($data[$i])){
              $msg[$i]=$title[$i]." must not empty";
            }
            if($i==0){
                $res = $this->check_partno_exist_upload($data[$i]);
                if($res == 'exist'){
                  $msg[$i]= $title[$i]." Already Exist";
                }
              }
          }/*else{
              if($i==3 || $i==6){
                if($data[$i] != ""){
                  $exp_str=explode(',',$data[$i]);
                  foreach ($exp_str as $key => $value) {
                    if(!in_array(strtolower(pathinfo($value, PATHINFO_EXTENSION)),$image_array)){
                      $msg[$i]=$title[$i]." having without image";                    
                    }else{
                      if(!file_get_contents("public://digitalassetsimages/".$value)){
                        $msg[$i]=$title[$i]." having image was not existing";
                      }
                    }
                  }
                }                
              }
            }*/
          }
        
        $result_data['data']=$data;        
        $result_data['error']=implode(PHP_EOL,$msg);
      }
        return $result_data;
    }


  public function get_application_id($make,$model,$year,$engine){
    $result_data['engine_id']="";
    if(!empty($make) && !empty($model) && !empty($year) && !empty($engine)){
      $connection = \Drupal::database();
      $query = $connection->query("SELECT engine_id FROM catapult_make A, catapult_model B, catapult_year C, catapult_engine D where D.make_id=A.make_id and D.model_id=B.model_id and D.year_id=C.year_id and A.make='".$make."' and B.model='".$model."' and C.year='".$year."' and  D.engine='".$engine."'");
      $result=$query->fetchAssoc();
      if(!empty($result)){
        $result_data['engine_id']=$result['engine_id'];
      }
    }
    return $result_data;
  }

  public function excel_export($data,$fileName){
// print_r($data);die;
   
    global $base_url;
    $objPHPExcel = new \PHPExcel();

    $sht = $objPHPExcel->getActiveSheet();

    $i=1;
    $sht->setCellValue('A'.$i, 'Partnumber');
    $sht->setCellValue('B'.$i, 'Part Description / Key Features ');
    $sht->setCellValue('C'.$i, 'Unit Price');
    // $sht->setCellValue('D'.$i, 'Part title');
    // $sht->setCellValue('E'.$i, 'Make');
    // $sht->setCellValue('F'.$i, 'Model');
    // $sht->setCellValue('G'.$i, 'Year');
    // $sht->setCellValue('H'.$i, 'Engine');
    $sht->setCellValue('D'.$i, 'Product Image');
    $sht->setCellValue('E'.$i, 'Brand');
    $sht->setCellValue('F'.$i, 'Category');
    $sht->setCellValue('G'.$i, 'Slider Image(comma separtated)');
    $sht->setCellValue('H'.$i, 'Featured Product (Yes/No)');
    $sht->setCellValue('I'.$i, 'SKU');
    $sht->setCellValue('J'.$i, 'Lester');
    $sht->setCellValue('K'.$i, 'Interchange');
    $sht->setCellValue('L'.$i, 'Remark');

    foreach ($data as $data_key => $data_value) {
      $i++;
      $sht->setCellValue('A'.$i, $data_value[0]);
      $sht->setCellValue('B'.$i, $data_value[1]);
      $sht->setCellValue('C'.$i, $data_value[2]);
      $sht->setCellValue('D'.$i, $data_value[3]);
      $sht->setCellValue('E'.$i, $data_value[4]);
      $sht->setCellValue('F'.$i, $data_value[5]);
      $sht->setCellValue('G'.$i, $data_value[6]);
      $sht->setCellValue('H'.$i, $data_value[7]);
      $sht->setCellValue('I'.$i, $data_value[8]);
      $sht->setCellValue('J'.$i, $data_value[9]);
      $sht->setCellValue('K'.$i, $data_value[10]);
      $sht->setCellValue('L'.$i, $data_value[11]);
      // $sht->setCellValue('M'.$i, $data_value[12]);
      // $sht->setCellValue('N'.$i, $data_value[13]);
      // $sht->setCellValue('O'.$i, $data_value[14]);
      // $sht->setCellValue('P'.$i, $data_value[15]);
      // $sht->setCellValue('Q'.$i, $data_value[16]);
      $sht->getStyle('L'.$i)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('FF0000');
    }


    $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    ob_end_clean();
    $objWriter->save($fileName);
  }


  public function getproductadmin(){
    global $base_url;
    $entity_manager     = \Drupal::entityManager();
    $all_variant = 0;
    $data1 =[];
    $connection = \Drupal::database();
    $products = array();
    $query = $connection->query("SELECT cp.product_id,fn.field_product_name_value,fp.field_partno_value,cam.field_category_name_target_id,fb.field_brand_target_id,ff.field_featured_value,ftp.field_total_price_number,fpi.field_product_image_value
              FROM commerce_product cp
              LEFT JOIN commerce_product__field_product_name fn ON cp.product_id=fn.entity_id
              LEFT JOIN commerce_product__field_partno fp ON cp.product_id=fp.entity_id
              LEFT JOIN commerce_product__field_category_name cam ON cp.product_id=cam.entity_id
              LEFT JOIN commerce_product__field_brand fb ON cp.product_id=fb.entity_id
              LEFT JOIN commerce_product__field_featured ff ON cp.product_id=ff.entity_id
              LEFT JOIN `commerce_product__field_total_price` ftp ON cp.product_id=ftp.entity_id
              LEFT JOIN `commerce_product__field_product_image` fpi ON cp.product_id=fpi.entity_id
              
              WHERE delete_flag=0 ORDER BY product_id DESC");
      $urldata=$connection->query("select dynamic_url from catapult_config_meta");
      $urlarr = $urldata->fetchAssoc();
    while($row = $query->fetchAssoc()){
      $id = $row['product_id'];
     /* $product      = \Drupal\commerce_product\Entity\Product::load($id);
      $vid          = 'category';
      $cid           = $row['field_category_name_target_id'];
      $term = Term::load($cid);*/
      $category_name = 'Alternators';
     /* $bid           = $row['field_brand_target_id'];
      $term = Term::load($bid);*/
      $brand_name = 'Xclutch';
       if($row['field_product_image_value'] =='no_image_icon.PNG'){
                  $spliturl=$base_url.'/sites/default/files/no_image_icon.PNG';
            }else{
               $spliturl=$urlarr['dynamic_url'].$row['field_product_image_value'];
            }

      $result_category = $this->get_category('category',$row['field_category_name_target_id']);
      $result_brand = $this->get_category('brand',$row['field_brand_target_id']);


      $prices = number_format((float)$row['field_total_price_number'], 2, '.', '');
      $data1[]      = array('product_id'=>$row['product_id'],'productname'=>$row['field_product_name_value'],'partno'=>$row['field_partno_value'],'category_value'=>$result_category,'brand'=>$result_brand,'featured'=>$row['field_featured_value'],'price'=>$prices,'pimage'=>$spliturl);
    }
    echo json_encode($data1);
    die();
  }
  public function getsingleproductadmin(){
    $all_variant              = 0;
    $id                       = $_POST['id'];
    $connection               = \Drupal::database();
    $products                 = array();
    $product                  = \Drupal\commerce_product\Entity\Product::load($id);
    $body                     =  strip_tags($product->body->value);
    $pname                    =  $product->get('field_product_name')->getValue();
    $partnos                  =  $product->get('field_partno')->getValue();
    $categorys                =  $product->get('field_category_name')->getValue();
    $brands                   =  $product->get('field_brand')->getValue();
    $featured                 =  $product->get('field_featured')->getValue();
    $field_marketing_messages =  $product->get('field_market_message')->getValue();
    $field_new                =  $product->get('field_new')->getValue();
    $field_stock_info         =  $product->get('field_stock_info')->getValue();
    $field_total_price        =  $product->get('field_total_price')->getValue();
    $field_new_feature        =  $product->get('field_featured')->getValue();
    $field_lester             =  $product->get('field_lester')->getValue()[0]['value'];
    $field_interchange        =  $product->get('field_interchange')->getValue()[0]['value'];
    $field_call_price     =  $product->get('field_call_price')->getValue()[0]['value'];
    $marketing_messages           =  "";
    if($field_call_price == null){
      $field_call_price = 'off';
    }
 $urldata=$connection->query("select dynamic_url from catapult_config_meta");
      $urlarr = $urldata->fetchAssoc();
///modified //////
 /*   $get_imgpath = $connection->query("SELECT * FROM `catapult_parts_img_link` where part_no='".$partnos[0]['value']."' and img_flag=1 " );
       $roww = $get_imgpath->fetchAssoc();

        $pimage                   = $roww['part_img'];*/ 

/////original balaji///
    //$pimage                   =  file_create_url($product->field_product_image->entity->getFileUri());
        $pimage =$urlarr['dynamic_url'].$product->get('field_product_image')->getValue()[0]['value'];
        $slider_image=$product->get('field_image_one')->getValue()[0]['value'];

        $pimagename=$product->get('field_product_image')->getValue()[0]['value'];
   
    $entity_manager           = \Drupal::entityManager();
    $price                    = "";
    $pricesss                 = "";
    $counts_variants =0;
    /*$field_marketing_messages1 = $field_marketing_messages[0]['value'];*/
    foreach($product->getVariationIds() as $value){
      $counts_variants = $counts_variants+1;
      $product_variation = $entity_manager->getStorage('commerce_product_variation')->load((int)$value);
      $data = $product_variation->get('price')->getValue();
      if(!empty($data) ) {
        foreach($data as $value){
          if($pricesss != ""){
            $pricesss .= ",";
          }
          $pricesss .= $value['number'];
        }
      }
    }
    if($counts_variants == 32){
      $all_variant = 1;
    }
    $productname  =  $pname[0]['value'];
    $partno       =  $partnos[0]['value'];
    $vid          = 'category';
    $cid          = $categorys[0]['target_id'];
    $terms        = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);
    foreach ($terms as $term) {
      $term_obj = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($cid);
      $category_name = $term->name;
    }
    $vid          = 'brand';
    $bid           = $brands[0]['target_id'];
    $terms        = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);
    foreach ($terms as $term) {
      $term_obj = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($bid);
      $brand_name = $term->name;
    }
   // $marketing_messages           = $field_marketing_messages[0]['target_id'];
    if( array_key_exists(0,$field_marketing_messages ) ){      
      $marketing_messages           = $field_marketing_messages[0]['target_id'];
    }

    $data1[]      = array('product_id'=>$id,
                          'productname'=>$productname,
                          'partno'=>$partno,
                          'category_value'=>$category_name,
                          'brand'=>$brand_name,
                          'featured'=>$featured[0]['value'],
                          'pimage'=>$pimage,
                          'price'=>$pricesss,
                          'category_id'=>$cid,
                          'brand_id'=>$bid,
                          'body'=>$body,
                          'field_market_message'=>$marketing_messages,
                          'field_new'=>$field_new[0]['value'],
                          'field_new_feature'=>$field_new_feature[0]['value'],
                          'field_stock_info'=>$field_stock_info[0]['value'],
                          'field_total_price'=>substr($field_total_price[0]['number'], 0, -4),
                          'all_variant'=>$all_variant,
                          'field_lester'=>$field_lester,
                          'field_interchange'=>$field_interchange,
                          'field_call_price'=>$field_call_price,
                          'slider_image'=>$slider_image,
                          'pimagename'=>$pimagename
    );
    echo json_encode($data1);
    die();
  }
   public function deletefullproduct(){
    $connection  = \Drupal::database();
    $delete_product = $_POST['id'];
    $entity_manager = \Drupal::entityManager();
    $product      = \Drupal\commerce_product\Entity\Product::load($delete_product);
    $array_check  = [];
    $error = 0;
    $purchased_entity = 0;
    foreach($product->getVariationIds() as $key=>$value){
      $array_check[] = $value;
    }
     $elastic = $GLOBALS['partdetail'];
   /* echo "<pre>";
    print_r($array_check);*/
    /*$explodes = implode(",",$array_check);

    $query = $connection->query("select count(purchased_entity) as entitys from commerce_order_item where purchased_entity
    in(".$explodes.")");


    while ($row = $query->fetchAssoc()) {

     $purchased_entity = $row['entitys'];
    }

    if($purchased_entity > 0){
      $error = 1;
    }else{*/
      $query = $connection->query("update commerce_product set delete_flag = 1 where product_id='".$delete_product."'");
       $params = [
                'index' => 'xclutchcpindex',
                'type' => 'products',
                'id' => $delete_product
                 ];
      $response = $elastic->delete($params);
      drupal_flush_all_caches();
      
      $error = 0;
    //}
    echo $error;
    die();

  }

  public function productupdate(){
    $ids                    = $_POST['id'];
    if($_POST['values'] == 1){
      $featured = 0;
    }else{
      $featured = 1;
    }
    $productss      = \Drupal\commerce_product\Entity\Product::load($ids);

    $productss->field_featured   = $featured;
    $productss->save();
    die();
  }

  public function categoryservice(){
    $vid   = 'category';
    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);
    foreach ($terms as $term){
     $term_data[] = array(
      'id' => $term->tid,
      'name' => $term->name
     );
    }
    echo json_encode($term_data);
    die();
  }

   public function getcategoryid($titles){
    $vid   = 'category';
    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);
    $id = "";
    foreach ($terms as $term){
      if($term->name == $titles){
        $id =  $term->tid;
      }
    }
    echo $id;
  }

  public function brandservice(){
    $vid   = 'brand';
    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);
    foreach ($terms as $term){
     $term_data[] = array(
      'id' => $term->tid,
      'name' => $term->name
     );
    }
    echo json_encode($term_data);
    die();
  }

  public function getallcolors(){
    $vid   = 'color_parent';
    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);
    foreach ($terms as $term){
     $term_data[] = array(
      'tid' => $term->tid,
      'tname' => $term->name
     );
    }
    echo json_encode($term_data);
    die();
  }

  public function getallcolors_select($target_id,$sku,$prices,$field_total_price){
    global $base_url;
    $vid   = 'color_parent';
    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);
    $select_value = "<div class='variation_id'>
                        <div class='row form-group'>
                          <div class='col-md-3 col-lg-4 label-text'>Colors<span class='red-text'>*</span></div>
                            <div class='col-md-6 col-lg-6 colorselect'>";
    $select_value          .= "<select name='color[]' required class='form-control'>";
    foreach ($terms as $term){
      if($term->tid != 152){
        if($target_id == $term->tid){
          $select_value .= "<option selected value=".$term->tid.">".$term->name."</option>";
        }else{
          $select_value .= "<option value=".$term->tid.">".$term->name."</option>";
        }
      }      
    }
    $select_value .=  "</select></div></div>";
    $price_value   = $prices-$field_total_price;
   // die();
   /* if ((int) $price_value == $price_value) {
      $prices = $price_value;
    }else{
      $prices = substr($price_value, 0, -4);

    }
*/
     $prices = $price_value;






    $select_value .= "<div class='row form-group'>
                                    <div class='col-md-3 col-lg-4 label-text'>Price<span class='red-text'>*</span></div>
                                    <div class='col-md-6 col-lg-5'>
                                      <input required type='number' step='0.01' value=".$prices." name='price[]' class='form-control'>
                                    </div>
                                     <div class='col-md1 wd_ht'>
                                      <span id='variation_add'><img src='../themes/APA Admin Theme/images/Add-icon.png'></span>
                                      <span id='variation_remove'><img src='../themes/APA Admin Theme/images/icons/table-delete.png'></span>
                                    </div>
                                  </div>";
    $select_value .= "</div>";
    return $select_value;
    /* $select_value; */
  }
///original function///
 public function getsinglevaritaion(){
    global $base_url;

    //echo $base_url;
    if( ($_POST['id'] != "") && isset($_POST['id']) ){
      $id = $_POST['id'];
      $product      = \Drupal\commerce_product\Entity\Product::load($id);
      $field_total_price      =  $product->get('field_total_price')->getValue()[0]['number'];
      $entity_manager = \Drupal::entityManager();
      ///////////Jp commented//////////////////
      //$image_url =" <div class='variation_class_image'><div class='variation_imagess'>";

      //$sliderimg = $product->get('field_image_one')->getValue();
      // $sliderimg =$product->get('field_total_price')->getValue()[0]['value'];
      // /*echo count($sliderimg);
      // exit;*/
      // $i=0;
      // foreach( $sliderimg as $key=>$value){
      //   $target_id = $value['target_id'];
      //   $file = \Drupal\file\Entity\File::load($target_id);
      //   $path = $file->getFileUri();
      //   $fileid=$file->id();
      //   $spliturl=$base_url."/sites/default/files/".str_replace('public://','',$path);
      //   $image_url .="<div class='row form-group'>
      //                                 <div class='col-md-3 col-lg-4 label-text'>Add Slider Image";
      //   if($i <= 0){
      //      $image_url .="<span class='red-text'>*</span></div>";
      //    }else{
      //       $image_url .="</div>";
      //    }  
      //   $image_url .="<div class='col-md-6 col-lg-6'>
      //                   <div class='file-upload'>
      //                     <div class='file-select'>
      //                       <div class='file-select-button' id='fileName'>Choose File</div>
      //                       <div class='file-select-name' id='noFile'>No file chosen...</div>
      //                       <input type='file' class='upl_cl' name='sliderimage[]'>
      //                       <input type='hidden' name='fileid[]' value=".$fileid.">
      //                     </div>
      //                   </div>
      //                   <img style='height:50px;' src=".$spliturl.">
      //                 </div>";

      //     if($i <= 0){
      //       $image_url .= "<div class='col-md1 wd_ht'>
      //                       <span id='variation_add_image'><img src='../themes/APA Admin Theme/images/Add-icon.png'></span>
      //                      </div>
      //                 </div>"; 
      //     }else{
            
      //       $image_url .= "<div class='col-md1 wd_ht'>
      //                       <span id='variation_add_image'><img src='../themes/APA Admin Theme/images/Add-icon.png'></span>
      //                       <span id='variation_image_remove'><img src='../themes/APA Admin Theme/images/icons/table-delete.png'></span>
      //                     </div>
      //                 </div>";
      //     }
      //     $i++;

        
      // }
      //  $image_url .= " </div></div>";
      //echo file_create_url($product->field_image_one->entity->getFileUri());
      ///////////Jp commented//////////////////

      $data5 = " <div id='firstvariation'>";
      foreach($product->getVariationIds() as $key=>$value){
        $product_variation = $entity_manager->getStorage('commerce_product_variation')->load((int)$value);
        $prices = $product_variation->get('price')->getValue();
        $sku = $product_variation->get('sku')->getValue();
        $data_color = $product_variation->get('field_color')->getValue();
        $skuvalue = $sku[0]['value'];
        if($data_color[0]['target_id'] != 152){
          $data5 .= $this->getallcolors_select($data_color[0]['target_id'],$skuvalue,$prices[0]['number'],$field_total_price);
        }
      }
      $data5 .= "</div>";
      //$data5 .=  "|".$image_url;
      echo $skuvalue;
      die();
    }
  }

  ///modified function balaji///
   public function getsinglevaritaion1(){
    global $base_url;
    $connection = \Drupal::database();
    //echo $base_url;
    if( ($_POST['id'] != "") && isset($_POST['id']) ){
      $id = $_POST['id'];
      $product      = \Drupal\commerce_product\Entity\Product::load($id);
      
      $field_total_price      =  $product->get('field_total_price')->getValue()[0]['number'];
      $entity_manager = \Drupal::entityManager();
      $image_url =" <div class='variation_class_image'><div class='variation_imagess'>";
      $product_no = $product->get('field_partno')->getValue()[0]['value'];
       $get_imgpath = $connection->query("SELECT * FROM `catapult_parts_img_link` where part_no='".$product_no."' and img_flag=2 " );
     //  $sliderimg = $get_imgpath->fetchAssoc();
 
      /*echo count($sliderimg);
      exit;*/
      $i=0;
      while( $sliderimg  = $get_imgpath->fetchAssoc()){
      //  $target_id = $value['target_id'];
       // $file = \Drupal\file\Entity\File::load($target_id);
       // $path = $file->getFileUri();
       // $fileid=$file->id();
       // $spliturl=$base_url."/sites/default/files/".str_replace('public://','',$path);
        $image_url .="<div class='row form-group'>
                                      <div class='col-md-3 col-lg-4 label-text'>Add Slider Image Link";
        if($i <= 0){
           $image_url .="<span class='red-text'>*</span></div>";
         }else{
            $image_url .="</div>";
         }  
        $image_url .="<div class='col-md-6 col-lg-6'>
                      <div class='file-upload'>                   
                      <input type='text' class='form-control' name='sliderimage[]' value='".$sliderimg['part_img']."'>                        
                        </div>
                      
                      </div>";

          if($i <= 0){
            $image_url .= "<div class='col-md1 wd_ht'>
                            <span id='variation_add_image'><img src='../themes/APA Admin Theme/images/Add-icon.png'></span>
                           </div>
                      </div>"; 
          }else{
            
            $image_url .= "<div class='col-md1 wd_ht'>
                            <span id='variation_add_image'><img src='../themes/APA Admin Theme/images/Add-icon.png'></span>
                            <span id='variation_image_remove'><img src='../themes/APA Admin Theme/images/icons/table-delete.png'></span>
                          </div>
                      </div>";
          }
          $i++;

        
      }
       $image_url .= " </div></div>";
      //echo file_create_url($product->field_image_one->entity->getFileUri());

      $data5 = " <div id='firstvariation'>";
      foreach($product->getVariationIds() as $key=>$value){
        $product_variation = $entity_manager->getStorage('commerce_product_variation')->load((int)$value);
        $prices = $product_variation->get('price')->getValue();
        $sku = $product_variation->get('sku')->getValue();
        $data_color = $product_variation->get('field_color')->getValue();
        $skuvalue = $sku[0]['value'];
        if($data_color[0]['target_id'] != 152){
          $data5 .= $this->getallcolors_select($data_color[0]['target_id'],$skuvalue,$prices[0]['number'],$field_total_price);
        }
      }
      $data5 .= "</div>";
      $data5 .=  "|".$image_url;
      echo $data5.'|'.$skuvalue;
      die();
    }
  }

  public function checkproducts_inorder(){
    $connection     = \Drupal::database();
    $delete_product = $_POST['id'];
    $entity_manager = \Drupal::entityManager();
    $product        = \Drupal\commerce_product\Entity\Product::load($delete_product);
    $array_check    = [];
    $error          = 0;
    $purchased_entity = 0;
    foreach($product->getVariationIds() as $key=>$value){
      $array_check[] = $value;
    }
    $explodes = implode(",",$array_check);
    if($explodes != ""){
      $query = $connection->query("select count(purchased_entity) as entitys from commerce_order_item where purchased_entity
      in(".$explodes.")");
      while ($row = $query->fetchAssoc()) {
        $purchased_entity = $row['entitys'];
      }
      if($purchased_entity > 0){
        $error = 1;
      }else{
        $error = 0;
      }
    }else{
       $error = 0;
    }
    echo $error;
    die();
  }

  public function check_partno_exist(){
    if( ($_POST['partno'] != "") && isset($_POST['partno']) ){
      $partno = $_POST['partno'];
      $data_count = 0;
      $connection = \Drupal::database();
      $query      = $connection->query("SELECT fp.entity_id  FROM commerce_product__field_partno  fp INNER JOIN  `commerce_product`  cp ON fp.entity_id=cp.product_id
        WHERE fp.field_partno_value = '".$partno."' AND cp.delete_flag='0'");
      while($row = $query->fetchAssoc()) {
        $data_count = $row['entity_id'];
      }
      if($data_count == 0){
        echo "not_exist";

      }else{
        echo "exist";
      }
    }
    die();
  }
  public function check_partno_exist_upload($partno){
    $data_count = 0;
    $connection = \Drupal::database();
    $query      = $connection->query("SELECT fp.entity_id  FROM commerce_product__field_partno  fp INNER JOIN  `commerce_product`  cp ON fp.entity_id=cp.product_id
      WHERE fp.field_partno_value = '".$partno."' AND cp.delete_flag='0'");
    while($row = $query->fetchAssoc()) {
      $data_count = $row['entity_id'];
    }
    if($data_count == 0){
      return 1;

    }else{
      return 0;
    }

  }

  /*public function upload_page_temps(){
    $error_array = [];
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', '0');
    
    global $base_url;
    $msg = "";
    $connection = \Drupal::database();
    $query_unit = $connection->query("select * from  part_temp limit 0,4000");
    $r = 0;
    while($row_unit = $query_unit->fetchAssoc()) {
      $error = 0;
      $r++;
      $field_partno       = $row_unit['partno'];
      $field_total_price  = rand(5,10);
      $sku                = $row_unit['partno'];
      $Interchange        = $row_unit['comp_part'];
      $lester             = '';

      $query_counts = $connection->query("select count(*) as counts from  commerce_product__field_partno where field_partno_value = '".$field_partno."' ");

      while($row_counts = $query_counts->fetchAssoc()) {
        $counts = $row_counts['counts'];
      }

      if($counts == 0 ){
          $field_product_name = "Battery";
          $editor1            = '<p>Every AutoTech Engineering unit is assembled 100% in Riverside, California. With over 100
            years of combined, “on hands” experience and the fact that we have been in business for over
            28 years has allowed us to source and supply the highest quality component parts to ensure
            your unit exceeds expectations. All are plug and play unless over wise stated. Some
            modifications such as adding spacers or plug harness are included at no extra cost. Voltage is
            set to OEM minimum specifications on each unit ordered unless otherwise requested. If you
            need higher voltage, contact us before ordering online.</p>';

          $query_interchange = $connection->query("select assetname from part_asset where partno='".$field_partno."' ;");

          $k = 0;
          $filew = [];
          ini_set('display_errors',1);
          $filew = [];
          $image_count = 0;
              while ($row_interchange = $query_interchange->fetchAssoc()) {
                
                if($k > 25) { break; }
                
                
                  if(!file_get_contents("public://digitalassetsimages/".$row_interchange['assetname'])){
                      $error = 1;
                    
                    }else{
                      $image_count = 1;
                     
                          //$Interchange =   $row_interchange['competitorpartno'];
                          $data = file_get_contents("public://digitalassetsimages/".$row_interchange['assetname']);
                          $file_product = file_save_data($data, "public://upload_parts/".$row_interchange['assetname'], FILE_EXISTS_REPLACE);
                        }
                        $slider_data = file_get_contents("public://digitalassetsimages/".$row_interchange['assetname']);
                        $slider_product = file_save_data($slider_data, "public://upload_parts/".$row_interchange['assetname'], FILE_EXISTS_REPLACE);
                        $filew[] = array(
                                    'target_id' => $slider_product->id(),
                                    'alt' => 'teee',
                                    'title' => "My title"
                        );
                    
                $k++;
                
              }
              if($image_count != 0){

              
                $product = Product::create([
                  'type' => 'default',
                  'body'  => ['value'=> $editor1,'format'=> 'basic_html'],
                  'title' => t($field_product_name .' | '. $field_partno),
                  'field_product_name' => $field_product_name .' | '.$field_partno,
                   'field_partno'=>$field_partno,
                   'field_category_name' => 129,
                   'field_featured'=>1,
                   'field_brand' => 243,
                   'field_stock_info' => 1,
                   'field_marketing_messages'=> 0,
                   'field_new'=>1,
                   'field_sku' => $sku,
                   'field_price' => 15,
                   'field_total_price'=> new Price((string)$field_total_price, 'USD'),
                   'field_product_image' => array(
                                'target_id' => $file_product->id(),
                                'alt' => 'test',
                                'title' => "My title",
                              ),
                   'field_image_one' => $filew,
                   'field_lester'=>$lester,
                   'field_interchange'=>$Interchange
                ]);
                $product->save();
                $id =  $product->id();
                $product_new_data =Product::load($id);
                $vid    = 'color_parent';
                $terms  = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);
                foreach ($terms as $term) {
                  $color_price = ($term->tid==152)?0:15;
                  $prices      =  $field_total_price+$color_price;
                  $color       =  $term->tid;
                  $variation   = ProductVariation::create([
                                            'type' => 'default',
                                            'sku' => $sku,
                                            'price' => new Price((string)$prices , 'USD'),
                                            'field_color'=>$color
                                          ]);

                  $variation->save();
                  $product_new_data->addVariation($variation);
                }
                $product_new_data->save();
              }else{
                $error_array[] = $field_partno;
              }
              $error = 0;
              $k = 0;

      }else{
        $missed_array[] = $field_partno;
      }
    }
    echo "<pre>";
    print_r($error_array);
    print_r($missed_array);
    die;
  }*/

  public function upload_page_temps(){
    $error_array = [];
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', '0');
    
    global $base_url;
    $msg = "";
    $connection = \Drupal::database();
    $query_unit = $connection->query("select * from  part_temp");
    $r = 0;
    while($row_unit = $query_unit->fetchAssoc()) {
      $error = 0;
      $r++;
      $field_partno       = $row_unit['partno'];
      $field_total_price  = 295;
      $sku                = $row_unit['partno'];
      $Interchange        = '';
      $lester             = '';

      $query_counts = $connection->query("select count(*) as counts from  commerce_product__field_partno where field_partno_value = '".$field_partno."' ");

      while($row_counts = $query_counts->fetchAssoc()) {
        $counts = $row_counts['counts'];
      }

      if($counts == 0 ){
          $field_product_name = "Trim Illusion";
          $editor1            = '<p>Every AutoTech Engineering unit is assembled 100% in Riverside, California. With over 100
            years of combined, “on hands” experience and the fact that we have been in business for over
            28 years has allowed us to source and supply the highest quality component parts to ensure
            your unit exceeds expectations. All are plug and play unless over wise stated. Some
            modifications such as adding spacers or plug harness are included at no extra cost. Voltage is
            set to OEM minimum specifications on each unit ordered unless otherwise requested. If you
            need higher voltage, contact us before ordering online.</p>';

          $query_interchange = $connection->query("select assetname from part_asset where partno='".$field_partno."' ;");

          $k = 0;
          $filew = [];
          ini_set('display_errors',1);
          $filew = [];
          $image_count = 0;
              while ($row_interchange = $query_interchange->fetchAssoc()) {
                
                if($k > 25) { break; }
                
                
                  if(!file_get_contents("public://trim_image/".$row_interchange['assetname'])){
                      $error = 1;
                    
                    }else{
                      $image_count = 1;
                     
                          //$Interchange =   $row_interchange['competitorpartno'];
                          $data = file_get_contents("public://trim_image/".$row_interchange['assetname']);
                          $file_product = file_save_data($data, "public://upload_parts/".$row_interchange['assetname'], FILE_EXISTS_REPLACE);
                        }
                        $slider_data = file_get_contents("public://trim_image/".$row_interchange['assetname']);
                        $slider_product = file_save_data($slider_data, "public://upload_parts/".$row_interchange['assetname'], FILE_EXISTS_REPLACE);
                        $filew[] = array(
                                    'target_id' => $slider_product->id(),
                                    'alt' => 'teee',
                                    'title' => "My title"
                        );
                    
                $k++;
                
              }
              if($image_count != 0){

                /*echo "test";
                die();
                echo "<br>";*/
                $product = Product::create([
                  'type' => 'default',
                  'body'  => ['value'=> $editor1,'format'=> 'basic_html'],
                  'title' => t($field_product_name .' | '. $field_partno),
                  'field_product_name' => $field_product_name .' | '.$field_partno,
                   'field_partno'=>$field_partno,
                   'field_category_name' => 248,
                   'field_featured'=>1,
                   'field_brand' => 249,
                   'field_stock_info' => 1,
                   'field_marketing_messages'=> 0,
                   'field_new'=>1,
                   'field_sku' => $sku,
                   'field_price' => 15,
                   'field_total_price'=> new Price((string)$field_total_price, 'USD'),
                   'field_product_image' => array(
                                'target_id' => $file_product->id(),
                                'alt' => 'test',
                                'title' => "My title",
                              ),
                   'field_image_one' => $filew,
                   'field_lester'=>$lester,
                   'field_interchange'=>$Interchange
                ]);
                $product->save();
                $id =  $product->id();
                $product_new_data =Product::load($id);
                $vid    = 'color_parent';
                $terms  = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);
                foreach ($terms as $term) {
                  $color_price = ($term->tid==152)?0:15;
                  $prices      =  $field_total_price+$color_price;
                  $color       =  $term->tid;
                  $variation   = ProductVariation::create([
                                            'type' => 'default',
                                            'sku' => $sku,
                                            'price' => new Price((string)$prices , 'USD'),
                                            'field_color'=>$color
                                          ]);

                  $variation->save();
                  $product_new_data->addVariation($variation);
                }
                $product_new_data->save();
              }else{
                $error_array[] = $field_partno;
              }
              $error = 0;
              $k = 0;

      }else{
        $missed_array[] = $field_partno;
      }
    }
    echo "<pre>";
    print_r($error_array);
    print_r($missed_array);
    die;
  }


  
  /*public function upload_page_temps(){



    $error_array = [];
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', '0');
    $error = 0;
    global $base_url;
    $msg="";
    $connection = \Drupal::database();

    $datas = Array
(
  'HP-14021-270',
  'HP-8248-270',
  ''
);

    
    foreach ($datas as  $value) {
        $query_counts = $connection->query("select count(*) as counts from  commerce_product__field_partno where field_partno_value = '".$value."' ");
        while ($row_counts = $query_counts->fetchAssoc()) {
          $counts = $row_counts['counts'];
        }

        $r = 0;

        if($counts == 0){

          $query_unit = $connection->query("select * from  unit_cost where Autotech_Part_No = '".$value."' ");


          while ($row_unit = $query_unit->fetchAssoc()) {
            $field_partno       = $row_unit['Autotech_Part_No'];
            $field_total_price  = $row_unit['Unit_Cost'];
            $sku                = $row_unit['SKU'];
            $lester             = $row_unit['Lester'];
            $field_product_name = "Alternators";

            $editor1            = '<p>Every AutoTech Engineering unit is assembled 100% in Riverside, California. With over 100
              years of combined, “on hands” experience and the fact that we have been in business for over
              28 years has allowed us to source and supply the highest quality component parts to ensure
              your unit exceeds expectations. All are plug and play unless over wise stated. Some
              modifications such as adding spacers or plug harness are included at no extra cost. Voltage is
              set to OEM minimum specifications on each unit ordered unless otherwise requested. If you
              need higher voltage, contact us before ordering online.</p>';


          

          $query_interchange = $connection->query("select CompetitorPartno from partnumber as pn where Autotech_Part_No = '".$field_partno."' ORDER BY RAND() ;");




            $k = 0;
            $filew = [];
            ini_set('display_errors',1);
            $filew = [];
            $file_product = '';
            while ($row_interchange = $query_interchange->fetchAssoc()) {
             
                if($k > 5) break;
                if(!file_get_contents("public://digitalassetsimages/no_image_icon.PNG")){
                     $error = 1;
                }else{
                  if($k == 0 ){
                    $Interchange =   $row_interchange['CompetitorPartno'];
                    $data = file_get_contents("public://digitalassetsimages/no_image_icon.PNG");
                    $file_product = file_save_data($data, "public://upload_parts/no_image_icon.PNG", FILE_EXISTS_REPLACE);
                  }
                  $slider_data = file_get_contents("public://digitalassetsimages/no_image_icon.PNG");
                  $slider_product = file_save_data($slider_data, "public://upload_parts/no_image_icon.PNG", FILE_EXISTS_REPLACE);
                  $filew[] = array(
                              'target_id' => $slider_product->id(),
                              'alt' => 'teee',
                              'title' => "My title"
                  );

                }

              $k++;
            }


            if($error != 1 && $k > 0 && $file_product != ''){

                $product = Product::create([
                  'type' => 'default',
                  'body'  => ['value'=> $editor1,'format'=> 'basic_html'],
                  'title' => t($field_product_name .' | '. $field_partno),
                  'field_product_name' => $field_product_name .' | '.$field_partno,
                   'field_partno'=>$field_partno,
                   'field_category_name' => 129,
                   'field_featured'=>1,
                   'field_brand' => 144,
                   'field_stock_info' => 1,
                   'field_marketing_messages'=> 2,
                   'field_new'=>1,
                   'field_sku' => $sku,
                   'field_price' => 15,
                   'field_total_price'=> new Price((string)$field_total_price, 'USD'),
                   'field_product_image' => array(
                                'target_id' => $file_product->id(),
                                'alt' => 'test',
                                'title' => "My title",
                              ),
                   'field_image_one' => $filew,
                   'field_lester'=>$lester,
                   'field_interchange'=>$Interchange
                ]);
                $product->save();
                $id =  $product->id();
                $product_new_data =Product::load($id);
                $vid    = 'color_parent';
                $terms  = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);
                foreach ($terms as $term) {
                  $color_price=($term->tid==152)?0:15;
                  $prices =  $field_total_price+$color_price;
                  $color  =  $term->tid;
                  $variation   = ProductVariation::create([
                                            'type' => 'default',
                                            'sku' => $sku,
                                            'price' => new Price((string)$prices , 'USD'),
                                            'field_color'=>$color
                                          ]);

                  $variation->save();
                  $product_new_data->addVariation($variation);
                }
              $product_new_data->save();

            }else{
              $error_array[] = $field_partno;
            }
            $error = 0;
            $k = 0;
            break;

          }

        }else{

        }
    }
    echo "<pre>";
    print_r($error_array);


    die;
  }*/

 /* public function parnto_excel_download(){
    $connection = \Drupal::database();
    $query_counts = $connection->query("SELECT field_partno_value,entity_id FROM commerce_product__field_partno");
    $entity_manager = \Drupal::entityManager();
    $r= 0;
    $array_data = [];
    while ($row_counts = $query_counts->fetchAssoc()) {
      $field_partno_value = $row_counts['field_partno_value'];
      $entity_id = $row_counts['entity_id'];
      $product   = \Drupal\commerce_product\Entity\Product::load($entity_id);
      foreach( ($product->get('field_image_one')->getValue()) as $key=>$value){
        $target_id = $value['target_id'];
        $file = \Drupal\file\Entity\File::load($target_id);
        $path = $file->getFilename();
        $array_data[] = array('filename'=>$path,'partno'=>$field_partno_value);
      }
      $r++;
    }
    $file = fopen("image_check.csv","w");
    foreach ($array_data as $line) {
      fputcsv($file, $line);
    }
    die();
  }*/

  public function parnto_excel_download(){
    $connection = \Drupal::database();
    $query_counts = $connection->query("SELECT field_partno_value,entity_id FROM commerce_product__field_partno limit 1401,500");
    $entity_manager = \Drupal::entityManager();
    $r              = 0;
    $array_data     = [];
    $r =1;
    $array_data[] = array('parnto'=>'Partno',
                            'price'=>'Price',
                            'lester'=>'Lester',
                            'sku'=>'SKU',
                            'image'=>'Image Available'
                           );
    while ($row_counts = $query_counts->fetchAssoc()) {
      $field_partno_value       = $row_counts['field_partno_value'];
      $entity_id                = $row_counts['entity_id'];
      $product                  = \Drupal\commerce_product\Entity\Product::load($entity_id);
      $field_total_price        = $product->get('field_total_price')->getValue()[0]['number'];
      $field_lester             = $product->get('field_lester')->getValue()[0]['value'];
      $field_interchange        = $product->get('field_interchange')->getValue()[0]['value'];
      $image                    = "";
      $pimage                   =  file_create_url($product->field_product_image->entity->getFileUri());
      if($pimage == "http://localhost/autotech1/sites/default/files/upload_parts/no_image_icon.PNG"){
        $pimage = "Image Not Available";
      }else{
        $pimage = "Image Available";
      }
      /*foreach( ($product->get('field_image_one')->getValue()) as $key=>$value){
        $target_id = $value['target_id'];
        $file = \Drupal\file\Entity\File::load($target_id);
        $path  = $file->getFilename();
        if($path=='no_image_icon.PNG'){
          $image = "No";
          break;
        }else{
          $image = "Yes";
          break;
        }
      }  */
      foreach($product->getVariationIds() as $value){
        $product_variation = $entity_manager->getStorage('commerce_product_variation')->load((int)$value);
        $sku               = $product_variation->get('sku')->getValue()[0]['value'];
      }
      $array_data[] = array('parnto'=>$field_partno_value,
                            'price'=>$field_total_price,
                            'lester'=>$field_lester,
                            'sku'=>$sku,
                            'image'=>$pimage
                           );

      $r++; 
       
    }    

    $file = fopen("part_detail_work_1800.csv","w");
    foreach ($array_data as $line) {
      fputcsv($file, $line);
    }
    die();
  }

  public function API_Search_Result(){
      $url = $_POST['url'];
      $connection = curl_init();
      curl_setopt($connection, CURLOPT_URL, $url);
      curl_setopt($connection, CURLOPT_POST, 0);
      curl_setopt($connection, CURLOPT_HEADER, 0);
      curl_setopt($connection, CURLOPT_RETURNTRANSFER, 1);
      $response = curl_exec($connection);
      $xml = simplexml_load_string($response);

      $result = [];
      foreach ($xml->searchResult->item as $value) {
          $title = (string)$value->title;
          $title = preg_replace('/\\\\/', '', $title);
          $title = str_replace("'", "\'" ,$title);
          $title = str_replace('"', '\"' ,$title);

          $currentPrice = (string)$value->sellingStatus->currentPrice;
          $shippingPrice = (string)$value->shippingInfo->shippingServiceCost;
          if ($shippingPrice == ''){
              $shippingPrice = 0;
          }
          $totalPrice = $currentPrice + $shippingPrice;

          $result[] = array('itemid'  => (string)$value->itemId,
                          'title'   => $title,
                          'galleryURL'  => (string)$value->galleryURL,
                          'sellerUserName'  => (string)$value->sellerInfo->sellerUserName,
                          'currentPrice'  => $currentPrice,
                          'shippingPrice'  => $shippingPrice,
                          'totalPrice'  => round($totalPrice,2)
                    );
      }
      
      echo json_encode($result);
      die();
  }

  public function CLP_keyword_save(){
  
    $partno          = $_POST['partno'];
    $or_keyword      = $_POST['or_keyword'];
    $and_keyword     = $_POST['and_keyword'];
    $exclude_keyword = $_POST['exclude_keyword'];
    $percentage      = $_POST['percentage'];
    $condition       = !empty($_POST['condition']) ? $_POST['condition'] : 0;
    $freeshiping     = !empty($_POST['freeshiping']) ? $_POST['freeshiping'] : 0;
  
    $connection = \Drupal::database();
    $part_exist = $connection->query("SELECT 1 FROM catapult_dive_keywords where partno='".$partno."'");
    $exist_count = $part_exist->fetchAssoc();

    if($exist_count[1] >= 1){
        $part_res = $connection->query("UPDATE catapult_dive_keywords SET keyword_or='".$or_keyword."',keyword_and='".$and_keyword."', exclude_keyword='".$exclude_keyword."', percentagee='".$percentage."' , conditionn='".$condition."', 
          freeshipping='".$freeshiping."' WHERE partno ='".$partno."'");
    }else{
        $part_res = $connection->query("INSERT INTO catapult_dive_keywords (partno, keyword_or,keyword_and,exclude_keyword, percentagee, conditionn, freeshipping, created_by,created_on) VALUES('".$partno."','".$or_keyword."','".$and_keyword."','".$exclude_keyword."','".$percentage."','".$condition."','".$freeshiping."','site_admin',NOW())");
    }
   
    echo json_encode(array('res'=>'success'));
    die();
  }

  public function delete_entire_product(){
    ini_set('max_execution_time', '0');
    ini_set('display_errors',1);
    ini_set('memory_limit', '-1');
    $connection = \Drupal::database();
    $i=1;
    $query = $connection->query("SELECT DISTINCT `field_partno_value` FROM `commerce_product__field_partno`");
    while($row = $query->fetchAssoc()){
          $nodeid = $row['field_partno_value'];
          $res = array($nodeid=>$nodeid);
          entity_delete_multiple('commerce_product', $res);
          echo $i."----".$nodeid."</br>";
          $i++;
   
    }
    die(); 
  }

   public function marketservice(){
    $vid   = 'marketmessage';
    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);
    $term_data = [];
    foreach ($terms as $term){
     $term_data[] = array(
      'id' => $term->tid,
      'name' => $term->name
     );
    }
    echo json_encode($term_data);
    die();
  }

   public function get_category($category,$category_id){
    $connection = \Drupal::database();
    $get_category = $connection->query("SELECT DISTINCT name FROM `taxonomy_term_field_data` 
       WHERE vid ='".$category."' AND tid ='".$category_id."'" );
    //$result = $get_category;
    
    $row = $get_category->fetchAssoc();
    return $row['name'];
  }

  public function parts_image_update(){
    ini_set('max_execution_time', '0');
    ini_set('display_errors',1);
    ini_set('memory_limit', '-1');    
    $elastic = $GLOBALS['partdetail'];
    $connection = \Drupal::database();
    $i=1;
    $query = $connection->query("select * from(select * from catapult_parts_img )a join (SELECT entity_id,field_partno_value FROM `commerce_product__field_partno`)b on partno=field_partno_value");
    while($row = $query->fetchAssoc()){
          $nodeid = $row['entity_id'];
          $partid = $row['field_partno_value'];
          if($row['part_main_img'] != ""){
                 $data = file_get_contents("public://digitalassetsimages/".$row['part_main_img']);
                 $file_product = file_save_data($data, "public://upload_parts/".$row['part_main_img'], FILE_EXISTS_REPLACE);
            }
            if($row['part_slider_img']  != ""){
                $exp_slider = explode(',', $row['part_slider_img']);
                $tem_exp_slider = (count($exp_slider)>5)?array_slice($exp_slider,5):$exp_slider;
                $slider_images=explode(',', $row['part_slider_img']);
                $filew=array();
                foreach ($slider_images as $key => $value) {
                     $slider_data = file_get_contents("public://digitalassetsimages/".$value);
                     $slider_product = file_save_data($slider_data, "public://upload_parts/".$value, FILE_EXISTS_REPLACE);
                     $filew[] = array(
                                'target_id' => $slider_product->id(),
                                'alt' => 'teee',
                                'title' => "My title"
                            );
                }
              }                
                $node                           = product::load($nodeid);
        
                  $field_product_image = array(
                      'target_id' => $file_product->id(),
                      'alt' => 'test',
                      'title' => "My title"
                  );
                  $node->field_product_image = $field_product_image;
                
                $node->field_image_one = $filew;
                $node->save();
               $elastic_body =[];      
               $elastic_body["product_image"]=$row['part_main_img']; 
             
                   $paramss = [
                         'index' => 'xclutchcpindex',
                         'type' => 'products',
                         'id' => $nodeid,
                          'body' => [                                  
                                    'doc' => $elastic_body                  
                                  ]
                            ];
                   $elastic->update($paramss);    
 
    }
     drupal_flush_all_caches();
    die(); 
  }

////////////////////import parts without siteadmin/////
public function upload_page_two(){
    ini_set('memory_limit', '-1');
    ini_set('display_errors',1);
    ini_set('max_execution_time', '0');
    global $base_url;
      $connection = \Drupal::database();
    $msg = "";
     
    
      $vid = 'category';
      $cat_data=array();
      $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);
      foreach ($terms as $term) {
              $term_obj = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($term->tid);           
              $cat_data[$term->name] = $term->tid ;
      } 
      
      $bid = 'brand';
      $brand_data=array();
      $termss = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($bid);
    foreach ($termss as $termm) {
        $term_obj = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($termm->tid);
        $brand_data[$termm->name] = $termm->tid ;    
      }
        $get_imgpath = $connection->query("SELECT * FROM `catapult_parts_path`" );
       $roww = $get_imgpath->fetchAssoc();


      $filename ='ProductBulkUpload1.xls';
      
        $targetPath = 'site/default/files/bulk_template/'.$filename;        

        $inputFile="public://bulk_template/".$filename;
        $extension = strtoupper(pathinfo($inputFile, PATHINFO_EXTENSION));
        $inputFileType = \PHPExcel_IOFactory::identify($inputFile);
        $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($inputFile);

        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestDatarow();
        $highestColumn = $sheet->getHighestDataColumn();
        $title_array = $sheet->rangeToArray('A' . $row=1 . ':' . $highestColumn . $row=1, NULL, TRUE, FALSE);
       
        $final_error_data=array();$error_count=0;$success_count=0;

        if($highestRow<=1){
          $msg="There is no data in excel";
        }

        for ($row = 1; $row <= $highestRow; $row++){
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);

            if($rowData[0][0]!=''){
              if($row!=1){
             
                $rowData[0][1] = $rowData[0][1];
                    

              }
              $error_message=$this->excel_field_validation_external($rowData[0],$row,$title_array[0]);


              $store_error[$row]=$error_message;
              $store_error[$row]['data'][11]=$store_error[$row]['error'];

              if($row!= 1){
                if(empty($error_message['error'])){
                     $field_partno               = $rowData[0][0];
                     $editor1                    = $rowData[0][1];
                     if($rowData[0][2] == ""){
                        $field_total_price       = 0;
                        $field_call_price        = "on";
                     }else{
                       $field_total_price       = $rowData[0][2];
                       $field_call_price        = "off";
                     }                     
                     $field_product_name         = $rowData[0][5]." | ".$rowData[0][0];


                     
                     if (array_key_exists($rowData[0][5],$cat_data)) {
                           $field_category=$cat_data[$rowData[0][5]];
                        }else{
                           $field_category="";
                        } 

                        if (array_key_exists($rowData[0][4],$brand_data)) {
                           $field_brand=$brand_data[$rowData[0][4]];
                        }else{
                           $field_brand="";
                        }

                        if($rowData[0][3] != ""){
                  $result_main_img = $connection->insert('catapult_parts_img_link')->fields([
                  'part_no'  => $field_partno,
                 // 'part_img' => $roww['image_path'].$rowData[0][3],
                  'part_img' => $rowData[0][3],
                  'img_flag' => 1                
                ])->execute();
                }

                   if($rowData[0][6] != ""){
                $exp_slider = explode(',', $rowData[0][6]);
                $tem_exp_slider = (count($exp_slider)>5)?array_slice($exp_slider,5):$exp_slider;

                $slider_images=explode(',', $rowData[0][6]);
              
                foreach ($slider_images as $key => $value) {
                         $result_main_img = $connection->insert('catapult_parts_img_link')->fields([
                  'part_no'  => $field_partno,
                  //'part_img' => $roww['image_path'].$value,
                  'part_img' => $value,
                  'img_flag' => 2                
                ])->execute();
                }
              }
                  

                     $product = Product::create([
                          'type' => 'default',
                          'body'  => ['value'=> $editor1,'format'=> 'basic_html'],
                          'title' => t($field_product_name),
                          'field_product_name' => $field_product_name,
                           'field_partno'=>$field_partno,
                           'field_category_name' => $field_category,
                           'field_featured'=>1,
                           'field_brand' => $field_brand,
                           'field_stock_info' => 1,
                           'field_marketing_messages'=> 2,
                           'field_new'=>1,
                           'field_sku' => $rowData[0][8],
                           'field_price' => 15,
                           'field_total_price'=> new Price((string)$field_total_price, 'USD'),
                           'field_lester'=>$rowData[0][9],
                           'field_interchange'=>$rowData[0][10],
                           'field_call_price'=> $field_call_price
                      ]);
                      $product->save();
                      
                      $id =  $product->id();
                      $product_new_data =Product::load($id);
                      $vid    = 'color_parent';
                      $terms  = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);
                        foreach ($terms as $term) {
                           $term_obj = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($term->tid);
                          $field_color_price_values = $term_obj->get('field_color_price_values')->value;
                          //$color_price = ($term->tid==152)?0:15;
                          $prices      = $field_total_price+$field_color_price_values;
                          $color       = $term->tid;
                          $variation   = ProductVariation::create([
                                                    'type'  => 'default',
                                                    'sku'   => $rowData[0][8],
                                                    'price' => new Price((string)$prices , 'USD'),
                                                    'field_color'=>$color
                                                  ]);
                          $variation->save();
                          $product_new_data->addVariation($variation);
                        }
                        $product_new_data->save();                     
                        $success_count++;                        
                  }else{
                    $error_count++;
                    $final_error_data[]=$store_error[$row]['data'];
                  }
                }
            }

          }
          $res_file = "";
          if($error_count!=0){
            if($success_count<=0){
              $msg="Parts uploading failed";
            }else{
              $msg="Parts upload success with errors ";
            }
            $res_file = "sites/default/files/upload_excel_error/Errors.xls";
            $data = $this->excel_export($final_error_data,$res_file);
           
          }
      

      $return_data['msg']=!empty($msg)?$msg:"Part uploaded successfully";
      $return_data['file']=!empty($res_file)?$res_file:"";
      echo json_encode($return_data);die;
    
}


public function excel_field_validation_external($data,$row,$title){

      $image_array=array('jpg','jpeg','png','gif');
      $msg=array();
    

      if($row==1){
        $result_data['data']=$data;
        $result_data['error']='';
      }else{
        for ($i=0; $i <11 ; $i++) {
          if($i==0 || $i==4 || $i==5){
            if(empty($data[$i])){
              $msg[$i]=$title[$i]." must not empty";
            }
            if($i==0){
                $res = $this->check_partno_exist_upload($data[$i]);
                if($res == 'exist'){
                  $msg[$i]= $title[$i]." Already Exist";
                }
              }
          }/*else{
              if($i==3 || $i==6){
                if($data[$i] != ""){
                  $exp_str=explode(',',$data[$i]);
                  foreach ($exp_str as $key => $value) {
                    if(!in_array(strtolower(pathinfo($value, PATHINFO_EXTENSION)),$image_array)){
                      $msg[$i]=$title[$i]." having without image";                    
                    }else{
                      if(!file_get_contents("public://digitalassetsimages/".$value)){
                        $msg[$i]=$title[$i]." having image was not existing";
                      }
                    }
                  }
                }                
              }
            }*/
          }
        
        $result_data['data']=$data;        
        $result_data['error']=implode(PHP_EOL,$msg);
      }
        return $result_data;
    }

////////////////////import parts without siteadmin/////
/************Automation Parts and YMM Bulk Import************/
/////////Parts Import start ////////////////
public function Productbulkupload(){
    ini_set('memory_limit', '-1');
    ini_set('display_errors',1);
    ini_set('max_execution_time', '0');
    global $base_url;
    $msg = "";
      $vid = 'category';
      $cat_data=array();
      $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);
      foreach ($terms as $term) {
              $term_obj = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($term->tid);           
              $cat_data[$term->name] = $term->tid ;
      } 
      
      $bid = 'brand';
      $brand_data=array();
      $termss = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($bid);
    foreach ($termss as $termm) {
        $term_obj = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($termm->tid);
        $brand_data[$termm->name] = $termm->tid ;    
      }

      $filename = "Parts_".date("m_d_Y").".xls";
      $excel_array=array('xls','xlsx');
      if(in_array(strtolower(pathinfo($filename, PATHINFO_EXTENSION)),$excel_array)){
        $inputFile="public://bulk_template/".$filename;
        if (!file_exists($inputFile)) {
          echo '500', "Error loading file " . $inputFile . ": File does not exist";
        }
        $extension = strtoupper(pathinfo($inputFile, PATHINFO_EXTENSION));
        $inputFileType = \PHPExcel_IOFactory::identify($inputFile);
        //echo $inputFileType;exit;
        $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($inputFile);
        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestDatarow();
        $highestColumn = $sheet->getHighestDataColumn();
        $title_array = $sheet->rangeToArray('A' . $row=1 . ':' . $highestColumn . $row=1, NULL, TRUE, FALSE);
          // echo "<pre>";
          // print_r($title_array);
          // die();
        $final_error_data=array();$error_count=0;$success_count=0;

        if($highestRow<=1){
          $msg="There is no data in excel";
        }
        for ($row = 1; $row <= $highestRow; $row++){
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);

            if($rowData[0][0]!=''){
              if($row!=1){
                $rowData[0][1] = $rowData[0][1];
                $filew="no_image_icon.PNG";
                if($rowData[0][6] != ""){
                  $filew =$rowData[0][6];
                  
                }
              }
              $error_message=$this->excel_field_validation_bulk($rowData[0],$row,$title_array[0]);

              $store_error[$row]=$error_message;
              $store_error[$row]['data'][11]=$store_error[$row]['error'];
              //echo $row;exit;
              if($row!= 1){
                if(empty($error_message['error'])){
                     $field_partno               = $rowData[0][0];
                     $editor1                    = $rowData[0][1];
                     if($rowData[0][2] == ""){
                        $field_total_price       = 0;
                        $field_call_price        = "on";
                     }else{
                       $field_total_price       = $rowData[0][2];
                       $field_call_price        = "off";
                     }                     
                     $field_product_name         = $rowData[0][5]." | ".$rowData[0][0];

                        if (array_key_exists($rowData[0][5],$cat_data)) {
                           $field_category=$cat_data[$rowData[0][5]];
                        }else{
                           $field_category="";
                        } 

                        if (array_key_exists($rowData[0][4],$brand_data)) {
                           $field_brand=$brand_data[$rowData[0][4]];
                        }else{
                           $field_brand="";
                        }
                        $alt ="no_image_icon.PNG";
                      
                      if($rowData[0][3] != ""){
                       
                        $alt = $rowData[0][3]; 
                      }
                      
                     $product = Product::create([
                          'type' => 'default',
                          'body'  => ['value'=> $editor1,'format'=> 'basic_html'],
                          'title' => t($field_product_name),
                          'field_product_name' => $field_product_name,
                           'field_partno'=>$field_partno,
                           'field_category_name' => $field_category,
                           'field_featured'=>1,
                           'field_brand' => $field_brand,
                           'field_stock_info' => 1,
                           'field_marketing_messages'=> 2,
                           'field_new'=>1,
                           'field_sku' => $rowData[0][8],
                           'field_price' => 15,
                           'field_total_price'=> new Price((string)$field_total_price, 'USD'),
                           'field_product_image' =>$alt ,
                           'field_image_one' => $filew,
                           'field_lester'=>$rowData[0][9],
                           'field_interchange'=>$rowData[0][10],
                           'field_call_price'=> $field_call_price
                      ]);
                      $product->save();
                      
                         $id =  $product->id();
                      $product_new_data =Product::load($id);
                      $vid    = 'color_parent';
                      $terms  = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);
                        foreach ($terms as $term) {
                           $term_obj = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($term->tid);
                          $field_color_price_values = $term_obj->get('field_color_price_values')->value;
                          //$color_price = ($term->tid==152)?0:15;
                          $prices      = $field_total_price+$field_color_price_values;
                          $color       = $term->tid;
                          $variation   = ProductVariation::create([
                                                    'type'  => 'default',
                                                    'sku'   => $rowData[0][8],
                                                    'price' => new Price((string)$prices , 'USD'),
                                                    'field_color'=>$color
                                                  ]);
                          $variation->save();
                          $product_new_data->addVariation($variation);
                        }
                        $product_new_data->save();                
                        $success_count++;                        
                  }else{
                    
                    $error_count++;
                    $final_error_data[]=$store_error[$row]['data'];
                  }
                }
            }

          }
          $res_file = "";
          if($error_count!=0){
            if($success_count<=0){
              $msg="Parts uploading failed";
            }else{
              $msg="Parts upload success with errors ";
            }
            $res_file = "sites/default/files/upload_excel_error/PartsErrors.xls";
            $data = $this->excel_export($final_error_data,$res_file);
           
          }
        }else{
          $msg="Invalid format uploaded";
        }
      
      $return_data['msg']=!empty($msg)?$msg:"Part uploaded successfully";
      $return_data['file']=!empty($res_file)?$res_file:"";
      $this->mmey_xls_upload();
      echo json_encode($return_data);die;
    }
    public function excel_field_validation_bulk($data,$row,$title){

      $image_array=array('jpg','jpeg','png','gif');
      $msg=array();
    

      if($row==1){
        $result_data['data']=$data;
        $result_data['error']='';
      }else{
        for ($i=0; $i <11 ; $i++) {
          if($i==0 || $i==4 || $i==5){
            if(empty($data[$i])){
              $msg[$i]=$title[$i]." must not empty";
            }
            if($i==0){
                $res = $this->check_partno_exist_upload_bulk($data[$i]);
                if($res == 'exist'){
                  $msg[$i]= $title[$i]." Already Exist";
                }
              }
          }
          }
        
        $result_data['data']=$data;        
        $result_data['error']=implode(PHP_EOL,$msg);
      }
        return $result_data;
    }
    public function check_partno_exist_upload_bulk($partno){
    $data_count = 0;
    $connection = \Drupal::database();
    $query      = $connection->query("SELECT fp.entity_id  FROM commerce_product__field_partno  fp INNER JOIN  `commerce_product`  cp ON fp.entity_id=cp.product_id
      WHERE fp.field_partno_value = '".$partno."' AND cp.delete_flag='0'");
    while($row = $query->fetchAssoc()) {
      $data_count = $row['entity_id'];
    }
    if($data_count == 0){
      return 1;

    }else{
      return 0;
    }

  }

/************************************YMM Bulk Upload **********************/
public function mmey_xls_upload(){ 
  ini_set('max_execution_time', 0);
  $connection = \Drupal::database();
  $mmey=[];
  $user        = \Drupal::currentUser();
  $user_display_name = $user->getDisplayName(); 
  $filename ="Ymme_".date("m_d_Y").".xls";
  $path="public://mmyeupload/";
  $mmey=[];
  $make=[];
  $model=[];
  $engine=[];
  $year=[];
  $partno=[];
    if(!file_exists("public://mmyeupload/".$filename)) {
      echo "file missing";exit;
    }       
  $inputFile="public://mmyeupload/".$filename;
  $extension = strtoupper(pathinfo($inputFile, PATHINFO_EXTENSION));
  $inputFileType = \PHPExcel_IOFactory::identify($inputFile);
  $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
  $objPHPExcel = $objReader->load($inputFile);
  $sheet = $objPHPExcel->getSheet(0);
  $highestRow = $sheet->getHighestDatarow();
  $highestColumn = $sheet->getHighestDataColumn();
  $title_array = $sheet->rangeToArray('A' . $row=1 . ':' . $highestColumn . $row=1, NULL, TRUE, FALSE);
  $header=array('Make','Model','Year','Engine');
  if($header != $title_array[0]) {

    echo "Headernotmatch";
    exit;
  }
  if($highestRow<=1){
    echo "Nodata";
    exit;
  }

              
    for ($row = 1; $row <= $highestRow; $row++){
        $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);  
        if($rowData[0][0]!=''){
          if($row!=1){
           array_push($make,$rowData[0][0]);
           array_push($model,$rowData[0][1]);
           array_push($engine,$rowData[0][3]);
           array_push($year,$rowData[0][2]);
           array_push($mmey,array('make'=>$rowData[0][0],'model'=>$rowData[0][1],'engine'=>$rowData[0][2],'year'=>$rowData[0][3]));
          }  
        }
    } 

    /*print_r($make);
    exit;*/

    /****************************************make insert**************************************/
    $make_ins='';
    $model_ins='';
    $year_ins='';
    $engine_ins='';
    $inserted_make=[];
    $make_exist_msg=[];
    $makeid_exist=[];
    $make_in = "'" . implode ( "', '", $make ) . "'";
    $query = $connection->query("SELECT make_id,make FROM catapult_make WHERE make in(".$make_in.")");
     while ($row = $query->fetchAssoc()) {
      $make_exist_msg[] =$row['make'];
      $makeid_exist[] =array($row['make'],$row['make_id']);
     }  
     $diff_arr_make=array_diff($make,$make_exist_msg);
     $arr_make=array_unique($diff_arr_make);
     if(count($arr_make) > 0){

        foreach ($arr_make as $key => $value) {
          $inserted_make[]=$value;
          $insert_make =$connection->query("insert into catapult_make(make,created_by,created_on) Values('".$value."','".$user_display_name."',Now())"); 
        }
        $make_ins=1;
      }

      $query = $connection->query("SELECT make_id,make FROM catapult_make WHERE make in(".$make_in.")");
         while ($row = $query->fetchAssoc()) {
          $makeid_exist[] =array($row['make'],$row['make_id']);
      }

     /****************************************make insert End**************************************/
     /****************************************model insert**************************************/
 
     $make_count=count($make);
     $empty=[];
    for ($i=0; $i <$make_count ; $i++) { 

      $comb=$make[$i]."$".$model[$i];
        if(in_array($comb, $empty)){

        }else{
          array_push($empty,$comb);
        }
       
     }

     foreach ($empty as $key => $value) {
        $explod_make_model[]=explode("$",$value);
     }
     
   
    $make_exist_count=count($explod_make_model);
    $makeid_exist_count=count($makeid_exist);
     for ($j=0; $j < $makeid_exist_count;$j++) { 
        for ($i=0; $i < $make_exist_count ; $i++) { 
               if($explod_make_model[$i][0] == $this->in_array_r($makeid_exist[$j])){
                $explod_make_model[$i][0]=$this->in_array_r_id($makeid_exist[$j]);
               }
        } 
    }

    

      $inserted_model=[];
      $model_make_exist=[];
      $model_make_exist_msg=[];
      $model_make_id_exist=[];
      
    foreach ($explod_make_model as $key=>$value) {
      $query = $connection->query("SELECT cmo.make,cmo.make_id,cm.model,cm.model_id FROM catapult_model cm INNER JOIN catapult_make cmo ON cm.make_id = cmo.make_id WHERE cm.model ='".$value[1]."' AND cm.make_id='".$value[0]."'");
        while ($row = $query->fetchAssoc()) {
            $model_make_exist_msg[] =array($row['make'],$row['model']);
            $model_make_exist[]=array($row['make_id'],$row['model']);
            $model_make_id_exist[] =array($row['model'],$row['model_id']);
        } 

    }
   
     $diff_arr_make_model = $this->make_model_array_diff($explod_make_model,$model_make_exist);
     if(count($diff_arr_make_model) > 0){
      foreach ($diff_arr_make_model as $key => $value) {

          $inserted_model[]=$value;
          $insert_make =$connection->query("insert into catapult_model(model,make_id,created_by,created_on) Values('".$value[1]."','".$value[0]."','".$user_display_name."',Now())"); 
      }
      
     
  }
    foreach ($explod_make_model as $key=>$value) {
      $query = $connection->query("SELECT cmo.make,cmo.make_id,cm.model,cm.model_id FROM catapult_model cm INNER JOIN catapult_make cmo ON cm.make_id = cmo.make_id WHERE cm.model ='".$value[1]."' AND cm.make_id='".$value[0]."'");
        while ($row = $query->fetchAssoc()) {
            
            $model_make_exist[]=array($row['make_id'],$row['model']);
            $model_make_id_exist[] =array($row['model'],$row['model_id']);
        } 

    }

     /****************************************model insert  End **************************************/
     

      /***************************************** Year Start *********************************************/
    
    $empty_year=[];
    for ($i=0; $i <$make_count ; $i++) { 

      $comb_year=$make[$i]."$".$model[$i]."$".$year[$i];
        if(in_array($comb_year, $empty_year)){

        }else{
          array_push($empty_year,$comb_year);
        }
       
     }

     foreach ($empty_year as $key => $value) {
        $explod_make_model_engine_year[]=explode("$",$value);
     }


    $make_exist_count=count($explod_make_model_engine_year);
    $makeid_exist_count=count($makeid_exist);
     for ($j=0; $j < $makeid_exist_count;$j++) { 
        for ($i=0; $i < $make_exist_count ; $i++) { 
               if($explod_make_model_engine_year[$i][0] == $this->in_array_r($makeid_exist[$j])){
                $explod_make_model_engine_year[$i][0]=$this->in_array_r_id($makeid_exist[$j]);
               }
        } 
    }

    $model_make_id_exist_count=count($model_make_id_exist);
     for ($j=0; $j < $model_make_id_exist_count;$j++) { 
        for ($i=0; $i < $make_exist_count ; $i++) { 
           if($explod_make_model_engine_year[$i][1] == $this->in_array_r($model_make_id_exist[$j])){
            $explod_make_model_engine_year[$i][1]=$this->in_array_r_id($model_make_id_exist[$j]);
           }
        } 
    }

   

      $inserted_year=[];
      $model_make_engine_year_exist=[];
      $model_make_engine_year_exist_msg=[];
      $year_engine_id_model_id_make_id_exist=[];
      
      foreach ($explod_make_model_engine_year as $key=>$value) {
      $query = $connection->query("SELECT cm.make,cm.make_id,cmo.model,cmo.model_id,cy.year,cy.year_id FROM catapult_year cy INNER JOIN catapult_make cm ON cm.make_id = cy.make_id INNER JOIN catapult_model cmo ON cmo.model_id = cy.model_id 
        WHERE cy.year='".$value[2]."' AND  cy.make_id='".$value[0]."' AND cy.model_id='".$value[1]."'");
              while ($row = $query->fetchAssoc()) {
                  $model_make_engine_year_exist_msg[] =array($row['make'],$row['model'],$row['year']);
                  $model_make_engine_year_exist[]=array($row['make_id'],$row['model_id'],$row['year']);
                  $year_engine_id_model_id_make_id_exist[] =array($row['make_id'],$row['model_id'],$row['year'],$row['year_id']);
              } 

      }

     
      if(count($model_make_engine_year_exist) > 0){
          $diff_arr_make_model_engine_year = $this->make_model_array_diff($explod_make_model_engine_year,$model_make_engine_year_exist);
      }else{
          $diff_arr_make_model_engine_year = $explod_make_model_engine_year;
      }

    
      
      if(count($diff_arr_make_model_engine_year) > 0){
        foreach ($diff_arr_make_model_engine_year as $key => $value) {
         
            $inserted_year[]=$value;
            $insert_make =$connection->query("insert into catapult_year(year,make_id,model_id,created_by,created_on) Values('".$value[2]."','".$value[0]."','".$value[1]."','".$user_display_name."',Now())"); 
           
        }
       
       
    }

    foreach ($explod_make_model_engine_year as $key=>$value) {
      $query = $connection->query("SELECT cm.make,cm.make_id,cmo.model,cmo.model_id,cy.year,cy.year_id FROM catapult_year cy INNER JOIN catapult_make cm ON cm.make_id = cy.make_id INNER JOIN catapult_model cmo ON cmo.model_id = cy.model_id 
        WHERE cy.year='".$value[2]."' AND  cy.make_id='".$value[0]."' AND cy.model_id='".$value[1]."'");
              while ($row = $query->fetchAssoc()) {
                if(count($model_make_engine_year_exist_msg) < 0){
                  $model_make_engine_year_exist_msg[] =array($row['make'],$row['model'],$row['year']);
                }
                  
                  $model_make_engine_year_exist[]=array($row['make_id'],$row['model_id'],$row['year']);
                  $year_engine_id_model_id_make_id_exist[] =array($row['make_id'],$row['model_id'],$row['year'],$row['year_id']);
              } 

      }

      /***************************************** Year end *********************************************/


      /****************************************Engine insert**************************************/

   
   
    $empty_engine=[];
    for ($i=0; $i <$make_count ; $i++) { 

      $comb_engine=$make[$i]."$".$model[$i]."$".$year[$i]."$".$engine[$i];
        if(in_array($comb_engine, $empty_engine)){

        }else{
          array_push($empty_engine,$comb_engine);
        }
       
     }

     foreach ($empty_engine as $key => $value) {
        $explod_make_model_engine[]=explode("$",$value);
     }

    

    $make_exist_count=count($explod_make_model_engine);
    $makeid_exist_count=count($makeid_exist);
     for ($j=0; $j < $makeid_exist_count;$j++) { 
        for ($i=0; $i < $make_exist_count ; $i++) { 
               if($explod_make_model_engine[$i][0] == $this->in_array_r($makeid_exist[$j])){
                $explod_make_model_engine[$i][0]=$this->in_array_r_id($makeid_exist[$j]);
               }
        } 
    }
    

    $make_exist_count=count($explod_make_model_engine);
    $model_make_id_exist_count=count($model_make_id_exist);
     for ($j=0; $j < $model_make_id_exist_count;$j++) { 
        for ($i=0; $i < $make_exist_count ; $i++) { 
           if($explod_make_model_engine[$i][1] == $this->in_array_r($model_make_id_exist[$j])){
            $explod_make_model_engine[$i][1]=$this->in_array_r_id($model_make_id_exist[$j]);
           }
        } 
    }
    
    $make_exist_count=count($explod_make_model_engine);
    $model_make_id_year_exist_count=count($year_engine_id_model_id_make_id_exist);
     for ($j=0; $j < $model_make_id_year_exist_count;$j++) { 
        for ($i=0; $i < $make_exist_count ; $i++) { 
         
          if($explod_make_model_engine[$i][0] == $this->in_array_r_zero($year_engine_id_model_id_make_id_exist[$j])){
            if($explod_make_model_engine[$i][1] == $this->in_array_r_one($year_engine_id_model_id_make_id_exist[$j])){
               if($explod_make_model_engine[$i][2] == $this->in_array_r_two($year_engine_id_model_id_make_id_exist[$j])){
                $explod_make_model_engine[$i][2]=$this->in_array_r_three($year_engine_id_model_id_make_id_exist[$j]);
               }
            }
          }
        } 
    }
   
      $inserted_engine=[];
      $model_make_engine_exist=[];
      $model_make_engine_exist_msg=[];
      $engine_model_id_make_id_exist=[];

      
      foreach ($explod_make_model_engine as $key=>$value) {
        if($value[3] != ''){
          $rep_eng=str_replace(";"," ",$value[3]);
          $query = $connection->query("SELECT cm.make,cm.make_id,cmo.model,cmo.model_id,ce.engine,ce.engine_id,cy.year,cy.year_id FROM catapult_engine ce 
      INNER JOIN catapult_make cm ON cm.make_id = ce.make_id INNER JOIN catapult_model cmo ON cmo.model_id = ce.model_id  
      INNER JOIN catapult_year cy ON cy.year_id = ce.year_id WHERE ce.engine='".$rep_eng."' AND ce.make_id='".$value[0]."' AND ce.model_id='".$value[1]."' AND ce.year_id='".$value[2]."'");
            /*exit;*/
        }else{
          $query = $connection->query("SELECT cm.make,cm.make_id,cmo.model,cmo.model_id,ce.engine,ce.engine_id,cy.year,cy.year_id FROM catapult_engine ce 
      INNER JOIN catapult_make cm ON cm.make_id = ce.make_id INNER JOIN catapult_model cmo ON cmo.model_id = ce.model_id  
      INNER JOIN catapult_year cy ON cy.year_id = ce.year_id WHERE  ce.make_id='".$value[0]."' AND ce.model_id='".$value[1]."' AND ce.year_id='".$value[2]."'");
        }
      
              while ($row = $query->fetchAssoc()) {
                  $model_make_engine_exist_msg[] =array($row['make'],$row['model'],$row['year'],$row['engine']);
                  $model_make_engine_exist[]=array($row['make_id'],$row['model_id'],$row['year_id'],$row['engine']);
                  $engine_model_id_make_id_exist[] =array($row['engine'],$row['engine_id']);
              } 

      }

      $diff_arr_make_model_engine = $this->make_model_array_diff($explod_make_model_engine,$model_make_engine_exist);
      if(count($diff_arr_make_model_engine) > 0){
        foreach ($diff_arr_make_model_engine as $key => $value) {
          if($value[3] != ''){
            $rep_eng=str_replace(";"," ",$value[3]);
            
            $insert_make =$connection->query("insert into catapult_engine(engine,make_id,model_id,year_id,created_by,created_on) Values('".$rep_eng."','".$value[0]."','".$value[1]."','".$value[2]."','".$user_display_name."',Now())"); 
          }
          else{
            $insert_make =$connection->query("insert into catapult_engine(make_id,model_id,year_id,created_by,created_on) Values('".$value[0]."','".$value[1]."','".$value[2]."','".$user_display_name."',Now())"); 
          }
           
        }
       
       
    }
      /****************************************Engine insert  End **************************************/
      if(count($model_make_engine_exist_msg) >0){
         echo "somealeadyexist";
       
       }else{
         echo "allinserted"; 
       }
  $this->aplmap_xls_upload();

}  

public function in_array_r($needle) {
/*  print_r($needle);
  exit; */
  return $needle[0];
    
}


public function in_array_r_id($needle) {
 /* print_r($needle);
  exit; */
  return $needle[1];
    
}

public function in_array_r_zero($needle) {
 
  return $needle[0];
    
}

public function in_array_r_one($needle) {

  return $needle[1];
    
}

public function in_array_r_two($needle) {

  return $needle[2];
    
}
public function in_array_r_three($needle) {

  return $needle[3];
    
}



public function make_model_array_diff($arraya, $arrayb) {

    foreach ($arraya as $keya => $valuea) {
        if (in_array($valuea, $arrayb)) {
            unset($arraya[$keya]);
        }
    }
    return $arraya;
}

////////////////////Applocation Map bulk///////////////

public function aplmap_xls_upload(){
  ini_set('max_execution_time', 0);
  $connection = \Drupal::database();
  $mmey=[];
  $make=[];
  $model=[];
  $engine=[];
  $year=[];
  $user        = \Drupal::currentUser();
  $user_display_name = $user->getDisplayName(); 
  $filename ="Ymme_mapping_".date("m_d_Y").".xls";       
  $inputFile="public://mmyeupload/".$filename;
  $extension = strtoupper(pathinfo($inputFile, PATHINFO_EXTENSION));
  $inputFileType = \PHPExcel_IOFactory::identify($inputFile);
  $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
  $objPHPExcel = $objReader->load($inputFile);

  $sheet = $objPHPExcel->getSheet(0);
  $highestRow = $sheet->getHighestDatarow();
  $highestColumn = $sheet->getHighestDataColumn();
  $title_array = $sheet->rangeToArray('A' . $row=1 . ':' . $highestColumn . $row=1, NULL, TRUE, FALSE);
 
  if($highestRow<=1){
    $msg="There is no data in excel";
  }
  
  for ($row = 1; $row <= $highestRow; $row++){
      $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);  
      if($rowData[0][0]!=''){
        if($row!=1){

         array_push($make,$rowData[0][1]);
         array_push($model,$rowData[0][2]);
         array_push($engine,$rowData[0][4]);
         array_push($year,$rowData[0][3]);
         array_push($mmey,array('product_id'=>$rowData[0][0],'make'=>$rowData[0][1],'model'=>$rowData[0][2],'engine'=>$rowData[0][4],'year'=>$rowData[0][3]));
        }  
      }

  } 

$engine_id=[];
$mmye_comb_not=[];
$exist_apl_map=[];
$exist_apl_map_data=[];



foreach ($mmey as $key => $value) {

  if($value['engine'] != ''){
     $query = $connection->query("SELECT cm.make,cm.make_id,cmo.model,cmo.model_id,ce.engine,ce.engine_id,cy.year,cy.year_id FROM catapult_engine ce 
      INNER JOIN catapult_make cm ON cm.make_id = ce.make_id INNER JOIN catapult_model cmo ON cmo.model_id = ce.model_id  
      INNER JOIN catapult_year cy ON cy.year_id = ce.year_id WHERE ce.engine='".$value['engine']."' AND cm.make='".$value['make']."' AND cmo.model='".$value['model']."' AND cy.year='".$value['year']."'");
  }else{
       $query = $connection->query("SELECT cm.make,cm.make_id,cmo.model,cmo.model_id,ce.engine,ce.engine_id,cy.year,cy.year_id FROM catapult_engine ce 
      INNER JOIN catapult_make cm ON cm.make_id = ce.make_id INNER JOIN catapult_model cmo ON cmo.model_id = ce.model_id  
      INNER JOIN catapult_year cy ON cy.year_id = ce.year_id WHERE ce.engine is null AND cm.make='".$value['make']."' AND cmo.model='".$value['model']."' AND cy.year='".$value['year']."'");
  }
 

  $query->allowRowCount = TRUE;
  $count = $query->rowCount();
  /*print_r($query);
  exit;*/
  if($count == 0){
    //$mmye_comb_not[]=$key;
    $mmye_comb_not[]=array(
        'partno' => $value['product_id'],
        'make' => $value['make'],
        'model' => $value['model'],
        'year' => $value['year'],
        'engine' => $value['engine'],
        'status' => 'mmye_comb'
      );
  }

   while ($row = $query->fetchAssoc()) {
    //echo "welcome";
      $engine_id[$key]=$row['engine_id'].'#'.$value['product_id'];

   }
}

if(count($engine_id) > 0){
    foreach ($engine_id as $key => $value) {
        $expolde=explode("#",$value);
        $query = $connection->query("SELECT product_id,engine_id FROM catapult_application_map WHERE engine_id ='".$expolde[0]."' AND product_id='".$expolde[1]."'");
        $query->allowRowCount = TRUE;
        $count = $query->rowCount();
        if($count == 0){
          //echo $expolde[0]."#".$expolde[1];
           /*$insert_make =$connection->query("insert into catapult_application_map(product_id,engine_id,created_by,created_on) Values('".$expolde[0]."','".$expolde[1]."','".$user_display_name."',Now())"); */

            $insert_make =$connection->query("insert into catapult_application_map(product_id,engine_id,created_by,created_on) Values('".$expolde[1]."','".$expolde[0]."','".$user_display_name."',Now())"); 

        }
        while ($row = $query->fetchAssoc()) {
          $exist_apl_map[]=$row['product_id']."#".$row['engine_id'];
        }
      
    }
}

if(count($exist_apl_map)){
    foreach ($exist_apl_map as $val) {
      
       $expolde=explode("#",$val);
        $query = $connection->query("SELECT cm.make,cm.make_id,cmo.model,cmo.model_id,ce.engine,ce.engine_id,cy.year,cy.year_id FROM catapult_engine ce 
      INNER JOIN catapult_make cm ON cm.make_id = ce.make_id INNER JOIN catapult_model cmo ON cmo.model_id = ce.model_id  
      INNER JOIN catapult_year cy ON cy.year_id = ce.year_id WHERE ce.engine_id= '".$expolde[1]."'");
        while ($row = $query->fetchAssoc()) {
          $exist_apl_map_data[]=array(
            'partno' => $expolde[0],
            'make' => $row['make'],
            'model' => $row['model'],
            'year' => $row['year'],
            'engine' => $row['engine'],
            'status' => 'already_exist'

            );
        }
      
    }
}
$merge_arr=array_merge($mmye_comb_not,$exist_apl_map_data);

if(count($merge_arr) > 0){
  $res_file = "sites/default/files/mmyeupload/MappingErrors.xls";
  $data = $this->excel_export_mmy_map($merge_arr,$res_file);
  echo "Some Mapping Not Inserted.Please Check Your Error File";
}else{
  echo "Allinserted";
}

}

 public function excel_export_mmy_map($data,$fileName){
    global $base_url;
     $reason='';
    $objPHPExcel = new \PHPExcel();

    $sht = $objPHPExcel->getActiveSheet();

    $i=1;
    $sht->setCellValue('A'.$i, 'Partnumber');
    $sht->setCellValue('B'.$i, 'Make');
    $sht->setCellValue('C'.$i, 'Model');
    $sht->setCellValue('D'.$i, 'Year');
    $sht->setCellValue('E'.$i, 'Engine');
    $sht->setCellValue('F'.$i, 'Reason');

    foreach ($data as $data_key => $data_value) {
     /* echo $data_value['partno'];*/
      $i++;
      $sht->setCellValue('A'.$i, $data_value['partno']);
      $sht->setCellValue('B'.$i, $data_value['make']);
      $sht->setCellValue('C'.$i, $data_value['model']);
      $sht->setCellValue('D'.$i, $data_value['year']);
      $sht->setCellValue('E'.$i, $data_value['engine']);
      if($data_value['engine'] == 'mmye_comb'){
        $reason='There Is No Combination Make,Model,Year,Engine';
      }else if($data_value['engine'] == 'already_exist'){
        $reason='Applocation Mapping Already Exist';
      }
      $sht->setCellValue('F'.$i, $reason);
   
    }
    $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    ob_end_clean();
    $objWriter->save($fileName);
  }

////////// Part mapping bulk///////////////



/************************** Automation End ******************/
}
