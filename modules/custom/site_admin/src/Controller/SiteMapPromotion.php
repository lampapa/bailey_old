<?php
namespace Drupal\site_admin\Controller;
use Drupal\node\Entity\NodeType;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\commerce_product\Entity\Product;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Cache\CacheableMetadata;
/*use Drupal\commerce\commerce_product;
use Drupal\commerce;
use Drupal\commerce_cart;*/
use Symfony\Component\HttpFoundation\RedirectResponse;
/*use Drupal\commerce_cart\CartProviderInterface;
use Drupal\commerce_cart\CartManagerInterface;*/
use Symfony\Component\DependencyInjection\ContainerInterface;
/*
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_order\Entity\Order;*/
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\CssCommand;
use Drupal\taxonomy\Entity\Term;

//for catlisting in header
//use Drupal\drupalup_controller\Controller;

class SiteMapPromotion{


  public function page(){
    global $base_url;
    $success_status     = "";
    $upload_error       = "";    
    $error              = "";
    $empty_error        = "";
    $user        = \Drupal::currentUser();
    $user_display_name = $user->getDisplayName();
    $connection               = \Drupal::database();        
    if(isset($_SESSION['postid']) ){
      if($_SESSION['postid'] == ""){
        $_SESSION['postid'] = rand(10,100);
      }
    }else{
      $_SESSION['postid'] = rand(10,100);
    }    
    if(!empty($_POST)){
      if( (isset($_POST['postid'])) && ($_POST['postid'] != "")  ){
        if($_SESSION['postid'] == $_POST['postid']){
                  
          if( (isset($_POST['hidden_id'])) && ($_POST['hidden_id'] != "") ){    


              $query = $connection->query("update catapult_mappromotion set coupon_id ='".$_POST['coupen_code']."',market_id='".$_POST['markettag_msg']."' where id='".$_POST['hidden_id']."'");                   
             
              $success_status = "Message Updated Successfully";

          }else{

            /*print_r($_POST);
            exit;*/

            $result=$connection->query("insert into catapult_mappromotion(coupon_id,market_id,created_by,created_on) Values('".$_POST['coupen_code']."','".$_POST['markettag_msg']."','".$user_display_name."',Now())"); 
             
             $success_status = "Message Added Successfully";   
          }        
        }
      }  
      $_SESSION['postid'] = "";
    }  
   if($_SESSION['postid'] == ""){
      $_SESSION['postid'] = rand(10,100);      
    }  
    if($upload_error != ""){
      $error = $upload_error;     
    }     
    return array(
        '#theme' => 'site_mappromotion',
        '#postid'=> $_SESSION['postid'],
        '#title' => $success_status,
        '#error' => $error
    );
  }  
  public function fullmappromotion(){
        $connection = \Drupal::database();
        $query  = $connection->query("SELECT cm.id,cp.code,tt.name FROM `catapult_mappromotion` AS cm
        LEFT JOIN `commerce_promotion_coupon` AS cp ON cp.id=cm.coupon_id
        LEFT JOIN `taxonomy_term_field_data` AS tt ON tt.tid=cm.market_id");
            $map_data = [];
        while($row = $query->fetchAssoc()){   
         $map_data[] = array('id' => $row['id'],'code' => $row['code'],'name' => $row['name']);
        } 
        echo json_encode($map_data);
        die();
  }
  
   public function singlemappromotion(){ 
    $id       = $_POST['id']; 
    $connection = \Drupal::database();
    $query  = $connection->query("SELECT id,coupon_id,market_id FROM `catapult_mappromotion` where id='".$id."'");
    $row = $query->fetchAssoc();
    $data_arr[]=array('id'=>$row['id'],'market_id'=>$row['market_id'],'coupon_id'=>$row['coupon_id']);
    echo json_encode($data_arr);
    exit;  
        
    //echo json_encode($term_data);  
    exit();
  }  
  public function deletemappromotion(){
    $id = $_POST['tid'];
    $connection = \Drupal::database();
    $connection->query("delete FROM catapult_mappromotion where id='".$id."'");
    echo "deleted";
    die();
  }

  public function couponservice(){
    $connection = \Drupal::database();
    $query  = $connection->query("SELECT id,code FROM `commerce_promotion_coupon`;");
    $map_data = [];
    while($row = $query->fetchAssoc()){   
     $map_data[] = array('id' => $row['id'],'code' => $row['code']);
    } 
    echo json_encode($map_data);
    die();
  }


}