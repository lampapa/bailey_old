<?php
namespace Drupal\site_admin\Controller;
use Drupal\node\Entity\NodeType;
use Drupal\node\Entity\Node;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Cache\CacheableMetadata;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\file\Entity\File;
use Drupal\commerce\commerce_product;
use Drupal\commerce;

class Promotions{
  	public function page(){
	  	global $base_url;  
	  	$success_status = "";
	  	if(isset($_SESSION['postid']) ){
	      if($_SESSION['postid'] == ""){
	        $_SESSION['postid'] = rand(10,100);
	      }
	    }else{
	      $_SESSION['postid'] = rand(10,100);
	    }   
		$success_status = "";
	  	$upload_error = "";
	  	$error = "";
	  	$editor_validate = "";
	  	if(!empty($_POST)){
	      	if( (isset($_POST['postid'])) && ($_POST['postid'] != "")  ){
		        if($_SESSION['postid'] == $_POST['postid']){
					
				}				
			}
			$_SESSION['postid'] = "";	
	  	} 
	  	if($_SESSION['postid'] == ""){
	  		$_SESSION['postid'] = rand(10,100);      
		}
		if($upload_error != ""){
			$error = $upload_error;
		}
		if($editor_validate != ""){
			$error = $editor_validate;
		}
    	return array('#theme' => 'promotions',
    				 '#title' => $success_status,
    				 '#postid'=>$_SESSION['postid'],
    				 '#error'=>$error
    				);
  	}
  	public function promotionsubmit(){
  		if($_POST){
  			$varproduct1   = [];
  			$varproduct    = [];
  			$connection    = \Drupal::database();
  			$count         = 0;
  			$offer         = [];
  			$user        = \Drupal::currentUser();
  			$user_display_name = $user->getDisplayName();
  			if($_POST['hidden_id'] != ""){ 	
  				$promocode     = $_POST['promocode'];  			
	  			$promotion_id  = $_POST['hidden_id'];
	  			$startpromo    = $_POST['startpromo'];  			  			
	  			$endpromo      = $_POST['endpromo'];  			  			
	  			$product1      = $_POST['product1'];
	  			$coupon_id     = $_POST['coupon_id'];
	  			$select_marketing = $_POST['select_marketing'];

                    $promoname  = $_POST['promoname']; $promodescription  = $_POST['promodescription'];
					$promooffer = $_POST['promooffer']; $selectcategory = $_POST['selectcategory'];	  			
		  			$discount_amount = $_POST['discount_amount'];  			
		  		    $endpromo   = $_POST['endpromo'];
		  			 $varproduct   = $_POST['product'];

     
	  			$desc_discount =  $_POST['description'];
	  			$myArray    = explode(',', $product1);
	  				               
	  			foreach ($myArray as $key => $value) {	
	  				$query1    = $connection->query("SELECT cp.uuid FROM commerce_product as cp left join commerce_product__field_partno as cf on cp.product_id= cf.entity_id where cf.field_partno_value='".$value."'");
    			    while ($row2 = $query1->fetchAssoc()){
    			    	$uuid = $row2['uuid'];
    			    } 				
				   $varproduct[] = array('product'=>$uuid); 				    
	  			}	  			
	  			

                    $entity_manager      = \Drupal::entityManager();
					$datas               = $entity_manager->getStorage('commerce_promotion');
				    $entity              = $datas->load($promotion_id);
					$entity->name        = $promoname;
					$entity->description = $promodescription;
					$entity->start_date  = $startpromo;
					$entity->end_date    = $endpromo;
					$entity->quantity    = 5;
					$entity->order_types = 'default';


				if($promooffer == 'order_item_percentage_off'){
						if(isset($selectcategory) && ($selectcategory != "")){
							$varcategory=[];
							if($selectcategory=='all'){
                                $vid = 'category';
							    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);
							    foreach ($terms as $term) {
							      $term_object = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($term->tid);		      
							      $varcategory[] =$term_object->get('uuid')->value;
							    } 
							}
							else{
                                  $term_obj = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($selectcategory);		 
                                   $varcategory[]    =   $term_obj->get('uuid')->value;
							}
                         	$offer =  array(
								          'target_plugin_id' => $promooffer,
							              'target_plugin_configuration' => array(
							                'percentage'=>$discount_amount/100,
							                'display_inclusive' => TRUE,
							                'conditions' => [
											[
												'plugin' => 'order_item_product_category',								
												'configuration' => [ 'terms'=> $varcategory 											                          
							      			]
							    		]
							    	]
							    			)             
							  			); 		
						}
						else {
								$offer =  array(
								          'target_plugin_id' => $promooffer,
							              'target_plugin_configuration' => array(
							                'percentage'=>$discount_amount/100,
							                'display_inclusive' => TRUE,
							                'conditions' => [
											[
												'plugin' => 'order_item_product',
												'configuration' => [ 'products'=>$varproduct ]
							      			]
							    		]
							    			)             
							  			); 	
						}
					}
					if($promooffer == 'order_fixed_amount_off'){						
								$offer =  array(
								          'target_plugin_id' => $promooffer,
							              'target_plugin_configuration' => array(
							               'amount' => [											
												'number' => $discount_amount,
												'currency_code' =>  'USD'			      			
							    		       ]
							    			)             
							  			); 		
                    }
					if($promooffer == 'order_percentage_off'){						
								$offer =  array(
								          'target_plugin_id' => $promooffer,
							              'target_plugin_configuration' => array(
							                  'percentage'=>$discount_amount/100,
							    			)             
							  			); 		
                    }
                    if($promooffer == 'order_item_fixed_amount_off'){
						if(isset($selectcategory) && ($selectcategory != "")){
							$varcategory=[];
							if($selectcategory=='all'){
                                $vid = 'category';
							    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);
							    foreach ($terms as $term) {
							      $term_object = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($term->tid);		      
							      $varcategory[] =$term_object->get('uuid')->value;
							    } 
							}
							else{
                                 $term_obj = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($selectcategory);			 
                                   $varcategory[]    =   $term_obj->get('uuid')->value;
							}
                         	$offer =  array(
								          'target_plugin_id' => $promooffer,
							              'target_plugin_configuration' => array(							               
							                'display_inclusive' => TRUE,
							                'conditions' => [
											   [
												'plugin' => 'order_item_product_category',								
												'configuration' => [
												                     'terms'=> $varcategory 				                          
							      			                       ]
							    		         ]
							    	           ],
                                             'amount' => [											
												'number' => $discount_amount,
												'currency_code' =>  'USD'			      			
							    		       ]
							    			)             
							  			); 		
						}
						else {
								$offer =  array(
								          'target_plugin_id' => $promooffer,
							              'target_plugin_configuration' => array(							               
							                'display_inclusive' => TRUE,
							                'conditions' => [
											   [
												'plugin' => 'order_item_product',
												'configuration' => [ 'products'=>$varproduct ]
							      			   ]
							    		    ],
							    		     'amount' => [											
												'number' => $discount_amount,
												'currency_code' =>  'USD'			      			
							    		       ]
							    			)             
							  			); 	
						}
					
					
					}			
				$entity->offer       = $offer;
				$promotion     		 = $entity_manager->getStorage('commerce_promotion')->save($entity);
				$coupon              = $entity_manager->getStorage('commerce_promotion_coupon');
				$coupon_entity       = $coupon->load($coupon_id);
				$coupon_entity->promotion_id = $promotion_id;
			    $coupon_entity->code = $promocode;
			    $coupon_entity->usage_limit = 100000;
			    $coupon_entity->status = TRUE;							
				$coupon_entity->save();
				$query = $connection->query("update catapult_mappromotion set coupon_id ='".$promotion_id."',market_id='".$_POST['select_marketing']."' and coupon_code ='".$promocode."' where id='".$_POST['hidden_id']."'");

	  			
  			}else{
               /* echo "<pre>";
  				print_r($_POST);
  				exit;*/
///// check coupon code/////
  				$query      = $connection->query("SELECT count(*) as cf from commerce_promotion_coupon   where code= '".$_POST['promocode']."' ");
			        $ourlocation_array =[];
			    while ($row = $query->fetchAssoc()) {
			    	$count =  $row['cf'];			    	
			    }
			    if($count == 0){
			    	$maxid      = "";
					$promoname  = $_POST['promoname']; $promodescription  = $_POST['promodescription'];
					$promooffer = $_POST['promooffer']; $selectcategory = $_POST['selectcategory'];	  			
		  			$discount_amount = $_POST['discount_amount']; $promocode  = $_POST['promocode'];  			
		  			$startpromo = $_POST['startpromo']; $endpromo   = $_POST['endpromo'];
		  			$select_marketing = $_POST['select_marketing']; $varproduct   = $_POST['product'];
		  			
		  			$entity_manager      = \Drupal::entityManager();
					$store               = $entity_manager->getStorage('commerce_store')->loadDefault();
					$mail                = $store->getEmail();
					$entity              = $entity_manager->getStorage('commerce_promotion')->create();
					$entity->name        = $promoname;
					$entity->description = $promodescription;
					$entity->start_date  = $startpromo;
					$entity->end_date    = $endpromo;
					$entity->quantity    = 5;
					$entity->order_types = 'default';
					$entity->stores = $store->id();
					
					if($promooffer == 'order_item_percentage_off'){
						if(isset($selectcategory) && ($selectcategory != "")){
							$varcategory=[];
							if($selectcategory=='all'){
                                $vid = 'category';
							    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);
							    foreach ($terms as $term) {
							      $term_object = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($term->tid);		      
							      $varcategory[] =$term_object->get('uuid')->value;
							    } 
							}
							else{
                                  $term_obj = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($selectcategory);		 
                                   $varcategory[]    =   $term_obj->get('uuid')->value;
							}
                         	$offer =  array(
								          'target_plugin_id' => $promooffer,
							              'target_plugin_configuration' => array(
							                'percentage'=>$discount_amount/100,
							                'display_inclusive' => TRUE,
							                'conditions' => [
											[
												'plugin' => 'order_item_product_category',								
												'configuration' => [ 'terms'=> $varcategory 											                          
							      			]
							    		]
							    	]
							    			)             
							  			); 		
						}
						else {
								$offer =  array(
								          'target_plugin_id' => $promooffer,
							              'target_plugin_configuration' => array(
							                'percentage'=>$discount_amount/100,
							                'display_inclusive' => TRUE,
							                'conditions' => [
											[
												'plugin' => 'order_item_product',
												'configuration' => [ 'products'=>$varproduct ]
							      			]
							    		]
							    			)             
							  			); 	
						}
					}
					if($promooffer == 'order_fixed_amount_off'){						
								$offer =  array(
								          'target_plugin_id' => $promooffer,
							              'target_plugin_configuration' => array(
							               'amount' => [											
												'number' => $discount_amount,
												'currency_code' =>  'USD'			      			
							    		       ]
							    			)             
							  			); 		
                    }
					if($promooffer == 'order_percentage_off'){						
								$offer =  array(
								          'target_plugin_id' => $promooffer,
							              'target_plugin_configuration' => array(
							                  'percentage'=>$discount_amount/100,
							    			)             
							  			); 		
                    }
                    if($promooffer == 'order_item_fixed_amount_off'){
						if(isset($selectcategory) && ($selectcategory != "")){
							$varcategory=[];
							if($selectcategory=='all'){
                                $vid = 'category';
							    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);
							    foreach ($terms as $term) {
							      $term_object = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($term->tid);		      
							      $varcategory[] =$term_object->get('uuid')->value;
							    } 
							}
							else{
                                 $term_obj = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($selectcategory);			 
                                   $varcategory[]    =   $term_obj->get('uuid')->value;
							}
                         	$offer =  array(
								          'target_plugin_id' => $promooffer,
							              'target_plugin_configuration' => array(							               
							                'display_inclusive' => TRUE,
							                'conditions' => [
											   [
												'plugin' => 'order_item_product_category',								
												'configuration' => [
												                     'terms'=> $varcategory 				                          
							      			                       ]
							    		         ]
							    	           ],
                                             'amount' => [											
												'number' => $discount_amount,
												'currency_code' =>  'USD'			      			
							    		       ]
							    			)             
							  			); 		
						}
						else {
								$offer =  array(
								          'target_plugin_id' => $promooffer,
							              'target_plugin_configuration' => array(							               
							                'display_inclusive' => TRUE,
							                'conditions' => [
											   [
												'plugin' => 'order_item_product',
												'configuration' => [ 'products'=>$varproduct ]
							      			   ]
							    		    ],
							    		     'amount' => [											
												'number' => $discount_amount,
												'currency_code' =>  'USD'			      			
							    		       ]
							    			)             
							  			); 	
						}
					
					
					}
								
                   //echo "<pre>";
                   //print_r($offer);die;

					$entity->offer       = $offer;			
					$promotion     		 = $entity_manager->getStorage('commerce_promotion')->save($entity);

					$query = $connection->query("SELECT max(promotion_id) as maxid from commerce_promotion");
					while($row = $query->fetchAssoc()) { $maxid = $row['maxid']; }		
					if($maxid != ""){
						$coupon = $entity_manager->getStorage('commerce_promotion_coupon')->
							   create(
							   	array('promotion_id' => $maxid, 'code' => $promocode, 'usage_limit' => 100000,'status' => TRUE)
							   );
						$coupon->save();
						/*$result=$connection->query("insert into catapult_mappromotion(coupon_id,market_id,created_by,created_on,coupon_code) Values('".$maxid."','".$select_marketing."','".$user_display_name."',Now(),'".$promocode."')");*/ 
					}
			    }else{
			    	echo "Coupon_exist";
			    }
  				
  			}  						
  		}
  		die();
  	}
  	public function coupon_delete(){
  		$connection        = \Drupal::database();
  		if($_POST){
  			$coupon_id = $_POST['coupon_id']; $promotion_id = $_POST['promotion_id'];
  			$connection->query("delete FROM commerce_promotion where promotion_id='".$promotion_id."'");
  			$connection->query("delete FROM commerce_promotion_coupon where promotion_id='".$promotion_id."'");
  			$connection->query("delete FROM commerce_promotion__conditions where revision_id='".$promotion_id."'");
  			$connection->query("delete FROM commerce_promotion__coupons where revision_id='".$promotion_id."'");
  			$connection->query("delete FROM commerce_promotion_field_data where promotion_id='".$promotion_id."'");
  			$connection->query("delete FROM commerce_promotion__order_types where revision_id='".$promotion_id."'");
  			$connection->query("delete FROM commerce_promotion__stores where revision_id='".$promotion_id."'");
  			$connection->query("delete FROM commerce_promotion_usage where promotion_id='".$promotion_id."'"); 
  		}
  		die();
  	}
  	public function partnourl(){  	
		$connection        = \Drupal::database();
		$partno_array = [];
        $query             = $connection->query("SELECT cp.uuid as uuid,cf.field_partno_value as partno from commerce_product as cp left join commerce_product__field_partno as cf on cp.product_id = cf.entity_id  where cp.product_id = cf.entity_id"); 
        while ($row = $query->fetchAssoc()) {
		    $partno_array[] = array('uuid'=>$row['uuid'] , 'partno'=>$row['partno']);	      	
	    }
	    echo json_encode($partno_array);
	    die();
  	}



  	public function promotionurls_edit()
  	{
       $connection          = \Drupal::database();
  	   $entity_manager      = \Drupal::entityManager();
       $datas               = $entity_manager->getStorage('commerce_promotion');
     //  $coupon_datas               = \Drupal::entityTypeManager()->getStorage('commerce_promotion_coupon');
       $datas0			    = [];
     //  $coupon_datas0	    =[];
       $product_id           = [];
       $cat_id               = [];
       /*$order_storage = \Drupal::entityTypeManager()->getStorage('commerce_promotion');
       $order_query = $order_storage->getQuery(); 
	   $order_ids = $order_query->execute();*/
//print_r($order_ids);die;

		//foreach ($order_ids as $order_id) {
		 
		$datas0 = $datas->load($_POST['id']);
		//echo "<pre>";
		//print_r($datas0->get('offer')->target_plugin_configuration['amount']['number'] );die;

	/* $vid = 'category';
    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);
   
*/
		//$coupon_datas0 = $coupon_datas->load($datas0->get('offer')->coupons);
		if($datas0->get('offer')->target_plugin_id=='order_item_percentage_off'){
			
             $percentage = $datas0->get('offer')->target_plugin_configuration['percentage']*100;
             if($datas0->get('offer')->target_plugin_configuration['conditions'][0]['plugin']=='order_item_product')
             {
             	foreach ($datas0->get('offer')->target_plugin_configuration['conditions'][0]['configuration']['products'] as $key => $value) {
             		
             		$product_id []=$value['product'];
             	}
             }
             else
             {
             	foreach ($datas0->get('offer')->target_plugin_configuration['conditions'][0]['configuration']['terms'] as $key => $value) {             		
             		$cat_id []=$value;
             	}

             	if(count($cat_id)==1)
             	{ 
                 
                 $query = $connection->query("SELECT tid FROM `taxonomy_term_data` WHERE `vid`= 'category' and `uuid`= '".$cat_id[0]."'");
                 $row = $query->fetchAssoc();
                 $category_id=$row['tid'];
                

             	}else{ 
             		$category_id="all";
             	}
             	// print_r($category_id);die;
             }

		}elseif($datas0->get('offer')->target_plugin_id=='order_fixed_amount_off'){

				 $amount = $datas0->get('offer')->target_plugin_configuration['amount']['number'] ;
			
			

		}elseif($datas0->get('offer')->target_plugin_id=='order_percentage_off'){
			
				 $percentage = $datas0->get('offer')->target_plugin_configuration['percentage']*100 ;			

			

		}else{

            $amount=$datas0->get('offer')->target_plugin_configuration['amount']['number'];
			 if($datas0->get('offer')->target_plugin_configuration['conditions'][0]['plugin']=='order_item_product')
             {
             	foreach ($datas0->get('offer')->target_plugin_configuration['conditions'][0]['configuration']['products'] as $key => $value) {
             		
             		$product_id []=$value['product'];
             	}
             }
             else
             {
             	foreach ($datas0->get('offer')->target_plugin_configuration['conditions'][0]['configuration']['terms'] as $key => $value) {
             		
             		$cat_id []=$value;
             	}
             	if(count($cat_id)==1)
             	{ 
                 
                 $query = $connection->query("SELECT tid FROM `taxonomy_term_data` WHERE `vid`= 'category' and `uuid`= '".$cat_id[0]."'");
                 $row = $query->fetchAssoc();
                 $category_id=$row['tid'];
                

             	}else{ 
             		$category_id="all";
             	}
             }
		

		}
           foreach ($datas0->getCoupons() as $key => $value) { 
          
           	 $ourlocation_array[] = array(
		   	                           'promotion_id'     => $datas0->get('promotion_id')->value,
		   	                           'name'             => $datas0->get('name')->value,
		   	                           'description'      => $datas0->get('description')->value,
		   	                           'promo_offer_type' => $datas0->get('offer')->target_plugin_id,
		   	                          // 'promotion_id'=>$datas0->get('promotion_id')->value,
		   	                           'dates'            => $datas0->get('start_date')->value." to ".$datas0->get('end_date')->value,
		   	                           'code'             => $value->get('code')->value, 							
				    				    'coupon_code'     => $value->get('code')->value,
				    							 		 
		    							'coupon_id'       => $value->get('id')->value,
		    							                 
		    						    'amount'         => $amount,
		    						    'product_id'     => $product_id, 
		    						    'cat_id'     => $category_id,
		    						    'percentage'  => $percentage,  
                                       
		    							
								       );
           }


		  


	//	}
		
		echo json_encode($ourlocation_array);die;
		 //  echo json_encode($datas0);
	   // die();


  	}
  	/*public function promotionurls(){ 
  		$entity_manager      = \Drupal::entityManager();
  		$connection          = \Drupal::database();
		$datas               = $entity_manager->getStorage('commerce_promotion'); 		
		$products            = "";	
		$product_id 		 = "";
		$partno              = "";	
	    $datas0			     =[];
        $query               = $connection->query("SELECT * FROM commerce_promotion_field_data as fd left join commerce_promotion_coupon as pc on pc.promotion_id=fd.promotion_id");
        $ourlocation_array =[];
        $promotion_id = "";
	    while ($row = $query->fetchAssoc()){
	    	$promotion_id = $row['promotion_id'];
	    	
	    	//$datas0 = $datas->load($row['promotion_id']);
	    	$datas0 = $datas->load(1);
	    	
	    	//exit;
			foreach ($datas0->offer->target_plugin_configuration as $key => $value) {	
					if(is_array($value)){
						if ( array_key_exists('0',$value) ){
		            		foreach ($value[0]['configuration']['products'] as $key => $value1) {
		            			
		            			 $query1    = $connection->query("SELECT cp.product_id,cf.field_partno_value FROM commerce_product as cp left join commerce_product__field_partno as cf on cp.product_id= cf.entity_id where cp.uuid='".$value1['product']."'");
		            			  while ($row2 = $query1->fetchAssoc()){
		            			  		if($product_id == ""){
											$product_id = $row2['product_id'];
										}else{
											$product_id .= ','.$row2['product_id'];
										}
										if($partno == ""){
											$partno = $row2['field_partno_value'];
										}else{
											$partno .= ','.$row2['field_partno_value'];
										}
		            			  }

								if($products == ""){
									$products = $value1['product'];
								}else{
									$products .= ','.$value1['product'];
								}					
							}
          				}			
				

					}				
			}
			$market_message = $market_id = $id = "";			     
			if($promotion_id != ""){
				$query1    = $connection->query("SELECT * FROM catapult_mappromotion  as cm left join taxonomy_term_field_data as tf on cm.market_id=tf.tid where coupon_id='".$promotion_id."'");
			    while ($row2 = $query1->fetchAssoc()){
			    	$market_message = $row2['name'];
			    	$market_id = $row2['tid'];
			    	$id = $row2['id'];
			    }
			}				   
		    $ourlocation_array[] = array('promotion_id'=>$row['promotion_id'],
		    							 'name'=>$row['name'],
		    							 'description'=>$row['description'],
		    							 'promo_offer_type'=>$row['offer__target_plugin_id'],
		    							 'dates'=>$row['start_date']." to ".$row['end_date'],
		    							 'code'=>$row['code'],
		    							 'products'=>$products,
		    							 'coupon_code'=>$row['code'],
		    							 'coupon_id'=>$row['id'],
		    							'product_id'=>$product_id,
		    							 'partno'=>$partno,
		    							 'market_message'=>$market_message,
		    							 'market_id'=>$market_id,
		    							 'id'=>$id,
		    							 'data'=>$datas0
										);
	      	
	    }
	    echo json_encode($ourlocation_array);
	    die();
  	} */

  	  	public function promotionurls()
  	{
       $connection          = \Drupal::database();
  	   $entity_manager      = \Drupal::entityManager();
       $datas               = $entity_manager->getStorage('commerce_promotion');
     //  $coupon_datas               = \Drupal::entityTypeManager()->getStorage('commerce_promotion_coupon');
       $datas0			    = [];
     //  $coupon_datas0	    =[];
       $product_id           = [];
       $cat_id               = [];

       $order_storage = \Drupal::entityTypeManager()->getStorage('commerce_promotion');
       $order_query = $order_storage->getQuery(); 
	   $order_ids = $order_query->execute();


		foreach ($order_ids as $order_id) {
			$amount              =0;
            $percentage            =0;
		 
		$datas0 = $datas->load($order_id);
		
		//$coupon_datas0 = $coupon_datas->load($datas0->get('offer')->coupons);
		if($datas0->get('offer')->target_plugin_id=='order_item_percentage_off'){
			
             $percentage = $datas0->get('offer')->target_plugin_configuration['percentage']*100;
             if($datas0->get('offer')->target_plugin_configuration['conditions'][0]['plugin']=='order_item_product')
             {
             	foreach ($datas0->get('offer')->target_plugin_configuration['conditions'][0]['configuration']['products'] as $key => $value) {
             		
             		$product_id []=$value['product'];
             	}
             }
             else
             {
             	foreach ($datas0->get('offer')->target_plugin_configuration['conditions'][0]['configuration']['terms'] as $key => $value) {             		
             		$cat_id []=$value;
             	}

             	if(count($cat_id)==1)
             	{ 
                 
                 $query = $connection->query("SELECT tid FROM `taxonomy_term_data` WHERE `vid`= 'category' and `uuid`= '".$cat_id[0]."'");
                 $row = $query->fetchAssoc();
                 $category_id=$row['tid'];
                

             	}else{ 
             		$category_id="all";
             	}
             	// print_r($category_id);die;
            }

		/* }elseif($datas0->get('offer')->target_plugin_id=='order_fixed_amount_off'){

				 $amount = $datas0->get('offer')->target_plugin_configuration['amount']['number'] ;
			
			

		}elseif($datas0->get('offer')->target_plugin_id=='order_percentage_off'){
			
				 $percentage = $datas0->get('offer')->target_plugin_configuration['percentage']*100 ;			

			*/

		}else{

            $amount=$datas0->get('offer')->target_plugin_configuration['amount']['number'];
			 if($datas0->get('offer')->target_plugin_configuration['conditions'][0]['plugin']=='order_item_product')
             {
             	foreach ($datas0->get('offer')->target_plugin_configuration['conditions'][0]['configuration']['products'] as $key => $value) {
             		
             		$product_id []=$value['product'];
             	}
             }
             else
             {
             	foreach ($datas0->get('offer')->target_plugin_configuration['conditions'][0]['configuration']['terms'] as $key => $value) {
             		
             		$cat_id []=$value;
             	}
             	if(count($cat_id)==1)
             	{ 
                 
                 $query = $connection->query("SELECT tid FROM `taxonomy_term_data` WHERE `vid`= 'category' and `uuid`= '".$cat_id[0]."'");
                 $row = $query->fetchAssoc();
                 $category_id=$row['tid'];
                

             	}else{ 
             		$category_id="all";
             	}
             }
		

		}



			$market_message = $market_id = $id = "";			     
			if($promotion_id != ""){
				$query1    = $connection->query("SELECT * FROM catapult_mappromotion  as cm left join taxonomy_term_field_data as tf on cm.market_id=tf.tid where coupon_id='".$datas0->get('promotion_id')->value."'");
			    while ($row2 = $query1->fetchAssoc()){
			    	$market_message = $row2['name'];
			    	$market_id = $row2['tid'];
			    	$id = $row2['id'];
			    }
			}
           foreach ($datas0->getCoupons() as $key => $value) { 
          
           	 $ourlocation_array[] = array(
		   	                           'promotion_id'     => $datas0->get('promotion_id')->value,
		   	                           'name'             => $datas0->get('name')->value,
		   	                           'description'      => $datas0->get('description')->value,
		   	                           'promo_offer_type' => $datas0->get('offer')->target_plugin_id,
		   	                          // 'promotion_id'=>$datas0->get('promotion_id')->value,
		   	                           'dates'            => $datas0->get('start_date')->value." to ".$datas0->get('end_date')->value,
		   	                           'code'             => $value->get('code')->value, 							
				    				    'coupon_code'     => $value->get('code')->value,
				    							 		 
		    							'coupon_id'       => $value->get('id')->value,
		    							                 
		    						    'amount'         => $amount,
		    						    'product_id'     => $product_id, 
		    						    'cat_id'     => $category_id,
		    						    'percentage'  => $percentage, 
		    						     'market_message'=>$market_message,
		    							 'market_id'=>$market_id,
		    							 'id'=>$id, 
                                       //   'data'=>$datas0
		    							
								       );


           }


		  


		}
		
		echo json_encode($ourlocation_array);die;
		 //  echo json_encode($datas0);
	   // die();


  	}


  	public function promotion_check(){
  		if($_POST){
  			$connection          = \Drupal::database();
	  		$query1    = $connection->query("SELECT cp.product_id,cf.field_partno_value FROM commerce_product as cp left join commerce_product__field_partno as cf on cp.product_id= cf.entity_id where cp.uuid='".$value1['product']."'");
			while ($row2 = $query1->fetchAssoc()){

			}

  		}  		
  	}
  	public function change_category(){
  		if($_POST){
  			$result_arrray = [];
  			$connection    = \Drupal::database();
  			$category_id   = $_POST['category_id'];  			
	  		$query1        = $connection->query("SELECT * FROM commerce_product__field_category_name as cf left join commerce_product__field_partno as cp on cp.entity_id= cf.entity_id where cf.field_category_name_target_id='".$category_id."'");
			while ($row2 = $query1->fetchAssoc()){
				$result_arrray[] = array('partno'=>$row2['field_partno_value']);

			}
			echo json_encode($result_arrray);
  		}
  		die();  		
  	}

}