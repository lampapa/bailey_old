<?php
namespace Drupal\site_admin\Controller;
use Drupal\node\Entity\NodeType;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\commerce_product\Entity\Product;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Cache\CacheableMetadata;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\CssCommand;
use Drupal\taxonomy\Entity\Term;

class SiteBrand{

  public function page(){
    global $base_url;
    $success_status = "";
    $upload_error = "";
    $upload_video_error = "";
    $error = "";
    $editor_validate = "";
    $success_status = "";
   if(isset($_SESSION['postid']) ){
      if($_SESSION['postid'] == ""){
        $_SESSION['postid'] = rand(10,100);
      }
    }else{
      $_SESSION['postid'] = rand(10,100);
    }   
    if(!empty($_POST)){
      if( (isset($_POST['postid'])) && ($_POST['postid'] != "")  ){
        if($_SESSION['postid'] == $_POST['postid']){
          $alt        = $_POST['altext'];
          $brandnames = $_POST['brandnames'];
          $url        = $_POST['url'];
          if($_FILES["field_brand_image"]["name"] != ""){
            $name = $_FILES["field_brand_image"]["name"];          
            $exts = explode(".", $name);
            $extension = $exts[1];
            $allowedExts = array("jpeg", "jpg", "png","PNG","JPEG",'jpg');
            if(in_array($extension, $allowedExts)){
              $target_file =  basename($_FILES["field_brand_image"]["name"]);
              move_uploaded_file($_FILES["field_brand_image"]["tmp_name"], $target_file);
              chmod($_FILES["field_brand_image"]["name"],0777);
              $data = file_get_contents($base_url."/".$_FILES["field_brand_image"]["name"]);
              $file = file_save_data($data, "public://".$_FILES["field_brand_image"]["name"], FILE_EXISTS_REPLACE);
            }else{
              $upload_error = "File Type Should Be jpg,png";
            }            
          } 
          if( (isset($_POST['hidden_id'])) && ($_POST['hidden_id'] != "") ){ 
            $term = Term::load($_POST['hidden_id']);
            $term->name->setValue($brandnames);
            $term->field_urlaliaspaths->setValue($url);
            if($upload_error == ""){
              if($_FILES["field_brand_image"]["name"] != ""){
                $field_brand_image = array(
                    'target_id' => $file->id(),
                    'alt' => $alt,
                    'title' => "My title"
                );
                $term->field_brand_image = $field_brand_image;
              }
              $term->Save(); 
              $success_status = "Brand Updated Successfully"; 
            }         
          }else{  
             if( ($_FILES["field_brand_image"]["name"] != "") && ($upload_error == "") ){
              $term = Term::create([
                'name' => $brandnames, 
                'vid' => 'brand',
                'field_brand_image' => array(
                                  'target_id' => $file->id(),
                                  'alt' => $alt,
                                  'title' => "My title",
                                ),
                'field_urlaliaspaths' => $url
              ])->save();
              chmod($_FILES["field_brand_image"]["name"],0777);
              unlink($_FILES["field_brand_image"]["name"]);
              $success_status = "Brand Added Successfully";
            }else{
              $error = "please upload file";
            }        
          }        
        }
      }  
      $_SESSION['postid'] = "";
    }  
    if($_SESSION['postid'] == ""){
      $_SESSION['postid'] = rand(10,100);      
    }
    if($upload_error != ""){
      $error = $upload_error;
    }   
    return array(
        '#theme' => 'site_brand',
        '#postid' => $_SESSION['postid'],
        '#title' => $success_status,
        '#error'=>$error
    );
  }  
 
  public function fullbrandservice(){
    $vid = 'brand';
    $term_data=[];
    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);
    foreach ($terms as $term) {
      $term_obj = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($term->tid);
      $url = "";
      if(isset($term_obj->get('field_brand_image')->entity)){
        $url = file_create_url($term_obj->get('field_brand_image')->entity->getFileUri());
      }
      $term_data[] = [
        'tid' => $term->tid,
        'tname' => $term->name,
        'allias' => $term_obj->get('field_urlaliaspaths')->value,
        'image_url' => $url,
        'altvalue'=>$term_obj->field_brand_image->alt
      ];
    } 
    echo json_encode($term_data);
    die();
  }

  public function deletebrands(){
    $connection = \Drupal::database();
    $query  = $connection->query("SELECT count(product_id) as counts FROM commerce_product where delete_flag=0");
    $counts = 0;
    while($row = $query->fetchAssoc()){   
      $counts = $row['counts'];
    }    
   /* if($counts > 0){
      echo "record_exist";
    }else{*/
      $tid = $_POST['tid'];
      if ($term = \Drupal\taxonomy\Entity\Term::load($tid)) {
        // Delete the term itself
        $term->delete();
      }
     
    //}  
    die();
  }

  public function singlebrand(){ 
    $id       = $_POST['id'];   
    $term_obj = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($id);
    if(isset($term_obj->get('field_brand_image')->entity)){
      $url = file_create_url($term_obj->get('field_brand_image')->entity->getFileUri());
    }
    $term_data[] = [
      'tid' => $term_obj->get('tid')->value,
      'tname' => $term_obj->get('name')->value,
      'allias' => $term_obj->get('field_urlaliaspaths')->value,
      'image_url' => $url,
      'altvalue'=>$term_obj->field_brand_image->alt
    ];     
    echo json_encode($term_data);  
    exit();
  }
}