<?php
namespace Drupal\site_admin\Controller;


//for catlisting in header
//use Drupal\drupalup_controller\Controller;

class OurLocations{

  public function page() {
    $connection = \Drupal::database();   
    $success_status = "";
    if(isset($_SESSION['postid']) ){
      if($_SESSION['postid'] == ""){
        $_SESSION['postid'] = rand(10,100);
      }
    }else{
      $_SESSION['postid'] = rand(10,100);
    }   
    $success_status = "";
    $upload_error = "";
    $error = "";
    $editor_validate = "";
    if(!empty($_POST)){
      if( (isset($_POST['postid'])) && ($_POST['postid'] != "")  ){
        if($_SESSION['postid'] == $_POST['postid']){                 
          $heading      = $_POST['heading'];                 
          $company_name = $_POST['company_name'];             
          $address1     = $_POST['address1'];             
          $address2     = $_POST['address2'];             
          $zipcode      = $_POST['zip_code'];             
          $country      = $_POST['country'];             
          $state        = $_POST['state'];             
          $city         = $_POST['city'];             
          $email        = $_POST['email'];             
          $phone        = $_POST['phone'];             
          $id           = $_POST['hidden_id'];
          if($id != ""){
         
            $connection->query("
              update catapult_our_locations set heading='".$heading."',company_name='".$company_name."',address1='".$address1."',
              address2='".$address2."',zip_code='".$zipcode."',country='".$country."',state='".$state."',city='".$city."',email='".$email."',phone='".$phone."' where ourlocation_id='".$id."'"); 
            $success_status = "Location Added Updated Successfully";
          }else{
            $connection->query("insert into catapult_our_locations(heading,company_name,address1,address2,zip_code,country,state,city,email,phone,created_by,created_on)values('".$heading."','".$company_name."','".$address1."','".$address2."','".$zipcode."','".$country."','".$state."','".$city."','".$email."','".$phone."','JP',Now())");  
            $success_status = "Location  Added Successfully";
          }          
        }                                                               
      }
      $_SESSION['postid'] = "";
    }
    if($_SESSION['postid'] == ""){
        $_SESSION['postid'] = rand(10,100);      
    }    
    return array(
      '#theme' => 'our_locations',
      '#title' => $success_status,
      '#postid'=>$_SESSION['postid'],
      '#error'=>$error
    );
  }

   public function getourlocaionsinfo() {
    
      $connection = \Drupal::database();
      $query = $connection->query("SELECT * FROM catapult_our_locations");
      $ourlocation_results =[];
    while ($row = $query->fetchAssoc()) {
      
       $ourlocation_results[] = $row;
    }
    $data['ourlocation_results'] =$ourlocation_results;
    echo json_encode($data);
    exit;
   
  }
  public function ourlocationdelete(){
     $connection = \Drupal::database();
      $id = $_POST['id'];
      $typess = $_POST['typess'];
    if($typess == 'ourlocation'){
      $connection->query("delete FROM catapult_our_locations where ourlocation_id='".$id."'");
    }
    exit;
  }
  public function ourlocationedit(){ 
    $connection = \Drupal::database();
      $id   = $_POST['id'];
      $query = $connection->query("SELECT * FROM catapult_our_locations where ourlocation_id='".$id."'");
      $ourlocation_array =[];
    while ($row = $query->fetchAssoc()) {
    $ourlocation_array[] = array(
        'ourlocation_id'=>$row['ourlocation_id'],
        'heading'=>$row['heading'],
        'company_name'=>$row['company_name'],
        'address1'=>$row['address1'],
        'address2'=>$row['address2'],
        'zip_code'=>$row['zip_code'],
        'country'=>$row['country'],
        'state'=>$row['state'],
        'city'=>$row['city'],
        'email'=>$row['email'],
        'phone'=>$row['phone']
      );
    }
    echo json_encode($ourlocation_array);
    exit();
  }
     
}