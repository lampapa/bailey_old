<?php
namespace Drupal\site_admin\Controller;
use Drupal\node\Entity\NodeType;
use Drupal\node\Entity\Node;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Cache\CacheableMetadata;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\file\Entity\File;

class AboutSetting{
  	public function page(){
	  	global $base_url;  
	  	$success_status = "";
	  	if($_SESSION['postid'] == ""){
	  		$_SESSION['postid'] = rand(10,100);      
		}
		$success_status = "";
	  	$upload_error = "";
	  	$error = "";
	  	$editor_validate = "";
	  	if(!empty($_POST)){
	      	if( (isset($_POST['postid'])) && ($_POST['postid'] != "")  ){
		        if($_SESSION['postid'] == $_POST['postid']){		          	
			  		$value    = $_POST['editor1'];
			  		if($value == ""){
		  				$editor_validate = "Please Enter Content";
		  			}else{
						$title    = $_POST['head'];
						$alt      = $_POST['alt'];
						$sequence = $_POST['sequence']; 
						if($_FILES["newf"]["tmp_name"] != ""){
							$name = $_FILES["newf"]["name"];					
							$exts = explode(".", $name);
							$extension = $exts[1];
							$allowedExts = array("jpeg", "jpg", "png","PNG","JPEG",'jpg');
							if(in_array($extension, $allowedExts)){
								$target_file =  basename($_FILES["newf"]["name"]);
								move_uploaded_file($_FILES["newf"]["tmp_name"], $target_file);
								//chmod($_FILES["newf"]["name"],0777);
								$data = file_get_contents($base_url."/".$_FILES["newf"]["name"]);
								$file = file_save_data($data, "public://".$_FILES["newf"]["name"], FILE_EXISTS_REPLACE);
							}else{
								$upload_error = "File Type Should Be jpg,png";
							}
						}
				  		if($_POST['hidden_id'] != ""){
				  			$node                         = Node::load($_POST['hidden_id']);			
							$node->body->value            = $value;
							$node->body->format           = 'full_html';
							$node->title                  = $title;
							$node->field_about_sequence->value = $sequence;
							if($upload_error == ""){
								if($_FILES["newf"]["tmp_name"] != ""){
									$field_aboutusimage = array(
									    'target_id' => $file->id(),
									    'alt' => $alt,
									    'title' => "My title"
									);
									$node->field_aboutusimage = $field_aboutusimage;
								}
								$node->save();
								$success_status = "About us Updated Successfully";	
							}	
							
				  		}else{	  								    
						    if( ($_FILES["newf"]["name"] != "") && ($upload_error == "") ){
						    	$body = [
								    'value' => $value,
								    'format' => 'basic_html',
							    ];
								$node = Node::create([
									'type'  => 'about_us',
									'title'	=> $title,
									'body'	=> ['value'=> $value,'format'=> 'basic_html'],
									'field_about_sequence' => $sequence,
								  	'field_aboutusimage' => [
								    'target_id' => $file->id(),
								    'alt' => $alt,
								    'title' => 'Sample File'
								  ],
								]);
								$node->save();
								chmod($_FILES["newf"]["name"],0777);
								unlink($_FILES["newf"]["name"]);
								$success_status = "About Us Added Successfully";
							}else{
								$error = "please upload file";
							}	
						}
					}	
				}			
				
			}
			$_SESSION['postid'] = "";	
	  	} 
	  	if($_SESSION['postid'] == ""){
	  		$_SESSION['postid'] = rand(10,100);      
		}
		if($upload_error != ""){
			$error = $upload_error;
		}
		if($editor_validate != ""){
			$error = $editor_validate;
		}
    	return array('#theme' => 'about_setting',
    				 '#title' => $success_status,
    				 '#postid'=>$_SESSION['postid'],
    				 '#error'=>$error
    				);
  	}

  	public function aboutusurl(){  	
		$service_array =[];
	  	$nids          = \Drupal::entityQuery('node')->condition('type','about_us')->execute();
	  	foreach($nids as $key => $ids){
	  		if( ($ids != 110) && ($ids != 113) && ($ids != 114) && ($ids != 114)  ){	  			
				$node = \Drupal\node\Entity\Node::load($ids);
				/*echo $node->getTitle();
				echo "<br>";*/
				$res = $node->field_about_sequence->getValue();		
				$service_array[] = array(
							        'title'=>$node->getTitle(),
							        'body'=>strip_tags($node->body->value),
							        'file'=>file_create_url($node->field_aboutusimage->entity->getFileUri()),
							        'sequence'=>$res[0]['value'],
							        'id'=>$ids
	        						);
			}		
		}
		echo json_encode($service_array);	
		exit();	
  	}
  	public function deleteaboutus(){ 
	  	$nodeid = $_POST['id'];	  		  	
		$result = \Drupal::entityQuery('node')
	      ->condition('type', 'about_us')
	      ->execute();			     
	  	$res = array($nodeid=>$nodeid);
		entity_delete_multiple('node', $res);
		die();
  	}
  	public function deleteservice(){ 
	  	$result = \Drupal::entityQuery('node')
	      ->condition('type', 'services')
	      ->execute();
	      $nodeid = $_POST['id'];
	      $res = array($nodeid=>$nodeid);      
	      entity_delete_multiple('node', $res);
		die();
  	}
    public function singleservice(){ 
	  	$id   = $_POST['id'];
	  	$node = \Drupal\node\Entity\Node::load($id);
		$res  = $node->field_service_sequence->getValue();		
		$service_array[] = array(
						        'title'=>$node->getTitle(),
						        'body'=>strip_tags($node->body->value),
						        'file'=>file_create_url($node->field_aboutusimage->entity->getFileUri()),
						        'sequence'=>$res[0]['value'],
						        'id'=>$id,
						        'altvalue'=>$node->field_aboutusimage->alt
	    						);
		echo json_encode($service_array);
		exit();
	}
	public function aboutedit(){ 
	  	$id   = $_POST['id'];
	  	$node = \Drupal\node\Entity\Node::load($id);
		$res  = $node->field_about_sequence->getValue();		
		$service_array[] = array(
						        'title'=>$node->getTitle(),
						        'body'=>strip_tags($node->body->value),
						        'file'=>file_create_url($node->field_aboutusimage->entity->getFileUri()),
						        'sequence'=>$res[0]['value'],
						        'id'=>$id,
						        'altvalue'=>$node->field_aboutusimage->alt
	    						);
		echo json_encode($service_array);
		exit();
	}
}