<?php

/**
 * @file
 * Contains \Drupal\faq_custom_block\Plugin\Block
 */

namespace Drupal\faq_custom_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;


/**
 * below section is important
 * 
 * @Block(
 *  id = "faq_custom_block",
 *  admin_label = @Translation("FAQ custom Block"),
 *  category = @Translation("Custom")
 * ) 
 */
 
class FaqCustomBlock extends BlockBase{
 
   public function build(){
      return [
      '#theme' => 'faqtemplate',
      '#test_var' => $this->t('Test Value'),
    ];
  }
}

?>