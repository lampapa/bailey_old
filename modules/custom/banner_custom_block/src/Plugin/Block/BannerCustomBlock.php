<?php

/**
 * @file
 * Contains \Drupal\my_custom_block\Plugin\Block
 */

namespace Drupal\banner_custom_block\Plugin\Block;
use Drupal\node\Entity\NodeType;
use Drupal\node\Entity\Node;
use Drupal\Core\Block\BlockBase;


/**
 * below section is important
 * 
 * @Block(
 *  id = "banner_custom_block",
 *  admin_label = @Translation("Banner custom Block"),
 *  category = @Translation("Custom")
 * ) 
 */
 
class BannerCustomBlock extends BlockBase{

	
 
   public function build(){
   	$service_array =[];
  	$nids          = \Drupal::entityQuery('node')->condition('type','banner')->execute();
  	foreach($nids as $key => $ids){	  			  		
		$node = \Drupal\node\Entity\Node::load($ids);				
		$res = $node->field_banner_sequence->getValue();		
		$service_array[] = array(							       
					        'file'=>file_create_url($node->field_banner_upload->entity->getFileUri()),
					        'sequence'=>$res[0]['value'],
					        'id'=>$ids,
					        'altvalue'=>$node->field_banner_upload->alt
    						);
		
	}



    usort($service_array, function($a, $b) {
          return $a['sequence'] <=> $b['sequence'];
    });

    /*print_r($service_array);
    exit;*/


      return [
      '#theme' => 'bannertemplate',
      '#test_var' => $this->t('Test Value'),
      '#service_array'=>$service_array
    ];
  }
}

?>