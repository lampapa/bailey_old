<?php

/**
 * @file
 * Contains \Drupal\terms_custom_block\Plugin\Block
 */

namespace Drupal\terms_custom_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;


/**
 * below section is important
 * 
 * @Block(
 *  id = "terms_custom_block",
 *  admin_label = @Translation("Terms custom Block"),
 *  category = @Translation("Custom")
 * ) 
 */
 
class TermsCustomBlock extends BlockBase{
 
   public function build(){
      return [
      '#theme' => 'termstemplate',
      '#test_var' => $this->t('Test Value'),
    ];
  }
}

?>