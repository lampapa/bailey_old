<?php

/**
 * @file
 * Contains \Drupal\wishlist_custom_block\Plugin\Block
 */

namespace Drupal\wishlist_custom_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;


/**
 * below section is important
 * 
 * @Block(
 *  id = "wishlist_custom_block",
 *  admin_label = @Translation("Wishlist custom Block"),
 *  category = @Translation("Custom")
 * ) 
 */
 
class WishlistCustomBlock extends BlockBase{
 
   public function build(){
      return [
      '#theme' => 'wishlisttemplate',
      '#test_var' => $this->t('Test Value'),
    ];
  }
}

?>