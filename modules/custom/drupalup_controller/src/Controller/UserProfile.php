<?php
/**
 * @file
 * Contains \Drupal\hello\HelloController.
 */

namespace Drupal\drupalup_controller\Controller;
use CommerceGuys\Addressing\Subdivision\SubdivisionRepository;

class UserProfile {

  public function page() {

    $selected_bill_state = '';
    $selected_ship_state = '';
    $selected_bill_city='';
    $selected_ship_city='';
    $selected_bill_zip='';
    $selected_ship_zip='';



    $countries = \Drupal\Core\Locale\CountryManager::getStandardList();

    $countrys =[];
    foreach ($countries as $key => $value) {
       $country_name = (string) $value;       
       $countrys[$key]= $country_name;
    }   
   

    $connection  = \Drupal::database();
    $uid         = \Drupal::currentUser()->id();
    $user        = \Drupal\user\Entity\User::load($uid);
    $name        = $user->getUsername();      
    $username    =  $user->getUsername();
    $email =  $user->getEmail();
    $success_status = "";
    $error = "";
    if(isset($_SESSION['postid']) ){
      if($_SESSION['postid'] == ""){
        $_SESSION['postid'] = rand(10,100);
      }
    }else{
      $_SESSION['postid'] = rand(10,100);
    }
    //$pass $user->getPassword();

    //state list
    $state=[];
    $query       = $connection->query("SELECT distinct state_name  FROM zipcode_detail");
    while($row = $query->fetchAssoc()){
        $state[$row['state_name']] = $row['state_name'];
       
    }

   /* print_r($state);
    exit;*/

     $query       = $connection->query("SELECT * FROM profile_setting_users where profile_id='".$uid."'");
    while($row = $query->fetchAssoc()){
        $selected_bill_state = $row['bill_state'];
        $selected_ship_state = $row['s_state'];
        $selected_bill_city = $row['bill_city'];
        $selected_ship_city = $row['s_city'];
        $selected_bill_zip = $row['bill_zipcode'];
        $selected_ship_zip = $row['s_zipcode'];
    }

    $profile_array[] = array('id'=>$uid,'email'=>$email,'username'=>$username);
    $query       = $connection->query("SELECT count(id) as ids FROM profile_setting_users where profile_id='".$uid."'");

    while($row = $query->fetchAssoc()){             
        $counts = $row['ids'];
    }  
    if(!empty($_POST)){
        if( (isset($_POST['postid'])) && ($_POST['postid'] != "")  ){
            if($_SESSION['postid'] == $_POST['postid']){
                $profile_id    = $_POST['profile_id'];
                /*billing */
                $bill_firstname = addslashes($_POST['bill_firstname']);
                $bill_lastname  = addslashes($_POST['bill_lastname']);
                $bill_address1  = addslashes($_POST['bill_address1']);
                $bill_address2  = addslashes($_POST['bill_address2']);
                $bill_zipcode   = addslashes($_POST['bill_zipcode']);
                $bill_city      = addslashes($_POST['bill_city']);
                $bill_state     = addslashes($_POST['bill_state']);        
/*                $bill_country   = $_POST['bill_country'];
*/                $bill_phone     = addslashes($_POST['bill_phone']);        
                $bill_email     = addslashes($_POST['bill_email']);        
                /*end of billing*/


                /*shipping*/

                $s_firstname = addslashes($_POST['s_firstname']);
                $s_lastname  = addslashes($_POST['s_lastname']);
                $s_address1  = addslashes($_POST['s_address1']);
                $s_address2  = addslashes($_POST['s_address2']);
                $s_zipcode   = addslashes($_POST['s_zipcode']);
                $s_city      = addslashes($_POST['s_city']);
                $s_state     = addslashes($_POST['s_state']);        
/*                $s_country   = $_POST['s_country'];
*/                $s_phonenumber     = addslashes($_POST['s_phonenumber']);        
                $s_email     = addslashes($_POST['s_email']);        

                 

                /*end of shipping*/

               if($counts == 0){
                     $query = $connection->query("insert into profile_setting_users(bill_firstname,bill_lastname,bill_address1,bill_address2,bill_zipcode,bill_city,bill_state,bill_phone,bill_email,s_firstname,s_lastname,s_address1,s_address2,s_zipcode,s_city,s_state,s_phonenumber,s_email,profile_id) values('$bill_firstname','$bill_lastname','$bill_address1','$bill_address2','$bill_zipcode','$bill_city','$bill_state','$bill_phone','$bill_email','$s_firstname','$s_lastname','$s_address1','$s_address2','$s_zipcode','$s_city','$s_state','$s_phonenumber','$s_email','$uid')");
                    $success_status = "User Created Successfully";
                      

               }else{
                   $query = $connection->query("update profile_setting_users set bill_firstname='".$bill_firstname."',bill_lastname='".$bill_lastname."',
                        bill_address1='".$bill_address1."',bill_address2='".$bill_address2."',bill_zipcode='".$bill_zipcode."',bill_city='".$bill_city."',
                        bill_state='".$bill_state."',bill_phone='".$bill_phone."',bill_email='".$bill_email."',
                        s_firstname='".$s_firstname."',s_lastname='".$s_lastname."',s_address1='".$s_address1."',s_address2='".$s_address2."',s_zipcode='".$s_zipcode."',s_city='".$s_city."',s_state='".$s_state."',s_phonenumber='".$s_phonenumber."',s_email='".$s_email."' where profile_id='".$uid."'");
                   $success_status = "User Updated Successfully";     
                    header("Refresh:0");              

               }
            }           
        }
        $_SESSION['postid'] = "";
              
    }
    if($_SESSION['postid'] == ""){
        $_SESSION['postid'] = rand(10,100);      
    } 
    return array(
        '#theme' => 'user_profile',
        '#profileinfo'=>$profile_array,
        '#hiddenid'=>$uid,
        '#error'=>$error,
        '#title'=>$success_status,
        '#postid'=>$_SESSION['postid'],
        '#state'=>$state,
        '#selected_bill_state'=> $selected_bill_state,
        '#selected_ship_state'=>$selected_ship_state,
        '#selected_bill_city'=> $selected_bill_city,
        '#selected_ship_city'=>$selected_ship_city,
        '#selected_bill_zip'=> $selected_bill_zip,
        '#selected_ship_zip'=>$selected_ship_zip
    );
  }

  public function userprofiledetails(){
    $connection  = \Drupal::database();
    $uid         = \Drupal::currentUser()->id();
    $data        = array();
    
    $query       = $connection->query("SELECT * FROM profile_setting_users where profile_id='".$uid."'");
    while($row = $query->fetchAssoc()){             
        $id            = $row['id'];
        $profile_id    = $row['profile_id'];

        /*billing */
        $bill_firstname = $row['bill_firstname'];
        $bill_lastname  = $row['bill_lastname'];
        $bill_address1  = $row['bill_address1'];
        $bill_address2  = $row['bill_address2'];
        $bill_zipcode   = $row['bill_zipcode'];
        $bill_city      = $row['bill_city'];
        $bill_state     = $row['bill_state'];        
        $bill_country   = $row['bill_country'];
        $bill_phone     = $row['bill_phone'];        
        $bill_email     = $row['bill_email'];        
        /*end of billing*/


        /*shipping*/

        $s_firstname  = $row['s_firstname'];
        $s_lastname    = $row['s_lastname'];
        $s_address1    = $row['s_address1'];
        $s_address2    = $row['s_address2'];
        $s_zipcode     = $row['s_zipcode'];
        $s_city        = $row['s_city'];
        $s_state       = $row['s_state'];        
        $s_country     = $row['s_country'];
        $s_phonenumber = $row['s_phonenumber'];        
        $s_email       = $row['s_email']; 
        $data          = array('id'=>$id,'profile_id'=>$profile_id,'bill_firstname'=>$bill_firstname,'bill_lastname'=>$bill_lastname,
                        'bill_address1'=>$bill_address1,'bill_address2'=>$bill_address2,'bill_zipcode'=>$bill_zipcode,'bill_city'=>$bill_city,
                        'bill_state'=>$bill_state,'bill_country'=>$bill_country,'bill_phone'=>$bill_phone,'bill_email'=>$bill_email,
                        's_firstname'=>$s_firstname,'s_lastname'=>$s_lastname,
                        's_address1'=>$s_address1,'s_address2'=>$s_address2,'s_zipcode'=>$s_zipcode,'s_city'=>$s_city,
                        's_state'=>$s_state,'s_country'=>$s_country,'s_phone'=>$s_phonenumber,'s_email'=>$s_email,

                     );      

    }  
    echo json_encode($data);
    die();
  }
  public function country_rel_state(){

    $subdivisionRepository = new SubdivisionRepository();
    $country_code=$_POST['country_code'];
    $c_arr=[$country_code];
    $states = $subdivisionRepository->getList([$c_arr[0]]);


    
   /* print_r($states);
    exit;*/
    echo json_encode($states);
    exit;

   /* print_r($_POST);
    exit;*/
  }
  public function state_rel_city(){
    $city=[];
    $connection  = \Drupal::database();
    $query       = $connection->query("SELECT distinct city  FROM zipcode_detail where state_name='".$_POST['state']."'");
    while($row = $query->fetchAssoc()){             
       $city[$row['city']]  = $row['city'];
    }
    echo json_encode($city);
    exit;
}    

  public function city_rel_zip(){

    $zip=[];
    $connection  = \Drupal::database();
    $query       = $connection->query("SELECT distinct zip  FROM zipcode_detail where state_name='".$_POST['state']."' and city='".$_POST['city']."'");
    while($row = $query->fetchAssoc()){             
       $zip[$row['zip']]  = $row['zip'];
    }
    echo json_encode($zip);
    exit;
    
  }

}