<?php
/**
 * @file
 * Contains \Drupal\hello\HelloController.
 */

namespace Drupal\drupalup_controller\Controller;


//use Drupal\Core\Controller\ControllerBase;


/*class ArticleController extends ControllerBase {
  public function content() {
    return array(
        '#markup' => '' . t('Hello there!') . '',
    );
  }
}*/


class Videos{

  public function content() {
    return array(
        '#markup' => '' . t('Hellojjjjjj there!') . '',
    );
  }

  public function page() {
    $video_array =[];
     $connection = \Drupal::database();
    $nids = \Drupal::entityQuery('node')->condition('type','videos')->execute();
    foreach ($nids as $key => $ids) {
      $node = \Drupal\node\Entity\Node::load($ids);
      $res = $node->field_video_sequence->getValue();
      $video_array[] = array(
      'title'=>$node->getTitle(),
      'raw_html'=>$node->body->value,
      //'img_url'=>file_create_url($node->field_video_file->entity->getFileUri()),
      //'altvalue'=>$node->field_video_image->alt,
      'sequence'=>$res[0]['value']
      );
    }

    usort($video_array, function($a, $b) {
          return $a['sequence'] <=> $b['sequence'];
    });
    $seo_array=[];
    $query = $connection->query("SELECT h1_tag,h2_tag FROM catapult_seo where page_name='Videos'");
    while ($row = $query->fetchAssoc()){      
       $seo_array[] = array($row['h1_tag'],$row['h2_tag']);
    }
   /*echo "<pre>"; print_r($seo_array);
   die();*/
    return array(
      '#theme' => 'product_videos',
      '#items'=>$video_array,
      '#seo_array' => $seo_array
    //'#title'=>'Our Article List'
    );


  /*if($activeThemeName == "admin"){
    \Drupal::configFactory()
      ->getEditable('system.theme')
      ->set('default', 'home')
      ->save();
    }

  	 $items = array('dvsir1','nandha23','ganesh3','hema4');

    return array(
        '#theme' => 'product_videos',
        '#items'=>$items,
        '#title'=>''
    ); */
  }
}
