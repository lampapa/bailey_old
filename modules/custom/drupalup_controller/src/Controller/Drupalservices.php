<?php
/**
 * @file
 * Contains \Drupal\hello\HelloController.
 */

namespace Drupal\drupalup_controller\Controller;

class Drupalservices {

  public function page() {
  	$service_array =[];
    $connection = \Drupal::database();
  	$nids = \Drupal::entityQuery('node')->condition('type','services')->execute();
  	foreach ($nids as $key => $ids) {
		$node = \Drupal\node\Entity\Node::load($ids);		
		$service_array[] = array(
        $node->getTitle(),$node->body->value,file_create_url($node->field_service_image->entity->getFileUri())
        );
	}	

  $seo_array=[];
    $query = $connection->query("SELECT h1_tag,h2_tag FROM catapult_seo where page_name='Our Services'");
    while ($row = $query->fetchAssoc()){      
       $seo_array[] = array($row['h1_tag'],$row['h2_tag']);
    }
  	    return array(
        '#theme' => 'our_services',
        '#items'=>$service_array,
        '#seo_array' => $seo_array
        //'#title'=>'Our Article List'
    );
  }

}