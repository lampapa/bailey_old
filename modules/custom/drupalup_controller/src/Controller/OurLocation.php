<?php
/**
 * @file
 * Contains \Drupal\hello\HelloController.
 */

namespace Drupal\drupalup_controller\Controller;

class OurLocation {

  public function page() {

    $service_array = [];
    $connection = \Drupal::database();
    $query = $connection->query("SELECT heading,company_name,address1,address2,zip_code,country,state,city,email,phone FROM catapult_our_locations");

    while ($row = $query->fetchAssoc()) {
      $service_array[] =array('heading' =>$row['heading'],
        'company_name' =>$row['company_name'],
        'address1' =>$row['address1'],
        'address2' =>$row['address2'],
        'zip_code' =>$row['zip_code'],
        'country' =>$row['country'],
        'state' =>$row['state'],
        'city' =>$row['city'],
        'email' =>$row['email'],
        'phone' =>$row['phone'],
        );
     }
    $seo_array=[];
    $query = $connection->query("SELECT h1_tag,h2_tag FROM catapult_seo where page_name='Our Locations'");
    while ($row = $query->fetchAssoc()){      
       $seo_array[] = array($row['h1_tag'],$row['h2_tag']);
    }
     /*print_r($seo_array);
     exit;*/
    /*$nids = \Drupal::entityQuery('node')->condition('type','our_location')->execute();
    foreach ($nids as $key => $ids) {
      $node = \Drupal\node\Entity\Node::load($ids);   
      $service_array[] = array(
      $node->getTitle(),$node->body->value
      );
    } */
    return array(
    '#theme' => 'our_location',
    '#items'=>$service_array,
    '#seo_array' => $seo_array
    //'#title'=>'Our Article List'
    );
  }

}

?>