<?php
/**
 * @file
 * Contains \Drupal\hello\HelloController.
 */

namespace Drupal\drupalup_controller\Controller;

class FAQ{
 
  public function page() {
    
    $faq_array =[];
    $connection = \Drupal::database();
    $nids = \Drupal::entityQuery('node')->condition('type','FAQ')->execute();
    foreach ($nids as $key => $ids) {
    $node      = \Drupal\node\Entity\Node::load($ids); 
    $faq_value = ""; 
    if(isset($node->field_faq_list)){
      $faq_value = $node->field_faq_list->value;
    }
    $faq_array[] = array(
        $node->getTitle(),$node->body->value, $faq_value);
  } 
    $seo_array=[];
    $query = $connection->query("SELECT h1_tag,h2_tag FROM catapult_seo where page_name='Faq'");
    while ($row = $query->fetchAssoc()){      
      $seo_array[] = array($row['h1_tag'],$row['h2_tag']);
    }

  return array(
      '#theme' => 'FAQ',
      '#items'=>$faq_array,
      '#seo_array' => $seo_array,
      '#title'=>''
  );
  }
}

?>