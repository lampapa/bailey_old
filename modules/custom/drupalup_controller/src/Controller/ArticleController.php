<?php
/**
 * @file
 * Contains \Drupal\hello\HelloController.
 */

namespace Drupal\drupalup_controller\Controller;
use Drupal\node\Entity\NodeType;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\commerce_product\Entity\Product;
use Drupal\Core\Database\Database;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\commerce_price\Price;
use Drupal\taxonomy\Entity\Term;

require_once "sites/libraries/vendor/autoload.php";
$GLOBALS['partdetail'] = \Elasticsearch\ClientBuilder::create()->build();

class ArticleController{  
  public function content() {
    return array(
        '#markup' => '' . t('Hello there!') . '',
    );
  }

  public function page(){  
    $products_array     = [];     
    $user               = \Drupal::currentUser();
    $user_id            = $user->id();
    $temp_store_factory = \Drupal::service('session_based_temp_store');
    $temp_store         = $temp_store_factory->get('my_module_name', 4800);     
    $redirecturl        = $temp_store->get('last_url');
     global $base_url;
    if(isset($_SESSION['_sf2_attributes']['uid']) && $redirecturl != ''){
        $temp_store->delete('last_url');
        echo '<script>window.location.href="'.$redirecturl.'";</script>';exit;
    }
    $connection = \Drupal::database();
    $query      = $connection->query("SELECT NAME AS category_name,uri AS image_url,tid as category_id FROM taxonomy_term_field_data AS tfd LEFT JOIN taxonomy_term__field_category_image AS ci ON ci.entity_id = tfd.tid LEFT JOIN file_managed AS fm ON fm.fid =ci.field_category_image_target_id WHERE tfd.vid='category' ORDER BY tfd.tid DESC limit 0,4");
     while ($row = $query->fetchAssoc()) {
        $spliturl=str_replace('public://','',$row['image_url']);       
        $product1 = array($spliturl,$row['category_name'],$row['category_id']);
        $category_array[] = $product1;
    }
    $urldata=$connection->query("select dynamic_url from catapult_config_meta");
    $urlarr = $urldata->fetchAssoc();

    $query = $connection->query("SELECT * FROM commerce_product AS cp LEFT JOIN commerce_product__field_product_name AS pn ON
      pn.entity_id=cp.product_id LEFT JOIN commerce_product__field_new_feature AS nf ON nf.entity_id =cp.product_id LEFT JOIN commerce_product__field_partno AS pp ON pp.entity_id =cp.product_id LEFT JOIN commerce_product__field_stock_info AS si ON si.entity_id = cp.product_id LEFT JOIN commerce_product__field_total_price AS tp ON tp.entity_id=cp.product_id LEFT JOIN commerce_product__field_category_name AS fc ON fc.entity_id=cp.product_id LEFT JOIN taxonomy_term_field_data AS tfd ON fc.field_category_name_target_id = tfd.tid LEFT JOIN commerce_product__field_color AS pc ON pc.entity_id=cp.product_id LEFT JOIN commerce_product__field_featured  AS fea ON fea.entity_id=cp.product_id LEFT JOIN commerce_product__field_new  AS new ON new.entity_id=cp.product_id LEFT JOIN commerce_product__field_marketing_messages  AS mm ON mm.entity_id=cp.product_id LEFT JOIN commerce_product__field_wishlist AS fw ON fw.entity_id=cp.product_id LEFT JOIN commerce_product__field_product_image AS pim ON pim.entity_id =cp.product_id  where fea.field_featured_value = 1");
     while ($row = $query->fetchAssoc()) {
        $product_id = $row['product_id'];
            $status1 = 0;
            $query1 = $connection->query("SELECT * FROM commerce_wish_list_user where product_id ='".$product_id."' and user_id = '".$user_id."' ");
            while($row1 = $query1->fetchAssoc()){
              $status1 = $row1['status1'];
            } 
            if($row['field_product_image_value'] =='no_image_icon.PNG'){
                  $spliturl=$base_url.'/sites/default/files/no_image_icon.PNG';
            }else{
               $spliturl=$urlarr['dynamic_url'].$row['field_product_image_value'];
            }
           //$spliturl=str_replace('public://','',$row['uri']);
             // $spliturl=$urlarr['dynamic_url'].$row['field_product_image_value'];

       $prodname = mb_strimwidth($row['field_product_name_value'], 0, 30, "...");
        $product1 = array($row['product_id'],$prodname,$row['field_new_feature_value'],$row['field_partno_value'],$row['field_stock_info_value'],round($row['field_total_price_number'],2),$spliturl,$status1,$row['field_product_name_value'],$row['field_color_value'],$row['name'],$row['field_new_value'],$row['field_marketing_messages_value']);
        $products_array[] = $product1;
    }
    $drupalsample = "My hello"; 
    return array(
        '#theme' => 'article_list',
        '#items'=>$category_array,
        '#products' =>$products_array,
        '#drupalsample'=>$drupalsample,
        '#cache' => ['max-age' => 0,],        
    );
  }

  /*public function featured(){
    $user               = \Drupal::currentUser();
    $user_id            = $user->id();
    $temp_store_factory = \Drupal::service('session_based_temp_store');
    $temp_store         = $temp_store_factory->get('my_module_name', 4800);     
    $redirecturl        = $temp_store->get('last_url');
    if(isset($_SESSION['_sf2_attributes']['uid']) && $redirecturl != ''){
        $temp_store->delete('last_url');
        echo '<script>window.location.href="'.$redirecturl.'";</script>';exit;
    }
    $connection = \Drupal::database();
    $query = $connection->query("SELECT NAME AS category_name,uri AS image_url,tid as category_id FROM taxonomy_term_field_data AS tfd LEFT JOIN taxonomy_term__field_category_image AS ci ON ci.entity_id = tfd.tid LEFT JOIN file_managed AS fm ON fm.fid =ci.field_category_image_target_id WHERE tfd.vid='category' ORDER BY tfd.tid ASC limit 0,4");
    while($row = $query->fetchAssoc()) {
        $spliturl=str_replace('public://','',$row['image_url']);       
        $product1 = array($spliturl,$row['category_name'],$row['category_id']);
        $category_array[] = $product1;
    }     
    $query = $connection->query("SELECT * FROM commerce_product AS cp LEFT JOIN commerce_product__field_product_name AS pn ON
      pn.entity_id=cp.product_id LEFT JOIN commerce_product__field_new_feature AS nf ON nf.entity_id =cp.product_id LEFT JOIN commerce_product__field_partno AS pp ON pp.entity_id =cp.product_id LEFT JOIN commerce_product__field_stock_info AS si ON si.entity_id = cp.product_id LEFT JOIN commerce_product__field_total_price AS tp ON tp.entity_id=cp.product_id LEFT JOIN commerce_product__field_category_name AS fc ON fc.entity_id=cp.product_id LEFT JOIN taxonomy_term_field_data AS tfd ON fc.field_category_name_target_id = tfd.tid LEFT JOIN commerce_product__field_color AS pc ON pc.entity_id=cp.product_id LEFT JOIN commerce_product__field_featured  AS fea ON fea.entity_id=cp.product_id LEFT JOIN commerce_product__field_new  AS new ON new.entity_id=cp.product_id LEFT JOIN commerce_product__field_marketing_messages  AS mm ON mm.entity_id=cp.product_id LEFT JOIN commerce_product__field_wishlist AS fw ON fw.entity_id=cp.product_id LEFT JOIN commerce_product__field_product_image AS pim ON pim.entity_id =cp.product_id LEFT JOIN file_managed AS fm ON fm.fid =pim.field_product_image_target_id where fea.field_featured_value = 1");
     while ($row = $query->fetchAssoc()) {
        $product_id = $row['product_id'];
            $status1 = 0;
            $query1 = $connection->query("SELECT * FROM commerce_wish_list_user where product_id ='".$product_id."' and user_id = '".$user_id."' ");
            while($row1 = $query1->fetchAssoc()){
              $status1 = $row1['status1'];
            } 
           $spliturl=str_replace('public://','',$row['uri']);
        $prodname = mb_strimwidth($row['field_product_name_value'], 0, 30, "...");

       

        $entity_manager = \Drupal::entityManager();
        $product_get      = \Drupal\commerce_product\Entity\Product::load($row['product_id']);
        $color_value      = [];
        foreach($product_get->getVariationIds() as $value){
          $product_variation = $entity_manager->getStorage('commerce_product_variation')->load((int)$value);
          $data_color        = $product_variation->get('field_color')->getValue();
          $vid               = 'color_parent';
          $terms             = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);
          foreach ($terms as $term){
            if($data_color[0]['target_id'] == $term->tid){
              $color_value[] = $term->name;
            }  
          }    
        }     
        $variant_val = implode(',',$color_value);
       

         

        $product1 = array($row['product_id'],$prodname,$row['field_new_feature_value'],$row['field_partno_value'],$row['field_stock_info_value'],round($row['field_total_price_number'],2),$spliturl,$status1,$row['field_product_name_value'],$row['field_color_value'],$row['name'],$row['field_new_value'],$row['field_marketing_messages_value'],$variant_val);
        $products_array[] = $product1;
    }

    echo json_encode($products_array);
    exit();
  }
*/
  public function featured(){
    $connection         = \Drupal::database();
    $user               = \Drupal::currentUser();
    $user_id            = $user->id();
    $status1            = 0;
    $temp_store_factory = \Drupal::service('session_based_temp_store');
    $temp_store         = $temp_store_factory->get('my_module_name', 4800);     
    $redirecturl        = $temp_store->get('last_url');  
    global $base_url; 
    if(isset($_SESSION['_sf2_attributes']['uid']) && $redirecturl != ''){
        $temp_store->delete('last_url');
        echo '<script>window.location.href="'.$redirecturl.'";</script>';exit;
    } 
    $products_array=[];
    $products = array();     
    $query = $connection->query("select cp.product_id from commerce_product__field_featured as cf join commerce_product as cp
on cf.entity_id =cp.product_id join commerce_product__field_total_price as ct on ct.entity_id = cp.product_id 
where ct.field_total_price_number != 0 and cf.field_featured_value=1 and cp.delete_flag=0 limit 0,8"); 
    $urldata=$connection->query("select dynamic_url from catapult_config_meta");
    $urlarr = $urldata->fetchAssoc();

    $ss=1;     
    while($row = $query->fetchAssoc()){
      $id                           = $row['product_id'];
      $product                      = \Drupal\commerce_product\Entity\Product::load($id);      
      $prodname                     =  $product->get('field_product_name')->getValue()[0]['value'];          
      $field_new_feature            =  $product->get('field_featured')->getValue()[0]['value'];
      $field_total_price            =  $product->get('field_total_price')->getValue()[0]['number'];
      $partnos                      =  $product->get('field_partno')->getValue()[0]['value'];
      $field_new_value              =  $product->get('field_new')->getValue()[0]['value'];
      $field_stock_info             =  $product->get('field_stock_info')->getValue()[0]['value'];
      //$field_color_value            =  $product->get('field_color')->getValue()[0]['value'];
      $field_new                    =  $product->get('field_new')->getValue()[0]['value'];
      //$pimage                       =  file_create_url($product->field_product_image->entity->getFileUri());
      if($product->get('field_product_image')->getValue()[0]['value'] =='no_image_icon.PNG'){
                  $pimage=$base_url.'/sites/default/files/no_image_icon.PNG';
            }else{
               $pimage                       =$urlarr['dynamic_url'].$product->get('field_product_image')->getValue()[0]['value'];
            }
      //$pimage                       =$urlarr['dynamic_url'].$product->get('field_product_image')->getValue()[0]['value'];
      $field_marketing_messages     =  $product->get('field_marketing_messages')->getValue()[0]['value'];
           
      $query5                       = $connection->query("SELECT * FROM commerce_wish_list_user where product_id ='".$id."' and user_id = '".$user_id."' ");
      while($row5 = $query5->fetchAssoc()){
        $status1 = $row5['status1'];
      } 

      $star_array=[];
        $star=[];
        $tot_cnt=0;
        $total_rating=0;
        $queryrating = $connection->query("SELECT rating,COUNT(rating) AS cnt FROM catapult_review WHERE product_id='".$id."' and flag=1 GROUP BY rating");
        while ($row_rat = $queryrating->fetchAssoc()) {
            $star[]=$row_rat['rating'];
            $tot_cnt +=$row_rat['cnt'];
            $star_array[] = array(
            $row_rat['rating'],
            $row_rat['cnt']      
          );
        }
        $new_arr = range(1,5);  
        $mis_arr=array_diff($new_arr, $star);   
        $count=count($star_array);    
                                                 
       
        foreach($mis_arr as $k=>$v){
          $star_array[$count+1][0]=$v;
          $star_array[$count+1][1]=0;
          $count=count($star_array);  
        
        }

       usort($star_array, function($a, $b) {
          return $a[0] <=> $b[0];
       }); 

       

      if(count($star_array) > 0){
        if($tot_cnt != 0){
          $total_rating=(($star_array[0][0] * $star_array[0][1]) + ($star_array[1][0] * $star_array[1][1]) + ($star_array[2][0] * $star_array[2][1]) + ($star_array[3][0] * $star_array[3][1]) + ($star_array[4][0] * $star_array[4][1]))/$tot_cnt;
        }  
      }



      if($field_new_feature == 1){
        //if($field_total_price != 0){
        if($ss%2==0){
        $field_marketing_messages=0;
        }else{
  $field_marketing_messages=5;
        }
            $product1 = array($id,
                          $prodname,
                          $field_new_feature,
                          $partnos,
                          $field_stock_info,
                          round($field_total_price,2),
                          $pimage,
                          $status1,
                          $prodname,
                          '',
                          $prodname,
                          $field_new_value,
                          $field_marketing_messages,
                          $total_rating,
                          $tot_cnt);

        //}        
        $products_array[] = $product1;
        $ss ++;
      }       
      $status1          = 0;  
    

    }
    echo json_encode($products_array);
    die();
  }

  public function fullcategory(){
    $connection = \Drupal::database();
    $return_arr = array();
    $query = $connection->query("SELECT NAME AS category_name,uri AS image_url,tid as category_id FROM taxonomy_term_field_data AS tfd LEFT JOIN taxonomy_term__field_category_image AS ci ON ci.entity_id = tfd.tid LEFT JOIN file_managed AS fm ON fm.fid =ci.field_category_image_target_id WHERE tfd.vid='category' ORDER BY tfd.tid ASC limit 0,4");
    while ($row = $query->fetchAssoc()) {
      $row_array['spliturl'] = str_replace('public://','',$row['image_url']);
      $row_array['product1'] = $row['category_name'];
      $row_array['category'] = $row['category_id'];
      array_push($return_arr,$row_array);
    }
    echo json_encode($return_arr);
    exit();
  }

/*  public function fullappyear(){
    $connection = \Drupal::database();
    $return_arr = array();
    $query = $connection->query("SELECT distinct(year) from catapult_application");
    while ($row = $query->fetchAssoc()) {
      if($row['year'] != ""){     
        $row_array['actual_year'] = $row['year'];
        $row_array['actual_year'] = $row['year'];
        array_push($return_arr,$row_array);
      }  
    }
    echo json_encode($return_arr);
    exit();
  }

  public function fullmake(){
    $actual_year = $_POST['actual_year'];
    $connection  = \Drupal::database();
    $return_arr  = array();   
    $query       = $connection->query("SELECT distinct(make) from catapult_application where year='".$actual_year."'");
    while ($row = $query->fetchAssoc()) {
      if($row['make'] != ""){
        $row_array['make'] = $row['make'];
        $row_array['application_id'] = $row['application_id'];
        array_push($return_arr,$row_array);
      }  
    }
    echo json_encode($return_arr);
    exit();
  }

  public function fullmodel(){
    $make = $_POST['make'];
    $year = $_POST['year'];
    $connection  = \Drupal::database();
    $return_arr  = array();
    $query       = $connection->query("SELECT distinct(model) from catapult_application where make='".$make."' and year ='".$year."'");
    while ($row = $query->fetchAssoc()) {
      if($row['model'] != ""){
        $row_array['model']          = $row['model'];
        $row_array['application_id'] = $row['application_id'];
        array_push($return_arr,$row_array);
      }  
    }
    echo json_encode($return_arr);
    exit();
  }


  public function fullengine(){
    $model = $_POST['model'];
    $make  = $_POST['make'];
    $year  = $_POST['year'];
    $connection  = \Drupal::database();
    $return_arr  = array();    
    $query       = $connection->query("SELECT distinct(engine) from catapult_application where make='".$make."' and year = '".$year."' and model='".$model."' ");
    while ($row = $query->fetchAssoc()) {
      if($row['engine'] != ""){      
        $row_array['engine'] = $row['engine'];
        $row_array['application_id'] = $row['application_id'];
        array_push($return_arr,$row_array);
      }  
    }
    echo json_encode($return_arr);
    exit();
  }*/

  /*public function partnoservice(){
    $gridtwo_arr[] = [];
    $connection = \Drupal::database();      
      $query = $connection->query("SELECT  cpf.field_partno_value FROM commerce_product__field_partno cpf join commerce_product cp on cpf.entity_id = cp.product_id where cp.delete_flag = 0 ");
    while($row = $query->fetchAssoc()){                       
      $gridtwo_arr[] = array('name' => $row['field_partno_value']);
    }
    echo json_encode($gridtwo_arr);
    exit();
  }*/

  public function partnoservice(){
    if(!empty($_POST)){
      $connection  = \Drupal::database();
      $urldata=$connection->query("select dynamic_url from catapult_config_meta");
      $urlarr = $urldata->fetchAssoc();
      $partnoarray = [];
      $ressult3 = $ressult2  = $ressult1 = [];
      $elastic = $GLOBALS['partdetail'];
      $ids = str_replace("-","",$_POST['id']);
      $responsecount1 = $responsecount2 = $responsecount3 = $resdata =  0;
      //echo "<br>";
      $params1 = [
           'index' => 'xclutchcpindex',
           'type' => 'products',          
            'body' => [
              "size"=> 10,
                'query' => [
                    'bool' => [
                        'should' => [
                       
                              'query_string'=> [
                                      'query'=> '*'.$ids.'*',
                                      'fields'=> ['partnobcc']
                                ]
                              ],
                          ]
                      ]
                  ]
          ];

      $response1 = $elastic->search($params1);  
      $responsecount1 = count($response1['hits']['hits']);

     /* echo "<pre>";
      print_r($response1['hits']['hits']);
      die();
*/
      foreach($response1['hits']['hits'] as $value){
          $partnoarray[] = $value['_source']['partno'];
          if($value['_source']['call'] == 'off'){
          $ressult1[] = array(
                  
                      //"partno"=>$value['_source']['partno'],
                    "product_name"=>$value['_source']['product_name'],
                    "sku"=>$value['_source']['sku'],
                     "product_image"=>$urlarr['dynamic_url'].$value['_source']['product_image'],
                     "productid"=>$value['_source']['productid']
                    );
                  }                      
        
      }
      $responsecount1 = count($response['hits']['hits']);

      if($responsecount1 < 10){ 
        $resltcount2 = 10-$responsecount1;
        $params2 = [
           'index' => 'xclutchcpindex',
           'type' => 'products',          
            'body' => [
              "size"=> 10,
                'query' => [
                    'bool' => [
                        'should' => [
                       
                              'query_string'=> [
                                      'query'=> '*'.$ids.'*',
                                      'fields'=> ['product_name']
                                ]
                              ],
                          ]
                      ]
                  ]
          ];
          $response2 = $elastic->search($params2);  
          foreach($response2['hits']['hits'] as $value){
            if(!in_array($value['_source']['partno'], $partnoarray)){
                $partnoarray[] = $value['_source']['partno'];
                if($value['_source']['call'] == 'off'){
                  $ressult2[] = array(
                        
                        //"partno"=>$value['_source']['partno'],
                        "product_name"=>$value['_source']['product_name'],
                        "sku"=>$value['_source']['sku'],
                         "product_image"=>$urlarr['dynamic_url'].$value['_source']['product_image'],
                         "productid"=>$value['_source']['productid']
                        );
               }   
            }
        
          }
          $responsecount2 = count($response2['hits']['hits']);
      }
      $resdata = $responsecount1+$responsecount2;
      if($resdata < 10){

        $params2 = [
           'index' => 'xclutchcpindex',
           'type' => 'products',          
            'body' => [
              "size"=> 10,
                'query' => [
                    'bool' => [
                        'should' => [
                       
                              'query_string'=> [
                                      'query'=> '*'.$ids.'*',
                                      'fields'=> ['sku']
                                ]
                              ],
                          ]
                      ]
                  ]
          ];
          $response2 = $elastic->search($params2);  
          foreach($response2['hits']['hits'] as $value){
             if(!in_array($value['_source']['partno'], $partnoarray)){
                $partnoarray[] = $value['_source']['partno'];
                if($value['_source']['call'] == 'off'){
                  $ressult3[] = array(
                          
                          //"partno"=>$value['_source']['partno'],
                           "product_name"=>$value['_source']['product_name'],
                           "sku"=>$value['_source']['sku'],
                           "product_image"=>$urlarr['dynamic_url'].$value['_source']['product_image'],
                           "productid"=>$value['_source']['productid']
                          );
                }  
              }  
        
          }
      }

      $ressult=array_merge($ressult1,$ressult2,$ressult3);
      echo json_encode($ressult);
    }   
    exit();
  }


  public function get_color_variant($id){
    $connection = \Drupal::database();
    $qry_variation=$connection->query("SELECT cp.variation_id,cp.price__number as price_number,cp.price__currency_code as price_currency_code,cc.name as color_name,tp.value as c_value,tp.img_url as c_url FROM commerce_product_variation_field_data AS cp LEFT JOIN commerce_product_variation__field_color AS pn ON pn.entity_id=cp.variation_id LEFT JOIN taxonomy_term_field_data AS cc ON cc.tid=pn.field_color_target_id LEFT JOIN catapult_color_filter as tp on tp.name = cc.name

      where cp.product_id =".$id." AND cp.status = 1 AND cc.status = 1 ");
     return $qry_variation;
  }
}