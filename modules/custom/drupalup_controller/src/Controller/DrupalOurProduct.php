<?php
namespace Drupal\drupalup_controller\Controller;
use Drupal\node\Entity\NodeType;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\commerce_product\Entity\Product;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\commerce\commerce_product;
use Drupal\commerce;
use Drupal\commerce_cart;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\commerce_cart\CartProviderInterface;
use Drupal\commerce_cart\CartManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_order\Entity\Order;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\CssCommand;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\commerce_price\Price;
use Drupal\taxonomy\Entity\Term;

include_once "modules/phpmailer/PHPMailer.php";
include_once "modules/phpmailer/SMTP.php";

class DrupalOurProduct{

    public function page() {
      $user        = \Drupal::currentUser();
      $user_id     = $user->id();
      $connection = \Drupal::database();
      $products = array();
      global $base_url;
      $query = $connection->query("SELECT * FROM commerce_product AS cp LEFT JOIN commerce_product__field_product_name AS pn ON pn.entity_id=cp.product_id LEFT JOIN commerce_product__field_new_feature AS nf ON nf.entity_id =cp.product_id LEFT JOIN commerce_product__field_partno AS pp ON pp.entity_id =cp.product_id LEFT JOIN commerce_product__field_stock_info AS si ON si.entity_id = cp.product_id LEFT JOIN commerce_product__field_total_price AS tp ON tp.entity_id=cp.product_id LEFT JOIN commerce_product__field_category_name AS fc ON fc.entity_id=cp.product_id LEFT JOIN taxonomy_term_field_data AS tfd ON fc.field_category_name_target_id = tfd.tid LEFT JOIN commerce_product__field_color AS pc ON pc.entity_id=cp.product_id LEFT JOIN commerce_product__field_wishlist AS fw ON fw.entity_id=cp.product_id LEFT JOIN commerce_product__field_product_image AS pim ON pim.entity_id =cp.product_id ORDER BY cp.product_id ASC");
       $urldata=$connection->query("select dynamic_url from catapult_config_meta");
      $urlarr = $urldata->fetchAssoc();
      while($row = $query->fetchAssoc()){
        //$spliturl=str_replace('public://','',$row['uri']);
        //$spliturl=$urlarr['dynamic_url'].$row['field_product_image_value'];
         if($row['field_product_image_value'] =='no_image_icon.PNG'){
                  $spliturl=$base_url.'/sites/default/files/no_image_icon.PNG';
            }else{
               $spliturl=$urlarr['dynamic_url'].$row['field_product_image_value'];
            }
        $product_id = $row['product_id'];
            $status1 = 0;
            $query1 = $connection->query("SELECT * FROM commerce_wish_list_user where product_id ='".$product_id."' and user_id = '".$user_id."' ");
            while($row1 = $query1->fetchAssoc()){
              $status1 = $row1['status1'];
            }
        $prodname = mb_strimwidth($row['field_product_name_value'], 0, 30, "...");
        $product1 = array($row['product_id'],$prodname,$row['field_new_feature_value'],$row['field_partno_value'],$row['field_stock_info_value'],round($row['field_total_price_number'],2),$spliturl,$status1,$row['field_product_name_value'],$row['field_color_value'],$row['name']);
        $products[] = $product1;
      }

      $drupalsample = "My hello";

      //$cart_items = $this->cart_items();
      return array(
          '#theme' => 'our_product',
          '#products'=>$products,
          '#drupalsample'=>$drupalsample,
          //'#cart_items'=>$cart_items
          '#title'=>''
      );
    }

    public function addToCart($pid) {
      $ajax_response = new AjaxResponse();
      if(isset($pid) && !empty($pid)){
        $store_id = 1;
        $order_type = 'default';
        $variation_id = $pid;

        $entity_manager = \Drupal::entityManager();
        $cart_manager = \Drupal::service('commerce_cart.cart_manager');
        $cart_provider = \Drupal::service('commerce_cart.cart_provider');

        // Drupal\commerce_store\Entity\Store::load($store_id);
        $store = $entity_manager->getStorage('commerce_store')->load($store_id);
        $product_variation = $entity_manager->getStorage('commerce_product_variation')->load($variation_id);

        // order type and store
        $cart = $cart_provider->getCart($order_type, $store);
        if (!$cart) {
        $cart = $cart_provider->createCart($order_type, $store);
        }
        //Create new order item
        $order_item = $entity_manager->getStorage('commerce_order_item')->create(array(
        'type' => 'default',
        'purchased_entity' => (string) $variation_id,
        'quantity' => 1,
        'unit_price' => 1,
        ));
        $order_item->save();
        $res = $cart_manager->addOrderItem($cart, $order_item);

        $connection = \Drupal::database();
        $query = $connection->query("SELECT order_id FROM commerce_order ORDER BY order_id DESC LIMIT 1;");
        /*while($row = $query->fetchAssoc()){
          echo $id = $row['order_id'];
        }*/

        $query = $connection->query("SELECT count(distinct purchased_entity) as cnt FROM commerce_order_item");
        while($row = $query->fetchAssoc()){
          echo $id = $row['cnt'];
        }
      }
    }

    public function cartsaving(){
      $connection = \Drupal::database();
      $id         =  $_POST['ids'];
      $query      = $connection->query("SELECT count(quantity) as cnt FROM commerce_order_item where purchased_entity='".$id."'");
      while($row = $query->fetchAssoc()){
         $cnt    = $row['cnt'];
      }
      if($cnt >= 1){
        echo "exist";
      }else{
        $this->addToCart($id);
      }
      exit();
    }
    public function wish_list(){
      $products = array();
      $connection = \Drupal::database();
      $user        = \Drupal::currentUser();
      $user_id     = $user->id();
      global $base_url;
        /*$query = $connection->query("SELECT * FROM commerce_product AS cp LEFT JOIN commerce_product__field_product_name AS pn ON
      pn.entity_id=cp.product_id LEFT JOIN commerce_product__field_new_feature AS nf ON nf.entity_id =cp.product_id LEFT JOIN commerce_product__field_partno AS pp ON pp.entity_id =cp.product_id LEFT JOIN commerce_product__field_stock_info AS si ON si.entity_id = cp.product_id LEFT JOIN commerce_product__field_total_price AS tp ON tp.entity_id=cp.product_id LEFT JOIN commerce_product__field_category_name AS fc ON fc.entity_id=cp.product_id LEFT JOIN taxonomy_term_field_data AS tfd ON fc.field_category_name_target_id = tfd.tid LEFT JOIN commerce_product__field_color AS pc ON pc.entity_id=cp.product_id  LEFT JOIN commerce_product__field_product_image AS pim ON pim.entity_id =cp.product_id LEFT JOIN file_managed AS fm ON fm.fid =pim.field_product_image_target_id  ORDER BY cp.product_id ASC ");*/
      $urldata=$connection->query("select dynamic_url from catapult_config_meta");
      $urlarr = $urldata->fetchAssoc();

        $query = $connection->query("SELECT * FROM commerce_product AS cp LEFT JOIN commerce_product__field_product_name AS pn ON pn.entity_id=cp.product_id
    LEFT JOIN commerce_product__field_new_feature AS nf ON nf.entity_id =cp.product_id LEFT JOIN commerce_product__field_partno AS pp ON pp.entity_id =cp.product_id
    LEFT JOIN commerce_product__field_stock_info  AS si ON si.entity_id = cp.product_id LEFT JOIN commerce_product__field_total_price AS tp ON tp.entity_id=cp.product_id
    LEFT JOIN commerce_product__field_wishlist AS fw ON fw.entity_id=cp.product_id LEFT JOIN commerce_product__field_product_image AS pim ON pim.entity_id =cp.product_id 
    LEFT JOIN commerce_product__field_new  AS f_n ON f_n.entity_id=cp.product_id
    LEFT JOIN commerce_product__field_marketing_messages  AS mm ON mm.entity_id=cp.product_id

    LEFT JOIN commerce_product__field_brand AS b ON b.entity_id=cp.product_id
    LEFT JOIN commerce_product__field_category_name AS cn ON cn.entity_id=cp.product_id

    LEFT JOIN commerce_product__field_product_category AS fc ON fc.entity_id=cp.product_id
    LEFT JOIN commerce_product__field_color AS pc ON pc.entity_id=cp.product_id

    LEFT JOIN commerce_wish_list_user AS cw ON cw.product_id=cp.product_id

    where cw.user_id =".$user_id." and cw.status1=1

    ORDER BY cp.product_id ASC");


        while ($row = $query->fetchAssoc()) {
            /*$product_id = $row['product_id'];
            $status1 = 0;
            $query1 = $connection->query("SELECT * FROM commerce_wish_list_user where product_id ='".$product_id."' and user_id = '".$user_id."' ");
            while($row1 = $query1->fetchAssoc()){
              $status1 = $row1['status1'];
            } */
              $status1 = 1;
             // $spliturl=str_replace('public://','',$row['uri']);
            if($row['field_product_image_value'] =='no_image_icon.PNG'){
                  $spliturl=$base_url.'/sites/default/files/no_image_icon.PNG';
            }else{
               $spliturl=$urlarr['dynamic_url'].$row['field_product_image_value'];
            }
              //$spliturl=$urlarr['dynamic_url'].$row['field_product_image_value'];
              $prodname = mb_strimwidth($row['field_product_name_value'], 0, 55, "...");
              $pid = $row['product_id'];
              $category = $row['field_category_name_target_id'];
              $field_wishlist_value = $row['field_wishlist_value'];
              $category_name = $this->get_category_name($row['field_category_name_target_id']);

              $discount_price =round($row['field_total_price_number'],1)+((10/100)*round($row['field_total_price_number'],1));

              //color variant picking
              $temp_variant = array();
              $result_variant = $this->get_color_variant($pid);
              while($row1 = $result_variant->fetchAssoc()){
                $temp_variant[] = $row1['c_value'];
              }
              //shuffle($colortemp);
              $variant_val = implode(',',$temp_variant);

              $product1 = array($row['product_id'],$prodname,$row['field_new_feature_value'],$row['field_partno_value'],$row['field_stock_info_value'],round($row['field_total_price_number'],2),$spliturl,$status1,$row['field_product_name_value'],$row['field_color_value'],$field_wishlist_value,

                $category_name,round($discount_price,2),$row['field_new_value'],$row['field_marketing_messages_value'],$variant_val,$category
            );

            $products[] = $product1;
        }
        /*echo "<pre>";
        print_r($products);
        echo "</pre>";
        exit();*/
        return array(
        '#theme' => 'wish_list',
        '#products'=> $products,
        '#title'=>'Our Wish List'
        );
      }

      public function get_category_name($id){
          $connection = \Drupal::database();
          $get_category_name = $connection->query("SELECT DISTINCT tp.name FROM `taxonomy_term_field_data` tp JOIN commerce_product__field_category_name AS cn ON tp.tid=cn.field_category_name_target_id
             WHERE vid = 'category' AND STATUS='1' AND tp.tid ='".$id."'")->fetchAssoc();
          //print_r($get_category_name);
          $cat_name = $get_category_name['name'];
          return $cat_name;
    }


      public function cart_list(){        
        $connection     = \Drupal::database();
        $data           = "";
        $product_name   = array();
        $product_image  = array();
        $product_price  = array();
        $product_id     = array();
        $brand          = array();
        global $base_url;
        if(isset($_GET['id']) && (!empty($_GET['id'])) ){
            $myArray = explode(',', $_GET['id']);
          foreach ($myArray as $value){
            $query = $connection->query("SELECT * FROM commerce_product AS cp LEFT JOIN commerce_product__field_product_name AS pn ON pn.entity_id=cp.product_id
            LEFT JOIN commerce_product__field_new_feature AS nf ON nf.entity_id =cp.product_id LEFT JOIN commerce_product__field_partno AS pp ON pp.entity_id =cp.product_id
            LEFT JOIN commerce_product__field_stock_info  AS si ON si.entity_id = cp.product_id LEFT JOIN commerce_product__field_total_price AS tp ON tp.entity_id=cp.product_id
            LEFT JOIN commerce_product__field_wishlist AS fw ON fw.entity_id=cp.product_id LEFT JOIN commerce_product__field_product_image AS pim ON pim.entity_id =cp.product_id 
            where cp.product_id='".$value."'");
            $urldata=$connection->query("select dynamic_url from catapult_config_meta");
          $urlarr = $urldata->fetchAssoc();
            while($row = $query->fetchAssoc()){

            if($row['field_product_image_value'] =='no_image_icon.PNG'){
                  $spliturl=$base_url.'/sites/default/files/no_image_icon.PNG';
            }else{
               $spliturl=$urlarr['dynamic_url'].$row['field_product_image_value'];
            }
              
          //$spliturl=$urlarr['dynamic_url'].$row['field_product_image_value'];
              ////$spliturl=str_replace('public://','',$row['uri']);
              $prodname = mb_strimwidth($row['field_product_name_value'], 0, 30, "...");
              $product1 = array($row['product_id'],$prodname,$row['field_new_feature_value'],$row['field_partno_value'],$row['field_stock_info_value'],round($row['field_total_price_number'],2),$spliturl,$row['field_wishlist_value'],$row['field_product_name_value'],$row['field_color_value'],$row['field_product_category_value']);
              $product_name[]  = $prodname;
              $product_image[] = $spliturl;
              $product_price[] = round($row['field_total_price_number'],2);
              $product_id[]    = $row['product_id'];
              $pid             = $row['product_id'];
              $query           = $connection->query("SELECT *  FROM commerce_product__field_brand AS cpb   LEFT JOIN taxonomy_term_field_data AS txf ON cpb.field_brand_target_id =  txf.tid WHERE cpb.entity_id = '".$pid."'");
              while($row = $query->fetchAssoc()){
                 $brand[] = $row['name'];
              }
            }
          }        
        }
        return array(
            '#theme' => 'cart_list',
            '#product_name'=>$product_name,
            '#product_image'=>$product_image,
            '#product_price'=>$product_price,
            '#product_id'=>$product_id,
            '#brand'=>$brand,
            '#title'=>''
        );
      }

    public function getwishlist(){
      $delete_flag = 0;
      $connection  = \Drupal::database();
      $user        = \Drupal::currentUser();
      $user_id     = $user->id();
      $product1 = array();
      $status1  = "";
      $spliturl = "";
      $data = "";

      $pname                    =  "";
      $partnos                  =  "";
      $field_stock_info         =  "";
      $field_new_feature        =  "";
      $field_total_price        =  "";
      $field_new                =  "";
      $field_marketing_messages =  "";

      $query = $connection->query("SELECT product_id,status1 from commerce_wish_list_user where user_id =".$user_id." and status1=1  ORDER BY product_id ASC");
      while($row = $query->fetchAssoc()){
        $id = $row['product_id'];
        $query2       = $connection->query("SELECT delete_flag FROM commerce_product where product_id='".$id."'");
        while($row2 = $query2->fetchAssoc()){
             $delete_flag = $row2['delete_flag'];
        }

        if($delete_flag == 0){
          $status1      = $row['status1'];
          $product      = \Drupal\commerce_product\Entity\Product::load($id);
          if ( array_key_exists(0,$product->get('field_product_name')->getValue() ) ){
            $pname        =  $product->get('field_product_name')->getValue()[0]['value'];

          }
          if ( array_key_exists(0,$product->get('field_partno')->getValue() ) ){
            $partnos      =  $product->get('field_partno')->getValue()[0]['value'];
          }
          if ( array_key_exists(0,$product->get('field_stock_info')->getValue() ) ){
            $field_stock_info      =  $product->get('field_stock_info')->getValue()[0]['value'];
          }
          if ( array_key_exists(0,$product->get('field_new_feature')->getValue() ) ){
            $field_new_feature      =  $product->get('field_new_feature')->getValue()[0]['value'];
          }

          if ( array_key_exists(0,$product->get('field_total_price')->getValue() ) ){
            $field_total_price      =  $product->get('field_total_price')->getValue()[0]['number'];
          }

          if ( array_key_exists(0,$product->get('field_new')->getValue() ) ){
            $field_new      =  $product->get('field_new')->getValue()[0]['value'];
          }





         /* $field_stock_info =  $product->get('field_stock_info')->getValue()[0]['value'];
          $field_new_feature =  $product->get('field_new_feature')->getValue()[0]['value'];
          $field_total_price        =  $product->get('field_total_price')->getValue()[0]['number'];
          $field_new                =  $product->get('field_new')->getValue()[0]['value'];*/
          $field_marketing_messages =  "";
          $urldata=$connection->query("select dynamic_url from catapult_config_meta");
          $urlarr = $urldata->fetchAssoc();

            if($product->get('field_product_image')->getValue()[0]['value'] =='no_image_icon.PNG'){
                  $spliturl=$base_url.'/sites/default/files/no_image_icon.PNG';
            }else{
               $spliturl=$urlarr['dynamic_url'].$product->get('field_product_image')->getValue()[0]['value'];
            }
          //$spliturl=$urlarr['dynamic_url'].$product->get('field_product_image')->getValue()[0]['value'];
          //$spliturl       =  file_create_url($product->field_product_image->entity->getFileUri());
          $discount_price =round($field_total_price,2)+((10/100)*round($field_total_price,2));
          if(isset($product->get('field_brand')->getValue()[0]['value'])){
            $brands       =  $product->get('field_brand')->getValue()[0]['value'];
          }else{
            $brands = "";
          }

          $featured     =  $product->get('field_featured')->getValue();
          //$pimage       =  file_create_url($product->field_product_image->entity->getFileUri());
           if($product->get('field_product_image')->getValue()[0]['value'] =='no_image_icon.PNG'){
                  $pimage=$base_url.'/sites/default/files/no_image_icon.PNG';
            }else{
               $pimage=$urlarr['dynamic_url'].$product->get('field_product_image')->getValue()[0]['value'];
            }
          //$pimage       = $urlarr['dynamic_url'].$product->get('field_product_image')->getValue()[0]['value'];
          if($field_marketing_messages == 0){
            $field_marketing_messages ="Best Seller";
          }else if($field_marketing_messages == 1){
            $field_marketing_messages ="Only A Few Left";
          }else if($field_marketing_messages == 2){
            $field_marketing_messages ="10 % Off";
          }else if($field_marketing_messages == 3){
            $field_marketing_messages ="BOGO Sale";
          }else if($field_marketing_messages == 4 ){
            $field_marketing_messages ="SOLD OUT";
          }
          $product1[] = array('product_id'=>$row['product_id'],
                        'pname'=>$pname,
                        'field_new_feature'=>$field_new_feature,
                        'partnos'=>$partnos,
                        'field_stock_info'=>$field_stock_info,
                        'field_total_price'=>round($field_total_price,2),
                        'spliturl'=>$spliturl,
                        'status1'=>$status1,
                        'discount_price'=>$discount_price,
                        'field_new'=>$field_new,
                        'field_marketing_messages'=>$field_marketing_messages,
                        'brands'=>$brands,
                        'field_categoty'=>''
                      );
        }
        $status1 = 0;
      }

      //echo $data;

      echo json_encode($product1);
        exit();
      }

    public function cart_listcom(){
      global $base_url;
      $entity_manager = \Drupal::entityManager();
      $uid            = \Drupal::currentUser()->id();
      $connection     = \Drupal::database();

      $query1 = $connection->query("SELECT order_id,`total_price__number` FROM `commerce_order` WHERE uid=".$uid." AND state = 'draft' AND cart=1");
      $result = $query1->fetchAssoc();
      $var ='';
      if(!empty($result)){
          $order_id = $result['order_id'];
          $total_price = number_format($result['total_price__number'],2);
          $query = $connection->query("SELECT cp.product_id,cp.price__number,oi.title,oi.quantity,oi.unit_price__number,oi.total_price__number,oi.purchased_entity,pim.field_product_image_value FROM commerce_product_variation_field_data AS cp
          LEFT JOIN commerce_order_item AS oi ON cp.variation_id = oi.`purchased_entity`
          LEFT JOIN commerce_product__field_product_image AS pim ON pim.entity_id =cp.product_id
           WHERE oi.order_id =".$order_id."  ORDER BY cp.product_id ASC");
          $urldata=$connection->query("select dynamic_url from catapult_config_meta");
          $urlarr = $urldata->fetchAssoc();
          

          while($row = $query->fetchAssoc()){
            if($row['field_product_image_value'] =='no_image_icon.PNG'){
                  $spliturl=$base_url.'/sites/default/files/no_image_icon.PNG';
            }else{
               $spliturl=$urlarr['dynamic_url'].$row['field_product_image_value'];
            }
            //$spliturl=$urlarr['dynamic_url'].$row['field_product_image_value'];
              //$spliturl   =str_replace('public://','',$row['uri']);
              $prodname   = mb_strimwidth($row['title'], 0, 35, "...");
              $p_id       = $row['product_id'];
               $variant_id = $row['purchased_entity'];

              $color_select = "";

              /*color variation*/
              $product_detail      = \Drupal\commerce_product\Entity\Product::load($p_id);


              foreach($product_detail->getVariationIds() as $key=>$value){
               /* echo $value;
                echo "<br>";*/
                $product_variation = $entity_manager->getStorage('commerce_product_variation')->load((int)$value);
               /* echo "<pre>";
                print_r($product_variation);*/

                $data_color = $product_variation->get('field_color')->getValue()[0]['target_id'];
                if($variant_id == $value){
                   $color_select = $data_color;
                    $field_sku = $product_variation->get('sku')->getValue()[0]['value'];
                  //die();
                }
                if($data_color == 152){
                  $basic_price = $product_variation->get('price')->getValue()[0]['number'];
                }
              }

              $vid   = 'color_parent';
              $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);


               foreach ($terms as $term){
                $term_obj = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($term->tid);
                //if($term->tid != 152){
                  if($color_select == $term->tid){
                   $field_colorname = $term_obj->get('field_colorname')->value;

                  }
                //}
              }

              /*price calculate*/

        //      $p_price         =  round($row['unit_price__number'],2);
              $p_price         =  round($row['price__number'],2);
              $basic_price_val =  $p_price - $basic_price;
              $hidden_price    =  $p_price - $basic_price_val;
              $basic_price_val =   number_format((float)$basic_price_val, 2, '.', '');
              $hidden_price =   number_format((float)$hidden_price, 2, '.', '');






              /*end of price calculate*/

              /*color variation end*/




              $p_name =$row['title'];

              $p_quantity =intval($row['quantity']);
              $image = $spliturl;
              $urls = $base_url."/checkout/".$order_id."/orderinformation";
              $p_totalprice = $p_price*$p_quantity;

              ///// modified by nandha ////
              $qry_variation=$connection->query("SELECT cp.variation_id,cp.price__number as price_number,cp.price__currency_code as price_currency_code,cc.name as color_name,tp.value as c_value,tp.img_url as c_url FROM commerce_product_variation_field_data AS cp LEFT JOIN commerce_product_variation__field_color AS pn ON pn.entity_id=cp.variation_id LEFT JOIN taxonomy_term_field_data AS cc ON cc.tid=pn.field_color_target_id LEFT JOIN catapult_color_filter as tp on tp.name = cc.name

              where cp.product_id=".$p_id." AND cp.variation_id=".$variant_id." AND cp.status = 1 AND cc.status = 1 ");
              $vari_filter = [];
              while($row = $qry_variation->fetchAssoc()){
                $price_number = $row['price_number'];
                $price_currency_code = $row['price_currency_code'];
                $color_name = $row['color_name'];
                $c_value = $row['c_value'];
                $c_url = $row['c_url'];

              }

             /* echo $c_value;
              die();*/
              /*get color code*/
              /*$term_obj = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($id);
              if(isset($term_obj->get('field_colors')->entity)){
                $url = file_create_url($term_obj->get('field_colors')->entity->getFileUri());
              }
              $colornames = $term_obj->get('field_colorname')->value;

              if($color_names == $term_obj->get('name')->value){

              }  */
              /*end of color code*/
             /*$product      = \Drupal\commerce_product\Entity\Product::load($p_id);

             foreach($product->getVariationIds() as $key=>$value){
              $product_variation = $entity_manager->getStorage('commerce_product_variation')->load((int)$value);
              $prices = $product_variation->get('price')->getValue();
              $sku = $product_variation->get('sku')->getValue();
              $data_color = $product_variation->get('field_color')->getValue();
              $skuvalue = $sku[0]['value'];
              if($data_color[0]['target_id'] != 152){
                $data5 .= $this->getallcolors_select($data_color[0]['target_id'],$skuvalue,$prices[0]['number'],$field_total_price);
              }
            } */
       
              $var .="<li>
              <div class='cart-list-left'>
                  <img ng-src='$spliturl' err-SRC='".$base_url."/sites/default/files/logobrand.png' alt='' />
              </div>
              <div class='lf-hle'>
                <div class='lft'>
                  <div class='cart-list-right'>
                    <a href='".$base_url."/product_desc/".$p_id."' class='quick-shop info' data-toggle='tooltip' data-original-title='".$p_name."'>
                                      <div class='brand bld'>".$prodname."<br><font style='color:red'>Sku: </font>".$field_sku."</div>
                                    </a>";





                  if($color_name != 'No Color'){


                      $var .= "<div style='clear:both'></div><div><span class='price_color bgg'>
                      <div style='background-color:$field_colorname'></div>";
                      $var .=  "<div class='clr_bgg' style='background-color:$field_colorname'></div></span></div><span class='variantname quantity pop'> $color_name</span>";


                  }
                  $var .=  "<div class='hlle'><div class='fl_lf'><span class='brand bld fnt_style'>Total Price: <span class='price_color'>$".($hidden_price + $basic_price_val) ."</span></span></div>";
                    /*$var .= "<div class='fl_rgt'><div class='fnt_sze'><span class='price_color pos_rel'>($$hidden_price</span>";*/
                   /* $var .="<span class='pls' style='float: left;color: #000;'>+</span>";*/
                   // $var .= "<span class='price_color rt'>$$basic_price_val)</span></div></div>";
                  $var .= "</div><span class='quantity fl_lftt'>Qty. : $p_quantity</span></div>";

                  "</div>";
               $var .=  "</div>
                <div class='lft_wdt flt_nne'><div productid='$p_id' variantid='$variant_id' class='reomve_product tbl_cll' id='delete_items'><img src='$base_url/themes/AUTOTECH/images/table-delete.png' alt='delete-icon'></div>
                </div>
              </div></li>";
          }
      }
      if($var ==''){
        $var ='empty';
      }
      echo $var;
      exit();
  }


    public function wishlistadd() {
      $connection = \Drupal::database();

      //$data = json_decode(file_get_contents("php://input"), true);

      $product_id  = $_POST['ids'];
      $user        = \Drupal::currentUser();
      $user_id     = $user->id();
      if($user_id != 0){
        $query = $connection->query("SELECT count(user_id) as count FROM commerce_wish_list_user where product_id ='".$product_id."' and user_id = '".$user_id."' ");
        while($row = $query->fetchAssoc()){
           $count = $row['count'];
        }
        if($count == 1){
          $query = $connection->query("SELECT status1 FROM commerce_wish_list_user where product_id ='".$product_id."' and user_id = '".$user_id."' ");
          while($row = $query->fetchAssoc()){
             $wish_list = $row['status1'];
          }
          if($wish_list == 1){
            $query      = $connection->query("update commerce_wish_list_user set status1 = 0  where product_id = '".$product_id."' and user_id = '".$user_id."' ");
            echo 0;
          }else{
            $query      = $connection->query("update commerce_wish_list_user set status1 = 1  where product_id = '".$product_id."' and user_id = '".$user_id."' ");
            echo 1;
          }
        }else{
           $query   = $connection->query("insert into commerce_wish_list_user(status1,product_id,user_id) values(1,'".$product_id."','".$user_id."')");
              echo 1;
        }
      }else{
        echo"login";
      }
      exit();
    }

    public function prodcomp(){
      global $base_url;
        $connection = \Drupal::database();
        $data       = "";
        $urldata=$connection->query("select dynamic_url from catapult_config_meta");
      $urlarr = $urldata->fetchAssoc();
        foreach ($_POST['productids'] as $value){
          

          $query = $connection->query("SELECT * FROM commerce_product AS cp LEFT JOIN commerce_product__field_product_name AS pn ON pn.entity_id=cp.product_id
          LEFT JOIN commerce_product__field_new_feature AS nf ON nf.entity_id =cp.product_id LEFT JOIN commerce_product__field_partno AS pp ON pp.entity_id =cp.product_id
          LEFT JOIN commerce_product__field_stock_info  AS si ON si.entity_id = cp.product_id LEFT JOIN commerce_product__field_total_price AS tp ON tp.entity_id=cp.product_id
          LEFT JOIN commerce_product__field_wishlist AS fw ON fw.entity_id=cp.product_id LEFT JOIN commerce_product__field_product_image AS pim ON pim.entity_id =cp.product_id 
          where cp.product_id='".$value."'");

          while($row = $query->fetchAssoc()){
            $data      .= "<div class='compare-switcher-box'><div class='compare-close-icons'><i id='".$row['product_id']."' class='fa fa-times compare_remove' aria-hidden='true'></i></div>";
            if($row['field_product_image_value']=='no_image_icon.PNG'){
                  $spliturl=$base_url.'/sites/default/files/no_image_icon.PNG';
            }else{
               $spliturl   =$urlarr['dynamic_url'].$row['field_product_image_value'];
            }
            //$spliturl   = str_replace('public://','',$row['uri']);
            //$spliturl   =$urlarr['dynamic_url'].$row['field_product_image_value'];
            $prodname   = mb_strimwidth($row['field_product_name_value'], 0, 60, "...");
            $product1   = array($row['product_id'],$prodname,$row['field_new_feature_value'],$row['field_partno_value'],$row['field_stock_info_value'],round($row['field_total_price_number'],2),$spliturl,$row['field_wishlist_value'],$row['field_product_name_value']);

            $data .= "<img src='".$spliturl."'>
            <h6 class='text-up'>".$prodname."</h6>
          </div>";

            /*$data .= "<img class='featured_img' src='sites/default/files/".$spliturl."' alt='featured product'>";
            if($row['field_new_feature_value'] == 0){
              $data .= "<span class='new_brand'>New</span>";
            }else{
              $data .= "<img class='featured_img' src='sites/default/files/".$spliturl."' alt='featured product'>";
            }
            if($row['field_stock_info_value'] == 0){
              $data .= "<span class='stock_status in'>In Stock</span>";
            }else{
              $data .= "<span class='stock_status out'>Out of Stock</span>";
            }
            $data .= "<div class='featured_item_price'>
                        <span class='price_color'>$".round($row['field_total_price_number'])."</span>
                      </div>";


            $data .= "<div class='product_wishlist'>
                        <a id='wishtlist_class' wishvalue=".$row['product_id'].">
                          ";
                            if($row['field_wishlist_value'] == 1){

                              $data .=  "<i  class='fa fa-heart'";

                            }else{
                              $data .=  "<i  class='fa fa-heart-o'";
                            }
            $data .= "></i></a></div>";
            $data .= "</a>";*/


          }
        }
        /*$data .=  "<div class='compare-button'><a href='compare.html'>Compare</a></div>";*/
        echo $data;
        exit;
      }

    public function countofwishcart(){
      $wish_counts = 0;
      $uid = \Drupal::currentUser()->id();
      $user        = \Drupal::currentUser();
      $user_id     = $user->id();
      //$user_id     = $uid->id();
      $connection = \Drupal::database();
       $query = $connection->query("SELECT COUNT(DISTINCT oi.purchased_entity) AS cartcount FROM commerce_order_item AS oi LEFT JOIN `commerce_order` AS o ON oi.order_id=o.order_id WHERE o.cart=1 and o.uid='".$uid."'");



      /*wishlist count*/
       $wishlistdata = $connection->query("SELECT product_id,status1 FROM commerce_wish_list_user where  user_id = '".$user_id."' and status1 = 1  ");
    /*  $query5                       = $connection->query("SELECT * FROM commerce_wish_list_user where product_id ='".$id."' and user_id = '".$user_id."' ");*/
      $wisht_counts = 0;
      while($row_wishlist = $wishlistdata->fetchAssoc()){
        $product_id = $row_wishlist['product_id'];
        $query_product = $connection->query("SELECT delete_flag FROM commerce_product where product_id='".$product_id."'");
        while($row_product = $query_product->fetchAssoc()){
          if( ($row_product['delete_flag'] == 0) && ($row_wishlist['status1'] == 1) ){
            $wish_counts++;
          }
        }

      }
      $wishcartcount =[];
      $wishcartcount['wishcount'] =$wish_counts;
      /*end of wishtlistcount*/



        while($row = $query->fetchAssoc()){
          $cartcount = $row['cartcount'];
            $wishcartcount['cartcount'] =$cartcount;
        }
        /*while($row = $wishlistdata->fetchAssoc()){
          $status1 = $row['status1'];
          $wishcartcount['wishcount'] =$status1;
        }*/


        if($uid != 0){
        echo json_encode($wishcartcount);
        }else{
        $wishcartcount['cartcount'] =0;
        echo json_encode($wishcartcount);
        }
        exit();
    }

    public function deletecartdata(){

      $uid        = \Drupal::currentUser()->id();
      $connection = \Drupal::database();
      $cartempty  = '';
      $variantid  = $_POST['variantid'];



      $query1 = $connection->query("SELECT order_id FROM commerce_order WHERE uid ='".$uid."' and cart =1");
      while($res_order = $query1->fetchAssoc()){
        $order_ids = $res_order['order_id'];
      }

     /* echo $order_ids;
      die();*/

      $query1 = $connection->query("SELECT order_item_id FROM commerce_order_item WHERE purchased_entity ='".$variantid."' and order_id ='".$order_ids."'");
      while($res = $query1->fetchAssoc()){
        $oritem_ids = $res['order_item_id'];
      }
      //echo "delete from commerce_order_item where order_item_id ='".$oritem_ids."'";
      $query2 = $connection->query("delete from commerce_order_item where order_item_id ='".$oritem_ids."'");
      $query3 = $connection->query("delete from commerce_order__order_items where order_items_target_id = '".$oritem_ids."'");
      $q = $connection->query("select count(order_id) as cntorder from commerce_order_item where order_id ='".$order_ids."'");
      while($val = $q->fetchAssoc()){
        if($val['cntorder']==0){
          $query4 = $connection->query("delete from commerce_order where order_id ='".$order_ids."'");
           $query5 = $connection->query("delete from users_shipping_profile where order_id ='".$order_ids."'");
          $cartempty ='empty';
        }
      }


      /*update orders*/

       $query1 = $connection->query("SELECT total_price__number FROM commerce_order_item WHERE order_id ='".$order_ids."' ");
      $tot = 0;
      while($res_order = $query1->fetchAssoc()){
        $tot += $res_order['total_price__number'];
      }

      $query1 = $connection->query("update commerce_order set total_price__number='".$tot."' WHERE order_id ='".$order_ids."' ");

      /*end of update orders*/

      echo $tot;
      exit();
    }


    public function addToCart_quantity() {


      $uid = \Drupal::currentUser()->id();

      /*print_r($uid);
      exit;*/

      if(!isset($_SESSION['_sf2_attributes']['uid']))
      {
          $_SESSION['cart_items']['ids']       = $_POST['ids'];
          $_SESSION['cart_items']['item_qty'] = $_POST['item_qty'];
          $_SESSION['cart_items']['price']    = $_POST['price'];
          $_SESSION['cart_items']['variantid']    = $_POST['variantid'];

          $message = 'Login';
          echo $message; exit;
      }
      $ajax_response = new AjaxResponse();
      //$pid           = $_POST['ids'];
      $price         = $_POST['price'];
      $quantity      = $_POST['item_qty'];
      $pid      = $_POST['variantid'];


      $connection = \Drupal::database();
      /*$query      = $connection->query("SELECT variation_id FROM commerce_product_variation_field_data where product_id='".$pid."'");
      while($row = $query->fetchAssoc()){
         $pid    = $row['variation_id'];
      }*/

     /* echo "SELECT COUNT(oi.purchased_entity) AS cnt FROM commerce_order_item AS  oi
LEFT JOIN `commerce_order` AS o ON oi.order_id=o.order_id  WHERE o.cart=1 AND oi.purchased_entity='".$pid."' AND o.uid='".$uid."'";
*/
/*echo "SELECT COUNT(oi.purchased_entity) AS cnt FROM commerce_order_item AS  oi
LEFT JOIN `commerce_order` AS o ON oi.order_id=o.order_id  WHERE o.cart=1 AND oi.purchased_entity='".$pid."' AND o.uid='".$uid."'";*/

      $query      = $connection->query("SELECT COUNT(oi.purchased_entity) AS cnt FROM commerce_order_item AS  oi
LEFT JOIN `commerce_order` AS o ON oi.order_id=o.order_id  WHERE o.cart=1 AND oi.purchased_entity='".$pid."' AND o.uid='".$uid."'");
      while($row = $query->fetchAssoc()){
         $cnt    = $row['cnt'];
      }
      if($cnt >= 1){
        echo "exist";
      }else{
        $store_id = 1;
        $order_type = 'default';
        $variation_id = $pid;

        $entity_manager = \Drupal::entityManager();
        $cart_manager = \Drupal::service('commerce_cart.cart_manager');
        $cart_provider = \Drupal::service('commerce_cart.cart_provider');

        // Drupal\commerce_store\Entity\Store::load($store_id);
        $store = $entity_manager->getStorage('commerce_store')->load($store_id);
        $product_variation = $entity_manager->getStorage('commerce_product_variation')->load($variation_id);

        // order type and store
        $cart = $cart_provider->getCart($order_type, $store);
        if (!$cart) {
        $cart = $cart_provider->createCart($order_type, $store);
        }
        //Create new order item



        $order_item = $entity_manager->getStorage('commerce_order_item')->create(array(
        'type' => 'default',
        'purchased_entity' => (string) $variation_id,
        'quantity' => $quantity,
        'unit_price' => $product_variation->getPrice(),
        ));
        $order_item->save();
        $res = $cart_manager->addOrderItem($cart, $order_item);
        $query = $connection->query("SELECT COUNT(DISTINCT oi.purchased_entity) AS cnt FROM commerce_order_item AS oi LEFT JOIN `commerce_order` AS o ON oi.order_id=o.order_id WHERE o.cart=1 and o.uid='".$uid."'");
        while($row = $query->fetchAssoc()){
          echo $id = $row['cnt'];
        }
      }
      exit();
  }

  public function getproddesc() {
    global $base_url;
  if(isset($_POST['id']) && is_numeric($_POST['id'])){
    $items              = [];
    $alternate_partno   = '';
    $variant_val        = '';
    $id                 = $_POST['id'];
    $connection         = \Drupal::database();
    $user               = \Drupal::currentUser();
    $user_id            = $user->id();
    $status1            = 0;
    $temp_store_factory = \Drupal::service('session_based_temp_store');
    $temp_store         = $temp_store_factory->get('my_module_name', 4800);
    $redirecturl        = $temp_store->get('last_url');
    if(isset($_SESSION['_sf2_attributes']['uid']) && $redirecturl != ''){
        $temp_store->delete('last_url');
        echo '<script>window.location.href="'.$redirecturl.'";</script>';exit;
    }
    $product_inital   = \Drupal\commerce_product\Entity\Product::load($id);
   $partnos          =  $product_inital->get('field_partno')->getValue()[0]['value'];


    if(strpos($partnos, '-') !== false) {
      $partnos_split = explode('-', $partnos);
      $part_no=$partnos_split[1];
    }else{
      $part_no=$partnos;
    }
    $urldata=$connection->query("select dynamic_url from catapult_config_meta");
      $urlarr = $urldata->fetchAssoc();
   
   /*echo "<pre>";
   print_r($partnos_split);*/
   //die();
   //echo "SELECT entity_id FROM commerce_product__field_partno where entity_id != '".$id."' field_partno_value and like '%".$partnos_split[1]."%'";
    /*$query            = $connection->query("SELECT cpf.entity_id FROM commerce_product__field_partno as cpf JOIN commerce_product as cp on cp.product_id = cpf.entity_id where cpf.entity_id != '".$id."' and cpf.field_partno_value like '%".$part_no."%' and cp.delete_flag != 1" );*/
    $query            = $connection->query("SELECT entity_id FROM commerce_product__field_partno LIMIT 0,5" );
    

    while($row = $query->fetchAssoc()){
      $entity_id           = $row['entity_id'];
      $product             = \Drupal\commerce_product\Entity\Product::load($id);


        $prodname                     =  $product->get('field_product_name')->getValue()[0]['value'];
        $field_new_feature            =  $product->get('field_featured')->getValue();
        $field_total_price            =  $product->get('field_total_price')->getValue()[0]['number'];
        $partnos                      =  $product->get('field_partno')->getValue();



        $brands                       =  $product->get('field_brand')->getValue()[0]['target_id'];
        $field_new_value              =  $product->get('field_new')->getValue()[0]['value'];
        $field_stock_info             =  $product->get('field_stock_info')->getValue();
        $field_color_value            =  $product->get('field_color')->getValue();
        $field_new                    =  $product->get('field_new')->getValue();
        //$pimage                       =  file_create_url($product->field_product_image->entity->getFileUri());
        if($product->get('field_product_image')->getValue()[0]['value']=='no_image_icon.PNG'){
                  $pimage=$base_url.'/sites/default/files/no_image_icon.PNG';
            }else{
               $pimage                       =  $urlarr['dynamic_url'].$product->get('field_product_image')->getValue()[0]['value'];
            }
        //$pimage                       =  $urlarr['dynamic_url'].$product->get('field_product_image')->getValue()[0]['value'];
        $field_marketing_messages     =  $product->get('field_marketing_messages')->getValue()[0]['value'];
        $category_name                =  $product->get('field_category_name')->getValue()[0]['target_id'];
        $field_color_value            =  $product->get('field_color')->getValue();
        $query5                       = $connection->query("SELECT * FROM commerce_wish_list_user where product_id ='".$id."' and user_id = '".$user_id."' ");
        $discount_price               = round($field_total_price,1)+((10/100)*round($field_total_price,1));
        while($row5 = $query5->fetchAssoc()){
          $status1 = $row5['status1'];
        }

        $query4      = $connection->query("SELECT * from catapult_theme_setting");
        while ($row1 = $query4->fetchAssoc()){
          $wish    = $row1['wish'];
          $compare = $row1['compare'];

        }
        $msg_suffle=(rand(1,6));
        /*$total_rating=(rand(1,5));
        $total_cnt=(rand(1,50));*/



        $star_array=[];
        $star=[];
        $tot_cnt=0;
        $total_rating=0;
        $queryrating = $connection->query("SELECT rating,COUNT(rating) AS cnt FROM catapult_review WHERE product_id='".$id."' and flag=1 GROUP BY rating");
        while ($row_rat = $queryrating->fetchAssoc()) {
            $star[]=$row_rat['rating'];
            $tot_cnt +=$row_rat['cnt'];
            $star_array[] = array(
            $row_rat['rating'],
            $row_rat['cnt']      
          );
        }
        $new_arr = range(1,5);  
        $mis_arr=array_diff($new_arr, $star);   
        $count=count($star_array);    
                                                 
       
        foreach($mis_arr as $k=>$v){
          $star_array[$count+1][0]=$v;
          $star_array[$count+1][1]=0;
          $count=count($star_array);  
        
        }

      usort($star_array, function($a, $b) {
          return $a[0] <=> $b[0];
      });  
      if(count($star_array) > 0){
      $total_rating=(($star_array[0][0] * $star_array[0][1]) + ($star_array[1][0] * $star_array[1][1]) + ($star_array[2][0] * $star_array[2][1]) + ($star_array[3][0] * $star_array[3][1]) + ($star_array[4][0] * $star_array[4][1]))/$tot_cnt;
      }

        $product1 = array($id,$prodname,$field_new_feature[0]['value'],
                          $partnos[0]['value'],$field_stock_info[0]['value'],round($field_total_price,2),
                          $pimage,$status1,$prodname,$category_name,$brands,$field_color_value,
                          $category_name,round($discount_price,2),$field_new_value,
                          $field_marketing_messages,$variant_val,$wish,$compare,$msg_suffle,$total_rating,$tot_cnt
                        );
        $items[]  = $product1;

      $status1  = 0;
    }

    echo json_encode(array('products'=>$items));
  }else{
    echo json_encode(array());
  }

    exit();
  }

  public function get_color_variant($id){
    $connection = \Drupal::database();
    $qry_variation=$connection->query("SELECT cp.variation_id,cp.price__number as price_number,cp.price__currency_code as price_currency_code,cc.name as color_name,tp.value as c_value,tp.img_url as c_url FROM commerce_product_variation_field_data AS cp LEFT JOIN commerce_product_variation__field_color AS pn ON pn.entity_id=cp.variation_id LEFT JOIN taxonomy_term_field_data AS cc ON cc.tid=pn.field_color_target_id LEFT JOIN catapult_color_filter as tp on tp.name = cc.name

      where cp.product_id =".$id." AND cp.status = 1 AND cc.status = 1 ");
     return $qry_variation;
  }


  public function ordersummarys(){
     // SELECT `product_id` FROM `commerce_product_variation_field_data` WHERE `variation_id`=17;
      global $base_url;
      $uid = \Drupal::currentUser()->id();
      $entity_manager = \Drupal::entityManager();
      $connection = \Drupal::database();

      $tax_query = $connection->query("SELECT tax_percentage FROM catapult_tax_setting");
      $tax_result = $tax_query->fetchAssoc();
      
      $query1 = $connection->query("SELECT order_id,`total_price__number` FROM `commerce_order` WHERE uid=".$uid." AND state = 'draft' AND cart=1");

     $result = $query1->fetchAssoc();
      $var ='';
      if(!empty($result)){

          $order_id = $result['order_id'];
          $total_price = number_format($result['total_price__number'],2);
          $query = $connection->query("SELECT cp.product_id,oi.title,oi.quantity,oi.unit_price__number,oi.total_price__number,oi.purchased_entity,pim.field_product_image_value FROM commerce_product_variation_field_data AS cp
            LEFT JOIN commerce_order_item AS oi ON cp.variation_id = oi.`purchased_entity`
            LEFT JOIN commerce_product__field_product_image AS pim ON pim.entity_id =cp.product_id
             WHERE oi.order_id =".$order_id."  ORDER BY cp.product_id ASC");

              $var ='';
              $var .='<div class="lst-chk">
                              <table class="table checkout-table table-slim unstyled totals">';
              $urldata=$connection->query("select dynamic_url from catapult_config_meta");
              $urlarr = $urldata->fetchAssoc();
            $total_final=0;
            $tax_val=0;
            $total_coupon_applied=0;
     
            while($row = $query->fetchAssoc()){
              if($row['field_product_image_value']=='no_image_icon.PNG'){
                  $spliturl=$base_url.'/sites/default/files/no_image_icon.PNG';
              }else{
               $spliturl=$urlarr['dynamic_url'].$row['field_product_image_value'];
              }
                //$spliturl=str_replace('public://','',$row['uri']);
              //$spliturl=$urlarr['dynamic_url'].$row['field_product_image_value'];
                $prodname = mb_strimwidth($row['title'], 0, 30, "...");
                $p_id = $row['product_id'];
                $variant_id = $row['purchased_entity'];
                $p_name =$row['title'];
                $p_price =round($row['unit_price__number'],2);
                $p_quantity =intval($row['quantity']);
                //$order_id   = $row['order_id'];
                $image = $spliturl;
                $urls = $base_url."/checkout/".$order_id."/orderinformation";
                //old
            //    $p_totalprice = $p_price*$p_quantity;


                $product_detail      = \Drupal\commerce_product\Entity\Product::load($row['product_id']);
                foreach($product_detail->getVariationIds() as $key=>$value){
                  $product_variation = $entity_manager->getStorage('commerce_product_variation')->load((int)$value);
                  $data_color = $product_variation->get('field_color')->getValue()[0]['target_id'];
                  if($row['purchased_entity'] == $value){
                     $color_select = $data_color;
                  }
                  if($data_color == 152){
                    $basic_price = $product_variation->get('price')->getValue()[0]['number'];
                  }
                }
                //new//
                 $p_totalprice = $basic_price*$p_quantity;

                $vid   = 'color_parent';
                $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);
                foreach ($terms as $term){
                  $term_obj = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($term->tid);
                  //if($term->tid != 152){
                    if($color_select == $term->tid){
                     $field_colorname = $term_obj->get('field_colorname')->value;
                    }
                  //}
                }

                $p_price         =  round($row['unit_price__number'],2);
                $basic_price_val =  $p_price - $basic_price;
                $hidden_price    =  $p_price - $basic_price_val;

                //color variant picking
                ///// modified by nandha ////
                $qry_variation=$connection->query("SELECT cp.variation_id,cp.price__number as price_number,cp.price__currency_code as price_currency_code,cc.name as color_name,tp.value as c_value,tp.img_url as c_url FROM commerce_product_variation_field_data AS cp LEFT JOIN commerce_product_variation__field_color AS pn ON pn.entity_id=cp.variation_id LEFT JOIN taxonomy_term_field_data AS cc ON cc.tid=pn.field_color_target_id LEFT JOIN catapult_color_filter as tp on tp.name = cc.name

                where cp.product_id=".$p_id." AND cp.variation_id=".$variant_id." AND cp.status = 1 AND cc.status = 1 ");
                $vari_filter = [];
                while($row = $qry_variation->fetchAssoc()){
                  /*$vari_filter[]=array($row['variation_id'],$row['price_number'],$row['price_currency_code'],$row['color_name'],$row['c_value'],$row['c_url']);*/
                  $price_number = $row['price_number'];
                  $price_currency_code = $row['price_currency_code'];
                  $color_name = $row['color_name'];
                  $c_value = $row['c_value'];
                  $c_url = $row['c_url'];
                }
                /* <div class="filter-wrap ">
                                      <div class="filter-options pro_variant_block" style="text-align: left">
                                          <div style="width:100%">
                                            <a id="'.$c_value.'" data-val="'.$variant_id.'" class="color_variant" ><i aria-hidden="true" class="fa fa-check"></i> <img style="width: 20px;" src="'.$base_url.'/sites/default/files/color_img/'.$c_url.'" alt="'.$color_name.'"> <span id="variant_val" class="hidden">'.$variant_id.'</span></a> <span>'.$color_name.'</span>
                                          </div>
                                      </div>
                                    </div>
                                    */
              if($uid != 0){
                    $var .='  <tr>
                                  <td class="image p-b-10">
                                  <a href="'.$base_url.'/product_desc/'.$p_id.'" target="_blank" class="quick-shop info" data-toggle="tooltip" title="'.$p_name.'">
                                    <img class="product-image af" ng-src="'.$spliturl.'" err-SRC="'.$base_url.'/sites/default/files/logobrand.png" alt="product-image">
                                    </a>
                                  </td>
                                  <td class="details p-b-10">
                                    <div class="brand bld" >'.$prodname.' <span style="font: 600 16px  sans-serif; color: #c42706; padding-left: 5px; letter-spacing: 1px;">$'.sprintf('%0.2f',$basic_price).'</span></div>';
                                   /* <div class="brand bld" >'.$color_name.' <span style="font: 600 16px  sans-serif; color: #c42706; padding-left: 5px; letter-spacing: 1px;">$'.sprintf('%0.2f',$basic_price_val).'</span></div>*/

                                    $var .='<span class="removeItem link">Qty:<i class="bld">'.$p_quantity.'</i></span><br>
                                    <span class="removeItem link">Total Price: <i class="bld">$'.sprintf('%0.2f',$p_totalprice).'</i></span>
                                    </td>
                                </tr>';
              }
              $total_final+=($p_quantity*($basic_price));
              $tax_final+=($p_quantity*($basic_price))*($tax_result['tax_percentage']/100);
              $discount[]=($p_quantity*($basic_price_val));


            }

      //existing coupon apply check
      $coupon_qry = $connection->query("SELECT o.entity_id, o.coupons_target_id, p.code,p.promotion_id
                                      FROM commerce_order__coupons o JOIN commerce_promotion_coupon p 
                                      ON p.id = o.coupons_target_id
                                      WHERE o.entity_id ='".$order_id."'");
      $coupon_result = $coupon_qry->fetchAssoc();

      $summary = array();
      //percent get
      if($coupon_result){
        $promotion_storage = $entity_manager->getStorage('commerce_promotion');
        $promotion_check   = $promotion_storage->load($coupon_result['promotion_id']); 
  
        if($promotion_check->get('offer')->target_plugin_id =='order_item_fixed_amount_off'){
          //Fixed amount off each matching product
          $amount                 = $promotion_check->get('offer')->target_plugin_configuration['amount']['number'];
          $summary['discount']    = sprintf('%0.2f',array_sum($discount));
          $summary['total_coupon_applied'] = sprintf('%0.2f',$total_final-$summary['discount']);

        }elseif ($promotion_check->get('offer')->target_plugin_id =='order_fixed_amount_off') {
          //Fixed amount off the order subtotal
          $amount = $promotion_check->get('offer')->target_plugin_configuration['amount']['number'];
          $summary['discount']    = sprintf('%0.2f',$amount);
          $summary['total_coupon_applied'] = sprintf('%0.2f',$total_final-$summary['discount']);

        }elseif ($promotion_check->get('offer')->target_plugin_id =='order_item_percentage_off') {
          //Percentage off each matching product
          $coupon_percent    = $promotion_check->get('offer')[0]->target_plugin_configuration['percentage'];
          $summary['discount']    = sprintf('%0.2f',array_sum($discount));
          $summary['total_coupon_applied'] = sprintf('%0.2f',$total_final+$summary['discount']);

        }elseif ($promotion_check->get('offer')->target_plugin_id =='order_percentage_off') {
          //Percentage off the order subtotal
          $coupon_percent    = $promotion_check->get('offer')[0]->target_plugin_configuration['percentage'];
          $summary['discount']    = sprintf('%0.2f',$total_final*$coupon_percent);
          $summary['total_coupon_applied'] = sprintf('%0.2f',$total_final+$summary['discount']);

        }
        
        $total_coupon_applied = sprintf('%0.2f',$summary['total_coupon_applied']+$tax_final);
        $summary['coupon_code'] = $coupon_result['code'];
       
      }
      //existing coupon apply check End
            $var .='</table></div>';
            if($uid != 0){
                  $var .='<table id="" class="table checkout-table table-slim unstyled totals">
                            <tbody>
                              <tr>
                                 <td>Sub Total</td>
                                 <td class="text-right">
                                    <a id="shippingAmount"  class="underline hidden"></a>
                                    <span id="shippingFree">$'.sprintf('%0.2f',$total_final).'</span>
                                 </td>
                              </tr>
                              <tr>
                                 <td>Shipping &amp; Handling</td>
                                 <td class="text-right">
                                    <a id="shippingAmount" class="underline hidden"></a>
                                    <span id="shippingFree" data-at="shippingFree">$0.00</span>
                                 </td>
                              </tr>
                              <tr>
                                 <td>Taxes</td>
                                 <td class="text-right">
                                    <a id="shippingAmount" class="underline hidden"></a>
                                    <span id="shippingFree" data-at="shippingFree">$'.sprintf('%0.2f',$tax_final).' </span>
                                 </td>
                              </tr>

                              <tr class="subtotal">
                                 <td>Total</td>
                                 <td id="subTotal" class="lineTotal text-right">
                                    <span class="togglable">
                                    <span class="toggleOff">$'.sprintf('%0.2f',$total_final+$tax_final).'</span>
                                    <span class="toggleOn hide">$'.sprintf('%0.2f',$total_final+$tax_final).'</span>
                                    </span>
                                 </td>
                              </tr>
                              ';
                               if($total_coupon_applied!=0){
                               $var .='
                              <tr class="subtotal">
                                 <td>Discount</td>
                                 <td id="Discount" class="lineTotal text-right">
                                    <span class="togglable">
                                    <span class="toggleOff">$'.sprintf('%0.2f',$summary['discount']).'</span>
                                    <span class="toggleOn hide">$'.sprintf('%0.2f',$summary['discount']).'</span>
                                    </span>
                                 </td>
                              </tr><tr class="subtotal">
                                 <td>Total <span class="price_color_ord_inf">(Coupon applied)</span></td>
                                 <td id="total_coupon_applied" class="lineTotal text-right">
                                    <span class="togglable">
                                    <span class="toggleOff">$'.$total_coupon_applied.'</span>
                                    <span class="toggleOn hide">$'.$total_coupon_applied.'</span>
                                    </span>
                                 </td>
                              </tr>';
                              }
                            $var .=' </tbody>
                          </table>';



                          /*<table id="" class="table table-slim unstyled totals">
                           <tbody>

                              <tr class="subtotal">
                                 <td>Total</td>
                                 <td id="subTotal" class="lineTotal text-right">
                                    <span class="togglable">
                                    <span class="toggleOff">$8290.00</span>
                                    <span class="toggleOn hide">$8290.00</span>
                                    </span>
                                 </td>
                              </tr>

                           </tbody>
                        </table>*/

            }
      }
      if($var ==''){

        $var ='Your cart is empty';
      }
        echo $var;
        exit();
  }

  public function get_config_details(){
      $result_config = $this->get_config();
      while($row = $result_config->fetchAssoc()){
          $filter_combo = $row['filter_combo'];
          $engine_flag = $row['engine_flag'];
      }
      $year_arr = array();
      $make_arr = array();
      if($filter_combo == 1){
        $result_year = $this->get_year();
        while($row = $result_year->fetchAssoc()){
            array_push($year_arr,trim($row['year']));
        }
      }elseif($filter_combo == 2){
        $result_make = $this->get_make();
        while($row = $result_make->fetchAssoc()){
            array_push($make_arr,trim($row['make']));
        }
      }

    echo json_encode(array('year'=>$year_arr,'make'=>$make_arr,'filter_combo'=>$filter_combo,'engine_flag'=>$engine_flag));
    exit();
  }
  public function get_config(){
    $connection = \Drupal::database();
    $querys = $connection->query("SELECT filter_combo,engine_flag from catapult_config_meta");
    return $querys;
  }
  public function get_year(){
    $connection = \Drupal::database();
    $querys = $connection->query("SELECT distinct cy.year from catapult_engine ce INNER JOIN catapult_year cy ON cy.year_id=ce.year_id
      INNER JOIN `catapult_application_map` cam ON ce.engine_id=cam.engine_id
      order by cy.year DESC");
    return $querys;
  }
  public function get_make(){
    $connection = \Drupal::database();
    //$querys = $connection->query("SELECT distinct make from catapult_year order by make");
    $querys = $connection->query("SELECT DISTINCT cm.make FROM catapult_engine ce INNER JOIN catapult_make cm ON ce.make_id=cm.make_id
      INNER JOIN `catapult_application_map` cam ON ce.engine_id=cam.engine_id
      order by cm.make");
    return $querys;
  }
  public function ordertotal(){
    $user        = \Drupal::currentUser();
    $user_id     = $user->id();
    $entity_manager = \Drupal::entityManager();
    global $base_url;
    $order_id   = $_POST['id'];
     //$order_id   = 30;
    $connection = \Drupal::database();
         $tax_query = $connection->query("SELECT tax_percentage FROM catapult_tax_setting");
      $tax_result = $tax_query->fetchAssoc();
//old//
   /* $query1 = $connection->query("SELECT total_price__number FROM commerce_order_item WHERE order_id ='".$order_id."' ");
    $tot = 0;
    $tot_tax = 0;
    while($res_order = $query1->fetchAssoc()){
      $tot += $res_order['total_price__number'];
      $tot_tax    += sprintf('%0.2f',$res_order['total_price__number']*($tax_result['tax_percentage']/100));

    }
   
      $final_tot=$tot+$tot_tax;
    $query2 = $connection->query("update commerce_order set total_price__number='".$final_tot."' WHERE order_id ='".$order_id."' ");

    $query1     = $connection->query("SELECT total_price__number FROM `commerce_order` WHERE order_id='".$order_id."' ");
    while($row  = $query1->fetchAssoc()){
      $price = $row['total_price__number'];
    }
  */
//old//
$query1 = $connection->query("SELECT total_price__number FROM commerce_order_item WHERE order_id ='".$order_id."' ");
    $tot = 0;
    $tot_tax = 0;
    while($res_order = $query1->fetchAssoc()){
      $tot += $res_order['total_price__number'];
     

    }

  $query = $connection->query("SELECT cp.product_id,oi.purchased_entity,oi.quantity,oi.unit_price__number FROM commerce_product_variation_field_data AS cp 
                  LEFT JOIN commerce_order_item AS oi ON cp.variation_id = oi.purchased_entity
                   WHERE oi.order_id ='".$order_id."'");

  while($row = $query->fetchAssoc()){

          $p_id       = $row['product_id'];
          $variant_id = $row['purchased_entity'];
           $p_quantity = intval($row['quantity']);
          $product      = \Drupal\commerce_product\Entity\Product::load($p_id);    


          foreach($product->getVariationIds() as $key=>$value){

              $product_variation = $entity_manager->getStorage('commerce_product_variation')->load((int)$value);       
             /* echo "<pre>";
              print_r($product_variation);*/
              
              $data_color = $product_variation->get('field_color')->getValue()[0]['target_id'];   
              if($data_color == 152){
                $basic_price = $product_variation->get('price')->getValue()[0]['number'];
              }                                          
            }



          $qry_variation=$connection->query("SELECT cp.price__number as price_number FROM commerce_product_variation_field_data AS cp LEFT JOIN commerce_product_variation__field_color AS pn ON pn.entity_id=cp.variation_id LEFT JOIN taxonomy_term_field_data AS cc ON cc.tid=pn.field_color_target_id LEFT JOIN catapult_color_filter as tp on tp.name = cc.name

          where cp.product_id=".$p_id." AND cp.variation_id=".$variant_id." AND cp.status = 1 AND cc.status = 1 ");
          while($row = $qry_variation->fetchAssoc()){
               $basic_price_val = $row['price_number'] - $basic_price; 

          $hidden_price   = $row['price_number']-$basic_price_val;

        }

        $hidden_price = number_format((float)$hidden_price, 2, '.', '');
           $total_price +=  number_format((float)$hidden_price*$p_quantity, 2, '.', '');

                    
             $tax_val += ($hidden_price*$p_quantity)*($tax_result['tax_percentage']/100);
        
          
            }
   
 $discount=$total_price-$tot;
  $final_tot=$total_price+$tax_val;
  
   $grand_tot=sprintf('%0.2f',$final_tot-$discount);
  // print_r($grand_tot);die;
   
   $query2 = $connection->query("update commerce_order set total_price__number='".$grand_tot."' WHERE order_id ='".$order_id."' ");

    $query1     = $connection->query("SELECT total_price__number FROM `commerce_order` WHERE order_id='".$order_id."' ");
    while($row  = $query1->fetchAssoc()){
      $price = $row['total_price__number'];
    }


    echo round($price,2);
   // echo sprintf('%0.2f',$price);
    die();
  }
  public function ordercompletes(){
    $user        = \Drupal::currentUser();
    $user_id     = $user->id();
    global $base_url;
    $order_id   = $_POST['id'];
    $connection = \Drupal::database();
    $entity_manager           = \Drupal::entityManager();
    $query1     = $connection->query("SELECT changed FROM `commerce_order` WHERE order_id='".$order_id."' ");
    while($row  = $query1->fetchAssoc()){
      $changed = $row['changed'];
      $data[]=$row;
    }

    $tax_query = $connection->query("SELECT tax_percentage,tax_status FROM catapult_tax_setting");
      $tax_result = $tax_query->fetchAssoc();

    $query1     = $connection->query("update commerce_order set order_number='".$order_id."',state='completed',placed=unix_timestamp(now()),
      completed=unix_timestamp(now()),checkout_step='completed',cart=0,trans_id='".$_POST['trans_id']."' where  order_id='".$order_id."' ");




    $countries = \Drupal\Core\Locale\CountryManager::getStandardList();

    foreach ($countries as $key => $value) {
         $countrys[$key] = (string) $value;
    }

    $new_query=$connection->query("SELECT A.total_price__number,D.sku as partno,D.title as description,round(D.price__number) as price,D.price__number as price_two,round(B.quantity) as quantity,round(D.price__number*B.quantity) as subtotal,(D.price__number*B.quantity) as subtotal_two ,A.order_id,DATE_FORMAT(from_unixtime(A.completed),'%d-%m-%Y') as order_date, C.bill_firstname, C.bill_lastname, C.bill_phone, C.bill_email, C.bill_address1, C.bill_address2, C.bill_city, C.bill_zipcode, C.bill_state, C.bill_country, C.s_firstname, C.s_lastname, C.s_phonenumber, C.s_email, C.s_address1, C.s_address2, C.s_city, C.s_zipcode, C.s_state, C.s_country,A.trans_id,B.purchased_entity FROM `commerce_order` A,commerce_order_item B,users_shipping_profile C,commerce_product_variation_field_data D WHERE A.order_id=B.order_id and A.order_id=C.order_id and B.purchased_entity=D.variation_id and A.order_id=$order_id");
      $i=0;
     while ($row=$new_query->fetchAssoc()) {
        $data1=$row;
        $order_details[]=$row;

        $product_variation = $entity_manager->getStorage('commerce_product_variation')->load((int)$row['purchased_entity']);
        $data_color = $product_variation->get('field_color')->getValue()[0]['target_id'];   
        $vid   = 'color_parent';
        $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid); 
        foreach ($terms as $term){
          $term_obj = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($term->tid);
            if($data_color == $term->tid){
              /*echo $term->tid;*/
               $field_colorname[$row['order_id']] = $term_obj->get('field_colorname')->value;
               $color_name[$row['order_id']] = $term_obj->get('name')->value;
               //echo $color_name;
            }
        }

        $order_details[$i]['clr_name']=$color_name[$row['order_id']];
        $order_details[$i]['clr_code']=$field_colorname[$row['order_id']];

        $i++;

      }





    $billing_address=array();
    $billing_address[]=$data1['bill_firstname']." ".$data1['bill_lastname'];
    $billing_address[]=$data1['bill_phone'];
    $billing_address[]=$data1['bill_email'];
    $billing_address[]=$data1['bill_address1']." ".$data1['bill_address2'];
    $billing_address[]=$data1['bill_city']." ".$data1['bill_zipcode'];
    $billing_address[]=$data1['bill_state'];
    $billing_address[]=!empty($data1['bill_country'])?$countrys[$data1['bill_country']]:"";

    $bill_address=array();
    foreach ($billing_address as $key => $value) {
       if(!empty($value)){
        $bill_address[]=$value;
       }
    }

    $shipping_address=array();
    $shipping_address[]=$data1['s_firstname']." ".$data1['s_lastname'];
    $shipping_address[]=$data1['s_phonenumber'];
    $shipping_address[]=$data1['s_email'];
    $shipping_address[]=$data1['s_address1']." ".$data1['s_address2'];
    $shipping_address[]=$data1['s_city']." ".$data1['s_zipcode'];
    $shipping_address[]=$data1['s_state'];
    $shipping_address[]=!empty($data1['s_country'])?$countrys[$data1['s_country']]:"";

    $shipp_address=array();
    foreach ($shipping_address as $key => $value) {
       if(!empty($value)){
        $shipp_address[]=$value;
       }
    }

    $order_data['bill_address']=!empty($bill_address)?implode('<br>',$bill_address):"";
    $order_data['shipp_address']=!empty($shipp_address)?implode('<br>',$shipp_address):"";

    $mail['body']=$this->mail_msg($order_data,$order_details,$tax_result);
    $mail['to']=$order_details[0]['bill_email'];
    $mail['to_name']=$order_details[0]['bill_firstname'];
    $mail['subject']="XCLUTCH BILLING INVOICE";
   /* echo "<pre>";
    print_r($mail);die;*/
   echo $this->SendMail($mail['to'],$mail['to_name'],$mail['subject'],$mail['body']);die;
  }


  function mail_msg($order_data,$order_details,$tax_result){

    $user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());
    $currnetusername = $user->get('name')->value;
    global $base_url;
    $html='';
    $html.='<div style="margin-left:auto;width:1000px;margin-right:auto">
           <table style=" font-family: Verdana; font-size: 14px; background-color: white; margin-bottom: 25px; width: 100%;">
           <tr>
           <td>
           <table width="100%" style="font-size:14px;border:solid 1px #ccc">
           <tbody>
           <tr>
           <td style="height:10px"></td>
           <td></td>
           </tr>
           <tr width="100%" style="width:100%">
           <td align="left" style="font-weight:bold!important;font-size:18px;width:50%;padding-left: 25px;">
           <span style="vertical-align:sub;text-decoration:underline;">Order Confirmation </span>
           <br>
           <span style="vertical-align:sub;font-size: 13px;text-decoration: none;">Transaction ID :'.$order_details[0]['trans_id'].'</span>
           </td>
           <td style="width:50%;text-align:right">
           <span style="text-align:right;margin-right:10px">
            <img style="padding:5px;max-width:100%;width:200px;" src="http://61.16.143.100/xclutch_cp/sites/default/files/webshop_mail.png" width="100">
           </span>
           </td>
           </tr>
           <tr>
           <td style="height:10px"></td>
           <td></td>
           </tr>
           </tbody>
           </table>
           </td>
           </tr>

           <tr>
           <td>
           <table width="100%" cellspacing="0" style="font-size:14px;border:solid 1px #ccc">
           <tbody>
           <tr>
           <td width="50%" style="border-right:1px solid #ccc;padding-left:5px">
           <table>
           <tbody>
           <tr>
           <td style="white-space:nowrap"><b>Purchase Order :</b>  '.$order_details[0]['order_id'].'</td>
           </tr>
           </tbody>
           </table>
           </td>
           <td width="50%" style="padding-left:5px">
           <table>
           <tbody>
           <tr>
           <td><b>Order Date :</b> '.$order_details[0]['order_date'].'</td>
           <td align="left"></td>
           </tr>
           </tbody>
           </table>
           </td>
           </tr>
           </tbody>
           </table>
           </td>
           </tr>
                           <tr>
           <td>
           <table width="100%" cellspacing="0" style="font-size:14px;border:solid 1px #ccc">
           <tbody>
           <tr>
           <td width="50%" style="border-right:1px solid #ccc;padding-left:5px">
           <table>
           <tbody>
            <tr>
                      <td style="white-space:nowrap"><b>Billing Information</b></td>
            </tr>
            <tr>
                      <td style="white-space:nowrap"><b>'.$currnetusername.'</b></td>
            </tr>
                <tr>
                  <td>'.$order_data['bill_address'].'</td>
                </tr>
           </tbody>
           </table>
           </td>
           <td width="50%" style="padding-left:5px">
           <table>
           <tbody>
            <tr>
                 <td style="white-space:nowrap"><b>Shipping Information</b></td>
            </tr>
            <tr>
                 <td>'.$order_data['shipp_address'].'</td>
            </tr>
           </tbody>
           </table>
           </td>
           </tr>
           </tbody>
           </table>
           </td>
           </tr>
          <!-- <tr>
           <td>

           <table width="100%" cellspacing="0" style="font-size:14px;border:solid 1px #ccc; margin-bottom:10px;">
           <tbody>
           <tr>
           <td width="100%" style="border-right:1px solid #ccc;padding-left:5px">
           <table width="100%">
           <tbody>
           <tr>
            <td width="33%" align="left" style="word-break:break-all"><b>Ship Via : </b></td>
            <td width="33%" align="left" style="word-break:break-all"><b>&nbsp;&nbsp;&nbsp;&nbsp;Freight Terms : </b></td>
            <td width="33%" align="left" style="word-break:break-all"><b>&nbsp;&nbsp;&nbsp;&nbsp;Special Instructions : </b></td>
           </tr>
           </tbody>
           </table>
           </td>
           </tr> -->
           </tbody>
           </table>
           <table style="width:100%; border:1px solid #ccc; font-family: Verdana; font-size: 14px; padding:10px;">
           <tbody>
           <tr>
           <td>
           <table style="width:100%; border-color:#ccc;" border="1" cellpadding="0" cellspacing="0">
           <thead>
                <tr>
                      <th style="padding:5px;">S. No.</th>
                      <th style="padding:5px;">Sku</th>
                      <th style="padding:5px;">Description</th>
                      <th style="padding:5px;">Price</th>
                      <th style="padding:5px;">QTY</th>                      
                      <th style="padding:5px;">Sub Total</th>
                </tr>
          </thead>
          <tbody>';
/*<td class="cl_cust_wdt" style="padding:5px;text-align:center !important;" ><span class="bgg_cl" style='background-color:{{'{{data.color_code[key1]}}'}}'></span><span class="ws">{{'{{data.color_name[key1]}}'}} </span></td>*/

  $final_tax_tot=0;
          foreach ($order_details as $key => $value) {
                $html.='<tr>
                          <td style="padding:5px;text-align:center !important;">'.($key+1).'</td>
                          <td style="padding:5px;text-align:center !important;">'.$value['partno'].'</td>
                          <td style="padding:5px;text-align:center !important;">'.$value['description'].'</td>
                          <td style="padding:5px;text-align:right !important;"><span>$</span>'.number_format((float)$value['price_two'], 2, '.', '').'</td>
                          <td style="padding:5px;text-align:center !important;">'.$value['quantity'].'</td>.';
                          
                          $tax_val=number_format((float)$value['subtotal_two'], 2, '.', '')*($tax_result['tax_percentage']/100);
                           $final_sub_tot=$value['subtotal_two']+$tax_val;
                           $final_sub_tot=number_format((float)$final_sub_tot, 2, '.', '');

                            $final_sub_tot_two += number_format((float)$final_sub_tot, 2, '.', '');

                           $final_tax_tot +=$tax_val;


                         if($tax_result['tax_status']==1)
                         {
                           $html.='<td style="padding:5px;text-align:right !important;"><span>$</span>'.number_format((float)$value['subtotal_two'], 2, '.', '').'</td>';


                         }
                         else
                         {
                           
                            $html.='<td style="padding:5px;text-align:right !important;"><span>$</span>'.$final_sub_tot.' (Inclusive Tax)</td>';
                         }
                          
                      $html.='</tr>';
            }
        $discount_tot=$final_sub_tot_two;
        $discount_grand_tot=$value['total_price__number']-$discount_tot;

          $html.='</tbody>
                    </table>
                    </td>
                    </tr>
                    <tr>
                    <td>
                    <table style="width:100%;" cellpadding="0" cellspacing="0">
                    <tr>
                    <td style="height:15px;"></td>
                    </tr>';

                        if($tax_result['tax_status']==1)
                         {
                    $html.= '<tr>                    
                     <td style="text-align:right; font-size: 18px;">
                    <span style="font-weight: 600;">Taxes</span> : <span>$</span>'.
                    number_format((float)$final_tax_tot, 2, '.', '').'<span></span>
                    </td>
                     </tr>';
                     }

                                       
                        if($discount_grand_tot!=0 && $discount_grand_tot!='0.00' )
                         {
                    $html.= '

                     <tr>
                    <td style="text-align:right; font-size: 18px;">
                    <span style="font-weight: 600;">Total Amount</span> : <span>$</span>'.sprintf('%0.2f',$value['total_price__number']-$discount_grand_tot).'<span></span>
                    </td>
                    </tr>
                    <tr>                    
                     <td style="text-align:right; font-size: 18px;">
                    <span style="font-weight: 600;">Discount</span> : <span>$</span>'.
                    sprintf('%0.2f',$discount_grand_tot).'<span></span>
                    </td>
                     </tr>
                    <tr>
                    <td style="text-align:right; font-size: 18px;">
                    <span style="font-weight: 600;">Total Amount</span> : <span>$</span>'.sprintf('%0.2f',$value['total_price__number']).'<span></span>
                    </td>
                    </tr>
                     ';
                     }
                     else{
                       $html.='<tr>
                    <td style="text-align:right; font-size: 18px;">
                    <span style="font-weight: 600;">Total Amount</span> : <span>$</span>'.sprintf('%0.2f',$value['total_price__number']).'<span></span>
                    </td>
                    </tr>';
                     }

                   $html.=' </table>
                    </td>
                    </tr>
                    </tbody>
                    </table>
                    </td>
                    </tr>
                    </table>
              </div>';
              return $html;

  }

  public function SendMail($to,$to_name='',$subject='',$body,$attachment='',$cc='',$bcc=''){

    $db = \Drupal::database();
    $result=$db->query("SELECT * from tbl_smtp_setting where status='Y'");
    while ($row = $result->fetchAssoc()) {
      $smtp_settings[]=$row;
     }
     if(!isset($smtp_settings)){
        return 'Error';
     }else{

      $mail = new \PHPMailer();
      $mail->isSMTP();
      $mail->SMTPDebug = 0;
      $mail->Host = $smtp_settings[0]['hostid'];
      $mail->Port = $smtp_settings[0]['portno'];
      $mail->SMTPSecure = $smtp_settings[0]['protocol'];
      $mail->SMTPAuth = true;
      $mail->Username = $smtp_settings[0]['emailid'];
      $mail->Password = $smtp_settings[0]['password'];
      $mail->setFrom($smtp_settings[0]['emailid'], $smtp_settings[0]['username']);
      //$mail->addReplyTo('reply-box@hostinger-tutorials.com', 'Your Name');
      $mail->addAddress($to, $to_name);
      $mail->addBCC($smtp_settings[0]['emailid']);
      //$mail->addBCC("samdesantos@aol.com");
      $mail->Subject = $subject;
      $mail->msgHTML($body);
      // $mail->AltBody = 'This is a plain text message body';
      /*if($attachment){
        $mail->addAttachment($attachment);
      }

      if(!empty($cc)){
        if(is_array($cc)){
          foreach ($cc as $key => $value) {
            $mail->addCC($value);
          }
        }else{
          $mail->addCC($cc);
        }
      }

      if(!empty($bcc)){
        if(is_array($bcc)){
          foreach ($bcc as $key => $value) {
            $mail->addBCC($value);
          }
        }else{
          $mail->addBCC($bcc);
        }
      }*/
      // $mail->addAttachment('test.txt');
      if (!$mail->send()) {
        //die;
        return 'Error';
      } else {
        return 'Sent';
      }
    }
    die();
  }

}
