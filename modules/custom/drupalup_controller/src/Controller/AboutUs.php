<?php
    /**
     * @file
     * Contains \Drupal\hello\HelloController.
     */

    namespace Drupal\drupalup_controller\Controller;

    class AboutUs {

      public function page() {  	
        $service_array =[];
        $connection = \Drupal::database();
        $nids = \Drupal::entityQuery('node')->condition('type','about_us')->execute();
        foreach ($nids as $key => $ids) {
          $node = \Drupal\node\Entity\Node::load($ids);  
          $res = $node->field_about_sequence->getValue(); 
          $service_array[] = array('title'=>$node->getTitle(),
            'raw_html'=>$node->body->value,
            'img_url'=>file_create_url($node->field_aboutusimage->entity->getFileUri()),
            'sequence'=>$res[0]['value'],
            'altvalue'=>$node->field_aboutusimage->alt
          );
         
        } 
       
        usort($service_array, function($a, $b) {
            return $a['sequence'] <=> $b['sequence'];
        });
        $seo_array=[];
        $query = $connection->query("SELECT h1_tag,h2_tag FROM catapult_seo where page_name='About'");
        while ($row = $query->fetchAssoc()){      
           $seo_array[] = array($row['h1_tag'],$row['h2_tag']);
        }

      
        return array(
        '#theme' => 'about_us',
        '#items'=>$service_array,
        '#seo_array' => $seo_array
        //'#title'=>'Our Article List'
        );
      }

    }

?>
