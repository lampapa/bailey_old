<?php
/**
 * @file
 * Contains \Drupal\hello\HelloController.
 */

namespace Drupal\drupalup_controller\Controller;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Database\Database;
use Drupal\Core\Entity\Query\QueryFactory;

include_once "modules/phpmailer/PHPMailer.php";
include_once "modules/phpmailer/SMTP.php";
// include_once "modules/phpmailer/Exception.php";

class ContactUs extends ControllerBase  {

  public function page(){
   
  	$content = array('testing contact us');
    $error = '';$success = '';

    if(!isset($_SESSION['postid'])){
       $_SESSION['postid'] = rand(10,100);      
    }

    if(!empty($_POST)){
      if( (isset($_POST['postid'])) && ($_POST['postid'] != "")  ){
            if($_SESSION['postid'] == $_POST['postid']){  
                $name_data=!empty($_POST['name_data'])?$_POST['name_data']:"";
                $mail_data=!empty($_POST['mail_data'])?$_POST['mail_data']:"";
                $subject_data=!empty($_POST['subject_data'])?$_POST['subject_data']:"";
                $msg_data=!empty($_POST['msg_data'])?$_POST['msg_data']:"";
                $db = \Drupal::database();




                $result = $db->insert('contact_info')->fields([
                  'name' => $name_data,
                  'email' => $mail_data,
                  'subject' => $subject_data,
                  'message' => $msg_data,
                ])->execute();


                $mail_status=$this->SendMail($mail_data,$name_data,$subject_data,$msg_data);              

                if($mail_status=='Error'){
                  $error='Mail Sending Failed';
                }elseif($mail_status=='Sent'){
                  $success='Message sent!';
                }else{
                  $error='Something Went Wrong';
                }                

            }
          }
         $_SESSION['postid'] = "";
    }
    if($_SESSION['postid'] == ""){
      $_SESSION['postid'] = rand(10,100);      
    }
  /*  $service_array = [];
    $nids = \Drupal::entityQuery('node')->condition('type','our_location')->execute();
    foreach ($nids as $key => $ids) {
      $node = \Drupal\node\Entity\Node::load($ids);   
      $service_array[] = array(
      $node->getTitle(),$node->body->value
      );
    }*/ 

       
     $connection = \Drupal::database();
    $contact_results=[];
    $query1 = $connection->query("SELECT * FROM catapult_contact_info");
    while ($row = $query1->fetchAssoc()) {      
      $contact_results[] = 
      array($row['company_name'],$row['address1'],$row['address2'],$row['zip_code'],$row['country'],$row['state'],$row['city'],
        $row['email'],$row['phone'],$row['facebook'],$row['twitter'],$row['youtube'],$row['linkedin'],$row['googleplus'],$row['emaildropline'],$row['showmap'],$row['googlemap']);
    }

   
    $service_array=[];
    $query = $connection->query("SELECT * FROM catapult_branch_info");
    while ($row = $query->fetchAssoc()){      
       $service_array[] = array($row['branch_name'],$row['branch_address1'],$row['branch_address2'],$row['branch_country'],$row['branch_state'],$row['branch_city'],$row['branch_email'],$row['branch_phone']

        );
    }

    $seo_array=[];
    $query = $connection->query("SELECT h1_tag,h2_tag FROM catapult_seo where page_name='Contact Us'");
    while ($row = $query->fetchAssoc()){      
       $seo_array[] = array($row['h1_tag'],$row['h2_tag']);
    }
   /* print_r($service_array);
    die();*/
    return array(
        '#theme' => 'contact_us',
        '#items'=>$service_array,
        '#contact'=>$contact_results,
        '#seo_array' => $seo_array,
        '#error'=>$error,
        '#success'=>$success,
        '#postid'=>$_SESSION['postid'],
    );
  }

  public function SendMail($to,$to_name='',$subject='',$body,$attachment='',$cc='',$bcc=''){

    $db = \Drupal::database();
    $result=$db->query("SELECT * from tbl_smtp_setting where status='Y'");
    while ($row = $result->fetchAssoc()) {
      $smtp_settings[]=$row;
     }
     if(!isset($smtp_settings)){
        return 'Error';
     }else{

      $mail = new \PHPMailer();
      $mail->isSMTP();
      $mail->SMTPDebug = 0;
      $mail->Host = $smtp_settings[0]['hostid'];
      $mail->Port = $smtp_settings[0]['portno'];
      $mail->SMTPSecure = $smtp_settings[0]['protocol'];
      $mail->SMTPAuth = true;
      $mail->Username = $smtp_settings[0]['emailid'];
      $mail->Password = $smtp_settings[0]['password'];
      $mail->setFrom($smtp_settings[0]['emailid'], $smtp_settings[0]['username']);
      //$mail->addReplyTo('reply-box@hostinger-tutorials.com', 'Your Name');
      $mail->addAddress($to, $to_name);
      $mail->Subject = $subject;
      $mail->msgHTML($body);
      // $mail->AltBody = 'This is a plain text message body';
      if($attachment){
        $mail->addAttachment($attachment);
      }
      if(is_array($cc)){
        foreach ($cc as $key => $value) {
          $mail->addCC($value);
        }
      }else{
        $mail->addCC($attachment);
      } 
      
      if(is_array($bcc)){
        foreach ($bcc as $key => $value) {
          $mail->addBCC($value);
        }
      }else{
        $mail->addBCC($attachment);
      }
      // $mail->addAttachment('test.txt');
      if (!$mail->send()) {
        // $error='Mailer Error:' . $mail->ErrorInfo;
        return 'Error';
      } else {
        return 'Sent';
      }
    }
  }

}