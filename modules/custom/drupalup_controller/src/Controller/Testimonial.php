<?php
/**
 * @file
 * Contains \Drupal\hello\HelloController.
 */

namespace Drupal\drupalup_controller\Controller;


//use Drupal\Core\Controller\ControllerBase;


/*class ArticleController extends ControllerBase {
  public function content() {
    return array(
        '#markup' => '' . t('Hello there!') . '',
    );
  }
}*/


class Testimonial{
 
  public function content() {
    return array(
        '#markup' => '' . t('Hellojjjjjj there!') . '',
    );
  }

  public function page() {
    
    $testimonial_array =[];
     $connection = \Drupal::database();
    $nids = \Drupal::entityQuery('node')->condition('type','testimonial')->execute();
    foreach ($nids as $key => $ids) {
    $node = \Drupal\node\Entity\Node::load($ids);  
    $res = $node->field_testimonial_sequence->getValue(); 
    $testimonial_array[] = array(
        'title'=>$node->getTitle(),
        'raw_html'=>$node->body->value,
        'img_url'=>file_create_url($node->field_customerimage->entity->getFileUri()),
        'location'=>$node->field_location->value,
        'altvalue'=>$node->field_customerimage->alt,
        'sequence'=>$res[0]['value']
        );
  } 
  $seo_array=[];
    $query = $connection->query("SELECT h1_tag,h2_tag FROM catapult_seo where page_name='Testimonial'");
    while ($row = $query->fetchAssoc()){      
       $seo_array[] = array($row['h1_tag'],$row['h2_tag']);
    }

     usort($testimonial_array, function($a, $b) {
          return $a['sequence'] <=> $b['sequence'];
      });
  
  	 
    return array(
        '#theme' => 'testimonial_list',
        '#items'=>$testimonial_array,
         '#seo_array' => $seo_array,
        '#title'=>''
    );
  }
}