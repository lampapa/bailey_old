<?php
/**
 * @file
 * Contains \Drupal\hello\HelloController.
 */

namespace Drupal\drupalup_controller\Controller;
use Drupal\node\Entity\NodeType;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\commerce_product\Entity\Product;
use Drupal\Core\Database\Database;
require_once "sites/libraries/vendor/autoload.php";

$GLOBALS['partdetail'] = \Elasticsearch\ClientBuilder::create()->build();


class PartSearchelastic {


  public function page(){

    $categories = [];
    $brand = [];
    $result_category = $this->get_category();
    while($row = $result_category->fetchAssoc()){

        $categories[]=array('category_id'=>$row['tid'],'category_name'=>$row['name']);
    }
    $result_brand = $this->get_brand();
    while($row = $result_brand->fetchAssoc()){
        $brand[]=array('brand_id'=>$row['tid'],'brand_name'=>$row['name']);
    }
    $items = [];    
    return array(
        '#theme' => 'part_searchelastic',
        '#items'=>$items,
        '#categories' =>$categories,
        '#brand' => $brand,
        '#title'=>'',
        '#path'=>''
    );
  }
  public function get_category(){
    $connection = \Drupal::database();
    $get_category = $connection->query("SELECT DISTINCT tp.* FROM `taxonomy_term_field_data` tp JOIN commerce_product__field_category_name AS cn ON tp.tid=cn.field_category_name_target_id 
       WHERE vid = 'category' AND STATUS='1' order by tp.name" );
    //$result = $get_category;
    return $get_category;
  }
    public function get_brand(){
    $connection = \Drupal::database();
    $get_brand = $connection->query("SELECT DISTINCT tp.* FROM `taxonomy_term_field_data` tp JOIN commerce_product__field_category_name AS cn ON tp.tid=cn.field_category_name_target_id 
       WHERE vid = 'brand' AND STATUS='1'  order by tp.name" );
    return $get_brand;
  }

  public function getproduct_searchs(){
    global $base_url;
    $connection  = \Drupal::database();
    $data = json_decode(file_get_contents("php://input"), true);
    $keyword = $data['keyword'];

    $elastic = $GLOBALS['partdetail'];
    $temp_array = array('sku','product_name','partnobcc'); 
    $partno_array= array();
    $result = array(); 
      $urldata=$connection->query("select dynamic_url from catapult_config_meta");
    $urlarr = $urldata->fetchAssoc();
      foreach ($temp_array as $searchin) {
        $size = 10000; 
        $params  = [
             'index' => 'xclutchcpindex',
             'type' => 'products',          
              'body' => [
              "size"=> $size, //"producturl",
              "_source" => ["productid","product_name","featured","partno","stock","price","product_image","category","brand","new","marketing_message","sku","call"],
                  'query' => [
                    //"match_all" => (object)[]
                      'bool' => [
                          'should' => [
                          /*'multi_match'=>[ 
                                  'query' => strval($keyword),
                                  'fields' => [$searchin], //,'description','description'
                                   //'type' =>'best_fields',
                                   //'operator' => 'and',
                                  //  "minimum_should_match"=> "50%",
                                   //"tie_breaker" => 0.2
                                  ],*/
                                'query_string'=> [
                                      'query'=> $keyword.' OR *'.$keyword.'*',
                                      //'query'=> '*'.$keyword.'*',
                                      'fields'=> [$searchin]
                                ]
                                ],
                            ]
                        ]
                    ]
            ];
            $response = $elastic->search($params);

                //push data to array & check whether exist or not
                foreach($response['hits']['hits'] as $value){
                  if(!in_array($value['_source']['partno'], $partno_array)){
                    $partno_array[] = $value['_source']['partno'];
                  
                     // $pimage            = $base_url.'/sites/default/files/upload_parts/'. $value['_source']['product_image'];
                    if($value['_source']['product_image'] =='no_image_icon.PNG'){
                            $pimage=$base_url.'/sites/default/files/logobrand.png';
                      }else{
                         $pimage            = $urlarr['dynamic_url'].$value['_source']['product_image'];
                      }
                     //$pimage            = $urlarr['dynamic_url'].$value['_source']['product_image'];
                      $field_color_value =  "";
                      $category_name     = "";
                      $discount_price    = 0;
                      $variant_val = [];
                      $wish    = 1;
                      $compare = 1; 
                      $call_price = $value['_source']['call'];
                      if($call_price == null){
                          $call_price = "off";
                      }
                      $result[] = array(
                                 $value['_source']['productid'],
                                 $value['_source']['product_name'],
                                 $value['_source']['featured'],
                                 $value['_source']['partno'],
                                 $value['_source']['stock'],
                                 round($value['_source']['price'],2),
                                 $pimage,
                                 0,
                                 $value['_source']['product_name'],
                                 $value['_source']['category'],
                                 $value['_source']['brand'],
                                 $field_color_value,
                                 $value['_source']['category'],
                                 round($discount_price,2),
                                 $value['_source']['new'],
                                 $value['_source']['marketing_message'],
                                 $variant_val,
                                 $wish,
                                 $compare,
                                 $value['_source']['sku'],
                                 $call_price,                                 
                                 $total_rating,
                                 $total_cnt,
                                 $msg_suffle,
                                 $value['_source']['colors']
                          );
                    }

                  
                }

                
        }

    $color_filter = [];
    echo json_encode(array('products'=>$result,'colorfilter'=>$color_filter));
    exit(); 
  }

  public function getproducts_elastic() {
    global $base_url;
    $connection  = \Drupal::database();
    $urldata=$connection->query("select dynamic_url from catapult_config_meta");
    $urlarr = $urldata->fetchAssoc();
        $ressult = [];
        $url ="http://localhost:9200/xclutchcpindex/_search?size=10000&q=*:*";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL,$url);
        $result=curl_exec($ch);
        curl_close($ch);
        $response =json_decode($result, true);
        // echo "<pre>";
        // print_r($response);
        // exit;

        foreach($response['hits']['hits'] as $value){       
         // $pimage            = $base_url.'/sites/default/files/upload_parts/'. $value['_source']['product_image'];
          if($value['_source']['product_image'] =='no_image_icon.PNG'){
                  $pimage=$base_url.'/sites/default/files/logobrand.png';
            }else{
               $pimage            = $urlarr['dynamic_url'].$value['_source']['product_image'];
            }
           //$pimage            = $urlarr['dynamic_url'].$value['_source']['product_image'];
          $field_color_value            =  "";
          $category_name                = "";
          //$discount_price    = 0;
          $variant_val = [];
          $wish    = 1;
          $compare = 1; 
          $call_price = $value['_source']['call'];
          $total_rating=(rand(1,5));
          $total_cnt=(rand(1,50));
          $msg_suffle=(rand(1,6));
          
          if($call_price == null){
              $call_price = "off";
          }
          if($call_price == 'off'){
            $discount_price    =round($value['_source']['price'],1)+((10/100)*round($value['_source']['price'],1));
              $ressult[] = array(
                                 $value['_source']['productid'],
                                 $value['_source']['product_name'],
                                 $value['_source']['featured'],
                                 $value['_source']['partno'],
                                 $value['_source']['stock'],
                                 round($value['_source']['price'],2),
                                 $pimage,
                                 0,
                                 $value['_source']['product_name'],
                                 $value['_source']['category'],
                                 $value['_source']['brand'],
                                 $field_color_value,
                                 $value['_source']['category'],
                                 round($discount_price,2),
                                 $value['_source']['new'],
                                 $value['_source']['marketing_message'],
                                 $variant_val,
                                 $wish,
                                 $compare,
                                 $value['_source']['sku'],
                                 $call_price,                                 
                                 $total_rating,
                                 $total_cnt,
                                 $msg_suffle,
                                 $value['_source']['colors']
                                );

          }          
        } 
  
    $color_filter = [];
    echo json_encode(array('products'=>$ressult,'colorfilter'=>$color_filter));
    exit();    
  }

   public function getuserswishlist() {
      $user        = \Drupal::currentUser();
      $connection  = \Drupal::database();
      $user_id     = $user->id();
      $wishlistdata = [];
      if($user_id != 0){
        $wishlistquery = $connection->query("SELECT product_id FROM commerce_wish_list_user where user_id = '".$user_id."' and status1 = 1 " );
        while($row = $wishlistquery->fetchAssoc()){
          $wishlistdata[]=$row['product_id'];
        }
      }
      echo json_encode($wishlistdata);
      die();
   }

    public function get_market_msg(){
    $vid = 'marketmessage';
      $market_data=array();
      $termss = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);
    foreach ($termss as $termm) {
        $term_obj = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($termm->tid);
        $market_data[] = [
        'tid' => $termm->tid,
        'tname' => $termm->name,
            
      ];   
      }
        echo json_encode($market_data);
    die();

  }

}
