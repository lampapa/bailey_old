<?php
/**
 * @file
 * Contains \Drupal\hello\HelloController.
 */

namespace Drupal\drupalup_controller\Controller;
use Drupal\node\Entity\NodeType;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\commerce_product\Entity\Product;
use Drupal\Core\Database\Database;
require_once "sites/libraries/vendor/autoload.php";

$GLOBALS['partdetail'] = \Elasticsearch\ClientBuilder::create()->build();


class PartSearch {

  //include model functions
  //include_once("/model/CommonModel.php");

  //module_load_include('php', 'drupalup_controller','CommonModel.php');

  //include_once("/model/PartSearchModel.php");
  //public $connection = \Drupal::database();

  public function page(){

    
    
    $categories = [];
    $brand = [];
   // $activeThemeName = \Drupal::service('theme.manager')->getActiveTheme()->getName();
    //$connection = \Drupal::database();
    $result_category = $this->get_category();
    while($row = $result_category->fetchAssoc()){

        $categories[]=array('category_id'=>$row['tid'],'category_name'=>$row['name']);
    }
    $result_brand = $this->get_brand();
    while($row = $result_brand->fetchAssoc()){
        $brand[]=array('brand_id'=>$row['tid'],'brand_name'=>$row['name']);
    }
    $items = [];    
    return array(
        '#theme' => 'part_search',
        '#items'=>$items,
        '#categories' =>$categories,
        '#brand' => $brand,
        '#title'=>'',
        '#path'=>''
    );
  }
 
  /*public function getproducts() {
    $connection = \Drupal::database();
    $user        = \Drupal::currentUser();
    $user_id     = $user->id();
    
    $result = $this->getproducts_detail();
   $colortemp  = array('red,blue','pink,black','purple,white,black','bronze,gold','green','green,yellow');
    while($row = $result->fetchAssoc()){
      $spliturl=str_replace('public://','',$row['uri']); 
      $prodname = mb_strimwidth($row['field_product_name_value'], 0, 55, "...");          
      $pid = $row['product_id'];
      $category = $row['field_category_name_target_id'];
      
      $category_name = $this->get_category_name($row['field_category_name_target_id']);

      $discount_price =round($row['field_total_price_number'],1)+((10/100)*round($row['field_total_price_number'],1));
      
      $status1 = 0;
      $result_wish = $this->get_wish_list($pid,$user_id);
      while($row1 = $result_wish->fetchAssoc()){
        $status1 = $row1['status1'];
      }
      
      
      $temp_variant = array();
      $result_variant = $this->get_color_variant($pid);
      while($row1 = $result_variant->fetchAssoc()){
        $temp_variant[] = $row1['c_value'];
      }
      
      $variant_val = implode(',',$temp_variant);

      $query4      = $connection->query("SELECT * from catapult_theme_setting");
      while ($row1 = $query4->fetchAssoc()){
        $wish    = $row1['wish'];
        $compare = $row1['compare'];
          
      }

      $product1 = array($row['product_id'],$prodname,$row['field_new_feature_value'],$row['field_partno_value'],$row['field_stock_info_value'],round($row['field_total_price_number'],2),$spliturl,$status1,$row['field_product_name_value'],$category,$row['field_brand_target_id'],$row['field_color_value'],$category_name,round($discount_price,2),$row['field_new_value'],$row['field_marketing_messages_value'],$variant_val,$wish,$compare);
      $items[] = $product1;
    }
    
   
    $color_filter = [];
    $result_color_filter = $this->get_color_filter();
    while($row = $result_color_filter->fetchAssoc()){
        $color_filter[]=array('value'=>$row['value'],'name'=>$row['name'],'url'=>$row['img_url']);
    }

    
    echo json_encode(array('products'=>$items,'colorfilter'=>$color_filter));
    exit();
  }*/
  public function getproducts() {
    global $base_url;
    $items     = [];
    $connection         = \Drupal::database();
    $user               = \Drupal::currentUser();
    $user_id            = $user->id();
    $status1            = 0;
   /* $temp_store_factory = \Drupal::service('session_based_temp_store');
    $temp_store         = $temp_store_factory->get('my_module_name', 4800);     
    $redirecturl        = $temp_store->get('last_url');   
    if(isset($_SESSION['_sf2_attributes']['uid']) && $redirecturl != ''){
        $temp_store->delete('last_url');
        echo '<script>window.location.href="'.$redirecturl.'";</script>';exit;
    } */
    $products = array();   
    /*$query4      = $connection->query("SELECT * from catapult_theme_setting");
    while ($row1 = $query4->fetchAssoc()){
      $wish    = $row1['wish'];
      $compare = $row1['compare'];
        
    }*/ 
    $wish    = 1;
    $compare = 1; 
    $field_new_value              =  "";
    $field_stock_info             =  "";
    $field_color_value            =  "";
    $field_new                    =  "";
    $field_new_feature            = "";
    $field_marketing_messages     = "";
    $category_name                = "";



          

    

    //$query = $connection->query("SELECT product_id FROM commerce_product where delete_flag=0");    
    $query = $connection->query("SELECT distinct(c.product_id),p.field_partno_value ,t.field_total_price_number,
          pn.field_product_name_value,b.field_brand_target_id,cn.field_category_name_target_id,
          (select status1 from commerce_wish_list_user w where w.product_id = c.product_id 
          and w.user_id=".$user_id." limit 0,1) AS status1,cap.field_call_price_value,pim.field_product_image_value as field_product_image_value
          FROM commerce_product c JOIN commerce_product__field_partno p
          on c.product_id=p.entity_id
          JOIN commerce_product__field_total_price t ON t.entity_id = c.product_id
          JOIN commerce_product__field_product_name pn ON pn.entity_id = c.product_id
          JOIN commerce_product__field_brand b ON b.entity_id = c.product_id
          JOIN commerce_product__field_category_name cn ON cn.entity_id = c.product_id
          LEFT JOIN commerce_product__field_call_price cap ON cap.entity_id = c.product_id
          LEFT JOIN commerce_product__field_product_image AS pim ON pim.entity_id =c.product_id
           
          WHERE c.delete_flag = 0");

      $urldata=$connection->query("select dynamic_url from catapult_config_meta");
      $urlarr = $urldata->fetchAssoc();
      
      foreach($query as $row)  {  
        $sel_sku=array();

      $query1=$connection->query("select distinct(sku) as p_sku from commerce_product_variation_field_data where product_id='".$row->product_id."' and sku IS NOT NULL" );

     foreach($query1 as $row1)  {  
       $sel_sku[]=$row1->p_sku;
      }

         /* if($row->user_id == $user_id ){
            $status1 = $row->status1;
          }else{
            $status1 = 0;
          }*/
   // while($row = $query->fetchAssoc()){
      $id                           = $row->product_id;
      //$product                      = \Drupal\commerce_product\Entity\Product::load($row->product_id);   
      /*$prodname                     =  $product->get('field_product_name')->getValue()[0]['value'];
      $field_total_price            =  $product->get('field_total_price')->getValue()[0]['number'];
      $partnos                      =  $product->get('field_partno')->getValue()[0]['value'];
      $brands                       =  $product->get('field_brand')->getValue()[0]['target_id'];
      $category_name                =  $product->get('field_category_name')->getValue()[0]['target_id'];*/
      $prodname          =  $row->field_product_name_value;
      $field_total_price =  $row->field_total_price_number;
      $partnos           =  $row->field_partno_value;
      $brands            =  $row->field_brand_target_id;
      $category_name     =  $row->field_category_name_target_id;
      $status1           =  $row->status1;
      $call_price     =  $row->field_call_price_value;

      if($call_price == null){
        $call_price = "off";
      }
     //die();
    /* if($row->user_id == $user_id){       
        $status1           =  $row->status1;        
     }else{
        $status1 = 0;
     } */
      if($status1 == ""){
          $status1 = 0;
      }     
      $discount_price    = 0;
      if($row->field_product_image_value =='no_image_icon.PNG'){
              $pimage=$base_url.'/sites/default/files/no_image_icon.PNG';
        }else{
           $pimage            = $urlarr['dynamic_url'].$row->field_product_image_value;
        }
     // $pimage            = $urlarr['dynamic_url'].$row->field_product_image_value;
      //$pimage            =  file_create_url($product->field_product_image->entity->getFileUri());
     //     $pimage ='';
      /*$field_new_value              =  $product->get('field_new')->getValue()[0]['value'];
      $field_stock_info             =  $product->get('field_stock_info')->getValue()[0]['value'];
      $field_color_value            =  $product->get('field_color')->getValue();
      $field_new                    =  $product->get('field_new')->getValue();*/
     
      
     
      //$field_marketing_messages     =  $product->get('field_marketing_messages')->getValue()[0]['value'];
      //$category_name                =  $product->get('field_category_name')->getValue()[0]['target_id'];
     // $field_color_value            =  $product->get('field_color')->getValue();  
      /*$query5                       = $connection->query("SELECT * FROM commerce_wish_list_user where product_id ='".$id."' and user_id = '".$user_id."' ");*/
      //$discount_price               = round($field_total_price,1)+((10/100)*round($field_total_price,1));
      
      /*while($row5 = $query5->fetchAssoc()){
        $status1 = $row5['status1'];
      } */
        
     
 /*$product1[] = array('id'=>$id,
        'prodname'=>$prodname,
        'field_new_feature'=>$field_new_feature,
        'partnos'=>$partnos,
        'field_stock_info'=>$field_stock_info,
        'field_total_price'=>round($field_total_price,2),
        'pimage'=>$pimage,
        'status1'=>$status1,
        'prodname'=>$prodname,
        'category_name'=>$category_name,
        'brands'=>$brands,
        'field_color_value'=>$field_color_value,
        'category_name'=>$category_name,
        'discount_price'=>round($discount_price,2),
        'field_new_value'=>$field_new_value,
        'field_marketing_messages'=>$field_marketing_messages,
        'variant_val'=>$variant_val,
        'wish'=>$wish,
        'compare'=>$compare);*/
     
      //$variant_val = [];
        $discount_price               = round($field_total_price,1)+((10/100)*round($field_total_price,1));
      
        $variant_val = [];
        $product1 = [];
        if($call_price == 'off'){
                $product1 = array($id,
                                  $prodname,
                                  $field_new_feature,
                                  $partnos,
                                  $field_stock_info,
                                  round($field_total_price,2),
                                  $pimage,
                                  $status1,
                                  $prodname,
                                  $category_name,
                                  $brands,
                                  $field_color_value,
                                  $category_name,
                                  round($discount_price,2),
                                  $field_new_value,
                                  $field_marketing_messages,
                                  $variant_val,
                                  $wish,
                                  $compare,
                                  $sel_sku,
                                  $call_price);
        }        

  
      $items[]  = $product1;
      $status1  = 0;  
     
      
    }
    /* echo "<pre>";
      print_r($sel_sku);
die();*/
    $variant_val = [];
    $color_filter = [];
    echo json_encode(array('products'=>$items,'colorfilter'=>$color_filter));
    exit();    
  }


  public function getproduct_csv_write(){
    ini_set("display_errors", 1);
    global $base_url;
    $file = fopen("cronfile/"."cron44.csv","w");
    $items                    = [];
    $connection               = \Drupal::database();
    $user                     = \Drupal::currentUser();
    $user_id                  = $user->id();
    $status1                  = 0;
    $products                 = array();       
    $wish                     = 1;
    $compare                  = 1; 
    $field_new_value          = "";
    $field_stock_info         = "";
    $field_color_value        = "";
    $field_new                = "";
    $field_new_feature        = "";
    $field_marketing_messages_value = "";
    $category_name            = "";

    $query = $connection->query("SELECT distinct(c.product_id),cfs.field_stock_info_value,
      p.field_partno_value ,t.field_total_price_number,cpnew.field_new_value,cmsg.field_market_message_target_id,
          pn.field_product_name_value,b.field_brand_target_id,cn.field_category_name_target_id,
          cfeat.field_featured_value,
          (select status1 from commerce_wish_list_user w where w.product_id = c.product_id 
          and w.user_id=".$user_id." limit 0,1) AS status1,cap.field_call_price_value,pim.field_product_image_value as field_product_image_value
          FROM commerce_product c JOIN commerce_product__field_partno p
          on c.product_id=p.entity_id
          JOIN commerce_product__field_total_price t ON t.entity_id = c.product_id
          JOIN commerce_product__field_product_name pn ON pn.entity_id = c.product_id
          JOIN commerce_product__field_brand b ON b.entity_id = c.product_id
          JOIN commerce_product__field_category_name cn ON cn.entity_id = c.product_id
          JOIN commerce_product__field_stock_info AS cfs ON cfs.entity_id = c.product_id
          JOIN commerce_product__field_featured AS cfeat ON cfeat.entity_id = c.product_id
          JOIN commerce_product__field_new AS cpnew ON cpnew.entity_id = c.product_id
          LEFT JOIN commerce_product__field_market_message AS cmsg ON cmsg.entity_id = c.product_id          
          LEFT JOIN commerce_product__field_call_price cap ON cap.entity_id = c.product_id
          LEFT JOIN commerce_product__field_product_image AS pim ON pim.entity_id =c.product_id           
          WHERE c.delete_flag = 0");


      

    fputcsv($file, array("id"=>'Sno',
                        "product_name"=>'Product Name',
                        "featured"=>'Featured',
                        "partno"=>'Partno',
                        "stock"=>'stock',
                        "price"=>'Price',
                        "pimage"=>'Product Image',
                        "status"=>'Status',                        
                        "category"=>'Category',
                        "brands"=>'Brand',                        
                        "new"=>'New',
                        "message"=>'Marketing Message',
                        "sku"=>'Sku',
                        "call_price"=>'Call For Price',
                        'colors'=>'colors'
                      ));
     
    foreach($query as $row){  
      $sel_sku=array();
      $query1=$connection->query("select distinct(sku) as p_sku from commerce_product_variation_field_data where product_id='".$row->product_id."' and sku IS NOT NULL" );
      foreach($query1 as $row1)  {  
        $sel_sku1 = $row1->p_sku;
      }

      $id                =  $row->product_id;
      $prodname          =  $row->field_product_name_value;
      $field_total_price =  $row->field_total_price_number;
      $partnos           =  $row->field_partno_value;
      $brands            =  $row->field_brand_target_id;
      $category_name     =  $row->field_category_name_target_id;
      $status1           =  $row->status1;
      $call_price        =  $row->field_call_price_value;
      $field_stock_info  =  $row->field_stock_info_value;
      $field_new_feature =  $row->field_featured_value;
      $field_new_value   =  $row->field_new_value;
      //$field_marketing_messages_value = $row->field_marketing_messages_value;

      if($row->field_market_message_target_id !='')
      {
        $field_marketing_messages_value = $row->field_market_message_target_id;
      }
      else
      {
         $field_marketing_messages_value = 0;
      }

      if($call_price == null){
        $call_price = "off";
      }
      if($status1 == ""){
          $status1 = 0;
      }     
      $discount_price = 0;
      $pimage         = $row->field_product_image_value;        
      
      $colors =array();
      $colors = $this->color_set($id);
      /*echo "<pre>";
      print_r($colors);
      die();
*/  

        if($field_total_price !=0){
          fputcsv($file, array("id"=>$id,
                              "product_name"=>$prodname,
                              "featured"=>$field_new_feature,
                              "partno"=>$partnos,
                              "stock"=>$field_stock_info,
                              "price"=>round($field_total_price,2),
                              "pimage"=>$pimage,
                              "status"=>$status1,                        
                              "category"=>$category_name,
                              "brands"=>$brands,                        
                              "new"=>$field_new_value,
                              "message"=>$field_marketing_messages_value,
                              "sku"=>$sel_sku1,
                              "call_price"=>$call_price,
                              'color_value'=>$colors
                            ));
        }  
      
      //die();
     
    }  
    
    chmod("cronfile/"."cron44.csv",0777);
/*$params = [
             'index' => 'sivatest',
             'type' => 'products',
             'id' => 4,
             'body' => [
                   'productid'  => 1
                  ],
            ];            
          
          $elastic = $GLOBALS['partdetail'];
          $response = $elastic->index($params);*/
    exit();    
  }


  public function color_set($product_id){
      $result1 = [];
      $results = "";
       $connection               = \Drupal::database();
        $query1=$connection->query("SELECT group_concat(clr.field_color_target_id separator '|') as color_id 
                                    FROM commerce_product_variation__field_color clr
                                    INNER JOIN commerce_product__variations clr_v
                                    ON clr.entity_id = clr_v.variations_target_id
                                    where clr_v.entity_id='".$product_id."'
                                    group by clr_v.entity_id;" );
      foreach($query1 as $row1)  {  
        $results = $row1->color_id;
      }
      //$result1[] =  explode(",",$results);
      return $results;
  }
  public function elasticsearch_indexing(){
    ini_set('max_execution_time', 0);
    ini_set("memory_limit", "-1");
    global $base_url;
    error_reporting(E_ERROR | E_WARNING | E_PARSE);
    error_reporting(E_ALL);
    ini_set("error_reporting", E_ALL);
    error_reporting(E_ALL & ~E_NOTICE);
    ini_set("display_errors", 1);
    $elastic = $GLOBALS['partdetail'];
    $file = fopen("cronfile/"."cron44.csv","r");
    $id = 0;
    fgetcsv($file);
    while (($line = fgetcsv($file, 1000, ",")) !== FALSE) {
      $res = [];
      $res[] =  explode("|",$line[14]);
        $partno_bcc = str_replace("-","",$line[3]);
        /*$params = [
              'index' => 'xclutchcpindex',
              'type' => 'products',
              'body' => [
                  'query' => [
                      'match' => ['productid'=>$line[0]]
                  ]
              ]
        ];
        $elastic->deleteByQuery($params);*/
        $params = [
              'index' => 'xclutchcpindex',
              'type' => 'products',
              'id' => $line[0]
        ];
        //$elastic->delete($params);

        $params = [
             'index' => 'xclutchcpindex',
             'type' => 'products',
             'id' => $line[0],
             'body' => [
                   'productid'  => $line[0],
                   'product_name'  => $line[1],
                   'featured'  => $line[2],
                   'partno'  => $line[3],
                   'partnobcc'  => $partno_bcc,
                   'stock'  => $line[4],
                   'price'  => $line[5],
                   'product_image'  => $line[6],
                   'status'  => $line[7],
                   'category'  => $line[8],
                   'brand'  => $line[9],
                   'new'  => $line[10],
                   'marketing_message'  => $line[11],
                   'sku'  => $line[12],
                   'call'  => $line[13],
                   'colors'=>$res
                  ],
            ]; 
            // echo "<pre>";
            // print_r($params);
            // exit;
          $response = $elastic->index($params);
      $id++;      
    }
    fclose($file);
    die();
  }
  public function get_ymme() {
        $res_arr = array();
        $flag = '';
        $data = json_decode(file_get_contents("php://input"), true);
        if($data['flag'] == 'ye'){
          $res = $this->ymme('',$data['make'],'','ye');
          while($row = $res->fetchAssoc()){
            array_push($res_arr,trim($row['year']));
          }
          $flag = 'ye';
        }else if($data['flag'] == 'ma'){
          $res = $this->ymme($data['year'],'','','ma');
          while($row = $res->fetchAssoc()){
            array_push($res_arr,trim($row['make']));
          }
          $flag = 'ma';
        }else if($data['flag'] == 'mo'){
          $res = $this->ymme($data['year'],$data['make'],'','mo');
          while($row = $res->fetchAssoc()){
            if($row['model'] !='')array_push($res_arr,trim($row['model']));
          }
          $flag = 'mo';
          if(count($res_arr) == 0){
            $res = $this->ymme($data['year'],$data['make'],'','en');
            while($row = $res->fetchAssoc()){
              if($row['engine'] !='')array_push($res_arr,trim($row['engine']));
            }
            $flag = 'en';
          }
        }else if($data['flag'] == 'en'){
          $res = $this->ymme($data['year'],$data['make'],$data['model'],'en');
          while($row = $res->fetchAssoc()){
            if($row['engine'] !='')array_push($res_arr,trim($row['engine']));
          }
          $flag = 'en';
        }

        echo json_encode(array('result'=>$res_arr,'flag'=>$flag));
        exit();
  }

  public function get_filter_products() {
    $res_arr = array();
    $result = array();
    $data = json_decode(file_get_contents("php://input"), true);
    $res = $this->filter_products($data['year'],$data['make'],$data['model'],$data['engine'],$data['keywrd'],$data['product_ids']);
    while($row = $res->fetchAssoc()){
      array_push($res_arr,$row['product_id']);
    }
    
    /*if($data['keywrd'] == 1){
      $result = array_intersect($res_arr,$data['product_ids']);
    }else{
      $result = $res_arr;
    }
*/
    echo json_encode($res_arr);
    exit();
  }

  
  


  /******************************************************************************************************
  model functions ***************************************************************************************/
  
  public function getproducts_detail() {
    $connection = \Drupal::database();
    $query = $connection->query("SELECT * FROM commerce_product AS cp LEFT JOIN commerce_product__field_product_name AS pn ON pn.entity_id=cp.product_id 
    LEFT JOIN commerce_product__field_new_feature AS nf ON nf.entity_id =cp.product_id LEFT JOIN commerce_product__field_partno AS pp ON pp.entity_id =cp.product_id
    LEFT JOIN commerce_product__field_stock_info  AS si ON si.entity_id = cp.product_id LEFT JOIN commerce_product__field_total_price AS tp ON tp.entity_id=cp.product_id
    LEFT JOIN commerce_product__field_wishlist AS fw ON fw.entity_id=cp.product_id LEFT JOIN commerce_product__field_product_image AS pim ON pim.entity_id =cp.product_id  
    LEFT JOIN commerce_product__field_new  AS f_n ON f_n.entity_id=cp.product_id
    LEFT JOIN commerce_product__field_marketing_messages  AS mm ON mm.entity_id=cp.product_id

    LEFT JOIN commerce_product__field_brand AS b ON b.entity_id=cp.product_id
    LEFT JOIN commerce_product__field_category_name AS cn ON cn.entity_id=cp.product_id

    LEFT JOIN commerce_product__field_product_category AS fc ON fc.entity_id=cp.product_id 
    LEFT JOIN commerce_product__field_color AS pc ON pc.entity_id=cp.product_id

    ORDER BY cp.product_id ASC");
    return $query;
  }
  
  public function ymme($year,$make,$model,$flag){
    $connection = \Drupal::database();
    /*if($flag == 'ye'){
      $querys = $connection->query("SELECT distinct year from catapult_application where make='".$make."' order by year");
    }else if($flag == 'ma'){
      $querys = $connection->query("SELECT distinct make from catapult_application where year='".$year."' order by make");
    }else if($flag == 'mo'){
      $querys = $connection->query("SELECT distinct model from catapult_application where year='".$year."' and make='".$make."' order by model");
    }else if($flag == 'en'){
      $querys = $connection->query("SELECT distinct engine from catapult_application where year='".$year."' and make='".$make."' and model='".$model."' order by engine" );
    }*/
   /* if($flag == 'ye'){
      $querys = $connection->query("SELECT DISTINCT cy.year FROM catapult_year cy    INNER JOIN  `catapult_make`  cm ON cy.make_id=cm.make_id 
    INNER JOIN  `catapult_application_map`  cam ON cy.year_id=cam.year_id     WHERE cm.make='".$make."' ORDER BY cy.year");
    }else if($flag == 'ma'){
      $querys = $connection->query(" SELECT DISTINCT cm.make FROM catapult_year cy INNER JOIN  `catapult_make`  cm ON cy.make_id=cm.make_id 
    INNER JOIN  `catapult_application_map`  cam ON cy.year_id=cam.year_id 
    WHERE cy.year='".$year."' ORDER BY cm.make");
    }else if($flag == 'mo'){
      $querys = $connection->query(" SELECT DISTINCT cmo.model FROM catapult_year cy  INNER JOIN  `catapult_model`  cmo ON cy.model_id=cmo.model_id     INNER JOIN  `catapult_make`  cm ON cy.make_id=cm.make_id     INNER JOIN  `catapult_application_map`  cam ON cy.year_id=cam.year_id    WHERE cy.year='".$year."' AND cm.make='".$make."' ORDER BY cmo.model");
    }else if($flag == 'en'){
      $querys = $connection->query("SELECT DISTINCT e.engine FROM catapult_year cy  INNER JOIN  `catapult_model`  cmo ON cy.model_id=cmo.model_id  INNER JOIN  `catapult_make`  cm ON cy.make_id=cm.make_id     INNER JOIN  `catapult_engine`  e ON cy.engine_id=e.engine_id 
    INNER JOIN  `catapult_application_map`  cam ON cy.year_id=cam.year_id  WHERE cy.year='".$year."' AND cm.make='".$make."' AND cmo.model='".$model."' ORDER BY e.engine" );
    }*/
    if($flag == 'ye'){

    $querys = $connection->query("SELECT DISTINCT cy.year FROM catapult_engine ce 
    INNER JOIN `catapult_make` cm ON ce.make_id=cm.make_id 
    INNER JOIN `catapult_application_map` cam ON ce.engine_id=cam.engine_id 
    INNER JOIN `catapult_year` cy ON ce.year_id=cy.year_id  
    WHERE cm.make='".$make."' ORDER BY cy.year");


    }else if($flag == 'ma'){

    $querys = $connection->query(" SELECT DISTINCT cm.make FROM catapult_engine ce 
    INNER JOIN `catapult_make` cm ON ce.make_id = cm.make_id 
    INNER JOIN `catapult_application_map` cam ON ce.engine_id = cam.engine_id 
    INNER JOIN `catapult_year` cy ON ce.year_id=cy.year_id  
    WHERE cy.year='".$year."' ORDER BY cm.make");


    }else if($flag == 'mo'){

    $querys = $connection->query(" SELECT DISTINCT cmo.model FROM catapult_engine ce 
    INNER JOIN `catapult_model` cmo ON ce.model_id=cmo.model_id 
    INNER JOIN `catapult_make` cm ON ce.make_id=cm.make_id 
    INNER JOIN `catapult_application_map` cam ON ce.engine_id=cam.engine_id 
    INNER JOIN `catapult_year` cy ON ce.year_id=cy.year_id 
    WHERE cy.year='".$year."' AND cm.make='".$make."' ORDER BY cmo.model");

    }else if($flag == 'en'){

    $querys = $connection->query("SELECT DISTINCT ce.engine FROM catapult_engine ce 
    INNER JOIN `catapult_make` cm ON ce.make_id=cm.make_id 
    INNER JOIN `catapult_model` cmo ON ce.model_id=cmo.model_id
    INNER JOIN `catapult_application_map` cam ON ce.engine_id=cam.engine_id 
    INNER JOIN `catapult_year` cy ON ce.year_id=cy.year_id
    WHERE cy.year='".$year."' AND cm.make='".$make."' AND cmo.model='".$model."' ORDER BY ce.engine");

    }
    return $querys;
  }

  public function filter_products($year,$make,$model,$engine,$keywrd_flag,$exist_product_ids){
    $connection = \Drupal::database();
    $engine_array=array();
    $partno_arr=[];
    $smt = "SELECT distinct engine_id from catapult_engine where";
    if($year !=''){
      $smt .=" year_id in (select year_id from catapult_year where year='".$year."')"; 
    }
    if($make !=''){
      $smt .=" and make_id in (select make_id from catapult_make where make='".$make."')"; 
    }
    if($model !=''){
      $smt .=" and model_id in (select model_id from catapult_model where model ='".$model."')"; 
    }
    if($engine !=''){
      $smt .=" and engine ='".$engine."'"; 
    }
     $querys = $connection->query($smt);
    
    while($row = $querys->fetchAssoc()){
     $engine_array[] = $row['engine_id'];
    }
    $rem = implode(',',$engine_array);
    $exist_pids = implode(',',$exist_product_ids);
    if($keywrd_flag == 1){
      $pro_qry = $connection->query("SELECT DISTINCT cpn.`entity_id` as product_id FROM `catapult_application_map` ca INNER JOIN  `commerce_product__field_partno` cpn ON ca.product_id = cpn.`field_partno_value` WHERE ca.engine_id in(".$rem.")
        AND  cpn.entity_id IN (".$exist_pids.")");
      return $pro_qry;
    }else{
      $pro_qry = $connection->query("SELECT DISTINCT cpn.`entity_id` as product_id FROM `catapult_application_map` ca INNER JOIN  `commerce_product__field_partno` cpn ON ca.product_id = cpn.`field_partno_value` WHERE ca.engine_id in(".$rem.")");
      return $pro_qry;
    }
    
  }


  public function get_config_details(){
    $connection = \Drupal::database();
    $querys = $connection->query("SELECT filter_combo,engine_flag from catapult_config_meta");
    return $querys;
  }
  /*public function get_year(){
    $connection = \Drupal::database();
    $querys = $connection->query("SELECT distinct year from catapult_application order by year");
    return $querys;
  }
  public function get_make(){
    $connection = \Drupal::database();
    $querys = $connection->query("SELECT distinct make from catapult_application order by make");
    return $querys;
  }*/

  public function get_wish_list($pid,$user_id){
    $connection = \Drupal::database();
    $query1 = $connection->query("SELECT * FROM commerce_wish_list_user where product_id ='".$pid."' and user_id = '".$user_id."' ");
    return $query1;
  }
  public function get_category(){
    $connection = \Drupal::database();
    $get_category = $connection->query("SELECT DISTINCT tp.* FROM `taxonomy_term_field_data` tp JOIN commerce_product__field_category_name AS cn ON tp.tid=cn.field_category_name_target_id 
       WHERE vid = 'category' AND STATUS='1' order by tp.name" );
    //$result = $get_category;
    return $get_category;
  }
  public function get_brand(){
    $connection = \Drupal::database();
    $get_brand = $connection->query("SELECT DISTINCT tp.* FROM `taxonomy_term_field_data` tp JOIN commerce_product__field_category_name AS cn ON tp.tid=cn.field_category_name_target_id 
       WHERE vid = 'brand' AND STATUS='1'  order by tp.name" );
    return $get_brand;
  }

  public function get_color_filter(){
    $connection = \Drupal::database();
    $get_filter = $connection->query("SELECT DISTINCT tp.* FROM `catapult_color_filter` tp");
    //$result = $get_category;
    return $get_filter;
  }

  public function get_color_variant($id){
    $connection = \Drupal::database();
    $qry_variation=$connection->query("SELECT cp.variation_id,cp.price__number as price_number,cp.price__currency_code as price_currency_code,cc.name as color_name,tp.value as c_value,tp.img_url as c_url FROM commerce_product_variation_field_data AS cp LEFT JOIN commerce_product_variation__field_color AS pn ON pn.entity_id=cp.variation_id LEFT JOIN taxonomy_term_field_data AS cc ON cc.tid=pn.field_color_target_id LEFT JOIN catapult_color_filter as tp on tp.name = cc.name

      where cp.product_id =".$id." AND cp.status = 1 AND cc.status = 1  order by cc.name");
     return $qry_variation;
  }

  public function get_category_name($id){
    $connection = \Drupal::database();
    $get_category_name = $connection->query("SELECT DISTINCT tp.name FROM `taxonomy_term_field_data` tp JOIN commerce_product__field_category_name AS cn ON tp.tid=cn.field_category_name_target_id 
       WHERE vid = 'category' AND STATUS='1' AND tp.tid ='".$id."'")->fetchAssoc();
    //print_r($get_category_name);
    $cat_name = $get_category_name['name'];
    return $cat_name;
  }

  public function historysave(){
    $connection = \Drupal::database();
    $keyword = $_POST['keyword'];
    $result_count = $_POST['count'];
    $insertqry = $connection->query("INSERT INTO catapult_keyword_search(keyword,result_count,created_on) values ('".$keyword."','".$result_count."',NOW())");
    die();
  }
  

}
