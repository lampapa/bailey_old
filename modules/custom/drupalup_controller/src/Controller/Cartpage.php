<?php
namespace Drupal\drupalup_controller\Controller;
use Drupal\node\Entity\NodeType;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\commerce_product\Entity\Product;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\commerce\commerce_product;
use Drupal\commerce;
use Drupal\commerce_cart;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\commerce_cart\CartProviderInterface;
use Drupal\commerce_cart\CartManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_order\Entity\Order;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\CssCommand;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\commerce_price\Price;
use Drupal\taxonomy\Entity\Term;

class Cartpage{

    public function page() {  
      $entity_manager = \Drupal::entityManager();
      global $base_url;
      $total      = 0; 
      $data       = array();
      $urls       = '';      
      $uid        = \Drupal::currentUser()->id(); 
      $connection = \Drupal::database();
      $tax_val =0;
    
      $query1 = $connection->query("SELECT order_id,`total_price__number` FROM `commerce_order` WHERE uid=".$uid." AND state = 'draft' AND cart=1");
      $result = $query1->fetchAssoc();
      
        $tax_query = $connection->query("SELECT tax_percentage FROM catapult_tax_setting");
      $tax_result = $tax_query->fetchAssoc();

      $var ='';
      if(!empty($result)){

        $order_id = $result['order_id'];
        $total_price = number_format($result['total_price__number'],2);

        $query = $connection->query("SELECT cp.product_id,oi.purchased_entity,oi.quantity,oi.unit_price__number FROM commerce_product_variation_field_data AS cp 
                  LEFT JOIN commerce_order_item AS oi ON cp.variation_id = oi.purchased_entity
                   WHERE oi.order_id ='".$order_id."'");
      $urldata=$connection->query("select dynamic_url from catapult_config_meta");
          $urlarr = $urldata->fetchAssoc();
        while($row = $query->fetchAssoc()){
         // $spliturl=str_replace('public://','',$row['uri']);
          $p_id       = $row['product_id'];
          $variant_id = $row['purchased_entity'];
          //$p_name     = $row['title'];
          $p_price    = round($row['unit_price__number'],2);
          $p_quantity = intval($row['quantity']);
         

          $product      = \Drupal\commerce_product\Entity\Product::load($p_id);             
          $prodname     =  $product->get('field_product_name')->getValue()[0]['value'];
          //$image        =  file_create_url($product->field_product_image->entity->getFileUri()); 
          //$image        = $urlarr['dynamic_url'].$product->get('field_product_image')->getValue()[0]['value'];       
            if($product->get('field_product_image')->getValue()[0]['value'] =='no_image_icon.PNG'){
              $image=$base_url.'/sites/default/files/no_image_icon.PNG';
            }else{
               $image        = $urlarr['dynamic_url'].$product->get('field_product_image')->getValue()[0]['value'];
            }

          //$image = $base_url."/sites/default/files/".$spliturl;
          // $urls = $base_url."/checkout/".$order_id."/orderinformation";
           $urls = $base_url."/orderinformations";
         
          //$total_price = $p_price*$p_quantity;
        


          //color variant picking

          /*single color picking*/
          $product_variation = $entity_manager->getStorage('commerce_product_variation')->load((int)$variant_id);
          $data_color1 = $product_variation->get('field_color')->getValue()[0]['target_id'];
          $field_sku = $product_variation->get('sku')->getValue()[0]['value'];

          $vid   = 'color_parent';
          $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid); 
          
          foreach ($terms as $term){
          
            if($data_color1 == $term->tid){
              $term_obj      = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($term->tid);
              $variant_color = $term_obj->get('field_colorname')->value;
            }
           
          }
          //die();

          /*end of single color picking*/


            foreach($product->getVariationIds() as $key=>$value){

              $product_variation = $entity_manager->getStorage('commerce_product_variation')->load((int)$value);       
             /* echo "<pre>";
              print_r($product_variation);*/
              
              $data_color = $product_variation->get('field_color')->getValue()[0]['target_id'];   
              if($data_color == 152){
                $basic_price = $product_variation->get('price')->getValue()[0]['number'];
              }                                          
            } 
           // die();

          
          $qry_variation=$connection->query("SELECT cp.variation_id,cp.price__number as price_number,cp.price__currency_code as price_currency_code,cc.name as color_name,tp.value as c_value,tp.img_url as c_url FROM commerce_product_variation_field_data AS cp LEFT JOIN commerce_product_variation__field_color AS pn ON pn.entity_id=cp.variation_id LEFT JOIN taxonomy_term_field_data AS cc ON cc.tid=pn.field_color_target_id LEFT JOIN catapult_color_filter as tp on tp.name = cc.name

          where cp.product_id=".$p_id." AND cp.variation_id=".$variant_id." AND cp.status = 1 AND cc.status = 1 ");
          $vari_filter = [];
          while($row = $qry_variation->fetchAssoc()){
             $basic_price_val = $row['price_number'] - $basic_price;  ///old

         
           

            $hidden_price   = $row['price_number']-$basic_price_val;
            $vari_filter[]=array($row['variation_id'],$row['price_number'],$row['price_currency_code'],$row['color_name'],$row['c_value'],$row['c_url']);
          }



          $hidden_price = number_format((float)$hidden_price, 2, '.', '');
           $total_price =  number_format((float)$hidden_price*$p_quantity, 2, '.', '');

            //new//
            
             $tax_val += ($hidden_price*$p_quantity)*($tax_result['tax_percentage']/100);
        
            ///new//

         



          $data[] =array("pdid"=>$p_id,"pvariant"=>$variant_id,"pname"=>$prodname,"price"=>$p_price,"quantity"=>$p_quantity,'image'=>$image,
            'total_price'=>$total_price,'order_id'=>$order_id,'variant'=>$vari_filter,'basic_price'=>number_format((float)$basic_price_val, 2, '.', ''),'hidden_price'=>$hidden_price,'variant_color'=>$variant_color,'sku'=>$field_sku
          );
          if( is_numeric($total_price) ){
            $total = number_format((float)$total+$total_price, 2, '.', '');
          }
          if( is_numeric($tax_val) ){
            $total_tax = sprintf('%0.2f',$tax_val);
          }
        }

      } 
      
     $tax_value= $tax_result['tax_percentage']/100;
      /*echo "<pre>";
      print_r($data);*/
      return array(
          '#theme' => 'cart_page',
          '#product_name'=>$data,         
          '#totaltax'=>$total_tax,

          //old//
          //'#totalvalues'=>$total,
          //old//
          //new//
          '#totalvalues'=>$total+$total_tax,
           '#tax_value'=>$tax_value,
          //new//
          "#urls"=>$urls
      );
    }
    
    public function updatecart(){
      $total      =  $_POST['total'];
      $orderids   =  $_POST['orderids'];
      $quantitys  =  $_POST['quantitys'];
      $id   =  $_POST['id'];
      $variation_id   =  $_POST['vid'];
      $connection = \Drupal::database();
      
      /*$query = $connection->query("SELECT variation_id FROM commerce_product_variation_field_data WHERE product_id = $id");
      while($row = $query->fetchAssoc()){
        $variation_id = $row['variation_id'];
      }*/
      $query1 = $connection->query("update commerce_order set total_price__number=$total where order_id = $orderids");

      $query2 = $connection->query("update commerce_order_item set quantity='".$quantitys."',total_price__number=(".$quantitys." * unit_price__number) where purchased_entity = $variation_id");

     // echo "update commerce_order set total_price__number=$total where order_id = $orderids";
      exit;
    }

    public function updatecart1($total,$variation_id,$quantitys){
      /*echo "test";
      die();*/
      $connection     = \Drupal::database();
      $total          =  $total;
      $orderids       =  "";
      $quantitys      =  $quantitys; 
      $variation_id      = $variation_id;
     //die();
     /* echo "SELECT order_id FROM commerce_order_item WHERE purchased_entity ='".$variation_id."'";
      die();*/
      $query          = $connection->query("SELECT order_id FROM commerce_order_item WHERE purchased_entity ='".$variation_id."'");
      while($row = $query->fetchAssoc()){
       $orderids = $row['order_id'];      
      } 
      //die();   
      $query1 = $connection->query("update commerce_order set total_price__number=$total where order_id = '".$orderids."'");

      $query2 = $connection->query("update commerce_order_item set quantity='".$quantitys."',total_price__number=(".$quantitys." * unit_price__number) where purchased_entity = $variation_id");

      //echo "update commerce_order set total_price__number=$total where order_id = $orderids";
      exit;
    }
   
 
    public function cartservices() {       
      /*global $base_url;
      $uid = \Drupal::currentUser()->id(); 
      $connection = \Drupal::database();
      $query = $connection->query("SELECT product_id FROM commerce_product_variation_field_data WHERE variation_id IN(SELECT DISTINCT `purchased_entity` FROM `commerce_order_item`)");
      $arr = array();
      while($row = $query->fetchAssoc()){        
          
          array_push($arr,$row['product_id']);
      }
      $product_ids = implode(',',$arr);   
      $query = $connection->query("SELECT * FROM commerce_product AS cp LEFT JOIN commerce_product__field_product_name AS pn ON pn.entity_id=cp.product_id LEFT JOIN commerce_product__field_total_price AS tp ON tp.entity_id=cp.product_id LEFT JOIN commerce_product_variation_field_data AS vf ON vf.`product_id`=cp.product_id LEFT JOIN commerce_order_item AS oi ON vf.variation_id = oi.`purchased_entity` LEFT JOIN commerce_order AS o ON o.order_id=oi.order_id
        LEFT JOIN commerce_product__field_product_image AS pim ON pim.entity_id =cp.product_id LEFT JOIN file_managed AS fm ON fm.fid =pim.field_product_image_target_id WHERE o.cart=1 and o.uid='".$uid."'  and cp.product_id IN(".$product_ids.")  ORDER BY cp.product_id ASC");
        $var ='';    
        while($row = $query->fetchAssoc()){
          $spliturl=str_replace('public://','',$row['uri']);    
          $prodname = mb_strimwidth($row['field_product_name_value'], 0, 30, "...");         
          $product1 = array($row['product_id'],$prodname,$row['field_new_feature_value'],$row['field_partno_value'],$row['field_stock_info_value'],round($row['field_total_price_number'],2),$spliturl,$row['field_wishlist_value'],$row['field_product_name_value']);
          $products[] = $product1;
          $p_id = $row['product_id'];
          $p_name =$row['field_product_name_value'];
          $p_desc =$row['field_new_feature_value'];
          $p_partno =$row['field_partno_value'];
          $p_stock =$row['field_stock_info_value'];
          $p_price =round($row['field_total_price_number'],2);
          $p_wishlist =$row['field_wishlist_value'];
          $p_quantity =$row['quantity'];
          if($uid != 0){
            $var .="<li><div class='cart-list-left'><img src='$base_url/sites/default/files/$spliturl' alt='' /></div><div class='lf-hle'><div class='lft'><div class='cart-list-right'><a class='word_break' href='#'>$p_name</a><span class='price_color'>$$p_price</span><span class='quantity'>Qty. : $p_quantity</span></div></div><div class='rgt'><i productid='$p_id' class='fa fa-close reomve_product' id='delete_items'></i></div></div></li>";
          }
        
        $data =array("pdid"=>$p_id,"pname"=>$p_name,"pdesc"=>$p_desc,"partno"=>$p_partno,
          "stock"=>$p_stock,"price"=>$p_price,"quantity"=>$p_quantity
        );
        }
        print_r($data);*/
        
        exit();  
    }

  public function changevariation(){
    $check_array    = [];
    $connection     = \Drupal::database();
    $entity_manager = \Drupal::entityManager();
    $variantid      = $_POST['newvariant'];    
    $old_variants   = $_POST['old_variants'];    
    $variant_count  = $_POST['variant_count'];
    $total          = $_POST['front_total'];
    $query          = $connection->query("SELECT product_id FROM commerce_product_variation_field_data WHERE variation_id ='".$old_variants."'");
    while($row = $query->fetchAssoc()){
       $id = $row['product_id'];      
    } 
    

    $array = [];
    $product        = \Drupal\commerce_product\Entity\Product::load($id);    
    foreach($product->getVariationIds() as $value){
      $product_variation = $entity_manager->getStorage('commerce_product_variation')->load((int)$value);
      
      if($variantid   == $product_variation->get('field_color')->getValue()[0]['target_id']){
        $new_variant = $value;       
      }      
      $array[] = $product_variation->get('field_color')->getValue()[0]['target_id'];
    } 

  /* echo "test".$new_variant;
   echo "<br>";
   echo $old_variants;*/
   //print_r($array);
   
   
    if($new_variant != $old_variants){

        $query1 = $connection->query("SELECT quantity,order_item_id,order_id FROM commerce_order_item 
          where purchased_entity='".$old_variants."'");
        while($res = $query1->fetchAssoc()){
          $oritem_ids = $res['order_item_id'];
          $quantitys = $res['quantity'];
          $order_ids = $res['order_id'];
        }

        /*check existing variation check*/
       /* echo "SELECT purchased_entity FROM commerce_order_item 
          where order_id='".$order_ids."'";*/

        $query_var = $connection->query("SELECT purchased_entity FROM commerce_order_item 
          where order_id='".$order_ids."'");
        while($res = $query_var->fetchAssoc()){
          $check_array[] =$res['purchased_entity'];
        }
       
        if(in_array($new_variant, $check_array)){
          echo "color_exist";
          die();
        }

        /*end of exiting variation check*/



        /*delete variation*/

          $query2 = $connection->query("delete from commerce_order_item where order_item_id ='".$oritem_ids."'");
          $query3 = $connection->query("delete from commerce_order__order_items where order_items_target_id = '".$oritem_ids."'");
          $q = $connection->query("select count(order_id) as cntorder from commerce_order_item where order_id ='".$order_ids."'");
          while($val = $q->fetchAssoc()){
            if($val['cntorder']==0){
              $query4 = $connection->query("delete from commerce_order where order_id ='".$order_ids."'");         
            }
          }
      
      /*end of delete variation*/

         

        $store_id = 1;
        $order_type = 'default';
        $entity_manager = \Drupal::entityManager();
        $cart_manager = \Drupal::service('commerce_cart.cart_manager');
        $cart_provider = \Drupal::service('commerce_cart.cart_provider');

        // Drupal\commerce_store\Entity\Store::load($store_id);
        $store = $entity_manager->getStorage('commerce_store')->load($store_id); 

        // order type and store
        $cart = $cart_provider->getCart($order_type, $store);
        if (!$cart) {
        $cart = $cart_provider->createCart($order_type, $store);
        }
            //Create new order item 
        $order_item = $entity_manager->getStorage('commerce_order_item')->create(array(
            'type' => 'default',
            'purchased_entity' => (string) $new_variant,
            'quantity' => 1,
            'unit_price' => 1     
        ));
        $order_item->save();
        $res = $cart_manager->addOrderItem($cart, $order_item);
        if($quantitys > 1){
          $this->updatecart1($total,$new_variant,$quantitys);
        }

         

    }else{
      echo "ColorAdded";
    }    
    die();
  }

  public function changecolorscart(){
    $entity_manager = \Drupal::entityManager();
    $connection     = \Drupal::database();
    $variantid      = $_POST['variantid'];
    $query = $connection->query("SELECT product_id FROM commerce_product_variation_field_data where variation_id='".$variantid."'");
    while($row = $query->fetchAssoc()){
      $p_id       = $row['product_id'];
    }
    $product_detail      = \Drupal\commerce_product\Entity\Product::load($p_id);
    foreach($product_detail->getVariationIds() as $key=>$value){
     /* echo $value;
      echo "<br>";*/
      $product_variation = $entity_manager->getStorage('commerce_product_variation')->load((int)$value);
     /* echo "<pre>";
      print_r($product_variation);*/

      $data_color = $product_variation->get('field_color')->getValue()[0]['target_id'];
      if($variantid == $value){
         $color_select = $data_color;
        //die();
      }
      
    }
    $vid   = 'color_parent';
    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid); 
    $select_value = "<select name='color[]' id='variant_update' required class='form-control'>";
    foreach ($terms as $term){
      $term_obj = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($term->tid);
      $colorname = $term_obj->get('field_colorname')->value;
      if($term->tid == 152){
          if($color_select == $term->tid){
            $select_value .= "<option selected  value=".$term->tid.">".$term->name."</option>";
          }else{
            $select_value .= "<option value=".$term->tid.">".$term->name."</option>";
          }
      }      
    }   
    foreach ($terms as $term){
      $term_obj = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($term->tid);
      $colorname = $term_obj->get('field_colorname')->value;
      if($term->tid != 152){
        if($color_select == $term->tid){
          $select_value .= "<option  selected value=".$term->tid.">".$term->name."</option>";
        }else{
          $select_value .= "<option value=".$term->tid.">".$term->name."</option>"; 
        }     
      }  
    }
    echo $select_value .=  "</select>";
    die();
  }
    
    
}