<?php
namespace Drupal\drupalup_controller\Controller;
use Drupal\node\Entity\NodeType;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\commerce_product\Entity\Product;
use Drupal\Core\Session\AccountProxyInterface;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\commerce\commerce_product;
use Drupal\commerce;
use Drupal\commerce_cart;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\commerce_cart\CartProviderInterface;
use Drupal\commerce_cart\CartManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_order\Entity\Order;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\CssCommand;

//for catlisting in header
//use Drupal\drupalup_controller\Controller;

class OrderInformations{

    public function page() {  
      // SELECT `product_id` FROM `commerce_product_variation_field_data` WHERE `variation_id`=17;
      /*global $base_url;
      $uid = \Drupal::currentUser()->id(); 
      $entity_manager = \Drupal::entityManager();
      $connection = \Drupal::database();*/


    global $base_url;
    $connection      = \Drupal::database();  
    $total_price     = [];
    $uid             = \Drupal::currentUser()->id(); 
    $entity_manager  = \Drupal::entityManager();
    $basic_price_val = 0;
    $basic_price     = 0;
    $shipping_fee    = 0.00;
    $order_id        = 0;
    $data=$coupon_list=[];
    $promotion_storage               = $entity_manager->getStorage('commerce_promotion'); 
    $coupon_list=$entity_id_promo=$entity_id_order=$found_part=$not_found_part=[];
    $pr_id = $connection->query("SELECT `promotion_id` FROM `commerce_promotion_field_data`");
   
    //echo '<pre>';
   /* while ($row=$pr_id->fetchAssoc()) {
        $promotion=$promotion_storage->load($row['promotion_id']);
        $promoid=$row['promotion_id'];
        //print_r($promotion->offer->target_plugin_configuration);
        $data = [];
        foreach ($promotion->offer->target_plugin_configuration as $key => $value) {
          foreach ($value[0]['configuration']['products'] as $keys => $values) {
            $uid_product=$values['product'];
            $entity_id = $connection->query("SELECT product_id FROM `commerce_product` WHERE UUID='".$uid_product."'");
            $row1=$entity_id->fetchAssoc();
            $data[]=$row1['product_id'];
          }
           $entity_id_promo[$promoid]=$data;
        }
    }
    
    $qry_data = $connection->query("SELECT vi.product_id FROM `commerce_order` AS co LEFT JOIN `commerce_order_item` AS coi ON coi.order_id=co.order_id LEFT JOIN `commerce_product_variation_field_data` AS vi ON coi.purchased_entity=vi.variation_id WHERE co.uid=".$uid." AND co.state = 'draft' AND co.cart=1");
           while ($row=$qry_data->fetchAssoc()) {
            $entity_id_order[]=$row['product_id'];
           }
        foreach ($entity_id_order as $val) {
          //echo $val;
          foreach ($entity_id_promo as $key => $promo_val) {
            if (in_array($val, $promo_val)) 
            { 
              $found_part[]=$key;
            } 
          } 
        }

      if(!empty($found_part)){
        foreach ($found_part as $key => $value) {
            $qrr_cou = $connection->query("SELECT id,UUID,promotion_id,code,usage_limit FROM `commerce_promotion_coupon` WHERE STATUS=1 and promotion_id='.$value.'");
            while ($row=$qrr_cou->fetchAssoc()) {
              $coupon_list[]=$row['code'];
            }
       } 
      }*/
    
    /*print_r($coupon_list);
    echo '</pre>';
    exit;*/
    
   /* $qrr_cou = $connection->query("SELECT id,UUID,promotion_id,code,usage_limit FROM `commerce_promotion_coupon` WHERE STATUS=1");
    $coupen_arr=[];
    while ($row=$qrr_cou->fetchAssoc()) {
          $entity_manager = \Drupal::entityManager();
          $coupon_storage = $entity_manager->getStorage('commerce_promotion_coupon');
          $order_storage = $entity_manager->getStorage('commerce_order');
          $coupon = $coupon_storage->loadEnabledByCode($row['code']);
          $coupon->id();  //shipping_fee,
          $query1 = $connection->query("SELECT order_id,`total_price__number` FROM `commerce_order` WHERE uid=".$uid." AND state = 'draft' AND cart=1");
          $result = $query1->fetchAssoc();
          if(!empty($result)){
              $order_id = $result['order_id'];
              $order = $order_storage->load($order_id);
              //need validation
              $query_order = $connection->query("SELECT * FROM `commerce_order__coupons` WHERE entity_id=".$order_id."");
              $or_result = $query_order->fetchAssoc();
              if(!$or_result){
                 $order->get('coupons')->appendItem($row['id']);
                 $order->save();
              }
          } 
         /* $coupons = $order->get('coupons')->referencedEntities();
          foreach ($coupons as $index => $coupon) {
            $promotion = $coupon->getPromotion();
            if ($coupon->available($order) && $promotion->applies($order)) {
              $promotion->apply($order);
              //echo "up";
            }
          }*/
          //get productid based on coupen

         /* $coupons = $order->get('coupons')->referencedEntities();
          foreach ($coupons as $index => $coupon) {
            $promotion = $coupon->getPromotion();
            foreach ($promotion->offer->target_plugin_configuration as $key => $value) {
              foreach ($value[0]['configuration']['products'] as $key => $value) {
                echo $value['product'].'</br>';
              }
            }
         }*/
         
      // }  */


 

    //get productid based on coupen
         /* $coupons = $order->get('coupons')->referencedEntities();
          foreach ($coupons as $index => $coupon) {
            $promotion = $coupon->getPromotion();
            foreach ($promotion->offer->target_plugin_configuration as $key => $value) {
              foreach ($value[0]['configuration']['products'] as $key => $value) {
                echo $value['product'].'</br>';
              }
            }

         }*/
         




      

       $tax_query = $connection->query("SELECT tax_percentage FROM catapult_tax_setting");
      $tax_result = $tax_query->fetchAssoc();


      $query1 = $connection->query("SELECT order_id,`total_price__number` FROM `commerce_order` WHERE uid=".$uid." AND state = 'draft' AND cart=1");

     $order=array();$order_summary=array();$summary=array();

     $result = $query1->fetchAssoc();
      if(!empty($result)){
          $order_id = $result['order_id'];
         
          $query = $connection->query("SELECT cp.product_id,oi.title,oi.quantity,oi.unit_price__number,oi.total_price__number,oi.purchased_entity,pim.field_product_image_value FROM commerce_product_variation_field_data AS cp 
            LEFT JOIN commerce_order_item AS oi ON cp.variation_id = oi.`purchased_entity`
            LEFT JOIN commerce_product__field_product_image AS pim ON pim.entity_id =cp.product_id 
             WHERE oi.order_id =".$order_id."  ORDER BY cp.product_id ASC");
          
                $urldata=$connection->query("select dynamic_url from catapult_config_meta");
                $urlarr = $urldata->fetchAssoc();
                $summary['order_id']=$order_id;
                $summary['base_url']=$base_url;
                $i=0;
            while($row = $query->fetchAssoc()){
               if($row['field_product_image_value'] =='no_image_icon.PNG'){
                     $order_summary[$i]['spliturl']=$base_url.'/sites/default/files/no_image_icon.PNG';
                }else{
                   $order_summary[$i]['spliturl']=$urlarr['dynamic_url'].$row['field_product_image_value'];
                }
                //$order_summary[$i]['spliturl']=$urlarr['dynamic_url'].$row['field_product_image_value'];
                //$order_summary[$i]['spliturl']=str_replace('public://','',$row['uri']);    
                $order_summary[$i]['prodname'] = mb_strimwidth($row['title'], 0, 30, "..."); 
                $order_summary[$i]['p_id'] = $row['product_id'];
                $order_summary[$i]['variant_id'] = $row['purchased_entity'];
                $order_summary[$i]['p_name'] =$row['title'];
                $order_summary[$i]['p_price'] =round($row['unit_price__number'],2);
                $order_summary[$i]['p_quantity'] =intval($row['quantity']);
                $order_summary[$i]['image'] =$order_summary[$i]['spliturl'];
                $order_summary[$i]['urls'] = $base_url."/checkout/".$order_id."/orderinformation";

                ///old//
               /* $order_summary[$i]['p_totalprice'] = sprintf('%0.2f',($order_summary[$i]['p_price']*$order_summary[$i]['p_quantity']));*/
               
              
 

               /*$summary['tax_val'] += number_format((float)$order_summary[$i]['p_totalprice']*($tax_result['tax_percentage']/100), 2, '.', '');*/

                $product_detail      = \Drupal\commerce_product\Entity\Product::load($row['product_id']);
                foreach($product_detail->getVariationIds() as $key=>$value){
                  $product_variation = $entity_manager->getStorage('commerce_product_variation')->load((int)$value);
                  $data_color = $product_variation->get('field_color')->getValue()[0]['target_id'];
                  if($row['purchased_entity'] == $value){
                     $color_select = $data_color;
                  }
                  if($data_color == 152){
                    $basic_price = $product_variation->get('price')->getValue()[0]['number'];
                  }
                }

                $vid   = 'color_parent';
                $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);
                foreach ($terms as $term){
                  $term_obj = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($term->tid);
                  //if($term->tid != 152){
                    if($color_select == $term->tid){
                     $field_colorname = $term_obj->get('field_colorname')->value;
                    }
                  //}
                }

               // old $p_price         =  round($row['unit_price__number'],2);
                $p_price         =  round($row['unit_price__number'],2);

                $basic_price_val =  $p_price - $basic_price;
                $hidden_price    =  $p_price - $basic_price_val;
                
                $order_summary[$i]['field_colorname']=$field_colorname;
                // old $order_summary[$i]['basic_price']=sprintf('%0.2f',round($basic_price));
                $order_summary[$i]['basic_price']=sprintf('%0.2f',$basic_price);
                $order_summary[$i]['hidden_price']=$hidden_price;
                $order_summary[$i]['basic_price_val']=sprintf('%0.2f',round($basic_price_val));
                $total_price[]=$row['quantity']*$basic_price;
                $discount[]=$row['quantity']*$basic_price_val;
                /////new///
                 $order_summary[$i]['p_totalprice'] = sprintf('%0.2f',($order_summary[$i]['basic_price']*$order_summary[$i]['p_quantity']));
                 ///new//

                
            //  $tax_pr[] =sprintf('%0.2f', $row['quantity']*$basic_price*($tax_result['tax_percentage']/100));
                //color variant picking
                ///// modified by nandha //// 
                $qry_variation=$connection->query("SELECT cp.variation_id,cp.price__number as price_number,cp.price__currency_code as price_currency_code,cc.name as color_name,tp.value as c_value,tp.img_url as c_url FROM commerce_product_variation_field_data AS cp LEFT JOIN commerce_product_variation__field_color AS pn ON pn.entity_id=cp.variation_id LEFT JOIN taxonomy_term_field_data AS cc ON cc.tid=pn.field_color_target_id LEFT JOIN catapult_color_filter as tp on tp.name = cc.name

                where cp.product_id=".$order_summary[$i]['p_id']." AND cp.variation_id=".$order_summary[$i]['variant_id']." AND cp.status = 1 AND cc.status = 1 ");
                $vari_filter = [];
                while($row = $qry_variation->fetchAssoc()){
                  $order_summary[$i]['price_number'] = $row['price_number'];
                  $order_summary[$i]['price_currency_code'] = $row['price_currency_code'];
                  $order_summary[$i]['color_name'] = $row['color_name'];
                  $order_summary[$i]['c_value'] = $row['c_value'];
                  $order_summary[$i]['c_url'] = $row['c_url'];
                  $j=0;
                }
                $i++;
            }
      } 
      $tax_pr =sprintf('%0.2f',  array_sum($total_price)*($tax_result['tax_percentage']/100));
      $summary['tax_val'] = sprintf('%0.2f',$tax_pr);
      $summary['total_price']=sprintf('%0.2f', array_sum($total_price)); 

      //existing coupon apply check
      $coupon_qry = $connection->query("SELECT o.entity_id, o.coupons_target_id, p.code,p.promotion_id
                                      FROM commerce_order__coupons o JOIN commerce_promotion_coupon p 
                                      ON p.id = o.coupons_target_id
                                      WHERE o.entity_id ='".$order_id."'");
      $coupon_result = $coupon_qry->fetchAssoc();

      //percent get
      if($coupon_result){
        $promotion_check   = $promotion_storage->load($coupon_result['promotion_id']); 
     
        if($promotion_check->get('offer')->target_plugin_id =='order_item_fixed_amount_off'){
          //Fixed amount off each matching product
          $amount                 = $promotion_check->get('offer')->target_plugin_configuration['amount']['number'];
          $summary['discount']    = sprintf('%0.2f',array_sum($discount));
          $summary['total_coupon_applied'] = sprintf('%0.2f',array_sum($total_price)-$summary['discount']);

        }elseif ($promotion_check->get('offer')->target_plugin_id =='order_fixed_amount_off') {
          //Fixed amount off the order subtotal
          $amount = $promotion_check->get('offer')->target_plugin_configuration['amount']['number'];
          $summary['discount']    = sprintf('%0.2f',$amount);
          $summary['total_coupon_applied'] = sprintf('%0.2f',array_sum($total_price)-$summary['discount']);

        }elseif ($promotion_check->get('offer')->target_plugin_id =='order_item_percentage_off') {
          //Percentage off each matching product
          $coupon_percent    = $promotion_check->get('offer')[0]->target_plugin_configuration['percentage'];
          $summary['discount']    = sprintf('%0.2f',array_sum($discount));
          $summary['total_coupon_applied'] = sprintf('%0.2f',array_sum($total_price)+$summary['discount']);

        }elseif ($promotion_check->get('offer')->target_plugin_id =='order_percentage_off') {
          //Percentage off the order subtotal
          $coupon_percent    = $promotion_check->get('offer')[0]->target_plugin_configuration['percentage'];
          $summary['discount']    = sprintf('%0.2f',array_sum($total_price)*$coupon_percent);
          $summary['total_coupon_applied'] = sprintf('%0.2f',array_sum($total_price)+$summary['discount']);

        }
   
        $summary['coupon_code'] = $coupon_result['code'];
       
      }
      //existing coupon apply check End
    
   

      if(!isset($_SESSION['postid'])){
         $_SESSION['postid'] = rand(10,100);      
      }

      $result=$connection->query("SELECT * from users_shipping_profile where order_id=$order_id");
      $result_data=array();
      while ($row = $result->fetchAssoc()) {
        $result_data[]=$row;
       }

      $result=$connection->query("SELECT * from profile_setting_users where profile_id=$uid");
      
      $result_data1=array();
      while ($row = $result->fetchAssoc()) {
        $result_data1[]=$row;
       }

   
      if(!empty($_POST)){
         if( (isset($_POST['postid'])) && ($_POST['postid'] != "")  ){
            if($_SESSION['postid'] == $_POST['postid']){  
              
              $array_data['bill_firstname']=!empty($_POST['bill_firstname'])?"'".addslashes($_POST['bill_firstname'])."'":"''";
              $array_data['bill_lastname']=!empty($_POST['bill_lastname'])?"'".addslashes($_POST['bill_lastname'])."'":"''";
              $array_data['bill_address1']=!empty($_POST['bill_address1'])?"'".addslashes($_POST['bill_address1'])."'":"''";
              $array_data['bill_address2']=!empty($_POST['bill_address2'])?"'".addslashes($_POST['bill_address2'])."'":"''";
              $array_data['bill_zipcode']=!empty($_POST['bill_zipcode'])?"'".$_POST['bill_zipcode']."'":"''";
              $array_data['bill_city']=!empty($_POST['bill_city'])?"'".$_POST['bill_city']."'":"''";
              $array_data['bill_state']=!empty($_POST['bill_state'])?"'".$_POST['bill_state']."'":"''";
  //            $array_data['bill_country']=!empty($_POST['bill_country'])?"'".$_POST['bill_country']."'":"''";
              $array_data['bill_phone']=!empty($_POST['bill_phone'])?"'".addslashes($_POST['bill_phone'])."'":"''";
              $array_data['bill_email']=!empty($_POST['bill_email'])?"'".addslashes($_POST['bill_email'])."'":"''";
              $array_data['s_firstname']=!empty($_POST['s_firstname'])?"'".addslashes($_POST['s_firstname'])."'":"''";
              $array_data['s_lastname']=!empty($_POST['s_lastname'])?"'".addslashes($_POST['s_lastname'])."'":"''";
              $array_data['s_address1']=!empty($_POST['s_address1'])?"'".addslashes($_POST['s_address1'])."'":"''";
              $array_data['s_address2']=!empty($_POST['s_address2'])?"'".addslashes($_POST['s_address2'])."'":"''";
              $array_data['s_zipcode']=!empty($_POST['s_zipcode'])?"'".$_POST['s_zipcode']."'":"''";
              $array_data['s_city']=!empty($_POST['s_city'])?"'".$_POST['s_city']."'":"''";
              $array_data['s_state']=!empty($_POST['s_state'])?"'".$_POST['s_state']."'":"''";
              $array_data['s_country']=!empty($_POST['s_country'])?"'".$_POST['s_country']."'":"''";
              $array_data['s_phonenumber']=!empty($_POST['s_phonenumber'])?"'".addslashes($_POST['s_phonenumber'])."'":"''";
              $array_data['s_email']=!empty($_POST['s_email'])?"'".addslashes($_POST['s_email'])."'":"''";
          

              
              if(count($result_data1)==0){
                $array_data['profile_id']=$uid; 
                $query_profile="INSERT INTO profile_setting_users (".implode(',',array_keys($array_data)).") values(".implode(',',$array_data).")";
                $output = $connection->query($query_profile);
              }

              $array_data['user_id']=$uid;              
              $array_data['order_id']=$order_id;

               if(count($result_data)>0){
                  $update_var=array();
                  foreach ($array_data as $key => $value) {
                    $update_var[]="$key=".$value;
                  }
                  $query="UPDATE users_shipping_profile SET ".implode(',',$update_var)." where order_id=$order_id";
               }else{
                  $query="INSERT INTO users_shipping_profile (".implode(',',array_keys($array_data)).") values(".implode(',',$array_data).")";
               }
              $output = $connection->query($query);

              $update_query="UPDATE commerce_order SET checkout_flow='default',checkout_step='review' where order_id=".$order_id;
              $connection->query($update_query);

              $url=$base_url."/checkout/".$order_id."/review";
              $response =new RedirectResponse($url);
              return $response->send();
              // header('location:'.$url);

            }
            $_SESSION['postid'] == "";
          }
      }

     /* print_r($result_data1);
      exit;*/


      if($_SESSION['postid'] == ""){
        $_SESSION['postid'] = rand(10,100);      
      }

     /* $countries = \Drupal\Core\Locale\CountryManager::getStandardList();
       foreach ($countries as $key => $value) {
         $countrys[$key] = (string) $value;
       }*/
       //county list
       //$result_data1[0]['bill_state'];
      $state=[];
      $query       = $connection->query("SELECT distinct state_name  FROM zipcode_detail");
      while($row = $query->fetchAssoc()){
          $state[$row['state_name']] = $row['state_name'];
         
      }
     
    

      /* $query       = $connection->query("SELECT * FROM profile_setting_users where profile_id='".$uid."'");
      while($row = $query->fetchAssoc()){
          $selected_bill_state = $row['bill_state'];
          $selected_ship_state = $row['s_state'];
          $selected_bill_city = $row['bill_city'];
          $selected_ship_city = $row['s_city'];
          $selected_bill_zip = $row['bill_zipcode'];
          $selected_ship_zip = $row['s_zipcode'];
      }

*/





      return array(
          '#theme' => 'order_information',
          '#summary'=>$summary,
          '#uid'=>$uid,
          '#order_summary'=>$order_summary,
          '#states'=>$state,
          '#result_data'=>!empty($result_data)?$result_data:(!empty($result_data1)?$result_data1:""),
          '#postid'=>$_SESSION['postid'],
          '#coupon_list'=>$coupon_list
      );
    }  

    public function coupon_validate(){
      $order_det  = [];
      $error_msg = '';
      $coupon_code    = $_POST['coupon_code'];//'12345-5%off';
      $uid            = \Drupal::currentUser()->id(); 
      $connection     = \Drupal::database();
      $entity_manager = \Drupal::entityManager();
      $coupon_storage = $entity_manager->getStorage('commerce_promotion_coupon');
      $coupon         = $coupon_storage->loadEnabledByCode($coupon_code);
   
      if(!empty($coupon)){
        $coupon->id();
        $order_storage     = $entity_manager->getStorage('commerce_order');
        $promotion_storage = $entity_manager->getStorage('commerce_promotion')->load($coupon->getPromotionid()); 
        $coupon_percent    = $promotion_storage->get('offer')[0]->target_plugin_configuration['percentage'];
       
        $query1 = $connection->query("SELECT order_id,total_price__number FROM commerce_order 
                                      WHERE uid=".$uid." 
                                      AND state = 'draft' 
                                      AND cart = 1");

        $result = $query1->fetchAssoc();
          if(!empty($result)){
              $order_id = $result['order_id'];
              $order    = $order_storage->load($order_id);
              //coupon validation
              if (!$coupon->available($order)) {
                $msg = 'notavailable';
                $error_msg = 'The provided coupon code is invalid';
                $coupon_applied = array('msg'=>$msg,'orderinfo'=>$order_det,'error_msg'=>$error_msg);
                echo json_encode($coupon_applied);
                exit;
              }
              if (!$coupon->getPromotion()->applies($order)) {
                $msg = 'notapply';
                $error_msg = 'The provided coupon code is invalid';
                $coupon_applied = array('msg'=>$msg,'orderinfo'=>$order_det,'error_msg'=>$error_msg);
                echo json_encode($coupon_applied);
                exit;
              }
              //need validation
              $query_order = $connection->query("SELECT * FROM commerce_order__coupons 
                                                WHERE entity_id=".$order_id."");
              $or_result = $query_order->fetchAssoc();

              //commerce_order__coupons entity_id not availble - coupon apply
              if(!$or_result){
                  $order->get('coupons')->appendItem($coupon->id());
                  $order->save();

                  //coupon applied total price return
                  $query_res = $connection->query("SELECT order_id,ifnull(total_price__number,0) as total_price__number FROM commerce_order 
                                                  WHERE uid=".$uid." 
                                                  AND state = 'draft' 
                                                  AND cart = 1");
                  $order_det = $query_res->fetchAssoc();
                  $msg  = 'coupon';
                  $error_msg = 'Coupon Code Applied';
              }
          } 
      }else{
        $msg  = 'nocoupon';
        $error_msg = 'Invalid Coupon';
      }

    $coupon_applied = array('msg'=>$msg,'orderinfo'=>$order_det,'error_msg'=>$error_msg);
   
    echo json_encode($coupon_applied);
    exit;
       
    }

    public function apply_coupon_remove(){
      $discount   = 0;
      $order_det  = [];
      $uid            = \Drupal::currentUser()->id(); 
      $connection     = \Drupal::database();
      $coupon_code    = $_POST['coupon_code'];//'12345-5%off';
      $coupon_storage = \Drupal::entityTypeManager()->getStorage('commerce_promotion_coupon');
      $coupon         = $coupon_storage->loadEnabledByCode($coupon_code);

      $query1 = $connection->query("SELECT order_id FROM commerce_order 
                                    WHERE uid=".$uid." 
                                    AND state = 'draft' 
                                    AND cart = 1");
      $result = $query1->fetchAssoc();
      $order_id = $result['order_id'];

      $order_storage = \Drupal::entityTypeManager()->getStorage('commerce_order');
      $order = $order_storage->load($order_id);
      $coupon_ids = array_column($order->get('coupons')->getValue(), 'target_id');
      $coupon_index = array_search($coupon->id(), $coupon_ids);
      $order->get('coupons')->removeItem($coupon_index);
      $order->save();
      //coupon applied total price return
        $query_res = $connection->query("SELECT order_id,total_price__number FROM commerce_order 
                                        WHERE uid=".$uid." 
                                        AND state = 'draft' 
                                        AND cart = 1");
        $order_det = $query_res->fetchAssoc();
        $msg  = 'removecoupon';
        $discount = 0;

        $coupon_removed = array('msg'=>$msg,'orderinfo'=>$order_det,'discount'=>$discount);
   
        echo json_encode($coupon_removed);
        exit;
    }

    public function shippinginfo(){
      global $base_url;
      $uid = \Drupal::currentUser()->id(); 
      $connection = \Drupal::database();
      $query1 =$connection->query("SELECT order_id FROM `commerce_order` WHERE uid=".$uid." AND state = 'draft' AND cart=1");
      $result = $query1->fetchAssoc();

      $countries = \Drupal\Core\Locale\CountryManager::getStandardList();
       foreach ($countries as $key => $value) {
         $countrys[$key] = (string) $value;
       }


      $result=$connection->query("SELECT * from users_shipping_profile where order_id=".$result['order_id']);
      $address_profile=$result->fetchAssoc();

      $billing_address=array();
      $billing_address[]=$address_profile['bill_firstname']." ".$address_profile['bill_lastname'];
      $billing_address[]=$address_profile['bill_phone'];
      $billing_address[]=$address_profile['bill_email'];
      $billing_address[]=$address_profile['bill_address1']." ".$address_profile['bill_address2'];
      $billing_address[]=$address_profile['bill_city']." ".$address_profile['bill_zipcode'];
      $billing_address[]=$address_profile['bill_state'];
/*      $billing_address[]=!empty($address_profile['bill_country'])?$countrys[$address_profile['bill_country']]:"";
*/
      $bill_address=array();
      foreach ($billing_address as $key => $value) {
         if(!empty($value)){
          $bill_address[]=$value;
         }
      }

      $shipping_address=array();
      $shipping_address[]=$address_profile['s_firstname']." ".$address_profile['s_lastname'];
      $shipping_address[]=$address_profile['s_phonenumber'];
      $shipping_address[]=$address_profile['s_email'];
      $shipping_address[]=$address_profile['s_address1']." ".$address_profile['s_address2'];
      $shipping_address[]=$address_profile['s_city']." ".$address_profile['s_zipcode'];
      $shipping_address[]=$address_profile['s_state'];
      $shipping_address[]=!empty($address_profile['s_country'])?$countrys[$address_profile['s_country']]:"";

      $shipp_address=array();
      foreach ($shipping_address as $key => $value) {
         if(!empty($value)){
          $shipp_address[]=$value;
         }
      }

      $html="";
     $html.='
            <div class="row">
              <div class="col-md-6">
                <div class="shp">
                  <div class="headd" style="position: relative;">
                    <h1 style="font-size: 22px; font-weight: 600; border-bottom: 3px dotted #dedede; padding-bottom: 10px;">Billing Address</h1>
                    <h6 style="/* float: right; */position: absolute;bottom: 0;right: 20px;font-weight: 700;color: #2196F3;font-size: 14px;"><a href="'.$base_url.'/orderinformations">Edit</a></h6>
                  </div>
                  <p>'.implode('</p><p>',$bill_address).'</p>
                </div>
              </div>
              <div class="col-md-6">
                <div class="blng">
                  <div class="headd" style="position: relative;">
                  <h1 style="font-size: 22px; font-weight: 600; border-bottom: 3px dotted #dedede; padding-bottom: 10px;">Shipping Address</h1> 
                  <h6 style="/* float: right; */position: absolute;bottom: 0;right: 20px;font-weight: 700;color: #2196F3;font-size: 14px;"><a href="'.$base_url.'/orderinformations">Edit</a></h6>
                  </div>
                  <p>'.implode('</p><p>',$shipp_address).'</p>
                </div>
              </div>
            </div><br><br>';
      // $html.='<table class="table"><tr><th>Billing Information</th><th><a href="'.$base_url.'/orderinformations">Edit</a></th><th>Shipping Information</th></tr><tr>';
      // $html.="<td><p>".implode('</p><p>',$bill_address)."</p></td>";
      // $html.="<td></td>";
      // $html.="<td>".implode('<br>',$shipp_address)."</td>";
      // $html.="</tr></table>";

      echo $html;die;

    }

    public function ordermail(){

      $countries = \Drupal\Core\Locale\CountryManager::getStandardList();

      foreach ($countries as $key => $value) {
           $countrys[$key] = (string) $value;
      }

      $new_query=$connection->query("SELECT D.sku as partno,D.title as description,round(D.price__number) as price,round(B.quantity) as quantity,round(D.price__number*B.quantity) as subtotal,A.order_id,DATE_FORMAT(from_unixtime(A.completed),'%d-%m-%Y') as order_date, C.bill_firstname, C.bill_lastname, C.bill_phone, C.bill_email, C.bill_address1, C.bill_address2, C.bill_city, C.bill_zipcode, C.bill_state,  C.s_firstname, C.s_lastname, C.s_phonenumber, C.s_email, C.s_address1, C.s_address2, C.s_city, C.s_zipcode, C.s_state, C.s_country FROM `commerce_order` A,commerce_order_item B,users_shipping_profile C,commerce_product_variation_field_data D WHERE A.order_id=B.order_id and A.order_id=C.order_id and A.billing_profile__target_revision_id=C.revision_id and B.purchased_entity=D.variation_id and A.order_id=$order_id");

      while ( $row=$new_query->fetchAssoc()) {
        $data1=$row;
        $order_details[]=$row;
      }

      $billing_address=array();
      $billing_address[]=$data1['bill_firstname']." ".$data1['bill_lastname'];
      $billing_address[]=$data1['bill_phone'];
      $billing_address[]=$data1['bill_email'];
      $billing_address[]=$data1['bill_address1']." ".$data1['bill_address2'];
      $billing_address[]=$data1['bill_city']." ".$data1['bill_zipcode'];
      $billing_address[]=$data1['bill_state'];
/*      $billing_address[]=!empty($data1['bill_country'])?$countrys[$data1['bill_country']]:"";
*/
      $bill_address=array();
      foreach ($billing_address as $key => $value) {
         if(!empty($value)){
          $bill_address[]=$value;
         }
      }

      $shipping_address=array();
      $shipping_address[]=$data1['s_firstname']." ".$data1['s_lastname'];
      $shipping_address[]=$data1['s_phonenumber'];
      $shipping_address[]=$data1['s_email'];
      $shipping_address[]=$data1['s_address1']." ".$data1['s_address2'];
      $shipping_address[]=$data1['s_city']." ".$data1['s_zipcode'];
      $shipping_address[]=$data1['s_state'];
      $shipping_address[]=!empty($data1['s_country'])?$countrys[$data1['s_country']]:"";

      $shipp_address=array();
      foreach ($shipping_address as $key => $value) {
         if(!empty($value)){
          $shipp_address[]=$value;
         }
      }

      $order_data['bill_address']=!empty($bill_address)?implode('<br>',$bill_address):"";
      $order_data['shipp_address']=!empty($shipp_address)?implode('<br>',$shipp_address):"";

      

    }


public function state_rel_city(){
    $city=[];
    $connection  = \Drupal::database();
    $query       = $connection->query("SELECT distinct city  FROM zipcode_detail where state_name='".$_POST['state']."'");
    while($row = $query->fetchAssoc()){             
       $city[$row['city']]  = $row['city'];
    }
    echo json_encode($city);
    exit;
}    

  public function city_rel_zip(){

    $zip=[];
    $connection  = \Drupal::database();
    $query       = $connection->query("SELECT distinct zip  FROM zipcode_detail where state_name='".$_POST['state']."' and city='".$_POST['city']."'");
    while($row = $query->fetchAssoc()){             
       $zip[$row['zip']]  = $row['zip'];
    }
    echo json_encode($zip);
    exit;
    
  }

  /* public function apply_coupon(){
      $data = json_decode(file_get_contents("php://input"), true);
      $coupon=$data['coupen'];
      $app_or_rem =$data['apply'];

      if($app_or_rem == 'yes'){

        echo "apply";

      }else{
        echo "remove";

      }

     
      exit;
    }*/
public function test_coupon()
{

   $entity_manager  = \Drupal::entityManager();

$promotion_storage               = $entity_manager->getStorage('commerce_promotion')->load(2); 
   $order_storage = $entity_manager->getStorage('commerce_order')->load(25);
echo "<pre>";
//print_r($promotion_storage);die;
print_r($order_storage);die;

$promotion_query = $promotion_storage->getQuery();
//$order_query->condition('state', 'draft', '<>');
$order_idss = $promotion_query->execute();
print_r($order_idss);die;

       $order_storage = \Drupal::entityTypeManager()->getStorage('commerce_order');
// Load all placed orders.
$order_query = $order_storage->getQuery();
$order_query->condition('state', 'draft', '<>');
$order_ids = $order_query->execute();

foreach($order_ids as $order_id) {

 // $c= \ Drupal\commerce_order\Entity\Order::load($order_id)->get('coupons')[0]->entity->getCode();
            // print_r( $c);die;
 $orders =  \Drupal\commerce_order\Entity\Order::load($order_id);
 $state = $orders->get('state')->value;
 $total_price = $orders->getTotalprice()->getNumber();
 $currency = $orders->getTotalprice()->getCurrencyCode();
  print_r( $total_price);die;
}
}

}