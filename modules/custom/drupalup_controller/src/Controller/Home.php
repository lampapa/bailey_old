<?php
/**
 * @file
 * Contains \Drupal\hello\HelloController.
 */

namespace Drupal\drupalup_controller\Controller;
    use Drupal\Core\Controller\ControllerBase;
    use Symfony\Component\HttpFoundation\Request;
    use Drupal\Core\Database\Database;
    use Drupal\Core\Entity\Query\QueryFactory;

class Home {

  public function page() {
    
    return array(
        '#theme' => 'home',
        '#title'=>'Home Page',
        '#items'=>''
        //'#title'=>'Our Article List'
    );
  }

}