<?php

/**
 * @file
 * Contains \Drupal\my_custom_block\Plugin\Block
 */

namespace Drupal\apa_banner_custom_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;


/**
 * below section is important
 * 
 * @Block(
 *  id = "apabanner_custom_block",
 *  admin_label = @Translation("Apa Banner custom Block"),
 *  category = @Translation("Custom")
 * ) 
 */
 
class ApaBannerCustomBlock extends BlockBase{
 
   public function build(){
      return [
      '#theme' => 'apabannertemplate',
      '#test_var' => $this->t('Test Value'),
    ];
  }
}

?>