<?php
/**
 * @file
 * Contains \Drupal\amazing_forms\Form\ContributeForm.
 */

namespace Drupal\amazing_forms\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\UrlHelper;

/**
 * Contribute form.
 */
class BrandForm extends FormBase {
  /**
   * {@inheritdoc}
   */
 public function getFormId() {
    return 'amazing_forms_year_form';
  }
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    
    $form['CategoryName'] = array(
      '#type' => 'textfield',
      '#title' => t('CategoryName'),
      '#attributes' => array('class' => ['form-control'],'ng-model'=>['categoryname_model'])
    );
    $form['CategoryColor'] = array(
      '#type' => 'textfield',
      '#title' => t('CategoryColor'),
      '#attributes' => array('class' => ['form-control'],'ng-model'=>['categorycolor_model'])
    );
    $form['Sequence'] = array(
      '#type' => 'tel',
      '#title' => t('Sequence'),
      '#attributes' => array('class' => ['form-control'],'ng-model'=>['sequence_model'])
    );
    $form['Uploadimage'] = array(
      '#type' => 'file',
      '#title' => t('Upload Image'),
      '#attributes' => array('class' => ['form-control'],'ng-model'=>['upoadimage_model'])
    );
    $form['Altetertext'] = array(
      '#type' => 'textfield',
      '#title' => t('Alt text'),
      '#attributes' => array('class' => ['form-control'],'ng-model'=>['altertext_model'])
    );   
    $form['Showonhome'] = array(
      '#type' => 'checkbox',
      '#title' => t('Show On Home Page'),
      '#attributes' => array('class' => ['checkboxclass'],'ng-model'=>['showonhome_model'])
    ); 
    $form['submit'] = array(
      '#type' => 'submit',
      '#attributes' => array('class' => ['button-normal btn-save-icon']),
      '#value' => t('Save'),
    );
    $form['options']['reset'] = array(
      '#type' => 'submit',
      '#value' => t('Reset'),
      '#submit' => array(''),
      '#attributes' => array('class' => ['button-normal btn-reset-icon'])
    );
    return $form;
  }
  /**
   * {@inheritdoc}
   */
  public function MY_MODULE_FORM_ID_reset($form, &$form_state) {
    $form_state['rebuild'] = FALSE;
  }
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (!$form_state->getValue('CategoryName') || empty($form_state->getValue('CategoryName'))) {
        $form_state->setErrorByName('CategoryName', $this->t('Please Enter CategoryName'));
    }
    // Assert the lastname is valid
    if (!$form_state->getValue('CategoryColor') || empty($form_state->getValue('CategoryColor'))) {
        $form_state->setErrorByName('CategoryColor', $this->t('Please Enter CategoryColor'));
    }
    
    // Assert the subject is valid
    if (!$form_state->getValue('Sequence') || empty($form_state->getValue('Sequence'))) {
        $form_state->setErrorByName('Sequence', $this->t('Please Enter Sequence'));
    }
    // Assert the message is valid
    if (!$form_state->getValue('Altetertext') || empty($form_state->getValue('Altetertext'))) {
        $form_state->setErrorByName('Altetertext', $this->t('Plesae Enter Altetertext'));
    }
   
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

        $currentfilenamearray ='';
        $currenttimestamp = time();; 
        $filename = $_FILES['files']['name']['Uploadimage'];
        $location = __DIR__ . '/category_images/';
        $imageformat = explode('/',$_FILES['files']['type']['Uploadimage']);
        $resultimageformat = $imageformat[1];
        $currentfilename = $currenttimestamp.'.'.pathinfo($_FILES['files']['name']['Uploadimage'], PATHINFO_EXTENSION);
        move_uploaded_file($_FILES['files']['tmp_name']['Uploadimage'],$location.$currentfilename);
        chmod($location.$currentfilename,0777);
     $connection = \Drupal::database();
    $categoryname = $sequence=$altertext=$showonhome='';
    foreach ($form_state->getValues() as $key => $value) {
      if($key == 'CategoryName'){
        $categoryname =$value;
      }
      if($key == 'Sequence'){
        $sequence =$value;
      }
      if($key == 'Altetertext'){
        $altertext =$value;
      }
      if($key == 'Showonhome'){
        $showonhome =$value;
      }
       if($key == 'CategoryColor'){
        $categoryColor =$value;
      }
    }
        $connection->query("insert into catapult_categories_data(categoryname,categorycolor,sequence,altertext,showonhome,created_by,created_on,deletion_status,categoryimage) Values('".$categoryname."','".$categoryColor."','".$sequence."','".$altertext."','".$showonhome."','JP',Now(),'N','".$currentfilename."')");
     
    
  }
}