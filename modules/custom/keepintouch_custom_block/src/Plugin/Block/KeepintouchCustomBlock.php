<?php

/**
 * @file
 * Contains \Drupal\keepintouch_custom_block\Plugin\Block
 */

namespace Drupal\keepintouch_custom_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;


/**
 * below section is important
 * 
 * @Block(
 *  id = "keepintouch_custom_block",
 *  admin_label = @Translation("Keepintouch custom Block"),
 *  category = @Translation("Custom")
 * ) 
 */
 
class KeepintouchCustomBlock extends BlockBase{
 
   public function build(){


   	$connection = \Drupal::database();
	   	$keep_in = [];
	    $query1 = $connection->query("SELECT `facebook`,`twitter`,`youtube`,`linkedin`,`googleplus` FROM catapult_contact_info");
	    while($row = $query1->fetchAssoc()){
	     $keep_in[] = array(
	     	'facebook' => $row['facebook'],
	     	'twitter' => $row['twitter'],
	     	'youtube' => $row['youtube'],
	     	'linkedin' => $row['linkedin'],
	     	'googleplus' => $row['googleplus']
	     	
	     );
	    }



      return [
      '#theme' => 'keepintouchtemplate',
      '#test_var' => $this->t('Test Value'),
      '#keep_in' => $keep_in
    ];
  }
}

?>