<?php

/**
 * @file
 * Contains \Drupal\subscription_custom_block\Plugin\Block
 */

namespace Drupal\subscription_custom_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;


/**
 * below section is important
 * 
 * @Block(
 *  id = "subscription_custom_block",
 *  admin_label = @Translation("Subscription custom Block"),
 *  category = @Translation("Custom")
 * ) 
 */
 
class SubscriptionCustomBlock extends BlockBase{
 
   public function build(){
      return [
      '#theme' => 'subscriptiontemplate',
      '#test_var' => $this->t('Test Value'),
    ];
  }
}

?>