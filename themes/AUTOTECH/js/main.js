(function($) {
  "use strict";



  $(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip();
  });
  $(window).on('scroll', function() {
    var scroll = $(window).scrollTop();
    if (scroll < 100) {
      $(".sticky-header").removeClass("sticky");
    } else {
      $(".sticky-header").addClass("sticky");
    }
  });

  /*Video-Slide*/
  $(document).ready(function() {
    $('.video_slide').carousel({
      num: 3,
      maxWidth: 450,
      maxHeight: 300,
      distance: 50,
      scale: 0.6,
      animationTime: 700,
      showTime: 4000,

    });
  });

  $(document).ready(function() {

      setTimeout(function() {
        $(".fancybox").on("click", function() {
          $.fancybox({
            'titleShow': true,
            'href': this.href,
            'type':  $(this).data("type"),
            'swf': {
              'wmode': 'transparent',
              'allowfullscreen': 'true'
            }
            // href: this.href,
            // type: $(this).data("type")
          }); // fancybox
          return false
        });
      }, 1000);
    });

  $(document).on('click', '.quantity .plus, .quantity .minus', function(e) {
    // Get values
    var $qty = $(this).closest('.quantity').find('.qty'),
      currentVal = parseFloat($qty.val()),
      max = parseFloat($qty.attr('max')),
      min = parseFloat($qty.attr('min')),
      step = $qty.attr('step');
     if(currentVal)

    // Format values
    if (!currentVal || currentVal === '' || currentVal === 'NaN') currentVal = 1;
    if (max === '' || max === 'NaN') max = '';
    if (min === '' || min === 'NaN') min = 1;
    if (step === 'any' || step === '' || step === undefined || parseFloat(step) === 'NaN') step = 1;
    // Change the value
    if ($(this).is('.plus')) {
      if (max && (max == currentVal || currentVal > max)) {
        $qty.val(max);
      } else {
        $qty.val(currentVal + parseFloat(step));
      }
    } else {
      if (min && (min == currentVal || currentVal < min)) {
        $qty.val(min);
      } else if (currentVal > 1) {
        $qty.val(currentVal - parseFloat(step));
      }
    }
    // Trigger change event
    $qty.trigger('change');
    e.preventDefault();
  });

  $(document).ready(function() {

    var owl_1 = $('#owl_1');
    var owl_2 = $('#owl_2');

    owl_1.owlCarousel({
      autoplay: false,
      autoplayTimeout: 6000,
      loop: true,
      margin: 10,
      nav: false,
      items: 1,
      dots: false
    });

    owl_2.owlCarousel({
      // autoplay:true,
      // autoplayTimeout:10000,
      // loop:true,
      // if(owl_2.length  > 4){
      //     nav: true
      // }
      // else{
      //   nav: false
      // }
      margin: 10,
      nav: true,
      items: 4,
      dots: false
    });

    owl_2.find(".item").click(function() {
      var slide_index = owl_2.find(".item").index(this);
      owl_1.trigger('to.owl.carousel', [slide_index, 300]);
    });

    // Custom Button
    $('.next_btn').click(function() {
      owl_2.trigger('next.owl.carousel', 500);
    });
    $('.prev_btn').click(function() {
      owl_2.trigger('prev.owl.carousel', 500);
    });
  });

})(jQuery);
var $owl = $('#owl_2');

if($owl.length){
    $owl.on('initialized.owl.carousel', function(event) {
        var $itemsWrap = $owl.find("div.itemsWrap"); // or use Owl's .owl-stage class
        var items = event.item.count;
        var $owlControls = $('.slide_buttons');
        items <= 4 ? $owlControls.hide() : $owlControls.show();
    })
}
