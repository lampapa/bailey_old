(function($) {
    "use strict";
    // Document ready function

    $(function() {
          var lastURLSegment = pageURL.substr(pageURL.lastIndexOf('/') + 1);
         $('.card-header-toggle').click(function() {
            if( (lastURLSegment.includes("servicesetting")) || (lastURLSegment.includes("servicesetting#")) ){
              $('#updateservice').html("Add Services");
            }
            if( (lastURLSegment.includes("testimonialsetting")) || (lastURLSegment.includes("testimonialsetting#")) ){
              $('#updateservice').html("Add Testimonial");
            }
             if( (lastURLSegment.includes("videosetting")) || (lastURLSegment.includes("videosetting#")) ){
              $('.iframeid').css("display","none");
              $('#updateservice').html("Add Videos");
            }
            if( (lastURLSegment.includes("ourlocations")) || (lastURLSegment.includes("ourlocations#")) ){
              $('#updateservice').html("Add Locations");
            }
            if( (lastURLSegment.includes("colormaster")) || (lastURLSegment.includes("colormaster#")) ){
              $('#updateservice').html("Add Color Master");
            }
            if( (lastURLSegment.includes("banner")) || (lastURLSegment.includes("banner#")) ){
              $('#updateservice').html("Add Banner");
            }
            if( (lastURLSegment.includes("ourlocations")) || (lastURLSegment.includes("ourlocations#")) ){
              $('#updateservice').html("Add Locations");
            }
            if( (lastURLSegment.includes("categories")) || (lastURLSegment.includes("categories#")) ){
              $('#updateservice').html("Add Category");
              $('.iframeid').css("display","none");
            }
            if( (lastURLSegment.includes("buyaccessories")) || (lastURLSegment.includes("buyaccessories#")) ){
              $('#updateservice').html("Add Buy Accessories");
              $('.alternative-listing-middle').html("");
              $('.alternate_val').html("");
              $('.partno_required').html("");
              $('.alternativepartno_required').html("");
            }
            $("option:selected").removeAttr("selected");
            $('#hidden_id').val('');
            $("#resetform").trigger("reset");
            $(".resetform_input").trigger("reset");
            $(".uploadimage").css('display',"none");
            $(".uploadimage").attr('src',"");
            $('.card-header-toggle').toggleClass("show-card");
            $('.card-body-toggle').slideToggle("slow");
            var editor = CKEDITOR.instances['editor1'];
            editor.setData("");
          });

         $(".open-search-popup").click(function(){
          $(".search-box-popup").toggleClass("active");
        });

    });


})(jQuery);




$(document).ready(function(){
  $('ul.tabs li').click(function(){
    var tab_id = $(this).attr('data-tab');
    $('ul.tabs li').removeClass('current');
    $('.tab-content').removeClass('current');
    $(this).addClass('current');
    $("#"+tab_id).addClass('current');
  })
});


$(function() {
//----- OPEN
$('[data-popup-open]').on('click', function(e) {
var targeted_popup_class = jQuery(this).attr('data-popup-open');
$('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);

e.preventDefault();
});

//----- CLOSE
$('[data-popup-close]').on('click', function(e) {
var targeted_popup_class = jQuery(this).attr('data-popup-close');
$('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);

e.preventDefault();
});
});

$(document).ready(function() {

    setTimeout(function() {
      $(".fancybox").on("click", function() {
        $.fancybox({
          'titleShow': true,
          'href': this.href,
          'type':  $(this).data("type"),
          'swf': {
            'wmode': 'transparent',
            'allowfullscreen': 'true'
          }
          // href: this.href,
          // type: $(this).data("type")
        }); // fancybox
        return false
      });
    }, 1000);
  });
