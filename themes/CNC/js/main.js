(function($) {
  "use strict";



  $(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip();
  });
  $(window).on('scroll', function() {
    var scroll = $(window).scrollTop();
    if (scroll < 100) {
      $(".sticky-header").removeClass("sticky");
    } else {
      $(".sticky-header").addClass("sticky");
    }
  });

  /*Video-Slide*/
  $(document).ready(function() {
    $('.video_slide').carousel({
      num: 3,
      maxWidth: 450,
      maxHeight: 300,
      distance: 50,
      scale: 0.6,
      animationTime: 700,
      showTime: 4000,

    });
  });

  $(document).ready(function() {

    setTimeout(function() {
      $(".fancybox").on("click", function() {
        $.fancybox({
          'titleShow': true,
          'href': this.href.replace(new RegExp("watch\\?v=", "i"), 'v/'),
          'type': 'swf',
          'swf': {
            'wmode': 'transparent',
            'allowfullscreen': 'true'
          }
          // href: this.href,
          // type: $(this).data("type")
        }); // fancybox
        return false
      });
    }, 1000);
  });

$(document).on('click', '.plusclass', function(e) {
  var orderid = $(this).attr('order_id');
  e.preventDefault();  
  var ids = $(this).attr('fields');
  var quantity = $("#quantity"+ids).val();
  var unitprice = $("#value"+ids).html();
  var quantityplus = parseInt(quantity)+1;
  $("#quantity"+ids).val(quantityplus);  
  var singleprice = parseFloat(unitprice)*parseFloat(quantityplus);   
  $("#singleprice"+ids).html(singleprice.toFixed(2));
    
  var front_total = 0;
  $('.singledata').each(function(){
    front_total  = parseFloat(front_total)+parseFloat($(this).html());    
    front_total = front_total.toFixed(2);
  });
  $("#basic_total").html(front_total);
  $("#full_total").html(front_total);

  $.ajax({
      type: "POST",
      data:{total : front_total,orderids : orderid,quantitys :quantityplus,id:ids },
      url: baseurl+"updatecarts",
      success: function(data){          
      }     
  });        


});

$(document).on('click', '.minusclass', function(e) {
  var orderid = $(this).attr('order_id');
  e.preventDefault();  
  var ids          = $(this).attr('fields');
  var quantity     = $("#quantity"+ids).val();
  var unitprice    = $("#value"+ids).html();  
  if(quantity == 1){
    quantityplus = quantity;
  }else{
    var quantityplus = parseInt(quantity)-1;
  }  
  $("#quantity"+ids).val(quantityplus);
  var singleprice = parseFloat(unitprice)*parseFloat(quantityplus);
  $("#singleprice"+ids).html(singleprice.toFixed(2));
  var front_total = 0;
  $('.singledata').each(function(){    
    front_total  = parseFloat(front_total)+parseFloat($(this).html());    
    front_total = front_total.toFixed(2);
  });
  $("#basic_total").html(front_total);
  $("#full_total").html(front_total);

  $.ajax({
      type: "POST",
      data:{total : front_total,orderids : orderid,quantitys :quantityplus,id:ids },
      url: baseurl+"updatecarts",
      success: function(data){          
      }     
  });

});


  /*$(document).on('click', '.quantity .plus, .quantity .minus', function(e) {
    
    var $qty = $(this).closest('.quantity').find('.qty'),
      currentVal = parseFloat($qty.val()),
      max = parseFloat($qty.attr('max')),
      min = parseFloat($qty.attr('min')),
      step = $qty.attr('step');

    alert(currentVal);
    
    if (!currentVal || currentVal === '' || currentVal === 'NaN') currentVal = 0;
    if (max === '' || max === 'NaN') max = '';
    if (min === '' || min === 'NaN') min = 0;
    if (step === 'any' || step === '' || step === undefined || parseFloat(step) === 'NaN') step = 1;
    
    if ($(this).is('.plus')) {
      if (max && (max == currentVal || currentVal > max)) {
        $qty.val(max);
      } else {
        $qty.val(currentVal + parseFloat(step));
      }
    } else {
      if (min && (min == currentVal || currentVal < min)) {
        $qty.val(min);
      } else if (currentVal > 0) {
        $qty.val(currentVal - parseFloat(step));
      }
    }
    
    $qty.trigger('change');
    e.preventDefault();
  });
*/
  $(document).ready(function() {

    var owl_1 = $('#owl_1');
    var owl_2 = $('#owl_2');

    owl_1.owlCarousel({
      autoplay: false,
      autoplayTimeout: 6000,
      loop: true,
      margin: 10,
      nav: false,
      items: 1,
      dots: false
    });

    owl_2.owlCarousel({
      // autoplay:true,
      // autoplayTimeout:10000,
      // loop:true,
      margin: 10,
      nav: true,
      items: 5,
      dots: false
    });

    owl_2.find(".item").click(function() {
      var slide_index = owl_2.find(".item").index(this);
      owl_1.trigger('to.owl.carousel', [slide_index, 300]);
    });

    // Custom Button
    $('.next_btn').click(function() {
      owl_2.trigger('next.owl.carousel', 500);
    });
    $('.prev_btn').click(function() {
      owl_2.trigger('prev.owl.carousel', 500);
    });
  });

})(jQuery);
