(function($) {
  "use strict";



  $(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip();
  });
  $(window).on('scroll', function() {
    var scroll = $(window).scrollTop();
    if (scroll < 100) {
      $(".sticky-header").removeClass("sticky");
    } else {
      $(".sticky-header").addClass("sticky");
    }
  });

  /*Video-Slide*/
  $(document).ready(function() {
    $('.video_slide').carousel({
      num: 3,
      maxWidth: 450,
      maxHeight: 300,
      distance: 50,
      scale: 0.6,
      animationTime: 700,
      showTime: 4000,

    });
  });

    // select all checkbox for part-searchpage filters

    $('#Descriptions .select_all').click(function() {
      $('#Descriptions input').prop('checked', $(this).prop('checked'));
    });
    $('#Brand .select_all').click(function() {
      $('#Brand input').prop('checked', $(this).prop('checked'));
    });

    $('#Brandcolor .select_all').click(function() {
      $('#Brandcolor input').prop('checked', $(this).prop('checked'));
    });

  $(document).ready(function() {

    setTimeout(function() {
      $(".fancybox").on("click", function() {
        $.fancybox({
          'titleShow': true,
          'href': this.href.replace(new RegExp("watch\\?v=", "i"), 'v/'),
          'type': 'swf',
          'swf': {
            'wmode': 'transparent',
            'allowfullscreen': 'true'
          }
          // href: this.href,
          // type: $(this).data("type")
        }); // fancybox
        return false
      });
    }, 1000);
  });

  $(document).on('click', '.quantity .plus, .quantity .minus', function(e) {
    // Get values
    var $qty = $(this).closest('.quantity').find('.qty'),
      currentVal = parseFloat($qty.val()),
      max = parseFloat($qty.attr('max')),
      min = parseFloat($qty.attr('min')),
      step = $qty.attr('step');
    // Format values
    if (!currentVal || currentVal === '' || currentVal === 'NaN') currentVal = 0;
    if (max === '' || max === 'NaN') max = '';
    if (min === '' || min === 'NaN') min = 0;
    if (step === 'any' || step === '' || step === undefined || parseFloat(step) === 'NaN') step = 1;
    // Change the value
    if ($(this).is('.plus')) {
      if (max && (max == currentVal || currentVal > max)) {
        $qty.val(max);
      } else {
        $qty.val(currentVal + parseFloat(step));
      }
    } else {
      if (min && (min == currentVal || currentVal < min)) {
        $qty.val(min);
      } else if (currentVal > 0) {
        $qty.val(currentVal - parseFloat(step));
      }
    }
    // Trigger change event
    $qty.trigger('change');
    e.preventDefault();
  });

  $(document).ready(function() {

    var owl_1 = $('#owl_1');
    var owl_2 = $('#owl_2');

    owl_1.owlCarousel({
      autoplay: false,
      autoplayTimeout: 6000,
      loop: true,
      margin: 10,
      nav: false,
      items: 1,
      dots: false
    });

    owl_2.owlCarousel({
      // autoplay:true,
      // autoplayTimeout:10000,
      // loop:true,
      margin: 10,
      nav: true,
      items: 5,
      dots: false
    });

    owl_2.find(".item").click(function() {
      var slide_index = owl_2.find(".item").index(this);
      owl_1.trigger('to.owl.carousel', [slide_index, 300]);
    });

    // Custom Button
    $('.next_btn').click(function() {
      owl_2.trigger('next.owl.carousel', 500);
    });
    $('.prev_btn').click(function() {
      owl_2.trigger('prev.owl.carousel', 500);
    });
  });

})(jQuery);
